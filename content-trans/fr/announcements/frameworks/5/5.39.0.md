---
aliases:
- ../../kde-frameworks-5.39.0
date: 2017-10-14
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Only match real MIME types, not e.g. "raw CD image" (bug 364884)
- Remove pf.path() from container before the reference got screwed up by it.remove()
- Correction des étiquettes dans la description du protocole « KIO-slave »
- Considérer les fichiers « markdown » comme des documents

### Icônes « Breeze »

- Ajout d'une icône « overflow-menu » (bogue 385171)

### Modules additionnels « CMake »

- Fix python bindings compilation after 7af93dd23873d0b9cdbac192949e7e5114940aa6

### Intégration avec l'environnement de développement

- Make KStandardGuiItem::discard match QDialogButtonBox::Discard

### KActivitiesStats

- Changement de la limite par défaut de recherche à zéro
- Ajout d'une option pour activer le testeur de modèle

### KCMUtils

- Make KCMultiDialog scrollable (bug 354227)

### KConfig

- Deprecate KStandardShortcut::SaveOptions

### KConfigWidgets

- Deprecate KStandardAction::PasteText and KPasteTextAction

### KCoreAddons

- desktoptojson: Improve legacy service type detection heuristic (bug 384037)

### KDeclarative

- Changement de licence vers « LGPL2.1+ »
- Ajout d'une méthode « openService() » à « KRunProxy »

### KFileMetaData

- fix crash when more than one instances of ExtractorCollection are destructed

### KGlobalAccel

- Revert "KGlobalAccel: port to KKeyServer's new method symXModXToKeyQt, to fix numpad keys" (bug 384597)

### KIconThemes

- Ajouter une méthode pour ré-initialiser la palette personnalisée
- Utilisation de « qApp-&gt;palette() » quand aucune méthode n'est définie
- Allocation de la bonne taille du tampon
- allow to set a custom palette instead of colorSets
- Mettre en avant un ensemble de couleurs pour la feuille de style

### KInit

- Windows: Fix 'klauncher uses absolute compile time install path for finding kioslave.exe'

### KIO

- kioexec: Watch the file when it has finished copying (bug 384500)
- KFileItemDelegate: Always reserve space for icons (bug 372207)

### Kirigami

- don't instantiate Theme file in BasicTheme
- Ajouter un nouveau bouton « Suivant »
- Moins de contraste pour l'arrière-plan de la barre de défilement
- more reliable insert and remove from overflow menu
- Meilleur rendu pour l'icône contextuelle
- more careful to center the action button
- Utilisation de tailles d'icônes pour les boutons d'actions
- Taille optimale en pixels pour les icônes sur le bureau
- Effet sélectionné pour simuler une icône de poignée
- Correction de la couleur des poignées. 
- Meilleure couleur pour le bouton principal d'actions
- Correction d'un menu contextuel pour le style de bureau
- Amélioration du menu « More » à partir de la barre d'outils
- a proper menu for the intermediate pages context menu
- Ajout d'un champ de texte qui devrait proposer un clavier numérique 
- Ne pas avoir de plantage lors du lancement avec des styles inexistants
- Concept « ColorSet » dans l'élément « Thème »
- Simplification de la gestion de la molette de souris (bogue 384704)
- new example app with desktop/mobile main qml files
- Vérification de la validité de « currentIndex »
- Generate the appstream metadata of the gallery app
- Look for QtGraphicalEffects, so packagers don't forget it
- Don't include the control over the bottom decoration (bug 384913)
- lighter coloring when listview has no activeFocus
- Certaines prises en charge pour les formats « RTL »
- Désactiver les raccourcis quand une action est désactivée
- create the whole plugin structure in the build directory
- Correction d'accessibilité pour la page principale de la galerie
- If plasma isn't available, KF5Plasma isn't either. Should fix the CI error

### KNewStuff

- Require Kirigami 2.1 instead of 1.0 for KNewStuffQuick
- Création correcte de « KPixmapSequence »
- Don't complain the knsregistry file is not present before it's useful

### Environnement de développement « KPackage »

- kpackage: bundle a copy of servicetypes/kpackage-generic.desktop
- kpackagetool: bundle a copy of servicetypes/kpackage-generic.desktop

### KParts

- KPartsApp template: fix install location of kpart desktop file

### KTextEditor

- Ignore default mark in icon border for single selectable mark
- Utiliser « QActionGroup » pour la sélection du mode de saisie
- Fix missing spell check bar (bug 359682)
- Fix the fall-back "blackness" value for unicode &gt; 255 characters (bug 385336)
- Fix trailing space visualization for RTL lines

### KWayland

- Only send OutputConfig sendApplied / sendFailed to the right resource
- Don't crash if a client (legally) uses deleted global contrast manager
- Prise en charge de « XDG » v6

### KWidgetsAddons

- KAcceleratorManager: set icon text on actions to remove CJK markers (bug 377859)
- KSqueezedTextLabel: Squeeze text when changing indent or margin
- Use edit-delete icon for destructive discard action (bug 385158)
- Fix Bug 306944 - Using the mousewheel to increment/decrement the dates (bug 306944)
- KMessageBox: Use question mark icon for question dialogs
- KSqueezedTextLabel: Respect indent, margin and frame width

### KXMLGUI

- Correction de la boucle de coloration de « KToolBar » (bogue 377859)

### Environnement de développement de Plasma

- Fix org.kde.plasma.calendar with Qt 5.10
- [FrameSvgItem] Iterate child nodes properly
- [Containment Interface] Don't add containment actions to applet actions on desktop
- Add new component for the greyed out labels in Item Delegates
- Fix FrameSVGItem with the software renderer
- Ne pas animer « IconItem » en mode logiciel
- [FrameSvg] Utilisation la connexion « new-style »
- possibility to set an attached colorscope to not inherit
- Add extra visual indicator for Checkbox/Radio keyboard focus
- Ne pas créer de pixmap nul
- Pass item to rootObject() since it's now a singleton (bug 384776)
- Création, une seule fois, de la liste du nom des onglets
- Ne pas accepter le focus actif sur un onglet
- Enregistrement de la révision 1 pour « QQuickItem »
- [Plasma Components 3] Fix RTL in some widgets
- Correction d'un identifiant non valable dans « viewitem »
- update mail notification icon for better contrast (bug 365297)

### qqc2-desktop-style

Nouveau module : le style « QtQuickControls 2 » utilisant le « QStyle » de « QWidget » pour la peinture. Cela permet d'atteindre un plus haut degré de cohérence entre les applications reposant sur « QWidget » et celles reposant sur « QML ».

### Opaque

- [solid/fstab] Add support for x-gvfs style options in fstab
- [solid/fstab] Swap vendor and product properties, allow i18n of description

### Coloration syntaxique

- Fix invalid itemData references of 57 highlighting files
- Add support for custom search paths for application-specific syntax and theme definitions
- AppArmor : correction des règles « D-Bus »
- Highlighting indexer: factor out checks for smaller while loop
- ContextChecker: support '!' context switching and fallthroughContext
- Highlighting indexer: check existence of referenced context names
- Relicense qmake highlighting to MIT license
- Let qmake highlighting win over Prolog for .pro files (bug 383349)
- Support clojure's "@" macro with brackets
- Ajout d'une coloration syntaxique pour les profils « AppArmor »
- Highlighting indexer: Catch invalid a-Z/A-z ranges in regexps
- Fixing incorrectly capitalized ranges in regexps
- add missing reference files for tests, looks ok, I think
- Added Intel HEX file support for the Syntax highlighting database
- Disable spell checking for strings in Sieve scripts

### ThreadWeaver

- Correction d'une fuite de mémoire

### Informations sur la sécurité

Le code publié a été signé en « GPG » avec la clé suivante  pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Empreinte de la clé primaire : 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Vous pouvez discuter et partager vos idées sur cette version dans la section des commentaires de <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>l'article</a>.
