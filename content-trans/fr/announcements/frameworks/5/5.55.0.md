---
aliases:
- ../../kde-frameworks-5.55.0
date: 2019-02-09
layout: framework
libCount: 70
---
### Baloo

- [tags_kio] Disable access with a double slashed url, i.e. "tags://" (bug 400594)
- Instantiate QApplication before KCrash/KCatalog
- Ignore all non-storage deviceAdded signals from Solid
- Utilisation de l'améliorateur « K_PLUGIN_CLASS_WITH_JSON »
- Remove Qt 5.10 checks now that we require it as min version

### Icônes « Breeze »

- Ajout de l'icône pour les curseurs « KCM »
- Ajout et renommage de certaines icônes « YaST » et de liens symboliques
- Improve the Notification Bell Icon by using the KAlarm design (bug 400570)
- Ajouter la mise à jour « yast »
- Improve weather-storm-* icons (bug 403830)
- Add font-otf symlinks, just like the font-ttf symlinks
- Add proper edit-delete-shred icons (bug 109241)
- Add trim margins and trim to selection icons (bug 401489)
- Delete edit-delete-shred symlinks in preparation for replacing them with real icons
- Renommage de l'icône pour les activités « kcm »
- Ajout d'une icône « kcm » pour activités 
- Ajout d'une icône « kcm » pour les bureaux virtuels
- Add icons for Touch Screen and Screen Edge KCMs
- Fix file sharing preference related icon names
- Ajout d'une icône « preferences-desktop-effects »
- Ajout d'une icône de Plasma pour les préférences de thèmes
- Improve contrast of preferences-system-time (bug 390800)
- Ajout d'une nouvelle icône pour « preferences-desktop-theme-global »
- Ajout d'une icône d'outils
- document-new icon follow ColorScheme-Text
- Ajout d'une icône « preferences-system-splash » pour l'écran de démarrage de « KCM »
- Insertion des composants graphiques / 22
- Ajout des icônes de type « MIME » pour « Kotlin » (.kt)
- Improve the preferences-desktop-cryptography icon
- Fill lock in preferences-desktop-user-password
- Consistently fill the lock in the encrypted icon
- Use a Kile Icon that is similar to the original

### Modules additionnels « CMake »

- FindGperf: in ecm_gperf_generate set SKIP_AUTOMOC for generated file
- Move -Wsuggest-override -Wlogical-op to regular compiler settings
- Fix python binding generation for classes with deleted copy constructors
- Correction de la génération de module « qmake » pour Qt 5.12.1
- Utilisation de plus de liens « https »
- API dox: add missing entries for some find-modules &amp; modules
- FindGperf: improve api dox: mark-up usage example
- ECMGenerateQmlTypes: fix api dox: title needs more --- markup
- ECMQMLModules: fix api dox: title match module name, add missing "Since"
- FindInotify: fix api dox .rst tag, add missing "Since"

### KActivities

- Correction pour MacOS

### KArchive

- Save two KFilterDev::compressionTypeForMimeType calls

### KAuth

- Remove support for passing gui QVariants to KAuth helpers

### KBookmarks

- Compilation sans « D-Bus » sous Android
- Const'ify

### KCMUtils

- [kcmutils] Add ellipsis to search labels in KPluginSelector

### KCodecs

- nsSJISProber::HandleData: Don't crash if aLen is 0

### KConfig

- kconfig_compiler: delete the assignment operator and copy constructor

### KConfigWidgets

- Build without KAuth and D-Bus on Android
- Ajouter « KLanguageName »

### KCrash

- Commentaire sur pourquoi la modification dans « ptracer » est nécessaire
- [KCrash] Establish socket to allow change of ptracer

### KDeclarative

- [KCM Controls GridView] Add remove animation

### Prise en charge de « KDELibs 4 »

- Fix some country flags to use all the pixmap

### KDESU

- handle wrong password when using sudo which asks for another password (bug 389049)

### KFileMetaData

- exiv2extractor: add support for bmp, gif, webp, tga
- Correction du test défaillant des données « gps » de type « exiv »
- Tests pour des valeurs « GPS » vides ou nulles
- Ajout de la prise en charge de plus de types « MIME » à « taglibwriter »

### KHolidays

- holidays/plan2/holiday_ua_uk - updated for 2019

### KHTML

- Add JSON metadata to khtmlpart plugin binary

### KIconThemes

- Compilation sans « D-Bus » sous Android

### KImageFormats

- xcf: Fix fix for opacity being out of bounds
- Suppression des commentaires dans les directives d'inclusion « qdebug »
- tga: Fix Use-of-uninitialized-value on broken files
- L'opacité maximale est de 255.
- xcf: Fix assert in files with two PROP_COLORMAP
- ras: Fix assert because of ColorMapLength being too big
- pcx : correction d'un plantage sur fichiers incorrects
- xcf: Implement robustness for when PROP_APPLY_MASK is not on the file
- xcf: loadHierarchy: Obey the layer.type and not the bpp
- tga: Don't support more than 8 alpha bits
- ras: Return false if allocating the image failed
- rgb: Fix integer overflow in fuzzed file
- rgb: Fix Heap-buffer-overflow in fuzzed file
- psd : correction d'un plantage sur fichiers incorrects
- xcf : initialisation des décalages « x » / « y »
- rgb : correction d'un plantage sur images incorrectes
- pcx : correction d'un plantage sur images incorrectes
- rgb : correction d'un plantage sur fichiers incorrects
- xcf : initialisation du mode par calque
- xcf : initialisation de l'opacité d'un calque
- xcf: set buffer to 0 if read less data that expected
- bzero -&gt; memset
- Fix various OOB reads and writes in kimg_tga and kimg_xcf
- pic: resize header id back if didn't read 4 bytes as expected
- xcf: bzero buffer if read less data than expected
- xcf: Only call setDotsPerMeterX/Y if PROP_RESOLUTION is found
- xcf : initialisation de « num_colors »
- xcf : initialisation de la propriété de calque visible
- xcf: Don't cast int to enum that can't hold that int value
- xcf: Do not overflow int on the setDotsPerMeterX/Y call

### KInit

- KLauncher: handle processes exiting without error (bug 389678)

### KIO

- Améliorations des contrôles clavier du composant graphique pour la somme de contrôle.
- Add helper function to disable redirections (useful for kde-open)
- Revert "Refactor SlaveInterface::calcSpeed" (bug 402665)
- Don't set CMake policy CMP0028 to old. We don't have targets with :: unless they are imported.
- [kio] Add ellipsis to search label in Cookies section
- [KNewFileMenu] Don't emit fileCreated when creating a directory (bug 403100)
- Use (and suggest using) the nicer K_PLUGIN_CLASS_WITH_JSON
- avoid blocking kio_http_cache_cleaner and ensure exit with session (bug 367575)
- Fix failing knewfilemenu test and underlying reason for its failure

### Kirigami

- Mise en avant uniquement des boutons colorés (bogue 403807)
- Correction de la hauteur
- same margins sizing policy as the other list items
- === Opérateurs
- Prise en charge du concept d'éléments extensibles
- don't clear when replacing all the pages
- Le remplissage horizontal est double de celui vertical
- Prise en charge du remplissage pour le calcul des tailles des astuces
- [kirigami] Do not use light font styles for headings (2/3) (bug 402730)
- Unbreak the AboutPage layout on smaller devices
- stopatBounds for breadcrumb flickable

### KItemViews

- [kitemviews] Change the search in Desktop Behavior/Activities to more in line with other search labels

### KJS

- Set SKIP_AUTOMOC for some generated files, to deal with CMP0071

### KNewStuff

- Fix semantics for ghns_exclude (bug 402888)

### KNotification

- Corriger les fuites de mémoire lors du passage des données d'icônes vers Java
- Remove the AndroidX support library dependency
- Ajouter la prise en charge du canal de notifications « Android »
- Make notifications work on Android with API level &lt; 23
- Move the Android API level checks to runtime
- Supprimer la déclaration postérieure inutilisée
- Re-construction de « AAR » lors de modifications des sources « Java »
- Build the Java side with Gradle, as AAR instead of JAR
- Don't rely on the Plasma workspace integration on Android
- Recherche de configuration sur les évènements de notification dans les ressources « qrc »

### Environnement de développement « KPackage »

- Réaliser le travail de traduction

### KPty

- Corriger l'incohérence struct / classe

### KRunner

- Remove explicit use of ECM_KDE_MODULE_DIR, is part of ECM_MODULE_PATH

### KService

- Compilation sans « D-Bus » sous Android
- Suggest people to use K_PLUGIN_CLASS_WITH_JSON

### KTextEditor

- Qt 5.12.0 has issues in the regex implementation in QJSEngine, indenters might behave incorrectly, Qt 5.12.1 will have a fix
- KateSpellCheckDialog: Remove action "Spellcheck Selection"
- Update JavaScript library underscore.js to version 1.9.1
- Fix bug 403422: Allow changing the marker size again (bug 403422)
- SearchBar: Add Cancel button to stop long running tasks (bug 244424)
- Remove explicit use of ECM_KDE_MODULE_DIR, is part of ECM_MODULE_PATH
- Re-lecture du code pour la barre de saut de ligne de Kate
- ViewInternal: Fix 'Go to matching bracket' in override mode (bug 402594)
- Use HTTPS, if available, in links visible to users
- Re-lecture du code pour la barre d'état de Kate
- ViewConfig: Add option to paste at cursor position by mouse (bug 363492)
- Utilisation de l'améliorateur « K_PLUGIN_CLASS_WITH_JSON »

### KWayland

- [server] Générer des identifiants corrects d'actions tactiles
- Mise en conformité à la spécification de « XdgTest »
- Add option to use wl_display_add_socket_auto
- [server] Send initial org_kde_plasma_virtual_desktop_management.rows
- Ajout de colonnes dans le protocole Plasma pour les bureaux virtuels
- [client] Wrap wl_shell_surface_set_{class,title}
- Guard resource deletion in OuptutConfiguration::sendApplied

### KWidgetsAddons

- [KWidgetsAddons] Do not use light font styles for headings (3/3) (bug 402730)

### KXMLGUI

- Compilation sans « D-Bus » sous Android
- Make KCheckAccelerators less invasive for apps that don't directly link to KXmlGui

### ModemManagerQt

- Fix QVariantMapList operator &gt;&gt; implementation

### Environnement de développement de Plasma

- [Wallpaper templates] Add missing Comment= entry to desktop file
- Share Plasma::Theme instances between multiple ColorScope
- Make the clock svg's shadows more logically correct and visually appropriate (bug 396612)
- [frameworks] Do not use light font styles for headings (1/3) (bug 402730)
- [Dialog] Don't alter mainItem's visibility
- Réinitialiser « parentItem » lors de changements de « mainItem »

### Motif

- Utilisation de l'améliorateur « K_PLUGIN_CLASS_WITH_JSON »

### QQC2StyleBridge

- Fix combobox initial sizing (bug 403736)
- Set combobox popups to be modal (bug 403403)
- Retour partiel à « 4f00b0cabc1230fdf »
- Retour à la ligne automatique pour les infobulles longues (bogue 396385)
- Boîte à menu déroulant : correction de la délégation par défaut.
- Fake mousehover whilst combobox is open
- Set CombooBox QStyleOptionState == On rather than Sunken to match qwidgets (bug 403153)
- Correction de la boîte de dialogue avec case à cocher
- Always draw the tooltip on top of everything else
- Prise en charge d'une infobulle pour la zone souris
- Ne pas forcer l'affichage de texte pour le bouton d'outils

### Opaque

- Compilation sans « D-Bus » sous Android

### Sonnet

- Don't call this code if we have only space

### Coloration syntaxique

- Fix end of folding region in rules with lookAhead=true
- AsciiDoc: Fix highlighting of include directive
- Ajout de la prise en charge de « AsciiDoc »
- Fixed Bug Which Caused Infinite Loop While Highlighting Kconfig Files
- Vérifier pour des changements de contexte en boucle
- Ruby: fix RegExp after ": " and fix/improve detection of HEREDOC (bug 358273)
- Haskell : définition de « = » comme symbole spécial

### Informations sur la sécurité

Le code publié a été signé en « GPG » avec la clé suivante  pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Empreinte de la clé primaire : 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
