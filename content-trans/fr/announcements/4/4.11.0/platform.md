---
date: 2013-08-14
hidden: true
title: La plate-forme KDE 4.11 présente de meilleures performances
---
Les fonctionnalités de la plate-forme KDE 4 ont été gelées depuis la version 4.0. Cette version n'apporte en conséquence, qu'un certain nombre de corrections de bogues et d'améliorations de performances.

Le moteur sémantique de stockage et de recherche de Nepomuk a bénéficié d'optimisations de performances importantes comme avec un ensemble d'optimisations rendant la lecture de données 6 fois plus rapide. L'indexation est devenu plus performante avec un découpage en en deux étapes : la première étape traite immédiatement les informations générales (comme le type et le nom du fichier). Les informations complémentaires (telles que les balises MP3, les informations sur l'auteur et autres de nature similaire) sont extraites dans la seconde étape, d'une façon ou d'une autre plus lentement. L'affichage des métadonnées pour les contenus nouveaux ou récemment téléchargés est maintenant plus rapide. De plus, le système de sauvegarde et de restauration de Nepomuk a été amélioré par les développeurs. Enfin mais tout aussi important, Nepomuk peut maintenant indexer de nombreux formats de documents incluant ceux ayant des extensions « odt » et « docx ».

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/dolphin.png" caption=`Fonctionnalités sémantiques en action dans Dolphin` width="600px">}}

Le format optimisé d'enregistrement et l'outil d'indexation de courriers électroniques, totalement ré-écrit, nécessitent de relancer certaines indexations pour le contenu du disque dur. En conséquence, l'exécution de la ré-indexation consommera une quantité inhabituelle de ressources de traitement durant une certaine période - dépendant de la quantité de données ayant besoin d'être ré-indexées. Une conversion automatique de la base de données Nepomuk s'exécutera à la première connexion.

Il y a eu de nombreuses corrections mineures <a href='https://projects.kde.org/projects/kde/kdelibs/repository/revisions?rev=KDE%2F4.11'>pouvant être consultées dans les journaux Git</a>.

#### Installation de l'environnement de développement de KDE

Les logiciels de KDE, y compris toutes ses bibliothèques et ses applications, sont disponibles gratuitement sous des licences « Open Source ». Ils fonctionnent sur diverses configurations matérielles, sur des architectures processeurs comme « ARM » ou « x86 » et sur des différents systèmes d'exploitation. Ils utilisent tout type de gestionnaire de fenêtres ou environnements de bureaux. A côté des systèmes d'exploitation Linux ou reposant sur UNIX, vous pouvez trouver des versions « Microsoft Windows » de la plupart des applications de KDE sur le site <a href='http://windows.kde.org'>Logiciels KDE sous Windows</a> et des versions « Apple Mac OS X » sur le site <a href='http://mac.kde.org/'>Logiciels KDE sous Mac OS X</a>. Des versions expérimentales des applications de KDE peuvent être trouvées sur Internet pour diverses plate-formes mobiles comme « MeeGo », « MS Windows Mobile » et « Symbian » mais qui sont actuellement non prises en charge. <a href='http://plasma-active.org'>Plasma Active</a> est une interface utilisateur pour une large variété de périphériques comme des tablettes et d'autres matériels mobiles. <br />

Les logiciels KDE peuvent être obtenus sous forme de code source et de nombreux formats binaires à l'adresse <a href='http://download.kde.org/stable/4.11.0'>download.kde.org</a>. Ils peuvent être aussi obtenus sur <a href='/download'>CD-ROM</a> ou avec n'importe quelle distribution <a href='/distributions'>majeure de systèmes GNU / Linux et UNIX</a> publiée à ce jour.

##### Paquets

Quelques fournisseurs de systèmes Linux / Unix mettent à disposition gracieusement des paquets binaires de 4.11.0 pour certaines versions de leurs distributions. Dans les autres cas, des bénévoles de la communauté le font aussi. <br />

##### Emplacements des paquets

Pour obtenir une liste courante des paquets binaires disponibles, connus par l'équipe de publication de KDE, veuillez visiter le <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_4.11.0'>Wiki de la communauté</a>.

Le code source complet pour 4.11.0 peut être <a href='/info/4/4.11.0'>librement téléchargé</a>. Les instructions pour la compilation et l'installation des logiciels KDE 4.11.0 sont disponibles à partir de la <a href='/info/4/4.11.0#binary'>Page d'informations 4.11.0</a>.

#### Configuration minimale du système

Pour bénéficier au maximum de ces nouvelles versions, l'utilisation d'une version récente de Qt est recommandée, comme la version 4.8.4. Elle est nécessaire pour garantir un fonctionnement stable et performant, puisque certaines améliorations apportées aux logiciels de KDE ont été réalisées dans les bibliothèques Qt utilisées.<br /> Pour utiliser pleinement toutes les possibilités des logiciels de KDE, l'utilisation des tout derniers pilotes graphiques pour votre système est recommandée, puisque que ceux-ci peuvent grandement améliorer votre expérience d'utilisateur, à la fois dans les fonctionnalités optionnelles que dans les performances et la stabilité générales.

## Également annoncé aujourd'hui : 

## <a href="../plasma"><img src="/announcements/announce-4.11/images/plasma.png" class="app-icon" alt="The KDE Plasma Workspaces 4.11" width="64" height="64" /> Les espaces de travail de Plasma 4.11 continuent à améliorer l'expérience utilisateur</a>.

Engagé dans une maintenance à long terme, les environnements de bureaux Plasma apportent de nombreuses améliorations aux fonctionnalités de base, avec une barre de tâches plus facile, un composant graphique de batterie plus intelligent et un mélangeur de sons amélioré. L'introduction de KScreen apporte une gestion intelligente du mode multi-écran à l'environnement de bureaux et des améliorations très importantes de performances, conjugué avec de petits réglages d'ergonomie nécessaires à un confort général agréable.

## <a href="../applications"><img src="/announcements/4/4.11.0/images/applications.png" class="app-icon" alt="The KDE Applications 4.11"/> Les applications de KDE 4.11 apportent un important pas en avant concernant la gestion des informations personnelles et des améliorations sur toutes les fonctionnalités.</a>

Cette version intègre de très nombreuses améliorations dans la pile de KDE PIM, apportant de bien meilleures performances et plusieurs nouvelles fonctionnalités. Kate améliore la productivité pour les développeurs Python et Javascript avec de nouveaux modules externes. Dolphin est devenu plus rapide et les applications pour l'éducation bénéficient de nombreuses fonctionnalités variées.
