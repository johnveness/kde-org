---
aliases:
- ../announce-applications-16.12-rc
date: 2016-12-02
description: KDE publie la version candidate 16.12 des applications de KDE.
layout: application
release: applications-16.11.90
title: KDE publie la version candidate des applications KDE 16.12
---
02 Décembre 2016. Aujourd'hui, KDE a publié la version candidate des nouvelles versions des applications KDE. Les dépendances et les fonctionnalités sont figées. L'attention de l'équipe KDE se porte à présent sur la correction des bogues et les dernières finitions.

Veuillez vérifier les <a href='https://community.kde.org/Applications/16.12_Release_Notes'>notes de mises à jour de la communauté</a> pour plus d'informations sur les nouveaux fichiers compressés, maintenant bâtis pour KDE et les problèmes connus. Une annonce plus complète sera disponible avec la mise à jour finale.

Les versions des applications de KDE 16.12 ont besoin de tests intensifs pour maintenir et améliorer la qualité et l'interface utilisateur. Les utilisateurs actuels sont très importants pour maintenir la grande qualité de KDE. En effet, les développeurs ne peuvent tester toutes les configurations possibles. Votre aide est nécessaire pour aider à trouver les bogues suffisamment tôt pour qu'ils puissent être corrigés avant la version finale. Veuillez contribuer à l'équipe en installant la version candidate et <a href='https://bugs.kde.org/'>en signalant tout bogue</a>.
