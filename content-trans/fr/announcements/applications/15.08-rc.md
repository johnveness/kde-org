---
aliases:
- ../announce-applications-15.08-rc
date: 2015-08-06
description: KDE publie la version candidate 15.08 des applications de KDE.
layout: application
release: applications-15.07.90
title: KDE publie la version candidate des applications de KDE en version 15.08
---
06 Août 2015. Aujourd'hui, KDE publie la version candidate des nouvelles versions des applications KDE. Les dépendances et les fonctionnalités sont figées. L'attention de l'équipe KDE se porte à présent sur la correction des bogues et les dernières finitions.

Avec de nombreuses applications reposant sur l'environnement de développement version 5, les mises à jour des applications 15.08 ont besoin de tests intensifs pour maintenir et améliorer la qualité et l'interface utilisateur. Les utilisateurs actuels sont très importants pour maintenir la grande qualité de KDE. En effet, les développeurs ne peuvent tester toutes les configurations possibles. Votre aide est nécessaire pour aider à trouver les bogues suffisamment tôt pour qu'ils puissent être corrigés avant la version finale. Veuillez contribuer à l'équipe en installant la version et <a href='https://bugs.kde.org/'>en signalant tout bogue</a>.
