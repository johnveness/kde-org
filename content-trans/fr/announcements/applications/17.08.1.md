---
aliases:
- ../announce-applications-17.08.1
changelog: true
date: 2017-09-07
description: KDE publie les applications de KDE en version 17.08.1
layout: application
title: KDE publie les applications de KDE en version 17.08.1
version: 17.08.1
---
07 Septembre 2017. Aujourd'hui, KDE a publié la première mise à jour de consolidation pour les <a href='../17.08.0'>applications 17.08 de KDE</a>. Cette version ne contient que des corrections de bogues et des mises à jour de traduction, permettant une mise à jour sûre et appréciable pour tout le monde.

Plus de 20 corrections de bogues apportent des améliorations à Kontact, Gwenview, Kdenlive, Konsole, KWalletManager, Okular, Umbrello, Jeux KDE et bien d'autres.

Cette mise à jour inclut aussi une version de prise en charge à long terme pour la plate-forme de développement 4.14.36 de KDE.
