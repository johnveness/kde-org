---
aliases:
- ../announce-applications-16.04.2
changelog: true
date: 2016-06-14
description: KDE levererar KDE-program 16.04.2
layout: application
title: KDE levererar KDE-program 16.04.2
version: 16.04.2
---
14:e juni, 2016. Idag ger KDE ut den andra stabilitetsuppdateringen av <a href='../16.04.0'>KDE-program 16.04</a>. Utgåvan innehåller bara felrättningar och översättningsuppdateringar, och är därmed en säker och behaglig uppdatering för alla.

Mer än 25 registrerade felrättningar omfattar förbättringar av bland annat akonadi, ark, artikulate, dolphin. kdenlive och kdepim.

Utgåvan inkluderar också versioner för långtidsunderhåll av KDE:s utvecklingsplattform 4.14.21.
