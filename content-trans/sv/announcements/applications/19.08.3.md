---
aliases:
- ../announce-applications-19.08.3
changelog: true
date: 2019-11-07
description: KDE levererar Program 19.08.
layout: application
major_version: '19.08'
release: applications-19.08.3
title: KDE levererar Program 19.08.3
version: 19.08.3
---
{{% i18n_date %}}

Idag ger KDE ut den tredje stabilitetsuppdateringen av <a href='../19.08.0'>KDE-program 19.08</a>. Utgåvan innehåller bara felrättningar och översättningsuppdateringar, och är därmed en säker och behaglig uppdatering för alla.

Fler än ett dussin registrerade felrättningar omfattar förbättringar av bland annat Kontact, Ark, Cantor, K3b, Kdenlive, Konsole, Okular, Spectacle och Umbrello.

Förbättringar omfattar:

- I videoeditorn Kdenlive försvinner inte längre kompositioner när ett projekt med låsta spår öppnas igen
- Okulars kommentarvy visar nu skapelsetider med lokal tidszon istället för UTC
- Tangentbordskontroll har förbättrats i skärmbildsverktyget Spectacle
