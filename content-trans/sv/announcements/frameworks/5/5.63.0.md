---
aliases:
- ../../kde-frameworks-5.63.0
date: 2019-10-14
layout: framework
libCount: 70
---
### Breeze-ikoner

- Förbättra KFloppy ikonen (fel 412404)
- Lägg till format-text-underline-squiggle åtgärdsikoner (fel 408283)
- Lägg till färgrik preferences-desktop-filter ikoner (fel 406900)
- Lägg till en programikon för Kirogi Drone kontrollprogrammet
- Lägg till skript för att skapa ett webbteckensnitt från alla breeze åtgärdsikoner
- Lägg till enablefont och disablefont ikoner för kfontinst inställningsmodul
- Rätta att stor system-reboot ikoner roterar i en inkonsekvent riktning (fel 411671)

### Extra CMake-moduler

- ny modul ECMSourceVersionControl
- Rätta FindEGL när Emscripten används
- ECMAddQch: lägg till argumentet INCLUDE_DIRS

### Integrering med ramverk

- säkerställ att winId() inte anropas för icke-inbyggda grafiska komponenter (fel 412675)

### kcalendarcore

Ny modul, tidigare känd som kcalcore i kdepim

### KCMUtils

- Undertryck mushändelser i inställningsmoduler som orsakar fönsterförflyttningar
- justera marginaler för KCMultiDialog (fel 411161)

### KCompletion

- [KComboBox] Inaktivera Qt:s inbyggda komplettering riktigt [rättning av regression]

### KConfig

- Rätta generering av egenskaper som startar med en stor bokstav

### KConfigWidgets

- Gör KColorScheme kompatibel med QVariant

### kcontacts

Ny modul, tidigare del av KDE PIM

### KCoreAddons

- Lägg till KListOpenFilesJob

### KDeclarative

- Ta bort QQmlObjectSharedEngine sammanhang synkroniserat med QQmlObject
- [KDeclarative] Konvertera från QWheelEvent::delta() som avråds från till angleDelta()

### Stöd för KDELibs 4

- Stöd NetworkManager 1.20 och kompilera inte verkligen NM-gränssnittet

### KIconThemes

- Avråd från de globala [Small|Desktop|Bar]Icon() metoderna

### KImageFormats

- Lägg till filer för att testa fel 411327
- xcf: Rätta regression vid läsning av filer med "ostödda" egenskaper
- xcf: Läs bildupplösning riktigt
- Konvertera HDR (Radiance RGBE) bildinläsning till Qt5

### KIO

- [Places panel] Gör om sektionen Senaste sparade
- [DataProtocol] kompilera utan implicit konvertering från ascii
- Ta hänsyn till användning av WebDAV metoder tillräckligt för att anta WebDAV
- REPORT stöder också Depth sidhuvudet
- Gör konvertering mellan QSslError::SslError &lt;-&gt; KSslError::Error återanvändbar
- Avråd från konstruktorn KSslError::Error i KSslError
- [Windows] rätta listning av överliggande katalog för C:foo, det vill säga C: och inte C:
- Rätta krasch vid avslutning i kio_file (fel 408797)
- Lägg till operatorerna == och != i KIO::UDSEntry
- Ersätt KSslError::errorString med QSslError::errorString
- Flytt- och kopieringsjobb: hoppa över stat för källor om målkatalogen inte är skrivbar (fel 141564)
- Rättade interaktion med DOS/Windows körbara program i KRun::runUrl
- [KUrlNavigatorPlacesSelector] Identifiera rivningsåtgärd riktigt (fel 403454)
- KCoreDirLister: rätta krasch när nya kataloger skapas från kfilewidget (fel 401916)
- [kpropertiesdialog] lägg till ikoner för storlekssektionen
- Lägg till ikoner för menyerna "Open With" och "Actions"
- Undvik att initiera en onödig variabel
- Flytta mer funktionalitet från KRun::runCommand/runApplication till KProcessRunner
- [Avancerade rättigheter] Rätta ikonnamn (fel 411915)
- [KUrlNavigatorButton] Rätta användning av QString så att inte [] används utanför gränser
- Gör så att KSslError innehåller en QSslError internt
- Avdela KSslErrorUiData från KTcpSocket
- Konvertera kpac från QtScript

### Kirigami

- lagra bara sista objektet i cache
- mer z (fel 411832)
- rätta importversion i PagePoolAction
- PagePool är Kirigami 2.11
- ta hänsyn till draghastighet när en bläddring slutar
- Rätta kopiering av webbadresser till klippbordet
- kontrollera mer om vi ändrar överliggande objekt för ett verkligt objekt
- grundstöd för ListItem åtgärder
- introducera cachePages
- rätta kompatibilitet med Qt5.11
- introducera PagePoolAction
- ny klass: PagePool för att hantera återanvändning av sidor efter de har blivit poppade
- gör så att flikrader ser bättre ut
- någon marginal till höger (fel 409630)
- Återställ "Kompensera för mindre ikonstorlekar på mobil i ActionButton"
- låt inte listobjekt se inaktiva ut (fel 408191)
- Återställ "Ta bort skalning av ikonstorleksenhet för isMobile"
- Layout.fillWidth ska göras av klienten (fel 411188)
- Lägg till mall för Kirigami programutveckling
- Lägg till ett läge för att centrera åtgärder och utelämna titeln när stilen ToolBar används (fel 402948)
- Kompensera för mindre ikonstorlekar på mobil i ActionButton
- Rättade några körtidsfel för odefinierade egenskaper
- Rätta ListSectionHeader bakgrundsfärg för vissa färgscheman
- Ta bort anpassat innehållsobjekt från ActionMenu avskiljare

### KItemViews

- [KItemViews] Konvertera till QWheelEvent programmeringsgränssnitt som inte avråds från

### KJobWidgets

- städa dbus-relaterade objekt tidigt nog för att undvika hängning vid programavslutning

### KJS

- Tillägg av startsWith(), endsWith() och includes() JS strängfunktioner
- Rättade Date.prototype.toJSON() anropad med icke-Date objekt

### KNewStuff

- Led KNewStuffQuick till funktionsparitet med KNewStuff(Widgets)

### KPeople

- Hävda att Android är en plattform som stöds
- Driftsätt förvald avatar via qrc
- Lägg in insticksprogramfiler på Android
- Inaktivera D-Bus-delar på Android
- Rätta krasch vid övervakning av en kontakt som tas bort i PersonData (fel 410746)
- Använd fullständigt kvalificerade typer för signaler

### Kör program

- Betrakta UNC-sökvägar som NetworkShare sammanhang

### KService

- Flytta Amusement till katalogen Games istället för Games &gt; Toys (fel 412553)
- [KService] Lägg till kopieringskonstruktor
- [KService] lägg till workingDirectory(), avråd från path()

### KTextEditor

- försök undvika förvrängningar i textförhandsgranskning
- Variabelexpansion: Använd std::function internt
- QRectF istället för QRect löser beskärningsproblem, (fel 390451)
- nästa återgivningsförvrängning försvinner om du justerar beskärningsrektangeln lite grand (fel 390451)
- undvik magi för att välja teckensnitt och stäng av kantutjämning (fel 390451)
- KadeModeMenuList: rätta minnesläckor och annat
- försök att söka efter användbara teckensnitt, fungerar rimligt väl om du inte använder en dum skalfaktor såsom 1.1
- Statusrad lägesmeny: Återanvänd tom QIcon som delas implicit
- Exponera KTextEditor::MainWindow::showPluginConfigPage()
- Ersätt QSignalMapper med lambda
- KateModeMenuList: använd QString() för tomma strängar
- KateModeMenuList: lägg till sektionen "Bästa sökmatchningar" och rättningar för Windows
- Variabelexpansion: Stöd QTextEdits
- Lägg till snabbtangent för att byta inmatningsläge i redigeringsmeny (fel 400486)
- Dialogruta för variabelexpansion: hantera markeringsändringar och objektaktivering riktigt
- Dialogruta för variabelexpansion: lägg till radeditor för filter
- Säkerhetskopia vid spara: Stöd ersättningar av tid och datumsträng (fel 403583)
- Variabelexpansion: Föredra returvärde över returargument
- Ursprunglig start för variabeldialogruta
- Använd nytt programmeringsgränssnitt för format

### Ramverket KWallet

- Stöd för hög upplösning

### Kwayland

- Sortera filer alfabetiskt i cmake-lista

### KWidgetsAddons

- Gör knappen Ok inställningsbar i KMessageBox::sorry/detailedSorry
- [KCollapsibleGroupBox] Rätta QTimeLine::start varning vid körtid
- Förbättra namngivning av KTitleWidget ikonmetoder
- Lägg till QIcon set-funktioner för lösenordsdialogrutor
- [KWidgetAddons] konvertera till Qt programmeringsgränssnitt som inte avråds från

### KWindowSystem

- Ställ in XCB till krävs om X-gränssnittet byggs
- Använd uppräkningstyp alias NET::StaysOnTop som avråds från mindre ofta

### KXMLGUI

- Flytta alternativet "Fullskärmsläge" från menyn Inställningar till menyn Visa (fel 106807)

### NetworkManagerQt

- ActiveConnection: anslut stateChanged() signalen till korrekt gränssnitt

### Plasma ramverk

- Exportera loggkategori för Plasma kärnbibliotek, lägg till en kategori i en qWarning
- [pluginloader] Använd kategoriserad loggning
- gör editMode en global corona egenskap
- Ta hänsyn till global faktor för animeringshastighet
- installera hel plasmacomponent3 riktigt
- [Dialogruta] Verkställ fönstertyp efter flaggor ändras
- Ändra logiken för knappen för avslöja lösenord
- Rätta krasch för rivning med miniprogrammens ConfigLoader (fel 411221)

### QQC2StyleBridge

- Rätta flera byggsystemfel
- ta marginaler från qstyle
- [Flik] Rätta storlek (fel 409390)

### Syntaxfärgläggning

- Lägg till syntaxfärgläggning för RenPy (.rpy) (fel 381547)
- WordDetect regel: detektera avgränsare i strängens innerkant
- Färglägg GeoJSON-filer som om de var vanlig JSON
- Lägg till syntaxfärgläggning för SubRip Text (SRT) textning
- Rätta skipOffset med dynamiskt reguljärt uttryck (fel 399388)
- bitbake: hantera inbäddat skal och python
- Jam: rätta identifierare i en SubRule
- Lägg till syntax-definition för Perl6 (fel 392468)
- stöd .inl filändelse för C++, inte använd av andra xml-filer för tillfället (fel 411921)
- stöd *.rej för diff färgläggning (fel 411857)

### Säkerhetsinformation

Den utgivna koden GPG-signerad genom att använda följande nyckel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primärt nyckelfingeravtryck: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
