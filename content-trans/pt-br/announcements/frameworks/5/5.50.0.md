---
aliases:
- ../../kde-frameworks-5.50.0
date: 2018-09-08
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Attica

- Adição do suporte para as novas marcas propostas no OCS 1.7

### Baloo

- Correcção de um erro tipográfico no resultado do tamanho do índice (erro 397843)
- Remoção do URL 'src' em vez do 'dest' quando um URL deixar de ser indexável
- [tags_kio] Simplificação da pesquisa da localização no nome do ficheiro, usando um grupo de captura
- Reversão do "Parar a colocação em espera de ficheiros novos e não-indexáveis, retirando-os imediatamente do índice."
- [tags_kio] Simplificação do ciclo iterativo de pesquisa da localização dos ficheiros

### Ícones Breeze

- Adição do ícone do ficheiro de projectos do LabPlot
- ScalableTest, adição do 'plasma-browser-integration' "escalável" (erro 393999)

### Módulos extra do CMake

- Interfaces: Verificar se podem ser geradas as interfaces para uma versão específica do Python
- Interfaces: Tornar o gerador compatível posteriormente com o Python 3
- Desactivação da alteração do QT_PLUGIN_PATH pelo ECM ao executar os testes
- Interfaces: Adição do suporte de enumerados com âmbito definido (erro 397154)
- Possibilidade de o ECM detectar os ficheiros .po na altura da configuração

### Integração do Framework

- [KStyle]: Uso do 'dialog-question' para o ícone de interrogação

### KArchive

- tratamento de codificações não-ASCII dos nomes dos ficheiros nos pacotes TAR (erro 266141)
- KCompressionDevice: Não invocar o 'write' depois de um WriteError (erro 397545)
- Adição de macros Q_OBJECT em falta nas sub-classes do QIODevice
- KCompressionDevice: propagação dos erros do QIODevice::close() (erro 397545)
- Correcção da página principal do 'bzip'

### KCMUtils

- Uso de um QScrollArea personalizado com a sugestão de tamanho não limitada ao tamanho do texto (erro 389585)

### KConfig

- Remoção de aviso sobre uma funcionalidade antiga do Kiosk que já não se aplica
- Mudança do atalho predefinido do sistema Ctrl+0 para a acção "Tamanho Actual"

### KCoreAddons

- Não remover o espaço entre 2 URL's quando a linha começa com " (erro do KMail)
- KPluginLoader: uso do '/' mesmo no Windows; o 'libraryPaths()' devolve localizações com '/'
- KPluginMetaData: conversão do texto vazio para uma lista de texto vazia

### KDeclarative

- Reversão da "garantia de que se está sempre a escrever no contexto de topo do motor"
- Anexação da propriedade ao "delegado" (erro 397367)
- [KCM GridDelegate] Uso do efeito da camada apenas na infra-estrutura do OpenGL (erro 397220)

### KDocTools

- adição do acrónimo ASCII ao 'general.entities'
- adição do JSON ao 'general.entities'
- Tornar o 'meinproc5' mais descritivo no teste automático do 'install'
- No teste do 'kdoctools_install', usar localizações absolutas para encontrar os ficheiros instalados

### KFileMetaData

- Adição do nome alternativo do enumerado Property::Language para o erro Property::Langauge

### KHolidays

- Implementação de um algoritmo adequado para o cálculo de equinócios e solstícios (erro 396750)
- src/CMakeLists.txt - instalação dos ficheiros de inclusão como nas plataformas

### KI18n

- Verificação da utilização de um KCatalog sem uma QCoreApplication
- Migração do 'ki18n' do QtScript para o QtQml
- Verificação da pasta de compilação também para a pasta 'po/'

### KIconThemes

- Uso do Brisa como tema de ícones de contingência

### KIO

- [KSambaShareData] Aceitação dos espaços no nome da máquina da ACL
- [KFileItemListProperties] Uso do 'mostLocalUrl' para as capacidades
- [KMountPoint] Verificar também o "smb-share" no caso de ser uma montagem em SMB (erro 344146)
- [KMountPoint] Resolução das montagens do 'gvfsd' (erro 344146)
- [KMountPoint] Remoção de vestígios do 'supermount'
- [KMountPoint] Remoção do suporte para AIX e Windows CE
- Mostrar o tipo de sistema de ficheiros montado e a referência de origem da montagem na janela de propriedades (erro 220976)
- O 'kdirlistertest' não falha mais de forma aleatória
- [KUrlComboBox] Correcção de erro de migração do KIcon
- Migração do "truque" do KPATH_SEPARATOR para o QDir::listSeparator, adicionado no 5.6
- Correcção de fuga no KUrlComboBo::setUrl
- [KFileItem] Não ler o comentário da pasta nas montagens lentas
- Uso do QDir::canonicalPath em alternativa
- Ignorar a marca 'escondido' no volume-raiz (erro 392913)
- Atribuição de um botão Cancelar na janela "nome de pasta inválida"
- KPropertiesDialog: mudança para legenda no setFileNameReadOnly(true)
- Mudança no texto quando não é possível criar uma pasta com um nome inválido
- Uso de um ícone apropriado para um botão Cancelar, quando vai pedir um novo nome
- Tornar os nomes de ficheiros apenas-para-leitura aptos para selecção
- Uso de capitalização do texto para algumas legendas de botões
- Uso do KLineEdit para o nome da pasta se a mesma tiver acesso de escrita, caso contrário usar o QLabel
- KCookieJar: correcção de conversões erradas de fusos-horários

### Kirigami

- suporte do 'fillWidth' para os itens
- protecção contra a remoção externa de páginas
- mostrar sempre o cabeçalho quando estiver no modo colapsável
- correcção do comportamento do 'showContentWhenCollapsed'
- correcção de lacunas nos menus no estilo Material
- menu-padrão de acções para o menu de contexto da página
- Pedido explícito do QtQuick no Qt 5.7 para usar o Connections.enabled
- uso da cor da Window em vez de um item de fundo
- validação de que o 'drawer' é fechado antes de introduzir outro
- exportação do 'separatorvisible' para o objecto 'globaltoolbar'
- Correcção da geração de 'plugins' estáticos QRC do Kirigami
- Correcção da compilação no modo estático do LTO
- Validação de que a propriedade 'drawerOpen' fica sincronizada de forma correcta (erro 394867)
- drawer: Mostrar o item do conteúdo no arrastamento
- Permitir o uso dos elementos do 'qrc' nos ícones das acções
- 'ld' num 'gcc' antigo (erro 395156)

### KItemViews

- Descontinuação do KFilterProxySearchLine

### KNewStuff

- ID de fornecedor da 'cache'

### KNotification

- Suporte do 'libcanberra' para a notificação do áudio

### KService

- KBuildSycoca: processar sempre os ficheiros '.desktop' das aplicações

### KTextEditor

- Transformação do enumerado Kate::ScriptType numa classe enumerada
- correcção do tratamento de erros no QFileDevice e no KCompressedDevice
- InlineNotes: Não imprimir notas incorporadas
- Remoção do QSaveFile em detrimento da gravação à antiga de ficheiros
- InlineNotes: Uso das coordenadas globais do ecrã em todo o lado
- InlineNote: Inicialização da posição com o Cursor::invalid()
- InlineNote: 'Pimpl' dos dados da nota incorporada sem alocações
- Adição da interface de notas incorporadas
- Mostrar a antevisão do texto apenas se a janela principal estiver activa (erro 392396)
- Correcção de estoiro ao esconder o elemento TextPreview (erro 397266)
- Junção do ssh://git.kde.org/ktexteditor
- melhoria do desenho em HiDPI do contorno dos ícones
- Melhoria no tema de cores do Vim (erro 361127)
- Pesquisa: Adição de alternativa para os ícones em falta no tema de ícones do Gnome
- correcção da pintura exagerada do '_' ou de letras como o 'j' na última linha (erro 390665)
- Extensão da API de Programação para permitir a execução de comandos
- Programa de indentação de R
- Correcção de estoiro ao substituir a mudança de linha em torno das linhas vazias (erro 381080)
- remoção da janela de transferência de realces de sintaxe
- não existe necessidade de alocar/libertar o 'hash' em cada 'doHighlight', bastando apenas limpá-lo
- garantia do tratamento adequado de índices de atributos inválidos que possam ocorrer de forma residual após a mudança de realce de um documento
- deixar os ponteiros inteligentes tratar da remoção dos objectos, tendo assim menos acções manuais para fazer
- remoção do mapa onde pesquisa pelas propriedades adicionais do realce de sintaxe
- O KTextEditor usa a plataforma KSyntaxHighlighting para tudo
- uso das codificações de caracteres fornecidas pelas definições
- Junção da ramificação 'master' com o módulo 'syntax-highlighting' (realce de sintaxe)
- o texto não-negrito não é mais desenhado com uma espessura de texto fina (erro 393861)
- uso do 'foldingEnabled'
- remoção do EncodedCharaterInsertionPolicy
- Impressão: Respeito do tipo de letra do rodapé, correcção da posição vertical do rodapé, tornar a linha separadora do cabeçalho/rodapé visualmente mais clara
- Junção da ramificação 'master' com o módulo 'syntax-highlighting' (realce de sintaxe)
- Deixar a plataforma 'syntax-highlighting' tratar de toda a gestão de definições, agora que existe uma definição 'None' (nenhuma) no repositório
- item de completação: correcção do tamanho mínimo da secção do comentário
- Correcção: Deslocamento de linhas de visualização em vez de linhas reais para o deslocamento da roda e do rato por toque (erro 256561)
- remoção do teste de sintaxe, já que é agora testado na plataforma 'syntax-highlighting' propriamente dita
- A configuração do KTextEditor agora é local ao nível da aplicação de novo, onde a antiga configuração global será importada na primeira utilização
- Uso do KSyntaxHighlighting::CommentPosition em vez do KateHighlighting::CSLPos
- Uso do isWordWrapDelimiter() do KSyntaxHighlighting
- Mudança de nome do isDelimiter() para isWordDelimiter()
- implementação de mais pesquisas através da ligação formato -&gt; definição
- agora são recebidos sempre formatos válidos
- forma mais agradável de obter o nome do atributo
- correcção do teste de indentação de Python, acessor mais seguro para as listas de propriedades
- adição dos prefixos de definição correctos
- Junção da ramificação 'syntax-highlighting' do git://anongit.kde.org/ktexteditor com o 'syntax-highlighting'
- tentar obter de volta as listas necessárias para efectuar a configuração por esquema
- Uso do KSyntaxHighlighting::Definition::isDelimiter()
- o 'make' pode quebrar o 'bit' mais como o código da 'word'
- nenhuma lista ligada sem qualquer razão
- limpeza da inicialização das propriedades
- correcção da ordem dos formatos, recordação da definição na lista de realces
- tratamento de formatos válidos / formatos de tamanho zero
- remoção de mais alguns componentes de implementação antigos, correcção de alguns acessores para usar o código do formato
- correcção da dobragem com base na indentação
- remoção da exposição da pilha de contextos no 'doHighlight' + correcção do 'ctxChanged'
- iniciar o armazenamento dos dados de dobragem
- eliminação dos utilitários de realce, por não serem mais necessários
- remoção da necessidade do 'contextNum', adição do marcador 'FIXME-SYNTAX' nas coisas que precisam de ser corrigidas de forma adequada
- adaptação às modificações do 'includedDefinitions', remoção do 'contextForLocation', já que só são necessárias palavras-chave para a localização ou a verificação ortográfica para a localização, que poderá ser implementada mais tarde
- remoção de mais coisas que não são usadas no realce de sintaxe
- correcção do objecto 'm_additionalData' e da associação do mesmo, o que deverá funcionar para os atributos e não para o contexto
- criação dos atributos iniciais, ainda sem valores de atributos reais, tendo apenas uma lista com algumas coisas
- invocação do realce
- derivação do realce abstracto, criação da definição

### KWallet Framework

- Passagem do exemplo da 'techbase' para um repositório próprio

### KWayland

- Sincronização dos métodos 'set/send/update'
- Adição de número de série e ID do EISA à interface do OutputDevice
- Correcção das curvas de cores do dispositivo de saída
- Correcção da gestão de memória no WaylandOutputManagement
- Isolamento de todos os testes dentro do WaylandOutputManagement
- Escalas fraccionárias no OutputManagement

### KWidgetsAddons

- Criação de um primeiro exemplo do uso do KMessageBox
- Correcção de dois erros no KMessageWidget
- [KMessageBox] Invocação do estilo do ícone
- Adição de método alternativo para as legendas com mudança de linha (erro 396450)

### KXMLGUI

- Tornar o Konqi bonito em HiDPI
- Adição dos parêntesis em falta

### NetworkManagerQt

- Uso obrigatório no NetworkManager 1.4.0 e posterior
- gestor: adição do suporte para leitura/escrita na propriedade GlobalDnsConfiguration
- Permitir de facto definir a taxa de actualização das estatísticas do dispositivo

### Plasma Framework

- Contorno para um erro com o desenho nativo e a opacidade no texto do TextField (erro 396813)
- [Item de Ícones] Vigiar a mudança de ícones do KIconLoader quando usar o QIcon (erro 397109)
- [Item de Ícones] Uso do ItemEnabledHasChanged
- Remoção de usos desactualizados do QWeakPointer
- Correcção de folhas de estilo para o '22-22-system-suspend' (erro 397441)
- Melhoria na remoção dos Widgets e na configuração do texto

### Solid

- solid/udisks2: Adição do suporte para o registo por categorias
- [Dispositivo Windows] Mostrar a legenda do dispositivo só se existir alguma
- Forçar uma nova avaliação dos Predicates se as interfaces forem removidas (erro 394348)

### Sonnet

- hunspell: Reposição da compilação com o hunspell &lt;=v1.5.0
- Incorporação dos ficheiros de inclusão do Hunspell como inclusões de sistema

### sindicância

Novo módulo

### Realce de sintaxe

- realce de 20000 linhas por caso de teste
- tornar a classificação de performance do realce mais fácil de reproduzir, já que se pretende medir esta execução com p.ex. um 'perf' externo
- Afinação da pesquisa do KeywordList &amp; eliminação de alocações para os grupos de captura implícitos
- remoção das capturas do Int, por nunca terem sido implementadas
- iteração determinista dos testes para uma melhor comparação dos resultados
- tratamento dos atributos de inclusão encadeados
- actualização do realce para Modula-2 (erro 397801)
- pré-calculo do formato do atributo para o contexto &amp; regras
- evitar a verificação do separador de palavras no início de uma palavra-chave (erro 397719)
- Adição do realce de sintaxe para a linguagem políticas SELinux do 'kernel'
- esconder o 'bestCandidate' - pode ser uma função estática dentro do ficheiro
- Adição de algumas melhoras no 'kate-syntax-highlighter' para usar em programação
- adição do := como uma parte válida de um identificador
- uso dos nossos próprios dados de entrada na medição de performance
- tentativa de correcção de problema nos fins de linhas na comparação dos resultados
- tentar um resultado trivial do 'diff' no Windows
- adição do 'defData' de novo para a verificação de estado válido
- diminuição do espaço do StateData em mais de 50% e com metade do número de 'malloc's necessários
- melhoria na performance do Rule::isWordDelimiter e do KeywordListRule::doMatch
- Melhoria no tratamento da eliminação do deslocamento, permitindo saltar uma linha inteira em caso de não-correspondência
- verificação da lista de caracteres especiais das extensões
- mais realces de Asterisk, por experiência com algumas configurações do Asterisk, usando o estilo do INI, usando o '.conf' como finalização do INI
- correcção do realce dos elementos '#ifdef _xxx' (erro 397766)
- correcção dos caracteres especiais nos ficheiros
- Mudança para a licença MIT do KSyntaxHighlighting terminada
- JavaScript: adição dos binários, correcção dos octais, melhorias nas sequências de escape &amp; permitir identificadores não-ASCII (erro 393633)
- Possibilidade de desligar as pesquisas do QStandardPaths
- Possibilidade de instalação de ficheiros de sintaxe em vez de os ter num recurso
- tratamento dos atributos de mudança de contexto dos próprios contextos
- mudança de biblioteca estática para biblioteca de objectos com uma configuração correcta do PIC, o que deverá funcionar nas compilações estáticas + dinâmicas
- evitar alocações na memória de dados para o Format() construído por omissão e usado como "inválido"
- respeitar a variável do CMake para as bibliotecas estáticas vs. dinâmicas, como p.ex. o KArchive
- Mudança para a licença MIT, https://phabricator.kde.org/T9455
- remoção do programa antigo 'add_license', por não ser mais necessário
- Correcção do includedDefinitions, tratamento da mudança de definições na mudança de contextos (erro 397659)
- SQL: diversas melhorias e correcção da detecção do 'if/case/loop/end' com o SQL (Oracle)
- correcção dos ficheiros de referência
- SCSS: actualização da sintaxe. CSS: correcção do realce do Operador e da Marca de Selector
- debchangelog: adição do Bookworm
- Mudança da licença do Dockerfile para a licença MIT
- remoção da parte de configuração da verificação ortográfica (não mais suportada) que tinha sempre um modo e que agora foi tornado fixo
- Adição do suporte de realce de sintaxe para o Stan
- adição da indentação para trás
- Optimização de muitos ficheiros de realce de sintaxe e correcção do carácter '/' do SQL
- Linhas de Modo: adição da marca de ordem dos 'bytes' &amp; pequenas correcções
- Mudança do 'modelines.xml' para a licença MIT (erro 198540)
- Adição do método 'QVector&lt;QPair&lt;QChar, QString&gt;&gt; Definition::characterEncodings() const'
- Adição do método 'bool Definition::foldingEnabled() const'
- Adição do realce "None" (Nenhum) ao repositório por omissão
- Actualização do suporte da sintaxe da linguagem Logtalk
- Adição dos formatos de ficheiros da Autodesk EAGLE 'sch' e 'brd' à categoria do XML
- Realce de C#: Preferir a indentação ao estilo do C
- AppArmor: actualização da sintaxe e diversas melhorias/correcções
- Java: adição de binários &amp; vírgula-flutuante hexadecimal, assim como suporte de sublinhados nos números (erro 386391)
- Limpeza: a indentação foi movida da secção geral para a secção da linguagem
- Definição: Exposição dos marcadores de comandos
- Adição do realce de sintaxe do JavaScript React
- YAML: correcção de chaves, adição de números e outras melhorias (erro 389636)
- Adição do método 'bool Definition::isWordWrapDelimiter(QChar)'
- Definição: Mudança do nome do método isDelimiter() para isWordDelimiter()
- Anotação de algumas ideias de melhorias na API para o KF6 a partir da migração do KTE
- Oferta de um formato válido também para as linhas vazias
- Capacidade de o Definition::isDelimiter() funcionar também para definições inválidas
- Definição: Exposição do 'bool isDelimiter() const'
- Ordenação dos formatos devolvidos no Definition::formats() pelo ID

### Informações de segurança

O código lançado foi assinado com GPG, usando a seguinte chave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Impressão digital da chave primária: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB

Você pode discutir e compartilhar ideias sobre esta versão na seção de comentários do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
