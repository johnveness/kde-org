---
aliases:
- ../../kde-frameworks-5.12.0
date: 2015-07-10
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Módulos extra do CMake

- Melhoria na comunicação de erros da macro query_qmake

### BluezQt

- Remoção de todos os dispositivos do adaptador antes de removê-lo (erro <a href='https://bugs.kde.org/show_bug.cgi?id=349363'>349363</a>)
- Atualização dos links no README.md

### KActivities

- Adição da opção para não registrar dados do usuário em determinadas atividades (semelhante ao modo de 'navegação privativa' de um navegador Web)

### KArchive

- Manutenção das permissões executáveis dos arquivos no copyTo()
- Clarificação do ~KArchive com a remoção de código obsoleto.

### KAuth

- Possibilidade de usar o <i>kauth-policy-gen</i> a partir de diferentes fontes

### KBookmarks

- Não adicionar um favorito com URL e texto em branco
- Codificação da URL do KBookmark para corrigir a compatibilidade com os aplicativos do KDE4

### KCodecs

- Remoção do teste <i>x-euc-tw</i>

### KConfig

- Instalação do <i>kconfig_compiler</i> na <i>libexec</i>
- Nova opção de geração de código TranslationDomain=, para usar com o TranslationSystem=kde; normalmente necessário em bibliotecas.
- Possibilidade de usar o <i>kconfig_compiler</i> a partir de diferentes fontes

### KCoreAddons

- KDirWatch: Só estabelecer uma conexão ao FAM se for solicitada
- Permitir a filtragem de plugins e aplicativos pelo formato da tela
- Possibilidade de usar o <i>desktoptojson</i> a partir de diferentes fontes

### KDBusAddons

- Clarificação do valor de saída para as instâncias <i>Unique</i>

### KDeclarative

- Adição do clone para QQC do KColorButton
- Atribuição de um QmlObject para cada instância <i>kdeclarative</i> quando possível
- Executar o Qt.quit() a partir do código em QML
- Mesclagem da ramificação <i>mart/singleQmlEngineExperiment</i>
- Implementação do <i>sizeHint</i> com base no <i>implicitWidth/height</i>
- Subclasse do <i>QmlObject</i> com um mecanismo estático

### KDELibs 4 Support

- Correção da implementação do KMimeType::Ptr::isNull.
- Reativação do suporte para <i>streaming</i> do KDateTime para o kDebug/qDebug, para mais SC
- Carregamento do catálogo de traduções correto para o <i>kdebugdialog</i>
- Não ignorar a documentação de métodos obsoletos, para que as pessoas possam ler as sugestões de migração

### KDESU

- Correção do CMakeLists.txt para passar o KDESU_USE_SUDO_DEFAULT à compilação, de forma a ser usado pelo <i>suprocess.cpp</i>

### KDocTools

- Atualização de modelos Docbook do KF5

### KGlobalAccel

- A API de execução privada é instalada para permitir ao KWin fornecer plugins para o Wayland.
- Contingência na resolução de nomes do <i>componentFriendlyForAction</i>

### KIconThemes

- Não tentar pintar o ícone se o tamanho for inválido

### KItemModels

- Novo modelo de <i>proxy</i>: KRearrangeColumnsProxyModel. Suporte a reordenação e ocultação de colunas no modelo de origem.

### KNotification

- Correção dos tipos de imagens no org.kde.StatusNotifierItem.xml
- [ksni] Adição de método para obter a ação pelo seu nome (erro <a href='https://bugs.kde.org/show_bug.cgi?id=349513'>349513</a>)

### KPeople

- Implementação das funcionalidades de filtragem do <i>PersonsModel</i>

### KPlotting

- KPlotWidget: Adição do <i>setAutoDeletePlotObjects</i>, correção de esvaziamento de memória no <i>replacePlotObject</i>
- Correção das marcas de seleção ausentes quando o x0 &gt; 0.
- KPlotWidget: Não é necessário o <i>setMinimumSize</i> ou o <i>resize</i>.

### KTextEditor

- debianchangelog.xml: Adição do Debian/Stretch, Debian/Buster, Ubuntu-Wily
- Correção do par substituo do UTF-16 para o comportamento do Backspace/Delete.
- Permitir ao QScrollBar lidar com WheelEvents (erro <a href='https://bugs.kde.org/show_bug.cgi?id=340936'>340936</a>)
- Aplicação da <i>correção</i> do desenvolvedor do KWrite para o realce básico e puro - "Alexander Clay" &lt;tuireann@EpicBasic.org&gt;"

### KTextWidgets

- Correção da ativação/desativação do botão OK

### KWallet Framework

- Importação e melhoria da ferramenta de linha de comando <i>kwallet-query</i>.
- Suporte para a sobreposição de itens dos mapas.

### KXMLGUI

- Não mostrar a "Versão do KDE Frameworks" na caixa de diálogo "Sobre o KDE"

### Plasma Framework

- O tema escuro e o grupo complementar tornaram-se completamente escuros
- Cache do <i>naturalsize</i> em separado por fator de escala
- ContainmentView: Não causar falha com metadados inválidos do Corona
- AppletQuickItem: Não acessar ao KPluginInfo se não for válido
- Correção das páginas ocasionais de configuração vazias (erro <a href='https://bugs.kde.org/show_bug.cgi?id=349250'>349250</a>)
- Melhoria do suporte de DPIs altos (hidipi) no componente da grade do Calendário
- Verificação se o KService tem informações válidas do plugin antes de usá-lo
- [calendário] Certificação de que a grade é atualizada nas alterações de tema
- [calendário] Sempre começar a contagem de semanas a partir da segunda-feira (erro <a href='https://bugs.kde.org/show_bug.cgi?id=349044'>349044</a>)
- [calendário] Atualização da grade ao alterar a opção para mostrar os números da semana
- Agora é usado um tema opaco quando apenas o efeito de borrão estiver disponível (erro <a href='https://bugs.kde.org/show_bug.cgi?id=348154'>348154</a>)
- Lista de permissões das versões/miniaplicativos para um mecanismo separado
- Introdução de uma nova classe <i>ContainmentView</i>

### Sonnet

- Permissão para usar o realce da verificação ortográfica em um QPainTextEdit

Você pode discutir e compartilhar ideias sobre esta versão na seção de comentários do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
