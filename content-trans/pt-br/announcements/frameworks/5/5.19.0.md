---
aliases:
- ../../kde-frameworks-5.19.0
date: 2016-02-13
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Attica

- Simplificação da pesquisa e inicialização do plugin <b>attica</b>

### Ícones Breeze

- Muitos ícones novos
- Adição dos ícones de tipos MIME ausentes do conjunto de ícones Oxygen

### Módulos extra do CMake

- ECMAddAppIcon: Uso de caminhos completos ao lidar com ícones
- Certeza de que o prefixo é pesquisado no Android
- Adição de um módulo FindPoppler
- Uso dos PATH_SUFFIXES no ecm_find_package_handle_library_components()

### KActivities

- Não chamar o exec() a partir do QML (erro <a href='https://bugs.kde.org/show_bug.cgi?id=357435'>357435</a>)
- A biblioteca KActivitiesStats está agora em repositório separado

### KAuth

- Execução também do preAuthAction nos Backends com AuthorizeFromHelperCapability
- Correção do nome do serviço D-Bus do agente <i>polkit</i>

### KCMUtils

- Correção de problema com o HiDPI no KCMUtils

### KCompletion

- O método KLineEdit::setUrlDropsEnabled não pode ser marcado como obsoleto

### KConfigWidgets

- Adição de um esquema de cores "Complementar" ao <i>kcolorscheme</i>

### KCrash

- Atualização da documentação do KCrash::initialize. Os programadores de aplicativos são encorajados a chamá-la de forma explícita.

### KDeclarative

- Limpeza das dependências do KDeclarative/QuickAddons
- [KWindowSystemProxy] Adição de método de modificação do <i>showingDesktop</i>
- DropArea: Correção da omissão do evento <i>dragEnter</i> com o <i>preventStealing</i>
- DragArea: Implementação de um item delegado de captura
- DragDropEvent: Adição da função ignore()

### KDED

- Reversão do <i>hack</i> BlockingQueuedConnection, porque o Qt 5.6 terá uma correção melhor
- Fazer com que o <i>kded</i> se registre com nomes alternativos definidos pelos módulos do <i>kded</i>

### KDELibs 4 Support

- O <i>kdelibs4support</i> requer o <i>kded</i> (para o <i>kdedmodule.desktop</i>)

### KFileMetaData

- Permissão para pesquisa da URL de origem de um arquivo

### KGlobalAccel

- Evitar uma falha no caso de o D-Bus não estar disponível

### Complementos da interface KDE

- Correção da listagem das paletas disponíveis na caixa de diálogo de cores

### KHTML

- Correção da detecção do tipo de link do ícone ("favicon")

### KI18n

- Redução do uso da API do Gettext

### KImageFormats

- Adição dos plugins de E/S de imagens <b>kra</b> e <b>ora</b> (somente leitura)

### KInit

- Ignorar a área de visualização da área de trabalho atual na informação de inicialização
- Migração do <i>klauncher</i> para o <i>xcb</i>
- Uso do <i>xcb</i> na interação com o KStartupInfo

### KIO

- Nova classe FavIconRequestJob na nova biblioteca KIOGui, para a obtenção de <i>favicons</i>
- Correção da falha do KDirListerCache com duas listagens para uma pasta vazia no cache (erro <a href='https://bugs.kde.org/show_bug.cgi?id=278431'>278431</a>)
- Mudança da implementação em Windows do KIO::stat para o protocolo file:/ apresentar um erro se o arquivo não existir
- Não assumir que os arquivos somente para leitura não podem ser excluídos no Windows
- Correção do arquivo <i>.pri</i> do KIOWidgets: Depende do KIOCore, não de si próprio
- Correção do autocarregamento do <i>kcookiejar</i>, uma vez que os valores foram trocados na <i>correção</i> 6db255388532a4
- Permissão para acesso do <i>kcookiejar</i> no nome de serviço <i>dbus</i> <b>org.kde.kcookiejar5</b>
- kssld: Instalação do arquivo do serviço D-Bus para o <b>org.kde.kssld5</b>
- Disponibilização de um arquivo de serviço D-Bus para o <b>org.kde.kpasswdserver</b>
- [kio_ftp] Correção da apresentação da data/hora de modificação de arquivos/pastas (erro <a href='https://bugs.kde.org/show_bug.cgi?id=354597'>354597</a>)
- [kio_help] Correção do envio de lixo ao servir arquivos estáticos
- [kio_http] Tentativa da autenticação NTLMv2 se o servidor recusar o NTLMv1
- [kio_http] Correção de erros de migração que danificaram o cache
- [kio_http] Correção da criação da resposta de nível 3 do NTLMv2
- [kio_http] Correção da espera até a limpeza de cache atender no socket
- kio_http_cache_cleaner: não sair na inicialização se a pasta de cache ainda não existir
- Alteração do nome D-Bus do kio_http_cache_cleaner para que não saia se o <i>kde4</i> estiver em execução

### KItemModels

- KRecursiveFilterProxyModel::match: Correção de falha

### KJobWidgets

- Correção de falhas nas caixas de diálogo do KJob (erro <a href='https://bugs.kde.org/show_bug.cgi?id=346215'>346215</a>)

### Package Framework

- Evitar a procura do mesmo pacote várias vezes a partir de locais diferentes

### KParts

- PartManager: Parar de seguir um widget, mesmo que já não esteja mais no nível superior (erro <a href='https://bugs.kde.org/show_bug.cgi?id=355711'>355711</a>)

### KTextEditor

- Melhor comportamento na funcionalidade de "inserção de chaves automática"
- Alteração da chave de opção para forçar uma nova padrão. Nova linha no se o fim de arquivo = verdadeiro
- Remoção de algumas chamadas suspeitas ao <i>setUpdatesEnabled</i> (erro <a href='https://bugs.kde.org/show_bug.cgi?id=353088'>353088</a>)
- Atraso na emissão do <i>verticalScrollPositionChanged</i> até que tudo esteja consistente na dobragem (erro <a href='https://bugs.kde.org/show_bug.cgi?id=342512'>342512</a>)
- Correção da atualização da substituição de tags (erro <a href='https://bugs.kde.org/show_bug.cgi?id=330634'>330634</a>)
- Atualizar a paleta apenas uma vez para o evento de alteração que pertence ao <i>qApp</i> (erro <a href='https://bugs.kde.org/show_bug.cgi?id=358526'>358526</a>)
- Adição de novas linhas no EOF por padrão
- Adição do arquivo de realce de sintaxe para o NSIS

### KWallet Framework

- Duplicação do descritor de arquivos ao abrir o arquivo para ler o <i>env</i>

### KWidgetsAddons

- Correção dos widgets dependentes que lidam com o KFontRequester
- KNewPasswordDialog: uso do KMessageWidget
- Proteção contra "falha ao sair" no KSelectAction::~KSelectAction

### KWindowSystem

- Alteração do cabeçalho da licença de "Library GPL 2 or later" para "Lesser GPL 2.1 or later"
- Correção de falha se o KWindowSystem::mapViewport for chamado sem um <i>QCoreApplication</i>
- Cache do QX11Info::appRootWindow no <i>eventFilter</i> (erro <a href='https://bugs.kde.org/show_bug.cgi?id=356479'>356479</a>)
- Remoção da dependência do QApplication (erro <a href='https://bugs.kde.org/show_bug.cgi?id=354811'>354811</a>)

### KXMLGUI

- Adição de opção para desativar o KGlobalAccel durante a compilação
- Reparação do esquema de atalhos do caminho para o aplicativo
- Correção da listagem de arquivos de atalhos (uso incorreto do QDir)

### NetworkManagerQt

- Nova verificação do estado da conexão e de outras propriedades para se certificar que estão atualizadas (versão 2) (erro <a href='https://bugs.kde.org/show_bug.cgi?id=352326'>352326</a>)

### Ícones do Oxygen

- Remoção de arquivos com ligações incorretas
- Adição de ícones de aplicativos do KDE Applications
- Adição de ícones de locais do Breeze no Oxygen
- Sincronização dos ícones de tipos MIME do Oxygen com o Breeze

### Plasma Framework

- Adição de uma propriedade <i>separatorVisible</i>
- Remoção mais explícita do <i>m_appletInterfaces</i> (erro <a href='https://bugs.kde.org/show_bug.cgi?id=3358551'>358551</a>)
- Uso do <i>complementaryColorScheme</i> do KColorScheme
- AppletQuickItem: Não tentar definir um tamanho inicial maior que o tamanho do item-pai (erro <a href='https://bugs.kde.org/show_bug.cgi?id=358200'>358200</a>)
- IconItem: Adição da propriedade <i>usesPlasmaTheme</i>
- Não carregar a área de ferramentas nos tipos diferentes de 'desktop' ou 'panel'
- IconItem: Tentar o carregamento dos ícones do QIcon::fromTheme como SVG (erro <a href='https://bugs.kde.org/show_bug.cgi?id=353358'>353358</a>)
- Ignorar a verificação, caso apenas uma parte do arquivo seja zero no <i>compactRepresentationCheck</i> (erro <a href='https://bugs.kde.org/show_bug.cgi?id=358039'>358039</a>)
- [Unidades] Devolver pelo menos 1ms nas durações (erro <a href='https://bugs.kde.org/show_bug.cgi?id=357532'>357532</a>)
- Adição do clearActions() para remover todas as ações de interface dos miniaplicativos
- [plasmaquick/dialog] Não usar o KWindowEffects para o tipo de janela <b>Notification</b>
- O Applet::loadPlasmoid() tornou-se obsoleto
- [PlasmaCore DataModel] Não reiniciar o modelo quando é removida uma origem
- Correção das sugestões de margem nos SVGs de fundo dos painéis opacos
- IconItem: Adição da propriedade <i>animated</i>
- [Unity] Escala do tamanho de ícones do ambiente de trabalho
- O botão é composto-nos-contornos
- paintedWidth/paintedheight no IconItem

Você pode discutir e compartilhar ideias sobre esta versão na seção de comentários do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
