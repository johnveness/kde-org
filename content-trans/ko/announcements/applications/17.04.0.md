---
aliases:
- ../announce-applications-17.04.0
changelog: true
date: 2017-04-20
description: KDE에서 KDE 프로그램 17.04.0 출시
layout: application
title: KDE에서 KDE 프로그램 17.04.0 출시
version: 17.04.0
---
2017년 4월 20일. KDE 프로그램 17.04를 출시했습니다. 프로그램과 기반 라이브러리 모두를 보다 안정적이고 사용하기 쉽도록 개선했습니다. 여러 사소한 곳을 수정하고 피드백을 반영하여 KDE 프로그램 제품군의 결함을 줄이고 더 친숙하게 만들었습니다.

새 앱을 즐겁게 사용하십시오!

#### <a href="https://edu.kde.org/kalgebra/">KAlgebra</a>

{{<figure src="/announcements/applications/17.04.0/kalgebra1704.jpg" width="600px" >}}

The developers of KAlgebra are on their own particular road to convergence, having ported the mobile version of the comprehensive educational program to Kirigami 2.0 -- the preferred framework for integrating KDE applications on desktop and mobile platforms.

Furthermore, the desktop version has also migrated the 3D back-end to GLES, the software that allows the program to render 3D functions both on the desktop and on mobile devices. This makes the code simpler and easier to maintain.

#### <a href="http://kdenlive.org/">Kdenlive</a>

{{<figure src="/announcements/applications/17.04.0/kdenlive1704.png" width="600px" >}}

KDE's video editor is becoming more stable and more full-featured with every new version. This time, the developers have redesigned the profile selection dialog to make it easier to set screen size, framerate, and other parameters of your film.

Now you can also play your video directly from the notification when rendering is finished. Some crashes that happened when moving clips around on the timeline have been corrected, and the DVD Wizard has been improved.

#### <a href="https://userbase.kde.org/Dolphin">Dolphin</a>

{{<figure src="/announcements/applications/17.04.0/dolphin1704.png" width="600px" >}}

Our favorite file explorer and portal to everything (except maybe the underworld) has had several makeovers and usability improvements to make it even more powerful.

The context menus in the <i>Places</i> panel (by default on the left of the main viewing area) have been cleaned up, and it is now possible to interact with the metadata widgets in the tooltips. The tooltips, by the way, now also work on Wayland.

#### <a href="https://www.kde.org/applications/utilities/ark/">Ark</a>

{{<figure src="/announcements/applications/17.04.0/ark1704.png" width="600px" >}}

The popular graphical app for creating, decompressing and managing compressed archives for files and folders now comes with a <i>Search</i> function to help you find files in crowded archives.

It also allows you to enable and disable plugins directly from the <i>Configure</i> dialog. Talking of plugins, the new Libzip plugin improves Zip archive support.

#### <a href="https://minuet.kde.org/">Minuet</a>

{{<figure src="/announcements/applications/17.04.0/minuet1704.png" width="600px" >}}

If you are teaching or learning to play music, you have to check out Minuet. The new version offers more scales exercises and ear-training tasks for bebop, harmonic minor/major, pentatonic, and symmetric scales.

You can also set or take tests using the new <i>Test mode</i> for answering exercises. You can monitor your progress by running a sequence of 10 exercises and you'll get hit ratio statistics when finished.

### 그리고 더 있습니다!

<a href='https://okular.kde.org/'>Okular</a>, KDE's document viewer, has had at least half a dozen changes that add features and crank up its usability on touchscreens. <a href='https://userbase.kde.org/Akonadi'>Akonadi</a> and several other apps that make up <a href='https://www.kde.org/applications/office/kontact/'>Kontact</a> (KDE's email/calendar/groupware suite) have been revised, debugged and optimized to help you become more productive.

<a href='https://www.kde.org/applications/games/kajongg'>Kajongg</a>, <a href='https://www.kde.org/applications/development/kcachegrind/'>KCachegrind</a> and more (<a href='https://community.kde.org/Applications/17.04_Release_Notes#Tarballs_that_were_based_on_kdelibs4_and_are_now_KF5_based'>Release Notes</a>) have now been ported to KDE Frameworks 5. We look forward to your feedback and insight into the newest features introduced with this release.

<a href='https://userbase.kde.org/K3b'>K3b</a> has joined the KDE Applications release.

### 버그 수정

Kopete, KWalletManager, Marble, Spectacle 등 프로그램에서 95개 이상의 버그가 수정되었습니다!

### 전체 변경 기록
