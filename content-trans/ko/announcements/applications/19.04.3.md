---
aliases:
- ../announce-applications-19.04.3
changelog: true
date: 2019-07-11
description: KDE Ships Applications 19.04.3.
layout: applications
major_version: '19.04'
release: applications-19.04.3
title: KDE에서 KDE 프로그램 19.04.3 출시
version: 19.04.3
---
{{% i18n_date %}}

Today KDE released the third stability update for <a href='../19.04.0'>KDE Applications 19.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Kontact, Ark, Cantor, JuK, K3b, Kdenlive, KTouch, Okular, Umbrello 등에 60개 이상의 버그 수정 및 기능 개선이 있었습니다.

개선 사항:

- QtWebEngine 5.13 사용 시 Konqueror와 Kontact가 더 이상 끝낼 때 충돌하지 않음
- 합성이 있는 그룹을 자를 때 Kdenlive가 더 이상 충돌하지 않음
- Umbrello UML 디자이너의 파이썬 가져오기 도구에서 기본값이 있는 인자를 올바르게 처리함
