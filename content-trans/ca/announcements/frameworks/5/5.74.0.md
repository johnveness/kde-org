---
aliases:
- ../../kde-frameworks-5.74.0
date: 2020-09-06
layout: framework
libCount: 70
---
### Attica

+ Usa Q_DECLARE_OPERATORS_FOR_FLAGS al mateix espai de noms que la definició dels indicadors

### Baloo

+ Canvia la llicència de molts fitxers a la LGPL-2.0 o posterior
+ Usa el codi comú de creació UDS també per a les etiquetes (error 419429)
+ Desfactoritza el codi comú de creació UDS dels processos de treball KIO
+ [balooctl] Mostra els formats permesos al text d'ajuda
+ [balooctl] Mostra el fitxer actual a la sortida d'estat en indexar
+ [balooctl] Estableix el mode del QDBusServiceWatcher a partir del constructor
+ [balooctl] Neteja del format
+ Usa Q_DECLARE_OPERATORS_FOR_FLAGS al mateix espai de noms que la definició dels indicadors
+ [OrPostingIterator] No avança quan l'ID demanat és inferior a l'actual
+ [Extractor] Elimina la dependència de QWidgets de l'ajudant de l'extractor
+ [Extractor] Elimina KAboutData de l'executable de l'ajudant de l'extractor
+ Actualitza diverses referències al README
+ [FileContentIndexer] Elimina un argument i un membre sense ús del constructor de la configuració
+ [balooctl] Corregeix l'avís d'obsolescència de QProcess::start, proporciona una llista buida d'arguments
+ [Engine] Propaga els errors de les transaccions (error 425017)
+ [Engine] Elimina el mètode «hasChanges» no usat de «{Write}Transaction»
+ No indexa els fitxers «.ytdl» (error 424925)

### Icones Brisa

+ Fa que la icona «keepassxc» sigui més fidel a l'oficial
+ Afegeix més enllaços simbòlics als noms nous de les icones de la safata del sistema del «keepassxc» (error 425928)
+ Afegeix un altre àlies per a la icona «keepass» (error 425928)
+ Afegeix una icona per al tipus MIME del projecte Godot
+ Afegeix una icona per a l'instal·lador de l'Anaconda
+ Afegeix icones de llocs de 96px
+ Elimina llocs sense ús a comunicar
+ Executa «application-x-bzip-compressed-tar» a través de <code>scour</code> (error 425089)
+ Fa un enllaç simbòlic d'«application-gzip» a «application-x-gzip» (error 425059)
+ Afegeix àlies per a les icones MP3 (error 425059)

### Mòduls extres del CMake

+ Elimina els zeros inicials dels números de versió numèrica al codi del C++
+ Afegeix un temps d'espera per a les crides «qmlplugindump»
+ Afegeix un mòdul de cerca de WaylandProtocols
+ Invoca «update-mime-database» amb -n

### Integració del marc de treball

+ Clarifica la declaració de llicència d'acord amb l'historial de KDElibs

### KActivitiesStats

+ Usa Boost::boost per a les versions més antigues del CMake

### KActivities

+ Elimina l'X-KDE-PluginInfo-Depends buit

### Eines de Doxygen del KDE

+ Documenta les dependències a «requirements.txt» i les instal·la a «setup.py»
+ Acceptació de la visualització de la llicència de biblioteca

### KAuth

+ Usa Q_DECLARE_OPERATORS_FOR_FLAGS al mateix espai de noms que la definició dels indicadors

### KBookmarks

+ KBookmarkManager: neteja l'arbre de la memòria quan se suprimeix el fitxer

### KCalendarCore

+ Emmagatzema sempre les propietats X-KDE-VOLATILE-XXX com a volàtils
+ Documenta les transicions esperades de la TZ a Praga

### KCMUtils

+ KCModuleData: corregeix les capçaleres, millora la documentació de l'API i reanomena el mètode
+ Amplia el KCModuleData amb les funcionalitats «revertToDefaults» i «matchQuery»
+ Intenta evitar la barra de desplaçament horitzontal a KCMultiDialog
+ Afegeix KCModuleData com a classe base per al connector
+ Permet que s'afegeixi un botó extra al KPluginSelector (error 315829)

### KConfig

+ Fa KWindowConfig::allConnectedScreens() estàtica i interna (error 425953)
+ Afegeix una drecera estàndard per a «Crea una carpeta»
+ Presenta un mètode per a consultar el valor predeterminat de KConfigSkeletonItem
+ Recorda les mides de les finestres segons un «per-screen-arrangement»
+ Extreu el codi per aconseguir la llista de les pantalles connectades en una funció reutilitzable
+ Afegeix funcions per a desar i restaurar les posicions de les finestres a les plataformes no Wayland (error 415150)

### KConfigWidgets

+ Evita canviar els valors predeterminats en llegir el de KConfigSkeletonItem
+ Corregeix KLanguageName::nameForCodeInLocale amb els codis desconeguts a QLocale
+ KLanguageName::allLanguageCodes: té en compte que pot haver-hi més d'un directori de configuració regional
+ KConfigDialog: intenta evitar les barres de desplaçament horitzontal
+ Funció que retorna la llista de codis d'idioma

### KContacts

+ Addressee::parseEmailAddress(): comprova la longitud del nom complet abans de retallar

### KCoreAddons

+ Afegeix el patró global «*.kcrash» al tipus MIME d'informe del KCrash
+ Usa Q_DECLARE_OPERATORS_FOR_FLAGS al mateix espai de noms que la definició dels indicadors
+ No espera indefinidament els esdeveniments «fam» (error 423818)
+ [KFormat] Permet els valors de formatació a unitats binàries arbitràries
+ El fa possible al KPluginMetadata des del QML
+ [KFormat] Corregeix l'exemple binari

### KDAV

+ metainfo.yaml: afegeix una clau de nivell i una descripció ajustada

### KDeclarative

+ [KeySequenceItem] Fa que funcionin les dreceres Meta+Maj+num
+ Exposa la propietat «checkForConflictsAgainst»
+ Afegeix una classe AbstractKCM nova
+ Adapta KRunProxy per a eliminar KRun

### KDED

+ org.kde.kded5.desktop: afegeix la clau requerida «Name» que manca (error 408802)

### KDocTools

+ Actualitza «contributor.entities»
+ Usa marcadors de posició

### KEmoticons

+ Recupera la informació de llicència d'EmojiOne

### KFileMetaData

+ Converteix els finals antics de línia del Mac a les etiquetes de lletres (error 425563)
+ Adapta el mòdul nou FindTaglib a l'ECM

### KGlobalAccel

+ Carrega fitxers de servei per a dreceres de directoris de dades d'aplicació com a reserva (error 421329)

### KI18n

+ Corregeix una possible condició de carrera amb definicions «i18n»

### KIO

+ StatJob: fa que «mostLocalUr» només funcioni amb «protoclClass == :local»
+ KPropertiesDialog: també carrega els connectors amb les metadades JSON
+ Reverteix «[KUrlCompletion]: no annexa / a carpetes completades» (error 425387)
+ Simplifica el constructor KProcessRunner
+ Permet les paraules clau CCBUG i FEATURE a bugs.kde.org (error )
+ Actualitza el text d'ajuda per a editar l'ordre de l'aplicació a l'entrada del «.desktop» per a complir amb l'especificació actual (error 425145)
+ KFileFilterCombo: no afegeix l'opció «allTypes» si només hi ha 1 element
+ Usa Q_DECLARE_OPERATORS_FOR_FLAGS al mateix espai de noms que la definició dels indicadors
+ Corregeix una fallada potencial en suprimir un menú al gestor d'esdeveniments (error 402793)
+ [filewidgets] Corregeix el farciment del KUrlNavigatorButton al fil d'Ariadna (error 425570)
+ ApplicationLauncherJob: ajusta la documentació
+ Corregeix els elements no traduïts a «kfileplacesmodel»
+ ApplicationLauncherJob: corregeix una fallada si no s'ha definit el gestor «open-with»
+ Reanomena el KCM «Dreceres web» a «Paraules clau de cerca a la web»
+ KFileWidget: torna a analitzar la configuració per aconseguir directoris afegits per altres instàncies de l'aplicació (error 403524)
+ Mou el KCM de dreceres web a la categoria de cerca
+ Elimina automàticament els espais en blanc inicials en lloc de mostrar un avís
+ Evita que es tanquin els fills bifurcats de les aplicacions llançades pel «systemd» (error 425201)
+ smb: corregeix la comprovació de disponibilitat del nom de compartició
+ smb: manté la «stderr» de les ordres de xarxa en afegir/eliminar (error 334618)
+ smb: reajusta la comprovació de convidats permesos i ho publica com a «areGuestsAllowed»
+ KFileWidget: neteja els URL abans de reconstruir la llista
+ KFileWidget: elimina el quadre combinat del camí superior de l'URL predeterminat
+ KFilePlacesModel: afegeix els llocs predeterminats en actualitzar des d'una versió més antiga
+ Corregeix una regressió de fa 2 anys a KUrlComboBox, «setUrl()» ja no en va afegir més

### Kirigami

+ kirigami.pri: afegeix «managedtexturenode.cpp»
+ Fa que el posicionament personalitzat de «buddyFor» torni a funcionar
+ Té en compte la mida del botó Més si ja se sap que serà visible
+ Afegeix una propietat a ToolBarLayout per a controlar com tractar l'alçada de l'element (error 425675)
+ Fa que es puguin tornar a llegir les etiquetes de la casella de selecció
+ Implementa «displayComponent» per accions
+ Fa visibles els submenús
+ Corregeix la declaració «forward» de QSGMaterialType
+ Fa que la capçalera i el peu d'OverlaySheet usin els colors de fons apropiats
+ Comprova el valor de la variable d'entorn de baix consum, no només si s'ha establert
+ Afegeix la funció «setShader» a ShadowedRectangleShader per a simplificar definir «shaders»
+ Afegeix una versió de baix consum del fitxer de funcions «sdf»
+ Fa sobrecàrregues de «sdf_render» usant la funció «full-argument sdf_render»
+ Elimina la necessitat de definir perfils de nucli al «main» del «shader» de «shadowedrect»
+ Basa el rectangle intern en el rectangle extern per al «shadowedrectangle» amb vores
+ Usa els «shaders» correctes per a ShadowedTexture en usar un perfil del nucli
+ Afegeix versions de baix consum dels «shaders» «shadowedrectangle»
+ Inicialitza el membre «m_component» a PageRoute
+ Corregeix SwipeListItem de Material
+ Lògica nova per a eliminar les marques d'acceleració (error 420409)
+ Elimina «forceSoftwarerendering» per ara
+ Mostra l'element origen a la renderització per programari
+ Una reserva per programari per a la textura «shadowed»
+ Usa la propietat nova «showMenuArrow» al fons per a la fletxa de menú
+ No oculta la capçalera en passar-hi ràpidament
+ Mou ManagedTextureNode al seu propi fitxer
+ ToolBarLayout: afegeix espaiat a «visibleWidth» si s'està mostrant el botó Més
+ No sobreescriu la mida del píxel de Heading a BreadCrumbControl (error 404396)
+ Actualitzar la plantilla d'aplicació
+ [passivenotification] Estableix un farciment explícit (error 419391)
+ Activa el retallat a l'StackView del GlobalDrawer
+ Elimina l'opacitat del PrivateActionToolButton desactivat
+ Fa que ActionToolBar sigui un control
+ swipenavigator: afegeix un control sobre quines pàgines es mostren
+ Declara el tipus subjacent de l'enumeració de DisplayHint que sigui «uint»
+ Afegeix ToolBarLayout/ToolBarLayoutDelegate al fitxer «pri»
+ kirigami.pro: usa la llista de fitxers origen a partir del «kirigami.pri»
+ No suprimeix els incubadors a la crida de retorn de compleció
+ Assegura que «menuActions» roman com una matriu en lloc d'una llista de propietats
+ Afegeix el tipus de retorn al «lambda» «singleton» de DisplayHint
+ Té en compte l'alçada de l'element en centrar verticalment els delegats
+ Sempre posa a la cua una disposició nova, encara que s'estigui disposant en aquest moment
+ Refà la disposició d'InlineMessages usant àncores
+ Usa l'amplada total per comprovar si totes les accions encaixen
+ Afegeix un comentari quant a l'espaiat extra per a centrar
+ Corregeix la comprovació del text del consell d'eina a PrivateActionToolButton
+ Solució temporal perquè ToolButton de «qqc2-desktop-style» no respecta un IconOnly no pla
+ Prefereix reduir les accions KeepVisible abans que ocultar més tard les accions KeepVisible
+ Oculta les accions següents quan la primera acció esdevé oculta
+ Afegeix un senyal de notificació per a ToolBarLayout::actions i l'emet en el moment correcte
+ Exposa l'acció al separador d'ActionsMenu
+ Reanomena la propietat «kirigamiAction» de «loaderDelegate» d'ActionsMenu a «action»
+ Afegeix el «loaderDelegate» que manca per comprovar la visibilitat
+ Oculta els delegats d'acció fins que estan posicionats
+ ToolBarLayout: substitueix la càrrega poc estricta personalitzada dels delegats per QQmlIncubator
+ Mou «displayHintSet» a C++ de manera que es pot invocar des d'allí
+ Admet el mode dreta a esquerra a la ToolBarLayout
+ Afegeix la propietat «minimumWidth» a ToolBarLayout
+ S'ha refet PrivateActionToolButton per a millorar-ne el rendiment
+ Fa obsolet ActionToolBar::hiddenActions
+ Estableix els consells de la disposició per a ActionToolBar
+ Usa el DisplayHint no obsolet a la ToolBarPageHeader
+ Mostra els errors del component si els elements delegats fallen en fer la instància
+ Oculta els delegats de les accions a on s'han eliminat
+ Neteja els elements delegats complets/icones en destruir els delegats
+ Força la visibilitat dels elements delegats complets/icones
+ Usa la ToolBarLayout per a disposar l'ActionToolBar
+ Afegeix diversos comentaris a ToolBarLayout::maybeHideDelegate
+ Afegeix la propietat «visibleWidth» a la ToolBarLayout
+ Presenta l'objecte natiu ToolBarLayout
+ Mou DisplayHint des d'Action a un fitxer d'enumeració en C++
+ Afegeix icones usades a la pàgina «Quant a» a la macro d'empaquetament de la icona del CMake

### KNewStuff

+ Corregeix problemes de sincronització de la memòria cau amb el diàleg del QtQuick (error 417985)
+ Elimina l'X-KDE-PluginInfo-Depends buit
+ Elimina entrades si no ja no coincideixen amb cap filtre (error 425135)
+ Corregeix un cas extrem en què el KNS queda encallat (error 423055)
+ Fa menys fràgil la tasca del funcionament intern del «kpackage» (error 425811)
+ Elimina el fitxer baixat en usar la instal·lació del «kpackage»
+ Admet un estil diferent del «knsrc» del «kpackage» per a reserva
+ Gestiona la notació /* per RemoveDeadEntries (error 425704)
+ Fa més fàcil aconseguir la memòria cau per a un motor ja inicialitzat
+ Afegeix una cerca inversa de les entrades segons els seus fitxers instal·lats
+ Usa la notació /* per a la descompressió de subdirectoris i permet la descompressió de subdirectoris si el fitxer és un arxiu
+ Corregeix la inundació d'avisos «El mapa de píxels és un mapa de píxels nul»
+ Elimina les barres del nom d'entrada en interpretar-lo com un camí (error 417216)
+ Corregeix una fallada esporàdica amb la tasca KPackageJob (error 425245)
+ Usa el mateix indicador rotatiu per a carregar i inicialitzar (error 418031)
+ Elimina el botó de detalls (error 424895)
+ Executa asíncronament l'script de desinstal·lació (bug 418042)
+ Desactiva la cerca quan no està disponible
+ Oculta la càrrega de més indicadors rotatius en actualitzar/instal·lar (error 422047)
+ No afegeix el directori de destí de baixada als fitxers d'entrada
+ Elimina el caràcter * en passar un directori a l'script
+ No dona el focus al primer element al mode de vista d'icones (error 424894)
+ Evita l'inici innecessari del treball de desinstal·lació
+ Afegeix una opció RemoveDeadEntries per als fitxers «knsrc» (error 417985)
+ [QtQuick dialog] Corregeix la darrera instància de la icona incorrecta d'actualització
+ [QtQuick dialog] Usa icones de desinstal·lació més apropiades

### Framework del KPackage

+ No suprimeix l'arrel del paquet si el paquet s'ha suprimit (error 410682)

### KQuickCharts

+ Afegeix una propietat «fillColorSource» als diagrames de línies
+ Càlcul suavitzat segons la mida de l'element i la relació de píxels del dispositiu
+ Usa la mitjana de la mida en lloc de la màxima per a determinar la suavitat de la línia
+ Basa la quantitat de suavitzat dels diagrames de línies en la mida del diagrama

### KRunner

+ Afegeix una plantilla per a l'executor en Python
+ Actualitza la plantilla per a la compatibilitat de la KDE Store i millora el README
+ Afegeix la implementació per a les sintaxis d'executors als executors del DBus
+ Desa RunnerContext després de cada sessió coincident (error 424505)

### KService

+ Implementa «invokeTerminal» al Windows amb «workdir», «command» i «envs»
+ Corregeix la preferència d'aplicació ordenant per tipus MIME amb herència múltiple (error 425154)
+ Empaqueta el tipus de servei Application en un fitxer «qrc»
+ Expandeix el caràcter titlla en llegir el directori de treball (error 424974)

### KTextEditor

+ Vimode: fa més accessible directament el registre de supressió petita (-)
+ Vimode: còpia el comportament dels registres numerats del Vim
+ Vimode: simplifica la implementació d'afegir-copiar
+ Fa la cerca «cerca per selecció» si no hi ha cap selecció
+ El mode multilínia només té sentit per a les «regex» multilínia
+ Afegeix un comentari quant a la comprovació de «pattern.isEmpty()»
+ Adapta la interfície de cerca QRegExp a QRegularExpression
+ Vimode: implementa afegir-copiar
+ Accelera *molt* la càrrega de fitxes grans
+ Només mostra el nivell de zoom quan no és el 100%
+ Afegeix un indicador de zoom a la barra d'estat
+ Afegeix una opció separada de configuració per a la vista prèvia de concordança de parèntesis
+ Mostra una vista prèvia de la línia de parèntesi concordant obert
+ Permet més control sobre els models de compleció quan no s'usa la invocació automàtica

### Framework del KWallet

+ Evita el conflicte amb una macro a «ctype.h» de l'OpenBSD

### KWidgetsAddons

+ Afegeix KRecentFilesMenu per a substituir KRecentFileAction

### KWindowSystem

+ Instal·la els connectors de plataforma en un directori sense punt al nom de fitxer (error 425652)
+ [xcb] Escala correctament la geometria de les icones a tot arreu

### KXMLGUI

+ Permet donar de baixa el recordatori de les posicions de les finestres a l'X11 (error 415150)
+ Desa i restaura la posició de la finestra principal (error 415150)

### Frameworks del Plasma

+ [PC3/BusyIndicator] Evita executar una animació invisible
+ No usa «highlightedTextColor» per als TabButtons
+ Elimina Layout.minimumWidth de Button i ToolButton
+ Usa la propietat «spacing» per a l'espaiat entre les icones Button/ToolButton i les etiquetes
+ Afegeix «private/ButtonContent.qml» per als PC3 Buttons i ToolButtons
+ Canvia la «implicitWidth» i «implicitHeight» del PC3 Button per a tenir en compte els valors inserits
+ Afegeix «implicitWidth» i «implicitHeight» a ButtonBackground
+ Corregeix un valor predeterminat incorrecte de PlasmaExtras.ListItem (error 425769)
+ No permet que el fons sigui més petit que el «svg» (error 424448)
+ Usa Q_DECLARE_OPERATORS_FOR_FLAGS al mateix espai de noms que la definició dels indicadors
+ Fa que els elements visuals de PC3 BusyIndicator mantinguin una relació d'aspecte d'1:1 (error 425504)
+ Usa ButtonFocus i ButtonHover en el PC3 ComboBox
+ Usa ButtonFocus i ButtonHover en el PC3 RoundButton
+ Usa ButtonFocus i ButtonHover en el PC3 CheckIndicator
+ Unifica el comportament pla/normal dels PC3 Buttons/ToolButtons (error 425174)
+ Fa que Heading usi la PC3 Label
+ [PlasmaComponents3] Fa que el text de la casella de selecció ompli la seva disposició
+ Dona al PC2 control lliscant una «implicitWidth» i «implicitHeight»
+ Còpia fitxers en lloc d'enllaços simbòlics trencats
+ Corregeix els marges de «toolbutton-hover» a «button.svg» (error 425255)
+ [PlasmaComponents3] Refactorització molt petita del codi d'ombra de ToolButton
+ [PlasmaComponents3] Corregeix la condició invertida per a l'ombra del ToolButton pla
+ [PlasmaComponents3] Treu els ampersands mnemotècnics del text de consell d'eina
+ Només dibuixa l'indicador de focus quan s'aconsegueix el focus a través del teclat (error 424446)
+ Elimina la mida mínima implícita dels PC2 i PC3 Buttons
+ Afegeix un equivalent PC3 a PC2 ListItem
+ Corregeix el «svg» de la barra d'eines
+ [PC3] Fa la ToolBar més alineada amb el «qqc2-desktop-style»
+ Exposa les metadades de la miniaplicació a AppletInterface
+ No trunca el DPR a un enter a la ID de la memòria cau
+ Afegeix la propietat «timeout» a ToolTipArea
+ Estableix el tipus a Dialog als indicadors si el tipus és Dialog::Normal

### Purpose

+ Aplica les dades de configuració inicial en carregar la IU de configuració
+ Restaura el comportament d'AlternativesView
+ [jobcontroller] Desactiva el procés separat
+ Refà la gestió de la vista de treballs (error 419170)

### QQC2StyleBridge

+ Corregeix les seqüències de la drecera StandardKey al MenuItem que es mostren com a números
+ No usa l'alçada/amplada del pare per la mida implícita de ToolSeparator (error 425949)
+ Només usa l'estil «focus» en prémer els botons d'eines no plans
+ Afegeix un farciment a la part superior i inferior de ToolSeparator
+ Fa que ToolSeparator respecti els valors de «topPadding» i «bottomPadding»
+ Fa que MenuSeparator usi l'alçada calculada del fons, no la «implicitHeight»
+ Corregeix els botons d'eina amb menús en usar el Breeze més nou
+ Dibuixa el control complet de la casella de selecció a través del QStyle

### Solid

+ Afegeix el mètode estàtic «storageAccessFromPath», necessari per https://phabricator.kde.org/D28745

### Sindicació

+ Corregeix l'excepció de la llicència

### Ressaltat de la sintaxi

+ Converteix tots els temes a les claus noves per als colors de l'editor
+ Canvia el format JSON del tema, usa metanoms d'enumeració d'objecte per als colors de l'editor
+ Comprova la «kateversion» &gt;= 5.62 per a «fallthroughContext» sense «fallthrough»=«true» i usa «attrToBool» per a atribut booleà
+ Afegeix la definició de sintaxi per al «todo.txt»
+ Corregeix «matchEscapedChar()»: s'ignora el darrer caràcter d'una línia
+ Corregeix «isDigit()», «isOctalChar()» i «isHexChar()»: només coincidiran els caràcters ASCII
+ Genera una descripció general dels temes
+ Afegeix les metadades del tema a la capçalera de les pàgines HTML que es generen
+ Comença a generar la pàgina de col·lecció de temes
+ Reanomena «Default» a «Breeze Light»
+ Varnish, Vala i TADS3: usa l'estil de color predeterminat
+ Millora el tema de color del Vim Dark
+ Afegeix el tema de color Vim Dark
+ Afegeix fitxers de tema de ressaltat de sintaxi al ressaltat del «json»
+ Ruby/Rails/RHTML: afegeix «spellChecking» a «itemDatas»
+ Ruby/Rails/RHTML: usa l'estil de color predeterminat i altres millores
+ LDIF, VHDL, D, Clojure i ANS-Forth94: usa l'estil de color predeterminat
+ ASP: usa l'estil de color predeterminat i altres millores
+ Permet que l'«Objective-C» guanyi els fitxers «.m»
+ «.mm» és més semblant a l'Objective-C++ que al «meta math»
+ Usa «notAsciiDelimiters» només amb un caràcter no ASCII
+ Optimitza «isWordDelimiter(c)» amb un caràcter ASCII
+ Optimitza Context::load
+ Usa «std::make_shared» que elimina l'assignació al bloc de control
+ Afegeix la llicència apropiada per actualitzar «scripty»
+ Mou l'script d'actualització del «kate-editor.org/syntax» al repositori «syntax»
+ SELinux CIL i Scheme: actualitza els colors dels parèntesis per als temes foscos
+ POV-Ray: usa l'estil de color predeterminat
+ Usa l'script NSIS instal·lador del Krita com a entrada d'exemple, agafat del «krita.git»
+ Afegeix un consell a l'script d'actualització per al lloc web
+ Afegeix un exemple mínim de plantilla Django
+ Actualitza les «refs» després dels darrers canvis del «hl»
+ CMake: corregeix els colors il·legibles en temes foscos i altres millores
+ Afegeix exemples de «pipe» i «ggplot2»
+ Corregeix un error de nom per al «hl» del Varnish
+ Usa el ressaltat correcte per al 68k ASM: Motorola 68k (VASM/Devpac)
+ Corregeix l'XML per a ser vàlid respecte a l'XSD
+ BrightScript: Afegeix una sintaxi d'excepció
+ Millora els comentaris a diverses definicions de sintaxi (part 3)
+ R Script: usa l'estil de color predeterminat i altres millores
+ PicAsm: corregeix el ressaltat de paraules clau desconegudes del preprocessador
+ Millora els comentaris a diverses definicions de sintaxi (part 2)
+ Modelines: elimina les regles LineContinue
+ Correcció ràpida de fitxer «hl» trencat
+ Optimització: comprova si està en un comentari abans d'usar ##Doxygen que conté diverses RegExpr
+ Llenguatges assembladors: diverses correccions i més ressaltat de context
+ ColdFusion: usa l'estil de color predeterminat i substitueix diverses regles de RegExpr
+ Millora els comentaris a diverses definicions de sintaxi (part 1)
+ Usa claus angulars per a la informació de context
+ Reverteix l'eliminació de «byte-order-mark»
+ XSLT: canvia el color de les etiquetes XSLT
+ Ruby, Perl, QML, VRML i XSLT: usa l'estil de color predeterminat i millora els comentaris
+ txt2tags: millores i correccions, usa l'estil de color predeterminat
+ Corregeix errors i afegeix «fallthroughContext=AttrNormal» per a cada context d'XML diferent perquè totes les regles contenen «column=0»
+ Corregeix problemes trobats pel verificador estàtic
+ Importa el ressaltat del Pure des de https://github.com/jgm/skylighting/blob/master/skylighting-core/xml/pure.xml
+ Corregeix l'atribut de «kateversion»
+ Fa que l'ordre de cerca del ressaltat sigui independent de les traduccions
+ Corregeix el format de la versió + problemes d'espaiat
+ Importa el ressaltat del Modula-3 des de https://github.com/jgm/skylighting/blob/master/skylighting-core/xml/modula-3.xml
+ Corregeix el format de la versió
+ Corregeix els problemes d'espaiat trobats pel verificador estàtic
+ Importa el ressaltat del LLVM des de https://github.com/jgm/skylighting/blob/master/skylighting-core/xml/llvm.xml
+ Corregeix els problemes d'espaiat trobats pel verificador estàtic
+ Importa el ressaltat de l'Idris des de https://github.com/jgm/skylighting/blob/master/skylighting-core/xml/idris.xml
+ Corregeix faltes trobades pel verificador estàtic
+ Importa el ressaltat de l'ATS des de https://github.com/jgm/skylighting/blob/master/skylighting-core/xml/ats.xml
+ Només desacobla per a les dades creades no recents
+ Crea StateData a demanda

### Informació de seguretat

El codi publicat s'ha signat amb GPG usant la clau següent: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Empremta digital de la clau primària: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
