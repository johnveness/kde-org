---
aliases:
- ../../kde-frameworks-5.20.0
date: 2016-03-13
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Icones Brisa

- Moltes icones noves
- Afegir icones de tipus MIME del Virtualbox i altres tipus MIME que manquen
- Afegir la implementació per les icones Synaptic and Octopi
- Esmenar la icona de retall (error 354061)
- Esmenar el nom de l'audio-headphones.svg (+=d)
- Icones de puntuació amb marges més petits (1px)

### Integració del marc de treball

- Eliminar un nom de fitxer possible a KDEPlatformFileDialog::setDirectory()
- No filtrar per nom si hi ha un tipus MIME

### KActivities

- Eliminar la dependència de Qt5::Widgets
- Eliminar la dependència de «KDBusAddons»
- Eliminar la dependència de KI18n
- Eliminar les inclusions no utilitzades
- Sortida millorada dels scripts de l'intèrpret d'ordres
- S'ha afegit el model de dades (ActivitiesModel) per mostrar les activitats a la biblioteca
- Per defecte, només es construeix la biblioteca
- Eliminar els components de servei i de l'espai de treball de la construcció
- Moure la biblioteca a src/lib des de src/lib/core
- Esmenar un avís del CMake
- Esmenar una fallada en el menú contextual de les activitats (error 351485)

### KAuth

- Esmenar un interbloqueig del «kded5» quan un programa usa les bifurcacions del «kauth»

### KConfig

- KConfigIniBackend: Esmenar un desacoblament costós en la cerca

### KCoreAddons

- Esmenar la migració de la configuració de les Kdelibs4 al Windows
- S'ha afegit una API per obtenir la informació de versió en temps d'execució dels Frameworks
- KRandom: No usar fins a 16K de /dev/urandom per sembrar «rand()» (error 359485)

### KDeclarative

- No cridar a cap objecte apuntador nul (error 347962)

### KDED

- Fer possible compilar amb -DQT_NO_CAST_FROM_ASCII

### Compatibilitat amb les KDELibs 4

- Afegir la gestió de sessions per a les aplicacions basades en KApplication (error 354724)

### KDocTools

- Usar caràcters Unicode per les bafarades

### KFileMetaData

- KFileMetadata ara pot consultar i emmagatzemar informació del correu original al qual s'havia adjuntat el fitxer

### KHTML

- Esmenar l'actualització del cursor a la vista
- Limitar l'ús de memòria en cadenes de text
- Visualitzador de miniaplicacions java del KHTML: Reparar una crida trencada de D-Bus al «kpasswdserver»

### KI18n

- Usar una macro d'importació portable per a «nl_msg_cat_cntr»
- Ometre la cerca de . i .. per a trobar les traduccions d'una aplicació
- Restringir l'ús de «ct _nl_msg_cat_cntr» a les implementacions del «gettext» de GNU
- Afegir KLocalizedString::languages()
- Efectuar les crides al Gettext només si s'ha localitzat el catàleg

### KIconThemes

- Assegura que la variable s'inicialitza

### KInit

- kdeinit: Preferir la càrrega de les biblioteques des de RUNPATH
- Implementar «Qt5 TODO: use QUrl::fromStringList»

### KIO

- Esmenar la connexió «app-slave» del KIO que es trencava si «appName» contenia un «/» (error 357499)
- Intentar diversos mètodes d'autenticació en cas de fallades
- Ajuda: Esmenar «mimeType()» en el «get()»
- KOpenWithDialog: Mostrar el nom del tipus MIME i el comentari en el text de la casella de selecció «Recorda» (error 110146)
- Una sèrie de canvis per evitar, en més casos, tornar a llistar un directori després de reanomenar un fitxer (error 359596)
- http: Reanomenar «m_iError» a «m_kioError»
- kio_http: Llegir i descartar el cos després d'un 404 amb «errorPage=false»
- kio_http: Esmenar la determinació del tipus MIME quan l'URL acaba amb «/»
- FavIconRequestJob: Afegir el mètode d'accés «hostUrl()» de manera que el Konqueror pugui trobar per a què era el treball, en el sòcol
- FavIconRequestJob: Esmenar que es pengi el treball quan s'interromp si la icona de web és massa gran
- FavIconRequestJob: Esmenar «errorString()», només té l'URL
- KIO::RenameDialog: Restaurar la implementació de la vista prèvia, afegir etiquetes de data i de mida (error 356278)
- KIO::RenameDialog: Refactoritzar codi duplicat
- Esmenar conversions «path-to-QUrl» errònies
- Usar kf5.kio en el nom de categoria per correspondre amb altres categories

### KItemModels

- KLinkItemSelectionModel: Afegir un constructor predeterminat nou
- KLinkItemSelectionModel: Fer que es pugui definir el model de selecció enllaçada
- KLinkItemSelectionModel: Gestionar els canvis del model «selectionModel»
- KLinkItemSelectionModel: No emmagatzemar localment el model
- KSelectionProxyModel: Esmenar un error d'iteració
- Reiniciar l'estat de KSelectionProxyModel quan calgui
- Afegir una propietat indicant si els models formen una cadena connectada
- KModelIndexProxyMapper: Simplificar la lògica de la verificació connectada

### KJS

- Limitar l'ús de memòria en cadenes de text

### KNewStuff

- Mostrar un avís si hi ha un error en el motor

### Paquets dels Frameworks

- Permetre que el KDocTools sigui opcional en el KPackage

### KPeople

- Esmenar una crida obsoleta de l'API
- Afegir «actionType» al connector declaratiu
- Invertir la lògica de filtratge a PersonsSortFilterProxyModel
- Fer que l'exemple en QML sigui lleugerament més usable
- Afegir «actionType» al PersonActionsModel

### KService

- Simplificar el codi, reduir els desreferenciaments d'apuntadors, millores relacionades amb els contenidors
- Afegir el programa de prova kmimeassociations_dumper, inspirat per l'error 359850
- Esmenar el motiu pel qual les aplicacions Chromium/Wine no es carreguen en diverses distribucions (error 213972)

### KTextEditor

- Esmenar el ressaltat de totes les ocurrències a ReadOnlyPart
- No iterar sobre una QString si és una QStringList
- Inicialitzar adequadament els QMaps estàtics
- Preferir «toDisplayString(QUrl::PreferLocalFile)»
- Acceptar caràcters suplents enviats des del mètode d'entrada
- No fallar en aturar quan l'animació de text encara està executant-se

### Framework del KWallet

- Assegura que se cerca el KDocTools
- No passar cap nombre negatiu al D-Bus, es declara en la «libdbus»
- Netejar els fitxers del CMake
- KWallet::openWallet(Synchronous): No excedir el temps després de 25 segons

### KWindowSystem

- Permetre «_NET_WM_BYPASS_COMPOSITOR» (error 349910)

### KXMLGUI

- Usar el nom d'idioma no natiu com a reserva
- Esmenar la gestió de sessions trencada des de KF5 / Qt5 (error 354724)
- Esquemes de dreceres: Permetre esquemes instal·lats globalment
- Usar «qHash(QKeySequence)» de les Qt quan es construeix amb les Qt 5.6+
- Esquemes de dreceres: Esmenar un error quan dos KXMLGUIClients amb el mateix nom sobreescriuen el fitxer d'esquema de l'altre
- kxmlguiwindowtest: Afegir un diàleg de dreceres, per provar l'editor d'esquemes de dreceres
- Esquemes de dreceres: Millorar la usabilitat canviant els textos en la IGU
- Esquemes de dreceres: Millorar la llista combinada d'esquemes (mida automàtica, no netejar els esquemes desconeguts)
- Esquemes de dreceres: No precedir el nom del client IGU al nom del fitxer
- Esquemes de dreceres: Crear el directori abans d'intentar desar un esquema de dreceres nou
- Esquemes de dreceres: Restaurar el marge de la disposició, altrament sembla molt estret
- Esmenar una fuita de memòria en el «hook» d'inici del KXmlGui

### Frameworks del Plasma

- IconItem: No sobreescriure l'origen en usar QIcon::name()
- ContainmentInterface: Esmenar l'ús de QRect «right()» i «bottom()»
- Eliminar el codi efectivament duplicat per gestionar els QPixmaps
- Afegir la documentació de l'API per a l'IconItem
- Esmenar el full d'estil (error 359345)
- No esborrar la màscara de finestra en cada canvi de geometria quan la composició és activa i encara no s'ha definit cap màscara
- Miniaplicació: No fallar en eliminar el plafó (error 345723)
- Tema: Descartar la memòria cau en canviar el tema (error 359924)
- IconItemTest: Ometre quan «grabToImage» falla
- IconItem: Esmenar el canvi de color de les icones SVG carregades des del tema d'icones
- Esmenar la resolució del «iconPath» del SVG a «IconItem»
- Si s'ha passat el camí, seleccionar el final (error 359902)
- Afegir les propietats «configurationRequired» i el motiu
- Moure «contextualActionsAboutToShow» a la miniaplicació
- ScrollViewStyle: No usar els marges de l'element «flickable»
- DataContainer: Esmenar les verificacions del sòcol abans de connectar/desconnectar
- ToolTip: Evitar diversos canvis de geometria en canviar els continguts
- SvgItem: No usar Plasma::Theme des del fil de representació
- AppletQuickItem: Esmenar la cerca de la disposició mateixa adjuntada (error 358849)
- Expansor més petit per a la barra de tasques
- ToolTip: Aturar la visualització del temporitzador si s'ha invocat «hideTooltip» (error 358894)
- Desactivar l'animació de les icones en els consells d'eina del Plasma
- Eliminar les animacions dels consells d'eina
- El tema predeterminat segueix l'esquema de colors
- Esmenar IconItem quan no carrega les icones amb nom que no són del tema (error 359388)
- Preferir altres contenidors abans que l'escriptori a «containmentAt()»
- WindowThumbnail: Descartar els mapes de píxels GLX a «stopRedirecting()» (error 357895)
- Eliminar el filtre de miniaplicacions antigues
- ToolButtonStyle: No confiar en un ID de fora
- No assumir que s'ha trobat la Corona (error 359026)
- Calendari: Afegir botons enrere/endavant adequats i un botó «Avui» (errors 336124, 348362, 358536)

### Sonnet

- No desactivar la detecció d'idioma només perquè s'hagi definit un idioma
- Desactivar la desactivació automàtica de la verificació ortogràfica automàtica per defecte
- Esmenar TextBreaks
- Esmenar els camins de cerca del diccionari Hunspell quan manca un «/» (error 359866)
- Afegir &lt;app dir&gt;/../share/hunspell al camí de cerca del diccionari

Podeu debatre i compartir idees quant a aquest llançament en la secció de comentaris en <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>l'article del Dot</a>.
