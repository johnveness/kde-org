---
aliases:
- ../../kde-frameworks-5.56.0
date: 2019-03-09
layout: framework
libCount: 70
---
### Baloo

- Substitueix diversos Q_ASSERTs per la comprovació adequada
- Comprova la longitud de la cadena per evitar fallar als URL «tags:/»
- [tags_kio] Esmena l'etiquetatge dels fitxers locals comprovant només l'etiqueta: URL amb barra doble
- Escriu al codi font l'interval d'actualització del temps romanent
- Soluciona una regressió per coincidir amb les carpetes incloses explícitament
- Neteja les entrades idempotents de la taula de mapatge dels tipus MIME
- [baloo/KInotify] Notifica si la carpeta s'ha mogut des d'un lloc no controlat (error 342224)
- Gestiona correctament la coincidència de les subcadenes de les carpetes incloses/excloses
- [balooctl] Normalitza els camins inclosos/exclosos abans d'usar-los per a la configuració
- Optimitza l'operador d'assignació de còpia Baloo::File, esmena Baloo::File::load(url)
- Usa el contingut per determinar el tipus MIME (error 403902)
- [Extractor] Exclou les dades GPG encriptades de la indexació (error 386791)
- [balooctl] Interromp realment una ordre mal formada en lloc de només dir-ho
- [balooctl] Afegeix l'ajuda que manca per a «config set», normalitza una cadena
- Substitueix un «isDirHidden» recursiu per una iteració, permet un argument «const»
- S'assegura que només s'afegeixen directoris al vigilant «inotify»

### Icones Brisa

- Afegeix la icona «code-oss»
- [breeze-icons] Afegeix icones de càmera de vídeo
- [breeze-icons] Usa icones noves de suspensió, hibernació i canvi d'usuari al tema d'icones Brisa
- Afegeix les versions de 16 px i 22 px de la icona de comandament de joc a «devices/»
- Fa coherents els textos dels consells d'eina del tema Brisa
- Afegeix les icones de bateria
- Reanomena les icones «visibility» i «hint» a «view-visible» i «view-hidden»
- [breeze-icons] Afegeix icones monocromes/més petites de targeta SD i de Memory stick (error 404231)
- Afegeix les icones de dispositiu per als drons
- Canvia les icones dels tipus MIME de capçalera/codi en C/C++ a l'estil cercle/línia
- Esmena les ombres que manquen a les icones de tipus MIME de capçaleres en C/C++ (error 401793)
- Elimina la icona «monochrome» de preferència de tipus de lletra
- Millora la icona de selecció de tipus de lletra
- Usa una icona nova d'estil campana per a tots els usuaris de «preferences-desktop-notification» (error 404094)
- [breeze-icons] Afegeix les versions de 16 px de «gnumeric-font.svg» i enllaça «gnumeric-font.svg» a «font.svg»
- Afegeix enllaços simbòlics de «preferences-system-users» que apunten a la icona «yast-users»
- Afegeix la icona «edit-none»

### Mòduls extres del CMake

- Soluciona l'obtenció de «releaseme» quan està inclòs en un subdirectori
- Mòdul nou de cerca per Canberra
- Actualitza els fitxers de la cadena d'eines de l'Android a la realitat
- Afegeix una verificació de compilació a FindEGL

### KActivities

- Usa l'ordenació natural a ActivityModel (error 404149)

### KArchive

- Protegeix KCompressionDevice::open de ser cridat sense cap dorsal disponible (error 404240)

### KAuth

- Explica a tothom que principalment cal usar KF5::AuthCore
- Compila l'ajudant propi contra AuthCore i no Auth
- Presenta el KF5AuthCore

### KBookmarks

- Substitueix la dependència de KIconThemes amb l'ús equivalent de QIcon

### KCMUtils

- Usa el nom del KCM a la capçalera del KCM
- Afegeix un «ifndef» KCONFIGWIDGETS_NO_KAUTH que manca
- Adapta els canvis a «kconfigwidgets»
- Sincronitza el farciment del mòdul QML per reflectir les pàgines de l'arranjament del sistema

### KCodecs

- Solució per al CVE-2013-0779
- QuotedPrintableDecoder::decode: retorna fals en lloc d'una asserció
- Marca que KCodecs::uuencode no fa res
- nsEUCKRProber/nsGB18030Prober::HandleData no falla si «aLen» és 0
- nsBig5Prober::HandleData: no falla si «aLen» és 0
- KCodecs::Codec::encode: no fa asserció/falla si «makeEncoder» retorna nul
- nsEUCJPProbe::HandleData: no falla si «aLen» és 0

### KConfig

- Escriu caràcters UTF8 vàlids sense escapar (error 403557)
- KConfig: gestiona correctament els enllaços simbòlics de directori

### KConfigWidgets

- Omet la prova de referència si no s'ha trobat cap fitxer d'esquema
- Afegeix una nota per al KF6 per usar la versió del nucli de KF5::Auth
- Situa a la memòria cau la configuració predeterminada del KColorScheme

### KCoreAddons

- Crea tel: enllaços per als números de telèfon

### KDeclarative

- Usa «KPackage::fileUrl» per a admetre paquets «rcc» als KCM
- [GridDelegate] Soluciona les etiquetes llargues que es barregen entre elles (error 404389)
- [GridViewKCM] Millora el contrast i la llegibilitat en passar per sobre dels botons en línia dels delegats (error 395510)
- Corregeix l'indicador d'acceptació de l'objecte d'esdeveniment en DragMove (error 396011)
- Usa una icona d'element «None» diferent en la vista de quadrícula dels KCM

### KDESU

- kdesud: KAboutData::setupCommandLine() ja defineix l'ajuda i la versió

### KDocTools

- Adapta la implementació per a compilació creuada a KF5_HOST_TOOLING
- Només informa que s'ha trobat el DocBookXML si realment s'ha trobat
- Actualitza les «entities» de l'espanyol

### KFileMetaData

- [Extractor] Afegeix metadades als extractors (error 404171)
- Afegeix un extractor per als fitxers AppImage
- Neteja l'extractor per al «ffmpeg»
- [ExternalExtractor] Proporciona una sortida més útil quan l'extractor falla
- Dona format a les dades EXIF del flaix de foto (error 343273)
- Evita els efectes col·laterals per al valor del «errno» obsolet
- Usa el Kformat per als bits i la freqüència de mostreig
- Afegeix les unitats a les dades «framerate» i «gps»
- Afegeix la funció de format de la cadena a la informació de les propietats
- Evita la fuita d'un QObject a ExternalExtractor
- Gestiona &lt;a&gt; com a element contenidor a SVG
- Comprova Exiv2::ValueType::typeId abans de convertir-lo a racional

### KImageFormats

- ras: soluciona una fallada en fitxers trencats
- ras: protegeix també la paleta QVector
- ras: ajusta la comprovació de fitxers màxims
- xcf: soluciona l'ús de memòria sense inicialitzar en documents trencats
- Afegeix «const», ajuda a entendre millor la funció
- ras: ajusta la mida màxima que encaixa en un QVector
- ras: no fa cap asserció perquè intenta assignar un vector enorme
- ras: es protegeix contra la divisió per zero
- xcf: no divideix per 0
- tga: falla adequadament si «readRawData» dona errors
- ras: falla adequadament si alçada*amplada*bpp &gt; longitud

### KIO

- kioexec: KAboutData::setupCommandLine() ja defineix l'ajuda i la versió
- Soluciona una fallada al Dolphin en deixar anar un fitxer eliminat a la paperera (error 378051)
- Elideix el centre dels noms de fitxer molt llargs a les cadenes d'error (error 404232)
- Afegeix la implementació per als portals en el KRun
- [KPropertiesDialog] Soluciona l'agrupació de quadres combinats (error 403074)
- Intenta localitzar adequadament el binari del «kioslave» a «$libexec» i «$libexec/kf5»
- Usa AuthCore en lloc d'Auth
- Afegeix el nom d'icona als proveïdors de serveis al fitxer «.desktop»
- Llegeix la icona del proveïdor de cerca IKWS des del fitxer «desktop»
- [PreviewJob] També passa pel que és el generador de miniatures en fer un «stat» de fitxer (error 234754)

### Kirigami

- Elimina l'embolic trencat amb el «contentY» a «refreshabeScrollView»
- Afegeix «OverlayDrawer» al material documentable per a Doxygen
- Mapa «currentItem» a la vista
- Color adequat per a la icona de fletxa avall
- SwipeListItem: crea espai per a les accions si «!supportsMouseEvents» (error 404755)
- ColumnView i refactorització parcial del C++ a PageRow
- Es poden usar la majoria de controls 2.3 a les Qt 5.10
- Soluciona l'alçada dels calaixos horitzontals
- Millorar el ToolTip al component ActionTextField
- Afegeix un component ActionTextField
- Esmena l'espaiat dels botons (error 404716)
- Esmena la mida dels botons (error 404715)
- GlobalDrawerActionItem: icona referenciada adequadament usant la propietat «group»
- Mostra el separador si la barra d'eines de capçalera és invisible
- Afegeix un fons de pàgina predeterminat
- DelegateRecycler: soluciona la traducció usant un domini incorrecte
- Soluciona un avís en usar QQuickAction
- Elimina diverses construccions QString innecessàries
- No mostra el consell d'eina quan es mostra el menú desplegable (error 404371)
- Oculta les ombres quan està tancat
- Afegeix les propietats necessàries per al color alternatiu
- Reverteix la majoria dels canvis heurístics de color de les icones
- Gestiona adequadament les propietats agrupades
- [PassiveNotification] No inicia el temporitzador fins que la finestra tingui el focus (error 403809)
- [SwipeListItem] Usa un botó d'eina real per millorar la usabilitat (error 403641)
- Admet fons alternants opcionals (error 395607)
- Només mostra les nanses quan hi ha accions visibles
- Admet colors a les icones a les accions dels botons
- Mostra sempre el botó enrere a les capes
- Actualitza la documentació de SwipeListItem al QQC2
- Esmena la lògica d'«updateVisiblePAges»
- Exposa les pàgines visibles en un «pagerow»
- Oculta el fil d'Ariadna quan la pàgina actual té una barra d'eines
- Admet la sobreescriptura de la pàgina «toolbarstyle»
- Propietat nova a la pàgina: «titleDelegate» per sobreescriure el títol a les barres d'eines

### KItemModels

- KRearrangeColumnsProxyModel: fa públics els mètodes de mapatge a dues columnes

### KNewStuff

- Filtra el contingut no vàlid a les llistes
- Esmena una fuita de memòria trobada per l'ASAN

### KNotification

- Adaptació a «findcanberra» des d'ECM
- Llista l'Android com a admès oficialment

### Framework del KPackage

- Elimina l'avís d'obsolet del «kpackage_install_package»

### KParts

- templates: KAboutData::setupCommandLine() ja defineix l'ajuda i la versió

### Kross

- Instal·la els mòduls del Kross a ${KDE_INSTALL_QTPLUGINDIR}

### KService

- kbuildsycoca5: no cal repetir el treball de KAboutData::setupCommandLine()

### KTextEditor

- Intenta millorar el pintat de l'alçada de les línies de text - l'error 403868 evita retallar - i altres parts encara estan trencades: alçada doble height en coses com barreja d'anglès/àrab, vegeu l'error 404713
- Usa QTextFormat::TextUnderlineStyle en lloc de QTextFormat::FontUnderline (error 399278)
- Fa possible mostrar tots els espais del document (error 342811)
- No imprimeix les línies amb sagnat
- KateSearchBar: També mostra que la cerca té un consell de reinici a «nextMatchForSelection()» o també Ctrl-H
- katetextbuffer: refactoritza TextBuffer::save() per a separar millor els camins del codi
- Usa AuthCore en lloc d'Auth
- Refactoritza KateViewInternal::mouseDoubleClickEvent(QMouseEvent *e)
- Millores a la compleció
- Estableix l'esquema de color a Printing per a la vista prèvia d'impressió (error 391678)

### KWayland

- Només publica XdgOutput::done si ha canviat (error 400987)
- FakeInput: afegeix la implementació per moure l'apuntador amb coordenades absolutes
- Afegeix un XdgShellPopup::ackConfigure que manca
- [server] Afegeix un mecanisme intermediari de dades de superfície
- [server] Afegeix el senyal «selectionChanged»

### KWidgetsAddons

- Usa la icona «no» correcta del KStandardGuiItem

### Frameworks del Plasma

- [Icon Item] El blocat de l'animació següent també està basat en la visibilitat de la finestra
- Mostra un avís si un connector requereix una versió més nova
- Actualitza les versions del tema perquè han canviat les icones, per invalidar les memòries cau antigues
- [breeze-icons] Renova «system.svgz»
- Fa coherents els textos dels consells d'eina del tema Brisa
- Canvia «glowbar.svgz» a un estil més suau (error 391343)
- Fa la reserva del contrast del fons en temps d'execució (error 401142)
- [breeze desktop theme/dialogs] Afegeix cantonades arrodonides als diàlegs

### Purpose

- pastebin: no mostra les notificacions de progrés (error 404253)
- sharetool: mostra l'URL compartit a la part superior
- Esmena la compartició de fitxers amb espais o cometes als noms via el Telegram
- Fa que ShareFileItemAction proporcioni una sortida o un error si s'han subministrat (error 397567)
- Habilita la compartició d'URL via correu electrònic

### QQC2StyleBridge

- Usa PointingHand en passar per sobre d'enllaços en una Label
- Respecta la propietat de visualització dels botons
- En fer clic en àrees buides es comporta com un Re Pàg/Av Pàg (error 402578)
- Admet icones als ComboBox
- Admet l'API de posicionament de text
- Admet colors dels fitxers locals als botons
- Usa el cursor correcte en passar per sobre de la part editable d'un botó de selecció de valors

### Solid

- Ajusta FindUDev.cmake als estàndards ECM

### Sonnet

- Gestiona el cas si a «createSpeller» es passa un llenguatge no disponible

### Ressaltat de la sintaxi

- Esmena un avís de supressió de repositori
- MustacheJS: també ressalta els fitxers de plantilla, esmena la sintaxi millora el suport per Handlebars
- Fa que els contexts no usats sigui fatals per a l'indexador
- Actualitza «example.rmd.fold» i «test.markdown.fold» amb números nous
- Instal·la la capçalera DefinitionDownloader
- Actualitza l'«octave.xml» a l'Octave 4.2.0
- Millora el ressaltat del TypeScript (i el React) i afegeix més proves per al PHP
- Afegeix més ressaltat per als llenguatges imbricats en el Markdown
- Retorna les definicions ordenades per als noms de fitxer i els tipus MIME
- Afegeix una actualització de referència que mancava
- BrightScript: números unaris i hexadecimals, @attribute
- Evita fitxers *-php.xml duplicats a «data/CMakeLists.txt»
- Afegeix funcions que retornen totes les definicions per a un tipus MIME o un nom de fitxer
- Actualitza el tipus MIME «literate» del Haskell
- Evita una asserció en carregar l'expressió regular
- cmake.xml: actualitzacions per a la versió 3.14
- CubeScript: soluciona l'escapada de la continuació de línies a les cadenes
- Afegeix un «Com es fa» mínim per afegir proves
- R Markdown: millora el plegat de blocs
- HTML: ressalta el codi JSX, TypeScript i MustacheJS a l'etiqueta &lt;script&gt; (error 369562)
- AsciiDoc: afegeix el plegat per a les seccions
- Ressaltat de la sintaxi de l'esquema de FlatBuffers
- Afegeix diverses constants i funcions del Maxima

### Informació de seguretat

El codi publicat s'ha signat amb GPG usant la clau següent: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Empremta digital de la clau primària: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
