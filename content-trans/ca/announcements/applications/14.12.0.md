---
aliases:
- ../announce-applications-14.12.0
changelog: true
date: '2014-12-17'
description: Es distribueixen les aplicacions 14.12 del KDE.
layout: application
title: KDE distribueix les aplicacions 14.12 del KDE
version: 14.12.0
---
17 de desembre de 2014. Avui KDE distribueix la versió 14.12 de les aplicacions del KDE. Aquest llançament aporta funcionalitats noves i esmenes d'errors a més d'un centenar d'aplicacions. La majoria d'aquestes aplicacions estan basades en la plataforma de desenvolupament 4 del KDE; algunes s'han convertit als nous <a href='https://dot.kde.org/2013/09/25/frameworks-5'>Frameworks 5 del KDE</a>, un conjunt de biblioteques modularitzades que estan basades en les Qt5, la darrera versió d'aquest popular entorn de treball d'aplicacions multiplataforma.

La <a href='http://api.kde.org/4.x-api/kdegraphics-apidocs/libs/libkface/libkface/html/index.html'>Libkface</a> és nova en aquest llançament; és una biblioteca que activa la detecció de cares i el reconeixement de cares en les fotografies.

Aquest llançament inclou la primera versió basada en els Frameworks 5 del KDE del <a href='http://www.kate-editor.org'>Kate</a> i el <a href='https://www.kde.org/applications/utilities/kwrite/'>KWrite</a>, el <a href='https://www.kde.org/applications/system/konsole/'>Konsole</a>, el <a href='https://www.kde.org/applications/graphics/gwenview/'>Gwenview</a>, el <a href='http://edu.kde.org/kalgebra'>KAlgebra</a>, el <a href='http://edu.kde.org/kanagram'>Kanagram</a>, el <a href='http://edu.kde.org/khangman'>KHangman</a>, el <a href='http://edu.kde.org/kig'>Kig</a>, el <a href='http://edu.kde.org/parley'>Parley</a>, el <a href='https://www.kde.org/applications/development/kapptemplate/'>KApptemplate</a> i l'<a href='https://www.kde.org/applications/utilities/okteta/'>Okteta</a>. Algunes biblioteques també estan llestes per al seu ús en els Frameworks 5 del KDE: analitza i libkeduvocdocument.

El <a href='http://kontact.kde.org'>paquet Kontact</a> és ara en suport a llarg termini a la versió 4.14 mentre els desenvolupadors estan invertint la seva energia a adaptar-lo als Frameworks 5 del KDE.

Algunes de les característiques noves d'aquest llançament inclouen:

+ El <a href='http://edu.kde.org/kalgebra'>KAlgebra</a> té una versió Android nova gràcies als Frameworks 5 del KDE i ara pot <a href='http://www.proli.net/2014/09/18/touching-mathematics/'>imprimir els seus gràfics en 3D</a>
+ El <a href='http://edu.kde.org/kgeography'>KGeography</a> té un mapa nou per Bihar.
+ El visualitzador de documents <a href='http://okular.kde.org'>Okular</a> ara ha implementat la cerca inversa per al «latex-synctex» en el dvi i algunes petites millores en la implementació de l'ePub.
+ L'<a href='http://umbrello.kde.org'>Umbrello</a> --el modelador UML-- té moltes funcionalitats noves massa nombroses per a llistar-les aquí.

El llançament d'abril 15.04 de les aplicacions del KDE inclourà moltes funcionalitats noves així com més aplicacions basades en els Frameworks 5 del KDE modulars.
