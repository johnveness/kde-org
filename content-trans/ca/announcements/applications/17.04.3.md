---
aliases:
- ../announce-applications-17.04.3
changelog: true
date: 2017-07-13
description: KDE distribueix les aplicacions 17.04.3 del KDE
layout: application
title: KDE distribueix les aplicacions 17.04.3 del KDE
version: 17.04.3
---
13 de juliol de 2017. Avui KDE distribueix la tercera actualització d'estabilització per a les <a href='../17.04.0'>aplicacions 17.04 del KDE</a>. Aquesta publicació només conté esmenes d'errors i actualitzacions de traduccions, proporcionant una actualització segura i millor per a tothom.

Hi ha més de 25 esmenes registrades d'errors que inclouen millores al Kdepim, Dolphin, Dragonplayer, Kdenlive, Umbrello, entre d'altres.

Aquest llançament també inclou les versions de suport a llarg termini de la plataforma de desenvolupament KDE 4.14.34.
