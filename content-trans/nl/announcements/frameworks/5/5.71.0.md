---
aliases:
- ../../kde-frameworks-5.71.0
date: 2020-06-06
layout: framework
libCount: 70
---
### Baloo

- Bestandsnaamtermen slechts eenmaal opslaan

### Breeze pictogrammen

- Nauwkeurigheid van batterijpercentagepictogrammen verbeteren
- KML-mimetype pictogram repareren
- Muispictogram wijzigingen om beter contrast te hebben in donker thema (bug 406453)
- Vereis in-bron bouwen (bug 421637)
- 48px plaatspictogrammen toevoegen (bug 421144)
- Een ontbrekend sneltoetspictogram van LibreOffice wordt toegevoegd

### Extra CMake-modules

- [android] nieuwere Qt versie in voorbeeld gebruiken
- [android] specificeren van APK installatielocatie toestaan
- ECMGenerateExportHeader: generatie van *_DEPRECATED_VERSION_BELATED() toevoegen
- ECMGeneratePriFile: reparatie voor absoluut zijn van ECM_MKSPECS_INSTALL_DIR
- ECMGeneratePriFile: de pri-bestanden relocatable maken
- Waarschuwing van niet overeenkomende naam met pakket find_package_handle_standard_args package onderdrukken

### KDE Doxygen hulpmiddelen

- Bestand logo.png gebruiken als logo als standaard, indien aanwezig
- public_example_dir wijzigen naar public_example_dirs, om meerdere mappen in te schakelen
- Code voor ondersteuning van Python2 laten vallen
- Koppelingen naar repo naar invent.kde.org aanpassen
- Koppeling naar kde.org impressum repareren
- Geschiedenispagina's van KDE4 zijn samengevoegd in de algemene geschiedenispagina
- Generatie van Unbreak met dep-diagrammen met Python 3 (break Py2)
- Metalist exporteren naar json-bestand

### KAuth

- ECMGenerateExportHeader gebruiken om afgekeurde API beter te beheren

### KBookmarks

- UI markeercontext gebruiken in meer aanroepen van tr()
- [KBookmarkMenu] m_actionCollection vroeg toekennen om crash te voorkomen (bug 420820)

### KCMUtils

- X-KDE-KCM-Args toevoegen als eigenschap, eigenschap lezen in modulelader
- Crash repareren bij laden van een externe KCM-toepassing zoals yast (bug 421566)
- KSettings::Dialog: gedupliceerde items vermijden vanwege cascading $XDG_DATA_DIRS

### KCompletion

- API dox ook omgeven met KCOMPLETION_ENABLE_DEPRECATED_SINCE; per methode gebruiken
- UI markeercontext gebruiken in meer aanroepen van tr()

### KConfig

- Afgekeurde SaveOptions enum-waarde niet proberen te initialiseren
- KStandardShortcut::findByName(const QString&amp;) toevoegen en find(const char*) afkeuren
- KStandardShortcut::find(const char*) repareren
- Naam van intern-geëxporteerde methode aanpassen zoals gesuggereerd in D29347
- KAuthorized: exportmethode om beperkingen opnieuw te laden

### KConfigWidgets

- [KColorScheme] gedupliceerde code verwijderen
- Zorg dat headerkleuren eerst terugvallen naar vensterkleuren
- Kleurenset voor de header introduceren

### KCoreAddons

- Bug 422291 repareren - Vooruitblik van XMPP URI's in KMail (bug 422291)

### KCrash

- Gelokaliseerde zaken van qstring niet aanroepen in kritieke sectie

### KDeclarative

- kcmshell.openSystemSettings() en kcmshell.openInfoCenter() functies aanmaken
- KKeySequenceItem overzetten naar QQC2
- Afgeleiden van GridViewInternal op pixel uitlijnen

### KDED

- Vage pictogrammen in titelbalk-appmenu door vlag UseHighDpiPixmaps toe te voegen
- Systemd gebruikerservicebestand voor kded toevoegen

### KFileMetaData

- [TaglibExtractor] ondersteuning voor hoorbare "Verbeterde audio" audioboeken
- de vlag extractMetaData honoreren

### KGlobalAccel

- Bug repareren met componenten met speciale tekens (bug 407139)

### KHolidays

- Taiwanese vakantiedagen bijgewerkt
- holidayregion.cpp - vertaalbare tekenreeksen voor de Duitse regio's leveren
- holidays_de-foo - naamvelden voor alle Duitse vakantiebestanden instellen
- holiday-de-&lt;foo&gt; - waar foo een regio is Duitsland is
- Vakantiebestand toevoegen voor DE-BE (Duitsland/Berlijn)
- holidays/plan2/holiday_gb-sct_en-gb

### KImageFormats

- Enige controle op juistheid en grenzen toevoegen

### KIO

- KIO::OpenUrlJob introduceren, een herziening en vervanging voor KRun
- KRun: alle statische 'run*' methoden afkeuren, met volledige instructies voor overzetten
- KDesktopFileActions: run/runWithStartup afkeuren, OpenUrlJob in plaats daarvan gebruiken
- Zoeken naar kded als afhankelijkheid tijden uitvoeren
- [kio_http] een FullyEncoded QUrl pad ontleden met TolerantMode (bug 386406)
- [DelegateAnimationHanlder] afgekeurd QLinkedList vervangen door QList
- RenameDialog: Waarschuwen wanneer bestandsgrootte niet hetzelfde zijn (bug 421557)
- [KSambaShare] Controleren dat zowel smbd als testparm beschikbaar zijn (bug 341263)
- Places: Solid::Device::DisplayName gebruiken voor DisplayRole (bug 415281)
- KDirModel: hasChildren() regressie voor bomen met getoonde bestanden repareren (bug 419434)
- file_unix.cpp: wanneer ::rename wordt gebruikt als voorwaarde vergelijk zijn terugkeer met -1
- [KNewFileMenu] Aanmaken van een map genaamd '~' toestaan (bug 377978)
- [HostInfo] QHostInfo::HostNotFound instellen wanneer een host niet is gevonden in de DNS-cache (bug 421878)
- [DeleteJob] Uiteindelijk aantal na beëindiging rapporteren (bug 421914)
- KFileItem: timeString lokaal maken (bug 405282)
- [StatJob] laat mostLocalUrl (ftp, http...etc) URL's op afstand negeren (bug 420985)
- [KUrlNavigatorPlacesSelector] het menu slechts een keer bijwerken bij wijzigingen (bug 393977)
- [KProcessRunner] alleen naam van uitvoerbaar bestand voor scope gebruiken
- [knewfilemenu] inline waarschuwing tonen bij aanmaken van items met vooraan of achteraan spaties (bug 421075)
- [KNewFileMenu] overtollige afsluitparameter verwijderen
- ApplicationLauncherJob: de Openen met-dialoog tonen als geen service is doorgegeven
- Ga na dat het programma bestaat en uitvoerbaar is voordat we kioexec/kdesu/etc invoegen
- URL doorgegeven als argument bij starten van een .desktop-bestand repareren (bug 421364)
- [kio_file] hernoemen van bestand 'A' naar 'a' op FAT32 bestandssystemen behandelen
- [CopyJob] Controleren of bestemmingsmap een symbolische koppeling is (bug 421213)
- Service-bestand die 'In terminal uitvoeren' specificeert die een foutcode 100 geeft repareren (bug 421374)
- kio_trash: ondersteuning toevoegen voor hernoemen van mappen in de cache toevoegen
- Als info.keepPassword niet is gezet waarschuwen
- [CopyJob] vrije ruimte controleren voor url's op afstand vóór kopiëren en andere verbeteringen (bug 418443)
- [kcm trash] percentage van grootte van kcm trash wijzigen naar 2 decimale plaatsen
- [CopyJob] een oude TODO kwijtraken en QFile::rename() gebruiken
- [LauncherJobs] beschrijving uitzenden

### Kirigami

- Sectiekoppen in zijbalk toevoegen wanneer alle acties zijn uit te vouwen
- reparatie: opvulling in overlay-sheet-header
- Een betere standaard kleur gebruiken voor Global Drawer bij gebruik als zijbalk
- Niet-accuraat commentaar over hoe GridUnit bepaald wordt repareren
- Betrouwbaarder ouderboomstructuurbeklimming voor PageRouter::pushFromObject
- PlaceholderMessage hangt af van Qt 5.13 zelfs als CMakeLists.txt 5.12 vereist
- de Header-groep introduceren
- Geen sluitenanimatie afspelen bij close() als sheet al is gesloten
- SwipeNavigator component toevoegen
- Avatar component introduceren
- De initialisatie van schaduwhulpbron voor statisch gebouwden repareren
- AboutPage: tekstballonnen toevoegen
- closestToWhite en closestToBlack zeker maken
- AboutPage: knoppen voor e-mail, webAddress toevoegen
- Altijd vensterkleurenset voor AbstractApplicationHeader gebruiken (bug 421573)
- ImageColors introduceren
- Betere berekening van breedte voor PlaceholderMessage aanbevelen
- PageRouter API verbeteren
- Voor BasicListItem ondertitel klein lettertype gebruiken
- Ondersteuning toevoegen voor lagen in PagePoolAction
- RouterWindow-control toevoegen

### KItemViews

- UI markeercontext gebruiken in meer aanroepen van tr()

### KNewStuff

- Foutmeldingen niet dupliceren in een passieve melding (bug 421425)
- Onjuiste kleuren in de KNS Quick berichtenbox repareren (bug 421270)
- Item niet markeren als niet geïnstalleerd als script voor installatie ongedaan maken is mislukt (bug 420312)
- KNS: methode isRemote afkeuren en fout bij ontleden juist behandelen
- KNS: item niet als geïnstalleerd markeren als script voor installeren is mislukt
- Updates tonen wanneer de optie geselecteerd is repareren (bug 416762)
- Automatische selectie van update repareren (bug 419959)
- Ondersteuning voor KPackage toevoegen aan KNewStuffCore (bug 418466)

### KNotification

- Besturing van zichtbaarheid van vergrendelscherm op Android implementeren
- Groepering van meldingen op Android implementeren
- Meldingen met opgemaakte tekst op Android tonen
- URL's implementeren met tips
- UI markeercontext gebruiken in meer aanroepen van tr()
- Ondersteuning implementeren voor urgentie van meldingen op Android
- Controle op meldingenservice verwijderen en terugvallen op KPassivePopup

### KQuickCharts

- Eenvoudiger controle op afronden van achtergrond
- PieChart: Een torussegment als achtergrond weergeven wanneer het geen volledige cirkel is
- PieChart: een hulpfunctie toevoegen die het afronden van torussegmenten doet
- PieChart: fromAngle/toAngle aan schaduwmaker blootstellen
- De achtergrond van de taartgrafiek altijd weergeven
- Achtergrondkleur altijd instellen, zelfs wanneer toAngle != 360
- Begin/einde toestaan minstens 2pi van elkaar weg laten zijn
- PieChart implementatie herschrijven
- sdf-functie van sdf_torus_segment opknappen en van commentaar voorzien
- Niet expliciet hoeveelheid gladmaken specificeren bij weergaven van sdf's
- Om gebrek van parameters voor array-functie in GLES2 schaduwmakers heen werken
- Verschillen tussen kern en oudere profielen in lijn/taart schaduwmakers behandelen
- Een paar validatiefouten repareren
- De validatie van schaduwmakerscript bijwerken met de nieuwe structuur
- Een aparte header voor GLES3 schaduwmakers toevoegen
- Gedupliceerde schaduwmakers voor kernprofiel verwijderen, in plaats van preprocessor gebruiken
- beide namen en shortName blootstellen
- verschillende lange/korte labels ondersteunen (bug 421578)
- model achter een QPointer bewaken
- MapProxySource toevoegen

### KRunner

- Niet vasthouden aan toevoegen van witte lijst aan configuratie
- KRunner voorbereid/neerhalen-signalen repareren (bug 420311)
- Lokale bestanden en mappen detecteren die beginnen met ~

### KService

- X-KDE-DBUS-Restricted-Interfaces toevoegen aan bureaublad-item-velden in Toepassingen
- Ontbrekende tag voor afkeuring in compiler toevoegen voor 5-argumenten KServiceAction-constructor

### KTextEditor

- .diff toevoegen aan de file-changed-diff om MIME-detectie op Windows in te schakelen
- scrollbar minimap: prestatie: bijwerken vertragen voor inactieve documenten
- Tekst altijd laten uitlijnen met basislijn van lettertype
- "Complete weergaveconfiguratie opslaan en ophalen in en uit sessieconfiguratie" terugdraaien
- Gemodificeerde regelmarkering in kate-minimap repareren

### KWidgetsAddons

- Luidruchtig zijn over afgekeurde KPageWidgetItem::setHeader(lege-niet-nul tekenreeks)

### KXMLGUI

- [KMainWindow] QIcon::setFallbackThemeName aanroepen (later) (bug 402172)

### KXmlRpcClient

- KXmlRpcClient markeren als hulp bij overzetten

### Plasma Framework

- PC3: scheidingsteken aan werkbalk toevoegen
- de headercolorgroup ondersteunen
- Schuif- en sleepaanpassingen van waarden voor besturing van SpinBox implementeren
- Lettertype gebruiken: in plaats van font.pointSize: waar mogelijk
- kirigamiplasmastyle: implementatie van AbstractApplicationHeader toevoegen
- Potentieel verbreken van alle signalen in IconItem vermijden (bug 421170)
- Klein lettertype gebruiken voor ExpandableListItem ondertitel
- kleine lettertypen toevoegen aan Kirigami plasma-stijlen
- [Plasmoid Heading] De kop alleen tekenen wanneer er een SVG in het thema zit

### QQC2StyleBridge

- Stijlen van ToolSeparator toevoegen
- "ingedrukt" verwijderen uit status CheckIndicator "aan" (bug 421695)
- De Header-groep ondersteunen
- smallFont in Kirigami plug-in implementeren

### Solid

- Een QString Solid::Device::displayName toevoegen, gebruikt in Fstab apparaat voor aankoppelen van netwerkbronnen (bug 415281)

### Sonnet

- Overschrijven bieden om automatische taaldetectie uit te schakelen (bug 394347)

### Accentuering van syntaxis

- Raku extensies bijwerken in Markdown blokken
- Raku: ingekapselde codeblokken in Markdown repareren
- Attribuut "identifier" toekennen aan openingsaanhalingsteken in plaats van "Commentaar" (bug 421445)
- Bash: commentaar na escapes repareren (bug 418876)
- LaTeX: invouwen in end{...} repareren en in gebiedsmarkeringen BEGIN-END (bug 419125)

### Beveiligingsinformatie

De vrijgegeven code is ondertekend met GPG met de volgende sleutel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Vingerafdruk van primaire sleutel: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
