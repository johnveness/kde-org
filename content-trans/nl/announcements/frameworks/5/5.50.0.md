---
aliases:
- ../../kde-frameworks-5.50.0
date: 2018-09-08
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Attica

- Ondersteuning voor voorgestelde toevoeging van tags toevoegen in OCS 1.7

### Baloo

- Een typfout gerepareerd in de uitvoer van de indexgrootte (bug 397843)
- Verwijder src- niet dest-url wanneer een url is nieuw niet te indexeren
- [tags_kio] vereenvoudig het opzoekpad van de bestandsnaam die overeenkomt met gebruik van een vangstgroep
- Draai "Sla het in de wachtrij zetten van nieuwe niet te indexeren bestanden en verwijder ze direct uit de index" terug.
- [tags_kio] opzoeken van bestandpad in loop vereenvoudigen

### Breeze pictogrammen

- LabPlot-project bestandspictogram toevoegen
- ScalableTest, "scalable" plasma-browser-integratie toevoegen (bug 393999)

### Extra CMake-modules

- Bindingen: controleer of bindingen gegenereerd kunnen worden voor een specifieke python-versie
- Bindings: generator forward compatibel maken met Python 3
- Veranderen van QT_PLUGIN_PATH door ECM bij uitvoeren van tests uitschakelen
- Bindings: ondersteuning voor scoped enums toevoegen (bug 397154)
- Het mogelijk maken voor ECM om po-bestanden te detecteren op het moment van configureren

### Frameworkintegratie

- [KStyle] vraagtekenpictogram gebruiken voor vraagdialoog

### KArchive

- niet-ASCII coderingen van bestandsnamen in tar-archieven behandelen (bug 266141)
- KCompressionDevice: write niet aanroepen na WriteError (bug 397545)
- Ontbrekende Q_OBJECT macro's voor QIODevice subclasses toevoegen
- KCompressionDevice: propageer fouten uit QIODevice::close() (bug 397545)
- Bzip hoofdpagina repareren

### KCMUtils

- Aangepaste QScrollArea met hint voor grootte gebruiken niet beperkt door lettergrootte (bug 389585)

### KConfig

- Waarschuwing verwijderen over oude kioskfunctie die niet langer van toepassing is
- Systeemstandaard instellen voor sneltoets Ctrl+0 voor actie "Actuele grootte"

### KCoreAddons

- Geen spatie verwijderen tussen twee url's wanneer regel begint met " (kmail bug)
- KPluginLoader: '/' gebruiken zelfs onder Windows, libraryPaths() geeft paden terug met '/'
- KPluginMetaData: lege tekenreeks converteren naar lege lijst met tekenreeksen

### KDeclarative

- Draai "ga na dat we altijd schrijven in de root-context van de engine" terug
- Eigenschap hangen aan "delegate" (bug 397367)
- [KCM GridDelegate] laageffect alleen gebruiken op OpenGL backend (bug 397220)

### KDocTools

- acroniem ASCII toevoegen aan general.entities
- JSON toevoegen aan general.entities
- meinproc5 uitvoeriger rapporteren in automatische test van 'install'
- In test met kdoctools_install absolute paden gebruiken om geïnstalleerde bestanden te vinden

### KFileMetaData

- enum-alias Property::Language toevoegen voor typo Property::Langauge

### KHolidays

- Juiste berekeningen van equinox en solstice algoritmen implementeren (bug 396750)
- src/CMakeLists.txt - headers installeren op de manier van frameworks

### KI18n

- Toekennen bij proberen om een KCatalog te gebruiken zonder een QCoreApplication
- ki18n uit QtScript overzetten naar QtQml
- De bouwmap voor po/ ook controleren

### KIconThemes

- Breeze als terugvalpictogramthema instellen

### KIO

- [KSambaShareData] spaties accepteren in ACL hostnaam
- [KFileItemListProperties] mostLocalUrl gebruiken voor capabilities
- [KMountPoint] "smb-share" ook controleren voor of het een SMB-aankoppeling is (bug 344146)
- [KMountPoint] gvfsd aankoppelingen oplossen (bug 344146)
- [KMountPoint] resten van supermount verwijderen
- [KMountPoint] ondersteuning van AIX en Windows CE verwijderen
- Aangekoppeld bestandssysteemtype en velden met aangekoppeld van tonen in eigenschappendialoog (bug 220976)
- kdirlistertest mislukt niet willekeurig
- [KUrlComboBox] KIcon fut bij overzetten repareren
- KPATH_SEPARATOR "hack" overbrengen naar QDir::listSeparator, toegevoegd in Qt 5.6
- Geheugenlek gerepareerd in KUrlComboBox::setUrl
- [KFileItem] Mapcommentaar niet lezen bij langzaam aankoppelen
- QDir::canonicalPath in plaats gebruiken
- NTFS verborgenvlag negeren voor root-volume (bug 392913)
- De dialoog "ongeldige mapnaam" een knop Annuleren geven
- KPropertiesDialog: omschakelen naar label in setFileNameReadOnly(true)
- Verfijn de verwoording wanneer een map met een ongeldige naam niet aangemaakt kan worden
- Het toepasselijke pictogram gebruiken voor een knop Annuleren die zal vragen om een nieuwe naam
- Alleen-lezen bestandsnamen selecteerbaar maken
- Type hoofd-/kleine letter gebruiken voor enige labels van knoppen
- KLineEdit gebruiken voor mapnaam als map schrijftoegang heeft, gebruik anders QLabel
- KCookieJar: verkeerde timezone conversie repareren

### Kirigami

- fillWidth voor items ondersteunen
- tegen externe verwijdering van pagina's waken
- altijd de header tonen wanneer we in modus ingevouwen zijn
- gedrag van showContentWhenCollapsed repareren
- gaten in menu's in Material stijl repareren
- standaard actiemenu voor de pagina contextmenu
- Vraag expliciet om QtQuick van Qt 5.7 om gebruik te maken van Connections.enabled
- Vensterkleur gebruiken in plaats van een achtergronditem
- zorg ervoor dat de la sluit zelfs bij opdringen van een nieuwe
- separatorvisible exporteren naar de globale werkbalk
- De Kirigami QRC statische plug-in generatie repareren
- Het bouwen in LTO statische modus repareren
- Maak zeker dat eigenschap drawerOpen juist gesynchroniseerd wordt (bug 394867)
- drawer: de inhoudwidget tonen bij slepen
- qrc-assets toestaan om gebruikt te worden in actiepictogrammen
- ld bij oude gcc (bug 395156)

### KItemViews

- Markeer KFilterProxySearch als verouderd

### KNewStuff

- providerId in cache zetten

### KNotification

- libcanberra ondersteunen voor audiomeldingen

### KService

- KBuildSycoca: behandel desktopbestanden van toepassingen altijd

### KTextEditor

- enum Kate::ScriptType in een enum-klasse instellen
- foutbehandeling voor QFileDevice en KCompressedDevice corrigeren
- InlineNotes: inline notities niet afdrukken
- QSaveFile verwijderen ten gunste van gewoon bestand opslaan
- InlineNotes: globale coördinaten overal gebruiken
- InlineNote: positie initialiseren met Cursor::invalid()
- InlineNote: Pimpl inline notitiegegevens zonder allocs
- Inline notitie-interface toevoegen
- Tekstvoorbeeld alleen tonen als hoofdvenster actief is (bug 392396)
- Crash repareren bij verbergen van het TextPreview-widget (bug 397266)
- ssh://git.kde.org/ktexteditor samenvoegen
- hidpi rendering van pictogramrand
- Vim kleurenthema verbeteren (bug 361127)
- Search: workaround toevoegen voor ontbrekende pictogrammen in pictogramthema van Gnome
- overschrijven van _ of letters zoals j in de laatste regel (bug 390665)
- Scripting API uitbreiden om uitvoeren van commando's toe te staan
- Inspringscript voor R
- Crash repareren bij vervangen van n rond lege regels (bug 381080)
- accentuering van downloaddialoog verwijderen
- geen noodzaak om nieuw/verwijder hash bij elke doHighlight, het wissen is goed genoeg
- zorg ervoor dat we ongeldige attribuutindexen kunnen behandelen die kunnen gebeuren als resten na HL omschakelen voor een document
- slimme aanwijzer verwijdering van objects laten behandelen, minder handmatige zaken te doen
- map om extra hl-eigenschappen op te zoeken verwijderen
- KTextEditor gebruikt het KSyntaxHighlighting framework voor alles
- tekencoderingen gebruiken zoals geleverd door de definities
- Branch 'master' in syntax-highlighting mengen
- niet-vette tekst rendert niet langer met lettertypegewicht dun maar (bug 393861)
- foldingEnabled gebruiken
- EncodedCharaterInsertionPolicy verwijderen
- Afdrukken: respecteer lettertype van voettekst, verticale positie van voettekst repareren, scheidingslijn van koptekst/voettekst visueel lichter maken
- Branch 'master' in syntax-highlighting mengen
- framework voor syntaxisaccentuering alle definitiebeheer laten behandelen nu dat er de Geen definitie in de opslagruimte aanwezig is
- widget voor voltooien: minimale grootte van sectie repareren
- Reparatie: kijklijnen schuiven in plaats van echte lijnen voor schuiven met wiel en touchpad (bug 256561)
- syntaxistest verwijderen, die nu wordt getest in het framework voor accentuering van syntaxis zelf
- Configuratie van KTextEditor is nu weer lokaal voor de toepassing, de oude globale configuratie zal bij eerste gebruik geïmporteerd worden
- KSyntaxHighlighting::CommentPosition gebruiken in plaats van KateHighlighting::CSLPos
- isWordWrapDelimiter() gebruiken uit KSyntaxHighlighting
- isDelimiter() hernoemen naar isWordDelimiter()
- meer op te zoeken zaken implementeren via formaat -&gt; definitiekoppeling
- we krijgen nu altijd geldige formaten
- nettere manier om attribuutnaam te krijgen
- python test van indenteren, veiligere toegang tot eigenschapzakken
- juiste definitieprefixen opnieuw toevoegen
- Branch van 'syntax-highlighting' van git://anongit.kde.org/ktexteditor mengen met syntax-highlighting
- lijsten terugbrengen proberen nodig om de configuratie per schema te doen
- Use KSyntaxHighlighting::Definition::isDelimiter() gebruiken
- make kan een beetje meer breken zoals in woordcode
- geen gekoppelde lijst zonder enige reden
- initialisatie van eigenschappen opschonen
- volgorde van formaten repareren, herinner definitie in accentueringszak
- ongeldige formaten / nul-lengte formaten behandelen
- meer oude implementatie-onderdelen verwijderen, enkele toegangen repAreren om de dingen voor formaat te gebruiken
- op inspringen gebaseerd invouwen repareren
- onbedoeld tonen van contextstapel in doHighlight + reparatie ctxChanged verwijderen
- zaken voor invouwen beginnen om op te slaan
- accentueringhelpers uitrukken, niet langer nodig
- noodzaak van contextNum verwijderen, FIXME-SYNTAX markering toevoegen aan zAken die het nodig hebben om netjes opgeschoond te worden
- aanpassen aan wijzigingen in includedDefinitions, contextForLocation verwijderen, alleen sleutelwoorden of spellingcontrole voor locatie is nodig, kan later geïmplementeerd worden
- meer niet langer gebruikte syntaxisaccentueringszaken verwijderen
- de m_additionalData opschonen en de mapping een beetje, zou moeten werken voor attributen, niet voor inhoud
- initiële attributen aanmaken, nog steeds zonder echte attribuutwaarden, alleen een lijst van iets
- de accentuering aanroepen
- afleiden uit abstracte accentuering, definitie instellen

### KWallet Framework

- Voorbeeld uit techbase naar eigen repo verplaatsen

### KWayland

- Set/send/update methoden synchroniseren
- Serienummer en EISA-ID toevoegen aan OutputDevice interface
- Kleurenkrommecorrectie van uitvoerapparaat
- Geheugenbeheer repareren in WaylandOutputManagement
- Elke test binnen WaylandOutputManagement isoleren
- Fractionele schaling van OutputManagement

### KWidgetsAddons

- Een eerste voorbeeld van het gebruik van KMessageBox aanmaken
- Twee bugs in KMessageWidget repareren
- [KMessageBox] stijl voor pictogram aanroepen
- Workaround toevoegen voor labels met woordafbreking (bug 396450)

### KXMLGUI

- Konqi er goed uit laten zien in HiDPI
- Ontbrekende haakjes toevoegen

### NetworkManagerQt

- NetworkManager 1.4.0 en nieuwer vereisen
- manager: ondersteuning aan R/W de GlobalDnsConfiguration eigenschap toevoegen
- Sta echt toe om de verversingssnelheid voor statistieken van apparaten in te stellen

### Plasma Framework

- Workaround voor bug met inheemse rendering en dekking in TextField tekst (bug 396813)
- [Icon Item] KIconLoader pictogram wijziging bekijken bij gebruik van QIcon (bug 397109)
- [Icon Item] ItemEnabledHasChanged gebruiken
- Gebruik van verouderde QWeakPointer weghalen
- Stylesheet voor 22-22-system-suspend repareren (bug 397441)
- Verwijderen van  Widgets verbeteren en tekst configureren

### Solid

- solid/udisks2: ondersteuning voor gecategoriseerde logging
- [Windows Device] apparaatlabel alleen tonen als er een is
- Opnieuw evalueren van Predicates afdwingen als interfaces verwijderd worden (bug 394348)

### Sonnet

- hunspell: bouwen met hunspell &lt;=v1.5.0 herstellen
- hunspell-headers invoegen als systeem-includes

### syndication

Nieuwe module

### Accentuering van syntaxis

- 20000 regels accentueren per testgeval
- benchmark voor accentuering meer reproduceerbaar maken, we willen in elk geval dit uitvoeren meten van buitenaf met bijv. perf
- KeywordList opzoeken afregelen &amp; allocaties voor impliciete vangstgroep vermijden
- vangsten voor Int verwijderen, nooit geïmplementeerd
- deterministische iteratie van testen voor beter vergelijken van resultaten
- geneste include-attributen behandelen
- Modula-2 accentuering bijwerken (bug 397801)
- attribuutformaat vooraf berekenen voor context &amp; regels
- Controle op woordscheiding vermijden bij begin van trefwoord (bug 397719)
- Syntaxisaccentuering voor SELinux kernel-policy-taal
- bestCandidate verbergen, kan statische functie zijn binnen bestand
- Enige verbeteringen toevoegen aan kate-syntax-highlighter voor gebruik in scripts
- := toegevoegd als een geldig onderdeel van een identifier
- uw eigen invoergegevens gebruiken voor benchmarking
- probleem met beëindigen van regel proberen te repareren in vergelijken van resultaten
- triviale diff-uitvoer voor Windows proberen
- defData opnieuw toevoegen voor geldige controle op status
- ruimte voor StateData verkleinen met meer dan 50% en de helft van het aantal benodigde mallocs
- prestatie verbeteren van Rule::isWordDelimiter en KeywordListRule::doMatch
- Behandeling van overslaan van offset verbeteren, sta overslaan van volledige regel toe bij geen overeenkomst
- jokerlijst van extensies controleren
- meer sterretje hl, ik probeerde enige configuraties met sterretjes, ze zijn gewoon ini-stijl, gebruik .conf als einde van ini
- accentuering voor #ifdef _xxx zaken repareren (bug 397766)
- jokertekens in bestanden repareren
- opnieuw licensering met MIT van KSyntaxHighlighting gedaan
- JavaScript: binaire bestanden toevoegen, octale getallen repareren, escapes verbeteren &amp; niet-ASCII identifiers toestaan (bug 393633)
- Uitzetten van opzoeken van de QStandardPaths toestaan
- Installeren van syntaxis bestanden toestaan in plaats van ze in een hulpbron te hebben
- attributen in context omschakelen van de contexten zelf behandelen
- wijzig van statisch lib naar object lib met juiste pic-instelling, zou moeten werken voor gedeelde + statische builds
- elke heap-allocatie vermijden voor standaard geconstrueerde Format() zoals gebruikt als "invalid"
- cmake-variabele honoreren voor statisch vs. dynamisch lib, zoals bijv. karchive
- opnieuw MIT licentie geven, https://phabricator.kde.org/T9455
- oude add_license-script verwijderen, niet langer nodig
- includedDefinitions repareren, behandel wijziging in definitie in context omschakelen (bug 397659)
- SQL: verschillende verbeteringen en detectie met SQL van if/case/loop/end repareren (Oracle)
- referentiebestanden repareren
- SCSS: syntaxis bijwerken. CSS: accentuering van operator- en selector-tag repareren
- debchangelog: Bookworm toevoegen
- Wijzig licentie van Dockerfile naar MIT licentie
- het niet langer ondersteunde configuratiedeel van de spellingcontrole die altijd niet meer dan één modus had die we nu hard coderen
- Ondersteuning voor syntaxisaccentuering voor Stan toevoegen
- back-indenter toevoegen
- Veel bestanden met syntaxisaccentuering optimaliseren en het teken '/' van SQL repareren
- Modelines: markering van volgorde van bytes toevoegen &amp; kleine reparaties
- Wijzig licentie van modelines.xml naar MIT licentie (bug 198540)
- Toevoegen QVector&lt;QPair&lt;QChar, QString&gt;&gt; Definition::characterEncodings() const
- Toevoegen bool Definition::foldingEnabled() const
- Standaard "None"-accentuering toevoegen aan opslagruimte
- Ondersteuning van taalsyntaxis van Logtalk bijwerken
- Autodesk EAGLE sch- en brd-bestandsformaat toevoegen aan de XML categorie
- C# accentuering: prefereer C-Stijl indentering
- AppArmor: syntaxis bijwerken en verschillende verbeteringen/reparaties
- Java: binaries &amp; hex-float toevoegen, en underscores in getallen ondersteunen (bug 386391)
- Cleanup: indentering was verplaatst van sectie algemeen naar taal
- Definitie: commandomarkeringen blootstellen
- Accentuering van JavaScript React toevoegen
- YAML: sleutels repareren, nummers toevoegen en andere verbeteringen (bug 389636)
- bool Definition::isWordWrapDelimiter(QChar) toevoegen
- Definitie: isDelimiter() hernoemen naar isWordDelimiter()
- Bemerk KF6 API ideeën voor verbetering uit de overbrenging van KTE
- Biedt ook een geldig formaat voor lege regels
- Laat Definition::isDelimiter() ook werken voor ongeldige definities
- Definitie: bool isDelimiter() const blootstellen
- Teruggegeven formaten sorteren in Definition::formats() op id

### Beveiligingsinformatie

De vrijgegeven code is ondertekend met GPG met de volgende sleutel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Vingerafdruk van primaire sleutel: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB

U kunt discussiëren en ideeën delen over deze uitgave in de section voor commentaar van <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>het artikel in the dot</a>.
