---
aliases:
- ../../kde-frameworks-5.73.0
date: 2020-08-01
layout: framework
libCount: 70
---
### Breeze pictogrammen

+ 16px help-over en help-wat-is-dit centreren
+ Kirigami-galerijpictogram toevoegen
+ Kontrast-pictogram toevoegen
+ Pictogram voor mime-type applications/pkcs12 toevoegen
+ Zorgen dat Breeze-dark pictogrammen voor audiovolume overeenkomen met Breeze
+ Zorgen dat microfoonstijl meer consistent is met andere audiopictogrammen en op pixel uitgelijnd
+ 35% dekking voor vervaagde golven in geluidsvolumepictogrammen gebruiken en maak gedempte golven vervaagd
+ Geluid-uit en geluidsvolume-gedempt hetzelfde maken
+ Vastklikhoekpictogram toevoegen
+ 22px application-x-ms-shortcut laten wijzen naar een 16px pictogram repareren
+ Inconsistenties in mime-typenpictogrammen tussen licht/donker repareren
+ Onzichtbare licht/donker verschillen in vscode en wayland pictogrammen
+ Pictogram document-vervangen toevoegen (zoals voor actie overschrijven)
+ Sjabloon toevoegen voor het maken van python-scripts die SVG's bewerken
+ Pictogram voor SMART-status toevoegen (bug 423997)
+ Pictogrammen voor terugkerende taken en terugkerende afspraken toevoegen (bug 397996)

### Extra CMake-modules

+ ecm_generate_dbus_service_file toevoegen
+ ecm_install_configured_file toevoegen
+ Wayland_DATADIR exporteren

### KActivitiesStats

+ Ignore BoostConfig.cmake negeren indien aanwezig (bug 424799)

### KCMUtils

+ Accentueringsindicator voor QWidget en op QtQuick gebaseerde module ondersteunen
+ Methode om plug-indelector te wissen toevoegen (bug 382136)

### KConfig

+ sGlobalFileName bijwerken wanneer QStandardPaths TestMode is omgeschakeld
+ API dox: noem expliciet verwachte codering voor KConfig sleutel &amp; groepsnamen

### KConfigWidgets

+ KCModule: aangeven wanneer een instelling uit de standaard waarde is gewijzigd
+ Bij resetten naar systeemstandaard het standaard palet van de stijl niet gebruiken

### KCoreAddons

+ Introduceer KRandom::shuffle(container)

### KDeclarative

+ SettingStateBinding : laat zien of niet standaard accentuering is ingeschakeld
+ Verzekeren dat KF5CoreAddons is geïnstalleerd alvorens KF5Declarative te gebruiken
+ KF5::CoreAddons toevoegen aan publieke interface voor KF5::QuickAddons
+ SettingState* elementen introduceren om KCM schrijven te vergemakkelijken
+ configuratiemelding in configpropertymap ondersteunen

### KDE GUI-addons

+ KCursorSaver verplaatsen van libkdepim, verbeterd

### KImageFormats

+ Licentie aanpassen naar LGPL-2.0-of-later

### KIO

+ KFilterCombo: application/octet-stream is ook hasAllFilesFilter
+ OpenOrExecuteFileInterface introduceren voor behandeling van openen van uitvoerbare bestanden 
+ RenameDialog: als bestanden identiek zijn dat tonen (bug 412662)
+ [rename dialog] overschrijfknop overzetten naar KStandardGuiItem::Overwrite (bug 424414)
+ Tot drie bestands-itemacties inline tonen, niet slechts één (bug 424281)
+ KFileFilterCombo: extensies tonen als er herhaalde MIME-commentaar is
+ [KUrlCompletion] geen / achtervoegen aan aangevulde mappen
+ [Properties] SHA512 algoritme toevoegen aan controlesomwidget (bug 423739)
+ [WebDav] kopieën repareren die overschrijvingen bevatten voor de webdav-slave (bug 422238)

### Kirigami

+ zichtbaarheid van acties ondersteunen in GlobalDrawer
+ FlexColumn-component introduceren
+ Optimalisaties van indeling mobiel
+ Een laag moet de tabbladbalk ook dekken
+ PageRouter: vooraf geladen pagina's echte gebruiken bij opdrukken
+ (Abstract)ApplicationItem documenten verbeteren
+ Segfault repareren bij PageRouter afbreken
+ ScrollablePage schuifbaar maken met toetsenbord
+ Toegankelijkheid van de Kirigami invoervelden repareren
+ Het statisch bouwen repareren
+ PageRouter: API's voor gemak toevoegen voor anders handmatige taken
+ PageRouter lifecycle API's introduceren
+ Een lage Z volgorde van ShadowedRectangle's software fallback afdwingen

### KItemModels

+ KSelectionProxyModel: gebruik van het model toestaan met nieuwe stijl verbinden
+ KRearrangeColumnsProxyModel: hasChildren() repareren wanneer nog geen kolommen zijn ingesteld

### KNewStuff

+ [QtQuick dialog] bijwerkpictogram dat bestaat gebruiken (bug 424892)
+ [QtQuick GHNS dialog] keuzelijstlabels verbeteren
+ De downloaditemsselector niet tonen als er slechts dan één te downloaden item is
+ De indeling iets herschikken voor een meer gebalanceerd uiterlijk
+ Indeling voor de EntryDetails statuskaart repareren
+ Alleen koppelen aan Core, niet aan de quick plug-in (omdat dat niet zal werken)
+ Enig mooier maken van het welkomstscherm (en geen qml-dialogen gebruiken)
+ De dialoog gebruiken wanneer een knsrc-bestand wordt doorgeven, anders niet
+ De knop uit het hoofdvenster verwijderen
+ Een dialoog toevoegen (voor direct tonen van de dialoog bij doorgeven van een bestand)
+ De nieuwe bits toevoegen aan de hoofdtoepassing
+ Een eenvoudig (ten minste voor nu) model toevoegen voor tonen van knsrc-bestanden
+ Het hoofd qml-bestand in een qrc verplaatsen
+ De testhulpmiddel van dialoog voor KNewStuff in een echt hulpbron omvormen
+ Bij te werken items  toestaan te worden verwijderd (bug 260836)
+ Niet automatisch eerste element focus geven (bug 417843)
+ Tonen van details- en installatie ongedaan maken-knop in weergavemodus van tegels (bug 418034)
+ Knoppen verplaatsen repareren wanneer zoektekst is ingevoegd (bug 406993)
+ Ontbrekende parameter voor vertaalde tekenreeks repareren
+ Details afrol voor installatie repareren in QWidgets dialoog (bug 369561)
+ Tekstballonnen toevoegen voor verschillende weergavemodi in QML dialoog
+ Item instellen op niet geïnstalleerd als installatie mislukt (bug 422864)

### KQuickCharts

+ Ook met modeleigenschappen rekening houden bij gebruik van ModelHistorySource

### KRunner

+ Bewaker van KConfig implementeren voor ingeschakelde plug-ins en runner van KCM's (bug 421426)
+ Geen virtuele methode uit build verwijderen (bug 423003)
+ AbstractRunner::dataEngine(...) afkeuren
+ Uitgeschakelde runners en runner-configuratie voor plasmoid repareren
+ Waarschuwingen uitzenden bij overzetten van metagegevens uitstellen tot KF 5.75

### KService

+ Overladen toevoegen om terminal aan te roepen met ENV-variabelen (bug 409107)

### KTextEditor

+ Pictogrammen aan alle knoppen van bericht bestand gewijzigd toevoegen (bug 423061)
+ De canonieke docs.kde.org URL's gebruiken

### KWallet Framework

+ betere naam gebruiken en HIG volgen
+ API als afgekeurd markeren ook in beschrijving van D-Bus-interface
+ Kopie van org.kde.KWallet.xml toevoegen zonder afgekeurde API

### KWayland

+ plasma-window-management: aanpassen aan wijzigingen in het protocol
+ PlasmaWindowManagement: wijzigingen in het protocol adopteren

### KWidgetsAddons

+ KMultiTabBar: tabbladpictogrammen actief tekenen bij muis erboven zweven
+ KMultiTabBar repareren om het pictogram verschoven te tekenen on omlaag/geactiveerd door stijlontwerp
+ Nieuw overschrijfpictogram gebruiken voor overschrijf GUI-item (bug 406563)

### KXMLGUI

+ KXmlGuiVersionHandler::findVersionNumber publiek maken in KXMLGUIClient

### NetworkManagerQt

+ Loggen van geheimen verwijderen

### Plasma Framework

+ Eigenaarschap van cpp instellen op het exemplaar van eenheden
+ Enige ontbrekende te importeren PlasmaCore's toevoegen
+ Gebruik van contexteigenschappen voor thema's en eenheden verwijderen
+ PC3: uiterlijk van het menu verbeteren
+ Audiopictogrammen opnieuw aanpassen om overeen te komen met breeze-pictogrammen
+ showTooltip() aanroepbaar maken vanuit QML
+ [PlasmaComponents3] eigenschap icon.[name|source] honoreren
+ Op formfactors filteren indien ingesteld
+ Pictogram voor dempen bijwerken om met breeze-pictogrammen overeen te komen
+ Gebroken registratie van metatype voor typen waarbij de "*" in de naam ontbreekt
+ 35% dekking gebruiken voor vervaagde elementen in de netwerkpictogrammen
+ Plasma-dialogen niet tonen in taakschakelaars (bug 419239)
+ QT Bug URL voor font-rendering-hack corrigeren
+ Handcursor niet gebruiken omdat het niet consistent is
+ [ExpandableListItem] standaard knopgrootte gebruiken
+ [PlasmaComponents3] focus van ToolButton-effect tonen en laat schaduw weg bij vlak
+ Hoogte van PC3-knoppen, tekstvelden en keuzevakken unificeren
+ [PlasmaComponents3] ontbrekende mogelijkheden aan tekstveld toevoegen
+ [ExpandableListItem] rare fout repareren
+ button.svg herschrijven om het gemakkelijker te begrijpen
+ DataEngine relays kopiëren voor itereren (bug 423081)
+ Signaalsterkte in netwerkpictogrammen zichtbaarder maken (bug 423843)

### QQC2StyleBridge

+ Stijl "raised" gebruiken voor niet-vlakke hulpmiddelknoppen
+ Ruimte voor het pictogram in hulpmiddelknop reserveren als we echt een pictogram hebben
+ Tonen van een menupijl op hulpmiddelknoppen ondersteunen
+ Hoogte van menuscheiding bij hoge DPI echt repareren
+ Mainpage.dox bijwerken
+ Hoogte van MenuSeparator op de juiste manier zetten (bug 423653)

### Solid

+ m_deviceCache wissen vóór opnieuw introspectie (bug 416495)

### Accentuering van syntaxis

+ DetectChar converteren naar juiste Detect2Chars
+ Mathematica: enige verbeteringen
+ Doxygen: enige fouten gerepareerd ; DoxygenLua: starts alleen met --- or --!
+ AutoHotkey: volledig herschreven
+ Nim: toelichting repareren
+ Syntaxisaccentuering voor Nim toevoegen
+ Schema: identifier repareren
+ Schema: datumcommentaar toevoegen, geneste-commentaar en andere verbeteringen
+ Enige RegExp door AnyChar, DetectChar, Detect2Chars of StringDetect vervangen
+ language.xsd: HlCFloat verwijderen en char-type introduceren
+ KConfig: $(...) en operators repareren + enige verbeteringen
+ ISO C++: prestaties in geaccentueerde nummers repareren
+ Lua: attribuut met Lua54 en enige andere verbeteringen
+ RegExpr=[.]{1,1} vervangen door DetectChar
+ PureScript accentuering toevoegen gebaseerd op Haskell regels. Het geeft een tamelijk goede benaderings- en startpunt voor PureScript
+ README.md: de canonieke docs.kde.org URL's gebruiken

### Beveiligingsinformatie

De vrijgegeven code is ondertekend met GPG met de volgende sleutel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Vingerafdruk van primaire sleutel: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
