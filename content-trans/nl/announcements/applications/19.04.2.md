---
aliases:
- ../announce-applications-19.04.2
changelog: true
date: 2019-06-06
description: KDE stelt Applicaties 19.04.2 beschikbaar
layout: application
major_version: '19.04'
release: applications-19.04.2
title: KDE stelt KDE Applicaties 19.04.2 beschikbaar
version: 19.04.2
---
{{% i18n_date %}}

Vandaag heeft KDE de tweede stabiele update vrijgegeven voor <a href='../19.04.0'>KDE Applicaties 19.04</a> Deze uitgave bevat alleen reparaties van bugs en bijgewerkte vertalingen, die een veilige en plezierige update voor iedereen levert.

Bijna 50 aangegeven reparaties van bugs, die verbeteringen bevatten aan Kontact, Dolphin, Juk, Kdenlive, KmPlot, Okular, Spectacle, naast andere.

Verbeteringen bevatten:

- Een crash bij bekijken van bepaalde EPUB-documenten in Okular is gerepareerd
- Geheime sleutels kunnen opnieuw geëxporteerd worden vanuit de beheerder van versleuteling Kleopatra
- De gebeurtenismelder KAlarm mislukt niet langer om te starten met de nieuwste PIM-bibliotheken
