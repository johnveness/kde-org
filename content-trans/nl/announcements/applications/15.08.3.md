---
aliases:
- ../announce-applications-15.08.3
changelog: true
date: 2015-11-10
description: KDE stelt KDE Applicaties 15.08.3 beschikbaar
layout: application
title: KDE stelt KDE Applicaties 15.08.3 beschikbaar
version: 15.08.3
---
10 november 2019. Vandaag heeft KDE de derde stabiele update vrijgegeven voor <a href='../15.08.0'>KDE Applicaties 15.08</a> Deze uitgave bevat alleen reparaties van bugs en updates van vertalingen, die een veilige en plezierige update voor iedereen levert.

Meer dan 20 aangegeven bugreparaties inclusief verbeteringen aan ark, dophin, kdenlive, kdepim, kig, lokalize en umbrello.

Deze uitgave bevat ook ondersteuning op de lange termijn versies van KDE Development Platform 4.14.14.
