---
aliases:
- ../announce-applications-15.08.1
changelog: true
date: 2015-09-15
description: KDE stelt KDE Applicaties 15.08.1 beschikbaar
layout: application
title: KDE stelt KDE Applicaties 15.08.1 beschikbaar
version: 15.08.1
---
15 september 2015. Vandaag heeft KDE de eerste stabiele update vrijgegeven voor <a href='../15.08.0'>KDE Applicaties 18.08</a> Deze uitgave bevat alleen bugreparaties en updates van vertalingen, die een veilige en plezierige update voor iedereen levert.

Meer dan 40 aangegeven bugreparaties inclusief verbeteringen aan kdelibs, kdepim, kdenlive, dolphin, marble, kompare, konsole, ark en umbrello.

Deze uitgave bevat ook ondersteuning op de lange termijn versies van KDE Development Platform 4.14.12.
