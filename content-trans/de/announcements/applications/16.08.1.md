---
aliases:
- ../announce-applications-16.08.1
changelog: true
date: 2016-09-08
description: KDE veröffentlicht die KDE-Anwendungen 16.08.1
layout: application
title: KDE veröffentlicht die KDE-Anwendungen 16.08.1
version: 16.08.1
---
08. September 2016. Heute veröffentlicht KDE die erste Aktualisierung der <a href='../16.08.0'>KDE-Anwendungen 16.08</a>. Diese Veröffentlichung enthält nur Fehlerkorrekturen und aktualisierte Übersetzungen und ist daher für alle Benutzer eine sichere und problemlose Aktualisierung.

Mehr als 45 aufgezeichnete Fehlerkorrekturen enthalten unter anderem Verbesserungen für KDEPim, Kate, Kdenlive, Konsole, Marble, Kajongg, Kopete und Umbrello.

Diese Veröffentlichung enthält die Version der KDE Development Platform %1 mit langfristige Unterstützung.
