---
aliases:
- ../announce-applications-14.12.2
changelog: true
date: '2015-02-03'
description: KDE veröffentlicht die Anwendungen 14.12.2
layout: application
title: KDE veröffentlicht die KDE-Anwendungen 14.12.2
version: 14.12.2
---
03. Februar 2015. Heute veröffentlicht KDE die zweite Aktualisierung der <a href='../14.12.0'>KDE-Anwendungen 14.12</a>. Diese Veröffentlichung enthält nur Fehlerkorrekturen und aktualisierte Übersetzungen und ist daher für alle Benutzer eine sichere und problemlose Aktualisierung.

Mehr als 50 aufgezeichnete Fehlerkorrekturen enthalten Verbesserungen für das Anagramm-Spiel Kanagram , das UML-Programm Umbrello, den Dokumentbetrachter Okular, und den virtuellen Globus Marble.

This release also includes Long Term Support versions of Plasma Workspaces 4.11.16, KDE Development Platform 4.14.5 and the Kontact Suite 4.14.5.
