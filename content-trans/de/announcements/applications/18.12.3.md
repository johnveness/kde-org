---
aliases:
- ../announce-applications-18.12.3
changelog: true
date: 2019-03-07
description: KDE veröffentlicht die Anwendungen 18.12.2.
layout: application
major_version: '18.12'
release: applications-18.12.3
title: KDE veröffentlicht die KDE-Anwendungen 18.12.3
version: 18.12.3
---
{{% i18n_date %}}

Heute veröffentlicht KDE die erste Aktualisierung der <a href='../18.12.0'>KDE-Anwendungen 18.12</a>. Diese Veröffentlichung enthält nur Fehlerkorrekturen und aktualisierte Übersetzungen und ist daher für alle Benutzer eine sichere und problemlose Aktualisierung.

More than twenty recorded bugfixes include improvements to Kontact, Ark, Cantor, Dolphin, Filelight, JuK, Lokalize, Umbrello, among others.

Verbesserungen umfassen:

- Loading of .tar.zstd archives in Ark has been fixed
- Dolphin no longer crashes when stopping a Plasma activity
- Switching to a different partition can no longer crash Filelight
