---
aliases:
- ../announce-applications-16.12.1
changelog: true
date: 2017-01-12
description: KDE veröffentlicht die KDE-Anwendungen 16.12.1
layout: application
title: KDE veröffentlicht die KDE-Anwendungen 16.12.1
version: 16.12.1
---
12. Januar 2017. Heute veröffentlicht KDE die erste Aktualisierung der <a href='../16.12.0'>KDE-Anwendungen 16.12</a>. Diese Veröffentlichung enthält nur Fehlerkorrekturen und aktualisierte Übersetzungen und ist daher für alle Benutzer eine sichere und problemlose Aktualisierung.

This release fixes a DATA LOSS bug in the iCal resource which failed to create std.ics if it didn't exist.

Mehr als 40 aufgezeichnete Fehlerkorrekturen enthalten unter anderen Verbesserungen für kdepim, Ark, Gwenview, Kajongg, Okular, Kate und Kdenlive.

Diese Veröffentlichung enthält die Version der KDE Development Platform %1 mit langfristige Unterstützung.
