---
aliases:
- ../announce-applications-15.12-rc
date: 2015-12-03
description: KDE lanza la versión candidata de las Aplicaciones 15.12.
layout: application
release: applications-15.11.90
title: KDE lanza la candidata a versión final para las Aplicaciones 15.12
---
Hoy, 3 de diciembre de 2015, KDE ha lanzado la candidata a versión final de las nuevas Aplicaciones. Una vez congeladas las dependencias y las funcionalidades, el equipo de KDE se ha centrado en corregir errores y en pulir aún más la versión.

Debido a las numerosas aplicaciones que se basan en KDE Frameworks 5, es necesario probar la versión 15.12 de las Aplicaciones de manera exhaustiva con el fin de mantener y mejorar la calidad y la experiencia del usuario de KDE. Los usuarios reales son de vital importancia para mantener la alta calidad de KDE porque los desarrolladores no pueden probar todas las configuraciones posibles. Contamos con ustedes para ayudar a encontrar errores de manera precoz de forma que se puedan corregir antes de la versión final. Considere la idea de unirse al equipo instalando la versión <a href='https://bugs.kde.org/'>e informando de cualquier error que encuentre</a>.
