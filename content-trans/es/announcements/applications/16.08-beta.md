---
aliases:
- ../announce-applications-16.08-beta
date: 2016-07-22
description: KDE lanza la Beta de las Aplicaciones 16.08.
layout: application
release: applications-16.07.80
title: KDE lanza la beta para las Aplicaciones de KDE 16.08
---
Hoy, 22 de julio de 2016, KDE ha lanzado la versión beta de las Aplicaciones KDE. Una vez congeladas las dependencias y las funcionalidades, el equipo de KDE se ha centrado en corregir errores y en pulir aún más la versión.

Consulte las <a href='https://community.kde.org/Applications/16.08_Release_Notes'>notas de lanzamiento de la comunidad</a> para obtener información sobre nuevos paquetes que ahora se basan en KF5 y sobre los problemas conocidos. Se realizará un anuncio más completo para la versión final.

Los lanzamientos de las Aplicaciones de KDE 16.08 necesitan una prueba exhaustiva con el fin de mantener y mejorar la calidad y la experiencia del usuario. Los usuarios reales son críticos para mantener la alta calidad de KDE porque los desarrolladores no pueden probar todas las configuraciones posibles. Contamos con ustedes para ayudar a encontrar errores de manera temprana de forma que se puedan corregir antes de la versión final. Considere la idea de unirse al equipo instalando la beta <a href='https://bugs.kde.org/'>e informando de cualquier error que encuentre</a>.
