---
aliases:
- ../../kde-frameworks-5.69.0
date: 2020-04-05
layout: framework
libCount: 70
---
### Baloo

- [SearchStore] Use categorized logging
- [QueryParser] Fix broken detection of end quote
- [EngineQuery] Provide toString(Term) overload for QTest
- [EngineQuery] Remove unused position member, extend tests
- [SearchStore] Avoid long lines and function nesting
- [baloosearch] Bail out early if specified folder is not valid
- [MTimeDB] Consolidate time interval handling code
- [AdvancedQueryParser] Test if quoted phrases are passed correctly
- [Term] Proporcionar la sobrecarga de «toString(Term)» para QTest.
- [ResultIterator] Se ha eliminado la declaración anticipada innecesaria de «SearchStore».
- [QueryTest] Make phrase test case data driven and extend
- [Inotify] Start the MoveFrom expire timer at most once per inotify batch
- [UnindexedFileIndexer] Marcar archivos para indexar su contenido solo cuando sea necesario.
- [Inotify] Llamar a «QFile::decode» en un único lugar.
- [QML] Correctly watch for unregistration
- [FileIndexScheduler] Update the content index progress more often
- [FileIndexerConfig] Replace config QString,bool pair with dedicated class
- [QML] Set the remaining time in the monitor more reliably
- [TimeEstimator] Correct batch size, remove config reference
- [FileIndexScheduler] Emit change to LowPowerIdle state
- [Debug] Improve readability of positioninfo debug format
- [Debug] Correct output of *::toTestMap(), silence non-error
- [WriteTransactionTest] Test removal of positions only
- [WriteTransaction] Extend position test case
- [WriteTransaction] Avoid growing m_pendingOperations twice on replace
- [FileIndexScheduler] Cleanup firstRun handling
- [StorageDevices] Fix order of notification connect and initialization
- [Config] Remove/deprecate disableInitialUpdate

### Iconos Brisa

- Se han corregido enlaces simbólicos erróneos.
- Move corner fold to top right in 24 icons
- Make find-location show a magnifier on a map, to be different to mark-location (bug 407061)
- Se han añadido iconos de 16 píxeles para LibreOffice.
- Fix configure when xmllint is not present
- Fix stylesheet linking in 8 icons
- Fix some stylesheet colors in 2 icon files
- Fix symlinks to incorrect icon size
- Add input-dialpad and call-voicemail
- Se ha añadido el icono «buho».
- Add calindori icon in the new pm style
- Se ha añadido el icono «nota».
- [Iconos Brisa] Se ha corregido la sombra en algunos iconos de usuario (applets/128).
- Add call-incoming/missed/outgoing
- Handle busybox's sed like GNU sed
- Se ha añadido «transmission-tray-icon».
- Improve pixel alignment and margins of keepassxc systray icons
- Revertir «[Iconos Brisa] Se han añadido iconos de la bandeja del sistema para "telegram-desktop"».
- Se han añadido iconos pequeños para «KeePassXC».
- [Iconos Brisa] Se han añadido iconos de TeamViewer para la bandeja del sistema.
- Se ha añadido el icono «edit-reset».
- Change document-revert style to be more like edit-undo
- Iconos para las categorías e emoticonos.
- Se han añadido iconos «flameshot» para la bandeja del sistema.

### KAuth

- Fix type namespace requirement

### KBookmarks

- Decouple KBookmarksMenu from KActionCollection

### KCalendarCore

- Fix fallback to vCalendar loading on iCalendar load failure

### KCMUtils

- listen to passiveNotificationRequested
- workaround to never make applicationitem resize itself

### KConfig

- [KConfigGui] Check font weight when clearing styleName property
- KconfigXT: Add a value attribute to Enum field choices

### KCoreAddons

- kdirwatch: fix a recently introduced crash (bug 419428)
- KPluginMetaData: manejar los tipos MIME no válidos en «supportsMimeType».

### KCrash

- Move setErrorMessage definition out of the linux ifdef
- Allow providing an error message from the application (bug 375913)

### KDBusAddons

- Check correct file for sandbox detection

### KDeclarative

- Se ha introducido una API para notificaciones pasivas.
- [KCM Controls GridDelegate] Use <code>ShadowedRectangle</code>
- [kcmcontrols] Respect header/footer visibility

### KDocTools

- Use bold italic at 100% for sect4 titles, and bold 100% for sect5 titles (bug 419256)
- Se ha actualizado la lista de las entidades italianas.
- Use the same style for informaltable as for table (bug 418696)

### KIdleTime

- Fix infinite recursion in xscreensaver plugin

### KImageFormats

- Port the HDR plugin from sscanf() to QRegularExpression. Fixes FreeBSD

### KIO

- New class KIO::CommandLauncherJob in KIOGui to replace KRun::runCommand
- New class KIO::ApplicationLauncherJob in KIOGui to replace KRun::run
- File ioslave: use better setting for sendfile syscall (bug 402276)
- FileWidgets: Ignore Return events from KDirOperator (bug 412737)
- [DirectorySizeJob] Fix sub-dirs count when resolving symlinks to dirs
- Mark KIOFuse mounts as Probably slow
- kio_file: Respetar «KIO::StatResolveSymlink» para «UDS_DEVICE_ID» y para «UDS_INODE».
- [KNewFileMenu] Add extension to proposed filename (bug 61669)
- [KOpenWithDialog] Add generic name from .desktop files as a tooltip (bug 109016)
- KDirModel: implement showing a root node for the requested URL
- Register spawned applications as an independent cgroups
- Add "Stat" prefix to StatDetails Enum entries
- Windows: Permitir el uso de la fecha de creación de los archivos.
- KAbstractFileItemActionPlugin: Add missing quotes in code example
- Avoid double fetch and temporary hex encoding for NTFS attributes
- KMountPoint: Omitir intercambio.
- Assign an icon to action submenus
- [DesktopExecParser] Abrir URL {ssh,telnet,rlogin}:// con «ktelnetservice» (error 418258).
- Fix exitcode from kioexec when executable doesn't exist (and --tempfiles is set)
- [KPasswdServer] replace foreach with range/index-based for
- KRun's KProcessRunner: terminate startup notification on error too
- [http_cache_cleaner] replace foreach usage with QDir::removeRecursively()
- [StatJob] Use A QFlag to specify the details returned by StatJob

### Kirigami

- Hotfix for D28468 to fix broken variable refs
- Deshacerse de la incubadora.
- disable mousewheel completely in outside flickable
- Add property initializer support to PagePool
- Se ha refactorizado «OverlaySheet».
- Se han añadido los elementos «ShadowedImage» y «ShadowedTexture».
- [controls/formlayout] Don't attempt to reset implicitWidth
- Add useful input method hints to password field by default
- [FormLayout] Definir el intervalo del temporizador de compresión a 0.
- [UrlButton] Disable when there is no URL
- simplify header resizing (bug 419124)
- Remove export header from static install
- Fix about page with Qt 5.15
- Fix broken paths in kirigami.qrc.in
- Add "veryLongDuration" animation duration
- Se han corregido las notificaciones multifila.
- don't depend on window active for the timer
- Permitir el uso de múltiples notificaciones pasivas apiladas.
- Fix enabling border for ShadowedRectangle on item creation
- Comprobar la existencia de ventanas.
- Se han añadido los tipos que faltaban a «qrc».
- Se ha corregido una comprobación no definida en el modo menú del cajón global (error 417956).
- Fallback to a simple rectangle when using software rendering
- Fix color premultiply and alpha blending
- [FormLayout] Propagate FormData.enabled also to label
- Se ha añadido un elemento «ShadowedRectangle».
- Propiedad «alwaysVisibleActions».
- don't create instances when the app is quitting
- Don't emit palette changes if the palette didn't change

### KItemModels

- [KSortFilterProxyModel QML] Make invalidateFilter public

### KNewStuff

- Se ha corregido el diseño de «DownloadItemsSheet» (fallo 419535).
- [QtQuick dialog] Port to UrlBUtton and hide when there's no URL
- Switch to using Kirigami's ShadowedRectangle
- Fix update scenarios with no explicit downloadlink selected (bug 417510)

### KNotification

- Nueva clase «KNotificationJobUiDelegate».

### KNotifyConfig

- Use libcanberra as primary means of previewing the sound (bug 418975)

### KParts

- New class PartLoader as replacement to KMimeTypeTrader for parts

### KService

- KAutostart: Add static method to check start condition
- KServiceAction: store parent service
- Properly read the X-Flatpak-RenamedFrom string list from desktop files

### KTextEditor

- Permitir que se pueda compilar con Qt 5.15.
- fix folding crash for folding of single line folds (bug 417890)
- [VIM Mode] Add g&lt;up&gt; g&lt;down&gt; commands (bug 418486)
- Add MarkInterfaceV2, to s/QPixmap/QIcon/g for symbols of marks
- Draw inlineNotes after drawing word wrap marker

### KWayland

- [xdgoutput] Only send initial name and description if set
- Se ha añadido «XdgOutputV1» versión 2.
- Broadcast application menu to resources when registering them
- Proporcionar una implementación para la interfaz de tabletas.
- [server] Don't make assumptions about the order of damage_buffer and attach requests
- Pass a dedicated fd to each keyboard for the xkb keymap (bug 381674)
- [server] Introduce SurfaceInterface::boundingRect()

### KWidgetsAddons

- New class KFontChooserDialog (based on KFontDialog from KDELibs4Support)
- [KCharSelect] Do not simplify single characters in search (bug 418461)
- If we readd items we need to clear it first. Otherwise we will see duplicate list
- Se ha actualizado «kcharselect-data» a Unicode 13.0.

### KWindowSystem

- Fix EWMH non-compliance for NET::{OnScreenDisplay,CriticalNotification}
- KWindowSystem: deprecate KStartupInfoData::launchedBy, unused
- Expose application menu via KWindowInfo

### Framework de Plasma

- Se ha añadido el componente «Page».
- [pc3/busyindicator] Hide when not running
- Update window-pin, Add more sizes, Remove redundant edit-delete
- Create a new TopArea element using widgets/toparea svg
- Added plasmoid heading svg
- Make highlighted property work for roundbutton

### Prison

- Also expose the true minimum size to QML
- Add a new set of barcode size functions
- Se ha simplificado el manejo del tamaño mínimo.
- Move barcode image scaling logic to AbstractBarcode
- Add API to check whether a barcode is one- or two-dimensional

### QQC2StyleBridge

- [Dialog] Use <code>ShadowedRectangle</code>
- Se ha corregido el tamaño de «CheckBox» y de «RadioButton» (error 418447).
- Use <code>ShadowedRectangle</code>

### Solid

- [Fstab] Ensure uniqueness for all filesystem types
- Samba: Ensure to differentiate mounts sharing the same source (bug 418906)
- hardware tool: define syntax via syntax arg

### Sonnet

- Fix Sonnet autodetect failing on Indian langs
- Create ConfigView an unmanaged ConfigWidget

### Resaltado de sintaxis

- LaTeX: Se han corregido los paréntesis matemáticos en las etiquetas opcionales (fallo 418979).
- Add Inno Setup syntax, including embedded Pascal scripting
- Lua: add # as additional deliminator to activate auto-completion with <code>#something</code>
- C: Se ha eliminado ' como separador de dígitos.
- add some comment about the skip offset stuff
- optimize dynamic regex matching (bug 418778)
- fix regex rules wrongly marked as dynamic
- extend indexer to detect dynamic=true regexes that have no place holders to adapt
- Se ha añadido el resaltado de sintaxis para Overpass QL.
- Agda: keywords updated to 2.6.0 and fix float points

### Información de seguridad

El código publicado se ha firmado con GPG usando la siguiente clave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;. Huella digital de la clave primaria: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB.
