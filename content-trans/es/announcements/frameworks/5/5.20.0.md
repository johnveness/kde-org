---
aliases:
- ../../kde-frameworks-5.20.0
date: 2016-03-13
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Iconos Brisa

- Muchos iconos nuevos
- Añadir iconos del tipo MIME Virtualbox y otros tipos MIME que faltaban.
- Añadir compatibilidad con iconos de synaptic y octopi
- Corregir icono cortado (error 354061)
- Corregir el nombre audio-headphones.svg (+=d)
- Clasificar iconos con un margen más pequeño (1px)

### Integración con Frameworks

- Eliminar el posible nombre de archivo en KDEPlatformFileDialog::setDirectory()
- No filtrar por nombre si se dispone de tipos MIME

### KActivities

- Eliminar dependencia en Qt5::Widgets
- Eliminar dependencia en KDBusAddons
- Eliminar dependencia en «KI18n».
- Eliminar includes que no se utilizaban
- Mejorar la salida de los scripts del intérprete de línea de órdenes
- Añadir la funcionalidad de que el modelo de datos (ActivitiesModel) muestre las actividades a la biblioteca
- DE manera predeterminada, compilar solo la biblioteca
- Eliminar los componentes de servicio y espacios de trabajo de la compilación
- Mover la biblioteca de src/lib/core a src/lib
- Corregir advertencia de CMake
- Corregir fallo en el menú de contexto de actividades (error 351485)

### KAuth

- Se ha corregido un bloqueo de «kded5» cuando termina un programa que usa «kauth».

### KConfig

- KConfigIniBackend: Se ha corregido una separación costosa en la búsqueda.

### KCoreAddons

- Se ha corregido la migración de la configuración de «Kdelibs4» para Windows.
- Se ha añadido una API para obtener información de la versión de Frameworks en tiempo de ejecución.
- KRandom: No usar más de 16K de «/dev/urandom» para sembrar «rand()» (error 359485).

### KDeclarative

- No llamar a un puntero de objeto nulo (error 347962).

### KDED

- Hacer que se pueda compilar con -DQT_NO_CAST_FROM_ASCII

### Soporte de KDELibs 4

- Añadir gestión de sesiones a las aplicaciones basadas en KApplication (error 354724)

### KDocTools

- Usar caracteres Unicode para las llamadas.

### KFileMetaData

- KFileMetadata puede ahora consultar y almacenar información sobre el mensaje de correo electrónico original del que puede haber sido adjunto un archivo guardado.

### KHTML

- Se ha corregido la actualización del cursor en la vista.
- Limitar el uso de memoria de cadena
- Miniaplicación java de visor de HTML: reparar llamadas incompletas de DBus a kpasswdserver

### KI18n

- Utilizar macro de importación portable para nl_msg_cat_cntr
- Saltar la búsqueda de . y .. para encontrar las traducciones para una aplicación
- Restringir el uso de _nl_msg_cat_cntr en las implementaciones GNU de gettext
- Añadido KLocalizedString::languages()
- Realizar las llamadas Gettext solo si el catálogo se ha localizado

### KIconThemes

- Asegurarse de que la variable se ha inicializado

### KInit

- kdeinit: dar preferencia a bibliotecas de carga de RUNPATH
- Implementar "Qt5 TODO: usar QUrl::fromStringList"

### KIO

- Arreglar la interrupción de la conexión KIO app-slave si appName contiene «/» (error 357499)
- Probar varios métodos de autenticación en caso de fallos
- ayuda: corregir mimeType() en get()
- KOpenWithDialog: mostrar el nombre del tipo MIME en el texto de la casilla de verificación "Recordar" (error 110146)
- Varios cambios para evitar que se vuelva a listar un directorio después de que se cambio el nombre de un archivo (error 359596)
- http: cambiar el nombre de m_iError a m_kioError
- kio_http: leer y descartar el cuerpo después de un 404 con errorPage=false
- kio_http: corregir la determinación del tipo MIME cuando la URL acaba en «/»
- FavIconRequestJob: añadir acceso a hostUrl() de forma que konqueror pueda saber para qué era la tarea.
- FavIconRequestJob: Se ha corregido el manejo de trabajos cuando se interrumpe debido a un «favicon» demasiado grande.
- FavIconRequestJob: Se ha corregido «errorString()», solo tiene el URL.
- KIO::RenameDialog: Se ha restaurado la vista previa y se han añadido etiquetas para la fecha y el tamaño (error 356278).
- KIO::RenameDialog: Refactorizar código duplicado.
- Se han corregido las conversiones de la ruta a QUrl errónea.
- Usar «kf5.kio» en el nombre de categoría para coincidir con otras categorías.

### KItemModels

- KLinkItemSelectionModel: Se ha añadido un nuevo constructor predeterminado.
- KLinkItemSelectionModel: Hacer que se pueda seleccionar el modelo de selección enlazado.
- KLinkItemSelectionModel: Contemplar los cambios del modelo «selectionModel».
- KLinkItemSelectionModel: No almacenar el modelo localmente.
- KSelectionProxyModel: Se ha corregido un error de iteración.
- Reiniciar el estado de «KSelectionProxyModel» cuando sea necesario.
- Añadir una propiedad para indicar si el modelo es de una cadena conectada.
- KModelIndexProxyMapper: Simplificar la lógica de la comprobación de conexión.

### KJS

- Limitar el uso de memoria de cadena

### KNewStuff

- Mostrar una advertencia si ocurre un error en el motor.

### Framework para paquetes

- Permitir que «KDocTools» se opcional en «KPackage».

### KPeople

- Corregir uso obsoleto de API
- Se ha añadido «actionType» al complemento declarativo.
- Se ha invertido la lógica de filtrado en «PersonsSortFilterProxyModel».
- Hacer que el ejemplo QML sea algo más usable.
- Se ha añadido «actionType» al «PersonActionsModel».

### KService

- Se ha simplificado el código, se han reducido las desreferencias de punteros y se han realizado mejoras relacionadas con los contenedores.
- Se ha añadido el programa de pruebas «kmimeassociations_dumper», inspirado por el error 359850.
- Se ha corregido que las aplicaciones chromium/wine no se cargaran en algunas distribuciones (error 213972).

### KTextEditor

- Se ha corregido el resaltado de todas las coincidencias en «ReadOnlyPart».
- No iterar sobre una «QString» si se trata de una «QStringList».
- Inicializar correctamente los «QMaps» estáticos.
- Preferir «toDisplayString(QUrl::PreferLocalFile)».
- Permitir el envío de caracteres de sustitución desde el método de entrada.
- No fallar al apagar cuando todavía se está reproduciendo una animación.

### Framework KWallet

- Asegúrese de que se busque «KDocTools».
- No pasar un número negativo a DBus, ya que producirá una aserción en «libdbus».
- Limpiar los archivos cmake
- KWallet::openWallet(Synchronous): No agotar el tiempo tras 25 segundos.

### KWindowSystem

- Permitir el uso de «_NET_WM_BYPASS_COMPOSITOR» (fallo 349910).

### KXMLGUI

- Usar un nombre de idioma no nativo como último recurso al que recurrir.
- Corregir gestión de sesiones fallido desde KF5 / Qt5 (error 354724)
- Esquemas de accesos rápidos: Permitir los esquemas instalados globalmente.
- Usar «qHash(QKeySequence)» de Qt cuando se compila con Qt 5.6+.
- Esquemas de accesos rápidos: Se ha corregido un error en el que dos «KXMLGUIClients» con el mismo nombre sobrescriben el archivo de esquema del otro.
- kxmlguiwindowtest: Se ha añadido un diálogo de accesos rápidos para probar el editor de esquemas de accesos rápidos.
- Esquemas de accesos rápidos: Se ha mejorado la usabilidad cambiando textos en la interfaz gráfica.
- Esquemas de accesos rápidos: Se ha mejorado la lista desplegable de esquemas (tamaño automático, no borrar cuando se selecciona un esquema desconocido).
- Esquemas de accesos rápidos: No anteponer el nombre «guiclient» al nombre de archivo.
- Esquemas de accesos rápidos: Crear el directorio antes de intentar guardar un nuevo esquema de accesos rápidos.
- Esquemas de accesos rápidos: Restaurar el margen del diseño, de lo contrario se ve muy estrecho.
- Se ha corregido una fuga de memoria en el gancho de inicio de «KXmlGui».

### Framework de Plasma

- IconItem: No sobrescribir la fuente al usar «QIcon::name()».
- ContainmentInterface: Se ha corregido el uso de «right()» y «bottom()» de QRect.
- Elimine eficazmente la ruta de código duplicada para manejar «QPixmaps».
- Se ha añadido documentación de la API para «IconItem».
- Corregir la hoja de estilos (error 359345)
- No limpiar la máscara de ventana en cada cambio de geometría cuando la composición está activa y no se ha definido ninguna máscara.
- Miniaplicación: No fallar al eliminar el panel (error 345723).
- Tema: Descartar la caché de mapas de bits al cambiar el tema (error 359924).
- IconItemTest: Omitir cuando falla «grabToImage».
- IconItem: Se ha corregido el cambio de color de los iconos SVG cargados del tema de iconos.
- Se ha corregido la resolución de «iconPath» de SVG en «IconItem».
- Si se pasa una ruta, escoger su final (error 359902).
- Se han añadido las propiedades «configurationRequired» y «reason».
- Mover «contextualActionsAboutToShow» a «Applet».
- ScrollViewStyle: No usar los márgenes del elemento parpadeante.
- DataContainer: Se han corregido las comprobaciones de «slots» antes de conectar/desconectar.
- Ayuda emergente: Impedir múltiples cambios de geometría cuando se cambia el contenido.
- SvgItem: No usar «Plasma::Theme» desde el hilo de renderización.
- AppletQuickItem: Se ha corregido la búsqueda de un esquema propio adjunto (error 358849).
- Expansor más pequeño para la barra de tareas.
- Ayuda emergente: Dejar de mostrar el temporizador si se llama a «hideTooltip» (error 358894).
- Desactivar la animación de iconos en las ayudas emergentes de Plasma.
- Descartar las animaciones de las ayudas emergentes.
- El tema predeterminado sigue el esquema de color.
- Se ha corregido que «IconItem» no cargue iconos que no son del tema con un nombre (error 359388).
- Preferir otros contenedores distintos al escritorio en «containmentAt()».
- WindowThumbnail: Descartar mapa de píxeles «glx» en «stopRedirecting()» (error 357895).
- Eliminar el filtro de miniaplicaciones heredadas.
- ToolButtonStyle: No confiar en una ID externa.
- No asumir que se ha encontrado una corona (error 359026).
- Calendario: Se han añadido botones «atrás/adelante» correctos y el botón «Hoy» (errores 336124, 348362, 358536).

### Sonnet

- No desactivar la detección del idioma solo porque se haya definido un idioma.
- Desactivar la desactivación automática de la comprobación ortográfica automática de forma predeterminada.
- Se ha corregido «TextBreaks».
- Se ha corregido la falta del carácter «/» en la búsqueda del diccionario Hunspell (error 359866).
- Añadir &lt;app dir&gt;/../share/hunspell a la ruta de búsqueda de diccionarios.

Puede comentar esta versión y compartir sus ideas en la sección de comentarios de <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>el artículo de dot</a>.
