---
aliases:
- ../announce-applications-18.08.3
changelog: true
date: 2018-11-08
description: KDE rilascia KDE Applications 18.08.3
layout: application
title: KDE rilascia KDE Applications 18.08.3
version: 18.08.3
---
8 novembre 2018. Oggi KDE ha rilasciato il primo aggiornamento di stabilizzazione per <a href='../18.08.0'>KDE Applications 18.08</a>. Questo rilascio contiene solo correzioni di errori e aggiornamenti delle traduzioni, costituisce un aggiornamento sicuro e gradevole per tutti.

Circa venti errori corretti includono, tra gli altri, miglioramenti a Kontact, Ark, Dolphin, KDE Games, Kate, Okular e Umbrello.

I miglioramenti includono:

- La modalità di visualizzazione in KMail viene ricordata e, se consentite, vengono di nuovo caricate le immagini esterne
- Kate ora ricorda le informazioni aggiuntive (inclusi i segnalibri) tra le varie sessioni
- Lo scorrimento automatico nell'interfaccia testuale di Telepathy è stato corretto con le versioni più recenti di QtWebEngine
