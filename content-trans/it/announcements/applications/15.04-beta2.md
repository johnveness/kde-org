---
aliases:
- ../announce-applications-15.04-beta2
date: '2015-03-12'
description: KDE rilascia Applications 15.04 Beta 2.
layout: applications
title: KDE rilascia la seconda beta di KDE Applications 15.04
---
6 marzo 2015. Oggi KDE ha rilasciato la beta della nuova versione delle applicazioni KDE (Applications). Con il &quot;congelamento&quot; di dipendenze e funzionalità, l'attenzione degli sviluppatori KDE è adesso concentrata sulla correzione degli errori e sull'ulteriore rifinitura del sistema.

Dato che molte applicazioni sono ora basate su KDE Frameworks 5, i rilasci di KDE Applications 15.04 hanno bisogno di una verifica accurata per poter mantenere e migliorare la qualità e l'esperienza utente. Gli utenti &quot;reali&quot; sono fondamentali per mantenere la qualità di KDE, perché gli sviluppatori non possono testare completamente ogni possibile configurazione. Contiamo su di voi per aiutarci a trovare i bug al più presto possibile perché possano essere eliminati prima della versione finale. Valutate la possibilità di contribuire al gruppo di sviluppo installando la versione &quot;beta&quot; <a href='https://bugs.kde.org/'>e segnalando ogni problema</a>.
