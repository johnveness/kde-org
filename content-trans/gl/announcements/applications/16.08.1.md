---
aliases:
- ../announce-applications-16.08.1
changelog: true
date: 2016-09-08
description: KDE publica a versión 16.08.1 das aplicacións de KDE
layout: application
title: KDE publica a versión 16.08.1 das aplicacións de KDE
version: 16.08.1
---
September 8, 2016. Today KDE released the first stability update for <a href='../16.08.0'>KDE Applications 16.08</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

As máis de 45 correccións de erros inclúen melloras en, entre outros, KDE PIM, Kate, Kdenlive, Konsole, Marble, Kajongg, Kopete e Umbrello.

This release also includes Long Term Support version of KDE Development Platform 4.14.24.
