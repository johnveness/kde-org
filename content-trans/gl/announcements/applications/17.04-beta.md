---
aliases:
- ../announce-applications-17.04-beta
date: 2017-03-24
description: KDE Ships Applications 17.04 Beta.
layout: application
release: applications-17.03.80
title: KDE publica a beta da versión 17.04 das aplicacións de KDE
---
24 de marzo de 2017. Hoxe KDE publicou a beta da nova versión das súas aplicacións. Coa desautorización temporal de dependencias e funcionalidades novas, agora o equipo de KDE centrase en solucionar fallos e pulir funcionalidades.

Check the <a href='https://community.kde.org/Applications/17.04_Release_Notes'>community release notes</a> for information on new tarballs, tarballs that are now KF5 based and known issues. A more complete announcement will be available for the final release

Hai que probar ben a versión 17.04 das aplicacións de KDE para manter e mellorar a calidade e a experiencia de usuario. Os usuarios reais son críticos para manter unha alta calidade en KDE, porque os desenvolvedores simplemente non poden probar todas as configuracións posíbeis. Contamos con vostede para axudarnos a atopar calquera fallo canto antes para poder solucionalo antes da versión final. Considere unirse ao equipo instalando a beta <a href='https://bugs.kde.org/'>e informando de calquera fallo</a>.
