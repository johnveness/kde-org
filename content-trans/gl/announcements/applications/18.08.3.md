---
aliases:
- ../announce-applications-18.08.3
changelog: true
date: 2018-11-08
description: KDE publica a versión 18.08.3 das aplicacións de KDE
layout: application
title: KDE publica a versión 18.08.3 das aplicacións de KDE
version: 18.08.3
---
November 8, 2018. Today KDE released the third stability update for <a href='../18.08.0'>KDE Applications 18.08</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Arredor de 20 correccións de erros inclúen melloras en, entre outros, Kontact, Ark, Dolphin, KDE Games, Kate, Okular e Umbrello.

Entre as melloras están:

- KMail lembra o modo de visión de HTML e volve gardar as imaxes externas se se lle permite
- Kate agora lembra os metadatos (incluídos os marcadores) entre sesións de edición
- Corrixiuse o desprazamento automático na interface de usuario de texto de Telepathy con novas versións de QtWebEngine
