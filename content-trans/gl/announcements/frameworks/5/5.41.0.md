---
aliases:
- ../../kde-frameworks-5.41.0
date: 2017-12-10
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Reducir e reescribir o escravo de KIO de etiquetas de Baloo (fallo 340099)

### BluezQt

- Non deixar os descritores de ficheiro de rfkill en memoria (fallo 386886)

### Iconas de Breeze

- Engadir tamaños de icona que faltaban (fallo 384473)
- engadir iconas de instalar e desinstalar para Discover

### Módulos adicionais de CMake

- Engadir a etiqueta de descrición aos ficheiros de pkgconfig xerados
- ecm_add_test: Usar o separador de ruta axeitado en Windows
- Engadir FindSasl2.cmake a ECM
- Só pasar ARGS ao usar Makefiles
- Engadir FindGLIB2.cmake e FindPulseAudio.cmake
- ECMAddTests: definir QT_PLUGIN_PATH de xeito que se poidan atopar os complementos construídos localmente
- KDECMakeSettings: máis documentación sobre a disposición do directorio de construción

### Integración de infraestruturas

- Permitir descargar a segunda e a terceira ligazóns dun produto KNS (fallo 385429)

### KActivitiesStats

- Comezar a corrixir libKActivitiesStats.pc: (fallo 386933)

### KActivities

- Corrixir unha condición de carreira que inicia kactivitymanagerd varias veces

### KAuth

- Permitir construír unicamente o xerador de código kauth-policy-gen
- Engadir unha nota sobre chamar ao asistente desde aplicacións con varios fíos

### KBookmarks

- Non mostrar a acción de editar os marcadores se keditbookmarks non está instalado
- Migrar do obsoleto KAuthorized::authorizeKAction a authorizeAction

### KCMUtils

- navegación co teclado dentro e fóra dos módulos KCM de QML

### KCompletion

- Non quebrar ao definir unha edición de liña nova nun selector despregábel editábel
- KComboBox: Volver anticipadamente ao definir o editábel ao valor anterior
- KComboBox: usar de novo o obxecto de completado existente no novo editor de liña

### KConfig

- Non buscar /etc/kderc cada vez

### KConfigWidgets

- Actualizar as cores predeterminadas para que coincidan coas novas cores de D7424

### KCoreAddons

- Validación de entrada de SubJobs
- Avisar sobre erros ao analizar ficheiros JSON
- Instalar as definicións de tipos MIME de ficheiros kcfg, kcfgc, ui.rc, knotify e qrc
- Engadir unha nova función para medir a lonxitude por texto
- Corrixir un fallo de KAutoSave con ficheiros con espazos en branco

### KDeclarative

- Facer que compile en Windows
- facer que compile con QT_NO_CAST_FROM_ASCII/QT_NO_CAST_FROM_BYTEARRAY
- [MouseEventListener] Permitir aceptar eventos de rato
- usar un único motor de QML

### KDED

- kded: retirar as chamadas de D-Bus a KSplash

### KDocTools

- Actualizar a tradución ao portugués do Brasil
- Actualizar a tradución ao ruso
- Actualizar a tradución ao ruso
- Actualizar customization/xsl/ru.xml (faltaba nav-home)

### KEmoticons

- KEmoticons: migrar os complementos a JSON e engadir a posibilidade de cargar con KPluginMetaData
- Non permitir fugas de símbolos de clases de código privado, protexer con Q_DECL_HIDDEN

### KFileMetaData

- A proba usermetadatawritertest require Taglib
- Se o valor da propiedade é nulo, retirar o atributo user.xdg.tag (fallo 376117)
- Abrir en modo de só lectura os ficheiros no extractor TagLib

### KGlobalAccel

- Agrupar algunhas chamadas de D-Bus que bloquean
- kglobalacceld: evitar cargar un cargador de icona sen motivo
- xerar cadeas de atallo correctas

### KIO

- KUriFilter: ignorar complementos duplicados
- KUriFilter: simplificar estruturas de datos, corrixir unha fuga de memoria
- [CopyJob] Non empezar desde cero tras retirar un ficheiro
- Corrixir a creación dun directorio mediante KNewFileMenu e KIO::mkpath en Qt ≥ 5.9.3 (fallo 387073)
- Creouse a función de asistencia «KFilePlacesModel::movePlace»
- Expose KFilePlacesModel 'iconName' role
- KFilePlacesModel: Evitar o sinal innecesario «dataChanged»
- Devolver un obxecto de marcador válido para calquera entrada en KFilePlacesModel
- Crear unha función «KFilePlacesModel::refresh»
- Crear a función estática «KFilePlacesModel::convertedUrl»
- KFilePlaces: Creouse a sección «remote»
- KFilePlaces: Engadir unha sección para dispositivos extraíbeis
- Engadíronse uns URL de Baloo ao modelo de lugares
- Corrixir KIO::mkpath con qtbase 5.10 beta 4
- [KDirModel] Emitir un cambio de HasJobRole cando os traballos cambien
- Cambiar a etiqueta «Opcións avanzadas» por «Opcións de terminal»

### Kirigami

- Desprazar a barra de desprazamento o tamaño da cabeceira (fallo 387098)
- marxe inferior baseada na presenza de actionbutton
- non asumir que applicationWidnow() está dispoñíbel
- Non notificar sobre cambios de valor se aínda estamos no construtor
- Substituír o nome da biblioteca nas fontes
- permitir cores en máis lugares
- colorar as iconas das barras de ferramentas se fai falla
- considerar as cores das iconas dos botóns de acción principais
- inicio dunha propiedade agrupada «icon»

### KNewStuff

- Reverter «Separar antes de definir o punteiro d» (fallo 386156)
- non instalar a ferramenta de desenvolvemento para agrupar ficheiros de escritorio
- [knewstuff] Evitar unha fuga de ImageLoader en caso de erro

### Infraestrutura KPackage

- Facer cadeas de maneira axeitada na infraestrutura de kpackage
- Non intentar xerar metadata.json se non hai un metadata.desktop
- corrixir o uso da caché de kpluginindex
- Mellorar a saída de erro

### KTextEditor

- Corrixir as ordes de búfer do modo VI
- evitar a ampliación accidental

### KUnitConversion

- Migrar de QDom a QXmlStreamReader
- Usar HTTPS para descargar os tipos de cambio de divisa

### KWayland

- Expoñer wl_display_set_global_filter como método virtual
- Corrixir kwayland-testXdgShellV6
- Engadir compatibilidade con zwp_idle_inhibit_manager_v1 (fallo 385956)
- [server] Permitir inhibir IdleInterface

### KWidgetsAddons

- Evitar un diálogo de contrasinal inconsistente
- Definir o consello enable_blur_behind cando se pida
- KPageListView: Actualizar a anchura ao cambiar a fonte

### KWindowSystem

- [KWindowEffectsPrivateX11] Engadir unha chamada de reserve()

### KXMLGUI

- Corrixir a tradución do nome da barra de ferramentas cando ten o contexto de internacionalización

### Infraestrutura de Plasma

- A directiva #warning non é universal, concretamente non é compatíbel con MSVC
- [IconItem] Usar ItemSceneHasChanged en vez de conectar a windowChanged
- [Icon Item] Emitir overlaysChanged de maneira explícita no definidor en vez de conectar con el
- [Dialog] Usar KWindowSystem::isPlatformX11()
- Reducir a cantidade de cambios de propiedade espurios en ColorScope
- [Elemento de icona] Emitir validChanged só se de verdade cambia
- Desactivar os indicadores de desprazamento innecesarios se o escintilante é unha ListView cunha orientación coñecida
- [AppletInterface] Emitir sinais de cambio de configurationRequired e -Reason
- Usar a anchura e a altura de setSize() en vez de as de setProperty
- Corrixiuse un problema polo que o menú de PlasmaComponents aparecían con esquinas rotas (fallo 381799)
- Corrixiuse un problema polo que os menús contextuais aparecían con esquinas rotas (fallo 381799)
- Documentación da API: engadir unha nota de obsolescencia atopada no rexistro de Git
- Sincronizar o compoñente co de Kirigami
- Buscar en todos os compoñentes de KF5 como tales en vez de como infraestruturas separadas
- Reducir as emisións de sinais espurias (fallo 382233)
- Engadir sinais que indiquen se unha pantalla s engadiu ou retirou
- instalar cousas de selector
- Non depender de inclusións de inclusións
- Optimizar os nomes de roles de SortFilterModel
- Retirar DataModel::roleNameToId

### Prison

- Engadir un xerador de código Aztec

### QQC2StyleBridge

- determinar a versión de QQC2 en tempo de construción (fallo 386289)
- manter o fondo invisíbel de maneira predeterminada
- engadir un fondo en ScrollView

### Solid

- Acelerar UDevManager::devicesFromQuery

### Sonnet

- Permitir compilación cruzada de Sonnet

### Realce da sintaxe

- Engadir PKGUILD á sintaxe de Bash
- JavaScript: incluír tipos MIME estándar
- debchangelog: engadir Bionic Beaver
- Actualizar o ficheiro de sintaxe de SQL de Oracle (fallo 386221)
- SQL: mover a detección de comentarios antes dos operadores
- crk.xml: engadiuse a liña de cabeceira de &lt;?xml&gt;

### Información de seguranza

The released code has been GPG-signed using the following key: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Pode comentar e compartir ideas sobre esta versión na sección de comentarios do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
