---
aliases:
- ../announce-applications-15.04.3
changelog: true
date: 2015-07-01
description: O KDE Lança as Aplicações do KDE 15.04.3
layout: application
title: O KDE Lança as Aplicações do KDE 15.04.3
version: 15.04.3
---
1 de Julho de 2015. Hoje o KDE lançou a terceira actualização de estabilidade para as <a href='../15.04.0'>Aplicações do KDE 15.04</a>. Esta versão contém apenas correcções de erros e actualizações de traduções, pelo que será uma actualização segura e agradável para todos.

As mais de 20 correcções de erros registadas incluem as melhorias no módulo 'kdepim' e nas aplicações 'kdenlive', 'kopete', 'ktp-contact-list', 'okteta', 'marble' e 'umbrello'.

Esta versão também inclui as versões de Suporte de Longo Prazo da Área de Trabalho do Plasma 4.11.21, a Plataforma de Desenvolvimento do KDE 4.14.10 e o pacote Kontact 4.14.10.
