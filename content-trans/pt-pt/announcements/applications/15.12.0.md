---
aliases:
- ../announce-applications-15.12.0
changelog: true
date: 2015-12-16
description: O KDE Lança as Aplicações do KDE 15.12.
layout: application
release: applications-15.12.0
title: O KDE Lança as Aplicações do KDE 15.12.0
version: 15.12.0
---
16 de Dezembro de 2015. O KDE anuncia hoje o lançamento das Aplicações do KDE 15.12.

O KDE está excitado em poder anunciar o lançamento das Aplicações do KDE 15.12, a actualização de Dezembro de 2015 para as Aplicações do KDE. Esta versão traz novas aplicações, adições de funcionalidades e correcções de erros nas aplicações existentes. A equipa tenta ao máximo trazer a melhor qualidade para o seu ambiente de trabalho e para essas aplicações. Como tal, contamos consigo para enviar as suas reacções.

Nesta versão, foi actualizado um grande número de aplicações para usarem as novas <a href='https://dot.kde.org/2013/09/25/frameworks-5'>Plataformas do KDE 5</a>, incluindo o <a href='https://edu.kde.org/applications/all/artikulate/'>Artikulate</a>, o <a href='https://www.kde.org/applications/system/krfb/'>Krfb</a>, o <a href='https://www.kde.org/applications/system/ksystemlog/'>KSystemLog</a>, o <a href='https://games.kde.org/game.php?game=klickety'>Klickety</a> e mais alguns Jogos do KDE, para além da Interface de 'Plugins' de Imagens do KDE e das suas bibliotecas de suporte. Isto aumenta o número total de aplicações que usam as Plataformas do KDE 5 para 126.

### Uma Nova Adição "Espectacular"

Depois de 14 anos como parte do KDE, o KSnapshot foi descontinuado e substituído por uma nova aplicação para tirar fotografias, o Spectacle.

Com novas funcionalidades e com uma interface completamente nova, o Spectacle torna a captura de imagens tão simples e não-intrusiva quanto possível. Para além do que podia fazer com o KSnapshot, com o Spectacle poderá agora tirar imagens compostas de menus em conjunto com as suas janelas-mãe ou tirar fotografias do ecrã inteiro (ou da janela activa de momento) sem sequer iniciar o Spectacle, usando simplesmente as combinações de teclas Shift+PrintScreen e Meta+PrintScreen, respectivamente.

Também optimizámos o tempo de arranque da aplicação de forma agressiva, por forma a minimizar o atraso de tempo entre o início da aplicação e a captura da imagem.

### Limpezas em Todo o Lado

Muitas das aplicações passaram por diversas melhorias e limpezas neste ciclo, para além de algumas correcções de erros e de estabilidade.

O <a href='https://userbase.kde.org/Kdenlive'>Kdenlive</a>, o editor de vídeo não-linear, viu diversas correcções importantes na sua interface de utilizador. Poderá agora copiar e colar itens na sua linha temporal, podendo também activar/desactivar a transparência de uma dada faixa. As cores dos ícones ajustam-se automaticamente ao tema principal da interface, tornando-se mais fáceis de ver.

{{<figure src="/announcements/applications/15.12.0/ark1512.png" class="text-center" width="600px" caption=`O Ark pode agora mostrar os comentários do ZIP`>}}

O <a href='https://www.kde.org/applications/utilities/ark/'>Ark</a>, o gestor de pacotes, consegue agora mostrar os comentários incorporados nos pacotes ZIP e RAR. Foi melhorado o suporte para caracteres Unicode nos nomes dos ficheiros nos pacotes ZIP, podendo agora detectar os pacotes danificados e recuperar o máximo de dados que for possível deles. Também poderá abrir os ficheiros arquivados no pacote nas suas aplicações predefinidas; foi também corrigido o modo de arrastamento para o ecrã, assim como as antevisões dos ficheiros XML.

### Tudo Diversão e Nada de Trabalho

{{<figure src="/announcements/applications/15.12.0/ksudoku1512.png" class="text-center" width="600px" caption=`O novo Ecrã de Boas-Vindas do KSudoku (num Mac OS X)`>}}

Os programadores dos Jogos do KDE estiveram a trabalhar bastante durante os últimos meses para optimizar os nossos jogos, para uma experiência mais suave e rica, sendo adicionadas diversas coisas novas nesta área para a diversão dos nossos utilizadores.

No <a href='https://www.kde.org/applications/games/kgoldrunner/'>KGoldrunner</a>, foram adicionados dois novos conjuntos de níveis, um que permite cavar enquanto se cai e outro sem essa possibilidade. Foram adicionadas soluções para os diversos conjuntos de níveis existentes. Para um desafio acrescido, agora foi desactivada a escavação em queda para alguns conjuntos de níveis mais antigos.

No <a href='https://www.kde.org/applications/games/ksudoku/'>KSudoku</a>, poderá agora imprimir 'puzzles' do Matedoku e do Sudoku Assassino. A nova disposição multi-colunas no Ecrã de Boas-Vindas do KSudoku torna mais fácil de ver mais sobre os diversos tipos de 'puzzles' disponíveis, sendo que as colunas se ajustam automaticamente à medida que a janela muda de tamanho. Foi feito o mesmo ao Palapeli, tornando mais fácil de ver mais sobre a sua colecção de 'puzzles' de uma vez.

Também foram incluídas correcções de estabilidade para jogos como o KNavalBattle, o Klickety, o <a href='https://www.kde.org/applications/games/kshisen/'>KShisen</a> e o <a href='https://www.kde.org/applications/games/ktuberling/'>KTuberling</a>, sendo que a experiência global ficou bastante melhor agora. O KTuberling, o Klickety e o KNavalBattle também foram actualizados para usar as novas Plataformas do KDE 5.

### Correcções Importantes

Não detesta quando a sua aplicação favorita estoira na altura mais inconveniente? Nós também e, para tentar remediar isso, trabalhámos com afinco para corrigir uma grande quantidade de erros para si, mas infelizmente poderão continuar a existir alguns menos óbvios, pelo que não se esqueça de <a href='https://bugs.kde.org'>os comunicar</a>!

No nosso gestor de ficheiros <a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, foram incluídas diversas correcções de estabilidade, bem como algumas correcções para tornar o deslocamento mais suave. No <a href='https://www.kde.org/applications/education/kanagram/'>Kanagram</a>, foi corrigido um problema incómodo com o texto branco sobre fundos brancos. No <a href='https://www.kde.org/applications/utilities/kate/'>Kate</a>, tentou-se corrigir um estoiro que estava a ocorrer no encerramento, para além da arrumação da interface e da adição de uma nova limpeza da 'cache'.

O <a href='https://userbase.kde.org/Kontact'>Pacote Kontact</a> viu diversas funcionalidades adicionadas, correcções de erros e optimizações de performance. De facto, existiu tanto desenvolvimento neste ciclo que avançámos o número da versão para 5.1. A equipa está a trabalhar ao máximo, e esta à espera de todas as suas reacções.

### Avanços

Como parte do esforço para modernizar as nossas ofertas, retirámos algumas aplicações das Aplicações do KDE e as mesmas já não serão lançadas como fazendo parte das Aplicações do KDE 15.12

Foram retiradas 4 aplicações desta versão - Amor, KTux, KSnapshot e SuperKaramba. Como foi dito acima, o KSnapshot foi substituído pelo Spectacle e o Plasma substitui na sua essência o SuperKaramba como motor gráfico. Foram retirados alguns protectores de ecrã autónomos, porque o bloqueio do ecrã é tratado de forma muito diferente nos ambientes de trabalho modernos.

Foram também retirados 3 pacotes de conteúdos gráficos (kde-base-artwork, kde-wallpapers e kdeartwork); o seu conteúdo já não é alterado há muito tempo.

### Registo de Alterações Completo
