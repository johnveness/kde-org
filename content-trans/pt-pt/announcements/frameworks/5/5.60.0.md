---
aliases:
- ../../kde-frameworks-5.60.0
date: 2019-07-13
layout: framework
libCount: 70
---
### Geral

- O Qt &gt;= 5.11 é agora obrigatório, agora que o Qt 5.13 foi lançado.

### Baloo

- [QueryTest] Testa se as frases independentes são realmente independentes
- [TermGenerator] Inserção de uma posição vazia entre termos independentes
- [QueryTest] Restruturação dos testes para uma melhor extensão
- [TermGenerator] Deixar as frases de termos únicos fora da PositionDB
- [TermGenerator] Fazer o corte de Termos antes da conversão para UTF-8
- [PostingIterator] Passagem do método positions() para o VectorPositionInfoIterator
- [TermGenerator] Usar o ByteArray em UTF-8 para o 'termList'
- [WriteTransactionTest] Limpeza da mistura entre QString e QByteArray
- [experimental/BalooDB] Correcção do aviso trivial de '0 / nullptr'
- [PositionDbTest] Correcção de fuga de memória trivial no teste
- [PendingFileQueueTest] Verificação se a criação + remoção não emitem eventos extra
- [PendingFileQueueTest] Verificação se a remoção + criação funcionam de facto
- [PendingFileQueue] Evitar conflitos remoção + criação / criação + remoção
- [PendingFileQueueTest] Uso de eventos de temporização sintéticos para acelerar os testes
- [XAttrIndexer] Actualização do DocumentTime quando as XAttrs são actualizadas
- [PendingFileQueueTest] Redução dos tempos-limite, verificação do tempo de acompanhamento
- [PendingFileQueue] Uso de um cálculo mais preciso do tempo restante
- [ModifiedFileIndexer] Uso do tipo MIME correcto para as pastas, atraso até que seja necessário
- [NewFileIndexer] Omissão das ligações simbólicas no índice
- [ModifiedFileIndexer] Evitar esconder as alterações dos registos XAttr com as alterações de conteúdo
- [NewFileIndexer] Uso do tipo MIME correcto para as pastas; validação do 'excludeFolders'
- [UnindexedFileIndexer] Captura das alterações de comentários, marcas e classificações
- [UnindexedFileIndexer] Ignorar as verificações das datas dos novos ficheiros
- [DocumentUrlDB] Evitar a manipulação da árvore inteira numa mudança de nome trivial
- [DocumentUrlDB] Captura prévia de URL's inválidos
- [DocumentUrlDB] Remoção do método não usado 'rename'
- [balooctl] Simplificação dos comandos de controlo da indexação
- [Transacção] Substituição do modelo da função com o std::function
- [FirstRunIndexer] Uso do tipo MIME correcto para as pastas
- Passagem do invariante IndexingLevel para fora do ciclo
- [BasicIndexingJob] Ignorar a pesquisa do tipo de documento no Baloo para as pastas
- [FileIndexScheduler] Garantia de que a indexação não é executada no estado suspenso
- [PowerStateMonitor] Ser conservador ao determinar o estado da energia
- [FileIndexScheduler] Paragem da indexação quando o quit() é invocado por DBus
- Evitar o destacamento do contentor em alguns locais
- Não tentar adicionar a uma QLatin1String
- Desactivação da detecção do Valgrind ao compilar com o MSVC
- [FilteredDirIterator] Combinação de todos os sufixos numa enorme RegExp
- [FilteredDirIterator] Evitar a sobrecarga do RegExp para correspondências exactas
- [UnindexedFileIterator] Atraso da detecção do tipo MIME até que seja necessária
- [UnindexedFileIndexer] Não tentar adicionar ficheiros inexistentes ao índice
- Detecção do Valgrind, evitando a remoção da base de dados ao usar o Valgrind
- [UnindexedFileIndexer] Optimizações do ciclo (evitar destacamentos, invariantes)
- Atraso na execução do UnindexedFileIndexer e no IndexCleaner
- [FileIndexScheduler] Adição de novo estado 'Idle' (Inactivo) da bateria
- [FileIndexScheduler] Atrasar as tarefas de manutenção enquanto estiver apenas com a bateria
- [FileIndexScheduler] Evitar emitir mudanças de estado várias vezes
- [balooctl] Clarificação e extensão do resultado do estado

### BluezQt

- Adição da API do MediaTransport
- Adição das APIS de Publicidade LE e GATT

### Ícones do Brisa

- Adição de 'id="current-color-scheme"' aos ícones 'collapse-all' (erro 409546)
- Adição de ícones do 'disk-quota' (erro 389311)
- Instalação de ligação simbólica para o 'edit-download'
- Mudança do ícone de configuração do 'joystick' para um controlador de jogos (erro 406679)
- Adição do 'edit-select-text', criação do 'draw-text' a 16px como o de 22px
- Actualização do ícone do KBruch
- Adição de ícones 'help-donate-[moeda]'
- Fazer com que o Brisa Escuro use o mesmo ícone do Kolourpaint que o Brisa
- Adição de ícones de notificações a 22px

### KActivitiesStats

- Correcção de um estoiro no KactivityTestApp quando o Result tem textos com caracteres não-ASCII

### KArchive

- Não estoirar se o ficheiro interno quiser ser maior que o tamanho máximo do QByteArray

### KCoreAddons

- KPluginMetaData: uso do Q_DECLARE_METATYPE

### KDeclarative

- [GridDelegate] Correcção dos intervalos nos cantos do realce da 'thumbnailArea'
- eliminação dos 'blockSignals'
- [KCM GridDelegate] Aviso silencioso
- [KCM GridDelegate] Ter em conta o 'implicitCellHeight' na altura do delegado interno
- Correcção do ícone do GridDelegate
- Correcção da comparação frágil com o i18n("None") e descrição do comportamento na documentação (erro 407999)

### WebKit do KDE

- Despromoção do KDEWebKit do Nível 3 para as Ajudas de Migração

### KDocTools

- Actualização do 'user.entities' em PT-BR

### KFileMetaData

- Correcção da extracção de algumas propriedades para corresponder ao que foi escrito (erro 408532)
- Uso da categoria de depuração na extracção/gravação da Taglib
- Formatação do valor de ponderação da exposição de fotografias (erro 343273)
- correcção do nome da propriedade
- Remoção do prefixo da fotografia de todos os nomes de propriedades EXIF (erro 343273)
- Mudança de nome das propriedades ImageMake e ImageModel (erro 343273)
- [UserMetaData] Adição de método para consultar os atributos que estão configurados
- Formatação da distância focal em milímetros
- Formatação do tempo de exposição da fotografia como um número racional, se se aplicar (erro 343273)
- Activação do 'usermetadatawritertest' para todos os UNIXes, não só para o Linux
- Formatação dos valores de abertura como números F (erro 343273)

### Extensões da GUI do KDE

- KModifierKeyInfo: estamos a partilhar a implementação interna
- Remoção de pesquisas duplas
- Mudança da decisão de usar o X11 ou não para o momento de execução

### KHolidays

- Correcção do feriado de Maio Prévio do Reino Unido para 2020 (erro 409189)
- Correcção do código ISO para Hesse / Alemanha

### KImageFormats

- QImage::byteCount -&gt; QImage::sizeInByes

### KIO

- Correcção do teste do KFileItemTest::testIconNameForUrl para reflectir um nome de ícone diferente
- Correcção do erro de número de argumentos do 'i18n' na mensagem de aviso do 'knewfilemenu'
- [ftp] Correcção da hora de acesso errada no Ftp::ftpCopyGet() (erro 374420)
- [CopyJob] Comunicação da quantidade processada em lote
- [CopyJob] Comunicação dos resultados após terminar a cópia (erro 407656)
- Passagem da lógica redundante em KIO::iconNameForUrl() para o KFileItem::iconName() (erro 356045)
- Instalação do KFileCustomDialog
- [Painel de Locais] Não mostrar a Raiz por omissão
- Despromoção da janela "Não é possível mudar as permissões" para um qWarning
- O O_PATH só esta disponível no Linux - evitar que o compilador desencadeie um erro
- Mostrar a reacção no interior ao criar novos ficheiros ou pastas
- Suporte de Autenticação: Eliminação dos privilégios se o destino não pertencer ao 'root'
- [copyjob] Só definir a hora de modificação se o kio-slave a devolveu (erro 374420)
- Cancelar a operação de privilégios para um destino apenas-para-leitura com o utilizador actual como dono
- Adição do KProtocolInfo::defaultMimetype
- Gravar sempre a configuração de visualização quando mudar de um modo de visualização para outro
- Reposição do grupo exclusivo para ordenar os itens do menu
- Modos de visualização como no Dolphin na janela de ficheiros (erro 86838)
- kio_ftp: melhoria no tratamento de erros quando a cópia para FTP falhar
- kioexec: mudança das mensagens de depuração assustadoras para a remoção posterior

### Kirigami

- [ActionTextField] Fazer brilhar a acção ao carregar nela
- suporte para o modo e posição do texto
- efeito à passagem do rato na lista de navegação no ecrã
- garantia de uma altura mínima de 2 unidades da grelha
- Configuração do 'implicitHeight' do SwipeListItem para ser o máximo do conteúdo e das acções
- Esconder a dica quando se carrega no PrivateActionToolButton
- Remoção de chamadas de funções deixadas por acaso no código do CMake para o estilo do Plasma
- ColumnView::itemAt
- forçar o 'breeze-internal' se não for indicado nenhum tema
- navegação correcta na página fixa à esquerda
- manutenção do registo do espaço coberto pelas páginas fixas
- mostrar um separador quando estiver no modo de barra lateral esquerda
- no modo de coluna única, a fixação não faz efeito
- primeiro protótipo semi-funcional da fixação

### KJobWidgets

- [KUiServerJobTracker] Tratamento da mudança de dono

### KNewStuff

- [kmoretools] Adição de ícones para as acções de transferência e instalação

### KNotification

- Não pesquisar pelo Phonon no Android

### KParts

- Adição da interface de suporte para perfis no TerminalInterface

### KRunner

- Não atrasar indefinidamente a emissão do 'matchesChanged'

### KService

- Adição do X-Flatpak-RenamedFrom como chave reconhecida

### KTextEditor

- Correcção do alinhamento ao centro do 'ir para a linha' (erro 408418)
- Correcção da visualização do ícone de favorito no contorno de ícones com resoluções baixas
- Correcção da acção "Mostrar o Contorno dos Ícones" para comutar o contorno de novo
- Correcção das páginas vazias na antevisão de impressão e das linhas impressas em duplicado (erro 348598)
- remoção do cabeçalho não mais usado
- correcção do deslocamento automático na velocidade de recepção (erro 408874)
- Adição das variáveis predefinidas na interface de variáveis
- Fazer com que funcione a verificação ortográfica automática após recarregar um documento (erro 408291)
- elevação do limite predefinido do tamanho da linha para 10000
- WIP: Desactivação do realce ao fim de 512 caracteres numa linha
- KateModeMenuList: migração para QListView

### KWayland

- Inclusão de uma descrição
- Prova de conceito de um protocolo do Wayland para permitir o funcionamento do motor de dados do estado das teclas

### KWidgetsAddons

- O KPasswordLineEdit agora herda correctamente o 'focusPolicy' do seu QLineEdit  (erro 398275)
- Substituição do botão de "Detalhes" por um KCollapsibleGroupBox

### Plataforma do Plasma

- [Svg] Correcção do erro de migração do QRegExp::exactMatch
- ContainmentAction: Correcção do carregamento do KPlugin
- [TabBar] Remoção das margens exteriores
- Os métodos de listagem de Applet, DataEngine e de contentores no Plasma::PluginLoader não filtra mais os 'plugins' com o X-KDE-ParentAppprovided quando for passado um texto vazio
- Reposição do funcionamento da redução por toque no calendário
- Adição de ícones do 'disk-quota' (erro 403506)
- Tornar o Plasma::Svg::elementRect um pouco mais simples
- Definição automática da versão dos pacotes 'desktopthemes' para a KF5_VERSION
- Não notificar a alteração para o mesmo estado em que estava
- Correcção do alinhamento da legenda do 'toolbutton'
- [PlasmaComponents3] Centrar também na vertical o texto do botão

### Purpose

- Mudança do tamanho inicial da janela de configuração
- Melhoria dos ícones e texto dos botões da janela de Tarefas
- Correcção da tradução do 'actiondisplay'
- Não mostrar uma mensagem de erro se a partilha for cancelada pelo utilizador
- Correcção de aviso ao ler os meta-dados do 'plugin'
- Remodelação das páginas de configuração
- ECMPackageConfigHelpers -&gt; CMakePackageConfigHelpers
- phabricator: Correcção de seguimento no código no 'switch'

### QQC2StyleBridge

- Mostrar o atalho no item do menu quando tal for indicado (erro 405541)
- Adição do MenuSeparator
- Correcção dos ToolButton que permanecem pressionados após deixar de estar pressionados
- [ToolButton] Passagem do tamanho de ícones personalizado ao StyleItem
- Respeito da política de visibilidade (erro 407014)

### Solid

- [Fstab] Selecção do ícone apropriado para a pasta pessoal ou de raiz
- [Fstab] Mostrar os sistemas de ficheiros "sobrepostos" montados
- [Infra-Estrutura do UDev] Redução dos dispositivos pesquisados

### Realce de Sintaxe

- Fortran: mudança de licença para a MIT
- Melhoria no realce de sintaxe do formato fixo do Fortran
- Fortran: implementação de formatos livres &amp; fixos
- Correcção do realce de parêntesis encadeados do COMMAND do CMake
- Adição de mais palavras-chave e suporte para o 'rr' no realce do 'gdb'
- Detecção prévia das linhas de comentários no realce do GDB
- AppArmor: actualização da sintaxe
- Julia: actualização da sintaxe e adição das palavras-chave de constantes (erro 403901)
- CMake: Realce das variáveis-padrão de ambiente do CMake
- Adição da definição de sintaxe para a compilação com o Ninja
- CMake: Suporte para as funcionalidades do 3.15
- Jam: diversas melhorias e correcções
- Lua: actualização para o Lua54 e classificação do fim da função como uma Keyword em vez de um Control
- C++: actualização para o C++20
- debchangelog: adição do Eoan Ermine

### Informação de segurança

O código lançado foi assinado com GPG, usando a seguinte chave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Impressão digital da chave primária: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
