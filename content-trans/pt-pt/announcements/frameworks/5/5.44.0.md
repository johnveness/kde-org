---
aliases:
- ../../kde-frameworks-5.44.0
date: 2018-03-10
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- balooctl: Remoção da opção 'checkDb' (erro 380465)
- indexerconfig: Descrição de algumas funções
- indexerconfig: Exposição da função 'canBeSearched' (erro 388656)
- Monitor do Balooctl: Esperar pela interface de D-Bus
- fileindexerconfig: Introdução do canBeSearched() (erro 388656)

### Ícones do Brisa

- remoção do 'view-media-playlist' dos ícones de preferências
- adição do ícone 'media-album-cover' em 24px
- adição de suporte QML para o Babe (22px)
- actualização dos ícones 'handle-' do Kirigami
- adição de ícones de 64px para o Elisa

### Módulos Extra do CMake

- Definição do **ANDROID_API**
- Correcção do nome do comando 'readelf' no x86
- Ferramentas do Android: adição da variável ANDROID_COMPILER_PREFIX, correcção do local de inclusão dos alvos x86, extensão do local de pesquisa das dependências do NDK

### Ferramentas de Doxygen do KDE

- Sair com erro se a pasta de saída não estiver vazia (erro 390904)

### KConfig

- Poupança de algumas alocações de memória ao usar a API correcta
- Exportação do 'kconf_update' com as ferramentas

### KConfigWidgets

- Melhoria do KLanguageButton::insertLanguage quando não é passado nenhum nome
- Adição de ícones para o 'Deselect' e 'Replace' do KStandardActions

### KCoreAddons

- Limpeza do 'm_inotify_wd_to_entry' antes de invalidar os ponteiros Entry (erro 390214)
- kcoreaddons_add_plugin: remoção do OBJECT_DEPENDS sem efeito no ficheiro 'json'
- Ajuda ao 'automoc' para encontrar os ficheiros JSON de meta-dados referenciados no código
- kcoreaddons_desktop_to_json: atenção ao ficheiro gerado no registo de compilação
- Evolução do 'shared-mime-info' para a versão 1.3
- Introdução do K_PLUGIN_CLASS_WITH_JSON

### KDeclarative

- Correcção de erro de compilação no armhf/aarch64
- Eliminação do QmlObjectIncubationController
- desligar o render() ao mudar de janela (erro 343576)

### KHolidays

- Allen Winter é agora o responsável oficial do KHolidays

### KI18n

- Documentação da API: adição de nota sobre a invocação do 'setApplicationDomain' após a criação do QApp

### KIconThemes

- [KIconLoader] Ter em conta o 'devicePixelRatio' nas camadas

### KIO

- Não assumir o formato das estruturas 'msghdr' e 'iovec' (erro 391367)
- Correcção da selecção de protocolos no KUrlNavigator
- Mudança do 'qSort' para o 'std::sort'
- [KUrlNavigatorPlacesSelector] Uso do KFilePlacesModel::convertedUrl
- [Tarefa de Largada] Criação de um ficheiro adequado do lixo ao criar uma ligação
- Correcção da activação não-intencional do item de menu de navegação (erro 380287)
- [KFileWidget] Esconder a moldura e cabeçalho dos locais
- [KUrlNavigatorPlacesSelector] Colocação das categorias em submenus (erro 389635)
- Tirar partido do cabeçalho do utilitário de testes do KIO
- Adição do Ctrl+H à lista de atalhos para "mostrar/esconder os ficheiros escondidos" (erro 390527)
- Adição do suporte da semântica de movimento para o KIO::UDSEntry
- Correcção da questão do "atalho ambíguo" introduzido com o D10314
- Colocação da mensagem "Não foi possível encontrar o executável" numa função-lambda de espera (erro 385942)
- Melhoria da usabilidade da janela "Abrir Com" ao adicionar uma opção para filtrar a árvore de aplicações
- [KNewFileMenu] KDirNotify::emitFilesAdded após o 'storedPut' (erro 388887)
- Correcção de validação ao cancelar a janela de reconstrução do ksycoca (erro 389595)
- Correcção do erro #382437 "Regressão no kdialog origina uma extensão errada do ficheiro" (erro 382437)
- Início mais rápido do 'simplejob'
- Reparação da cópia de ficheiros para o VFAT sem avisos
- kio_file: ignorar o tratamento de erros das permissões iniciais durante a cópia de ficheiros
- Permitir a geração da semântica de movimento do KFileItem. O construtor de cópia, o destruidor e o operador de atribuição por cópia são agora também gerados pelo compilador
- Não invocar o stat(/etc/localtime) entre o read() e o  write() ao copiar ficheiros (erro 384561)
- remote: não criar elementos com nomes vazios
- Adição da funcionalidade 'supportedSchemes'
- Uso do F11 como atalho para activar/desactivar a antevisão lateral
- [KFilePlacesModel] Agrupar as partilhas de rede sob a categoria "Remota"

### Kirigami

- Mostrar o botão de ferramenta como assinalado enquanto o menu estiver visível
- indicadores de deslocamento não interactivos em dispositivos móveis
- Correcção dos submenus de acções
- Possibilidade de uso do QQC2.Action
- Possibilitar o suporte de grupos de acções exclusivas (erro 391144)
- Mostrar o texto nos botões de ferramentas de acção das páginas
- Possibilidade de as acções mostrarem submenus
- Não ter uma posição do componente específica no seu elemento-pai
- Não gerar acções SwipeListItem a menos que estejam expostas
- Adição de uma validação isNull() antes de definir se o QIcon é uma máscara
- Adição do FormLayout.qml ao kirigami.qrc
- correcção das cores do 'swipelistitem'
- melhor comportamento dos cabeçalhos e rodapés
- Melhoria do preenchimento à esquerda e comportamento das reticências do ToolBarApplicationHeader
- Garantia que os botões de navegação não vão para baixo da acção
- suporte para as propriedades do cabeçalho e rodapé na 'overlaysheet'
- Eliminação do preenchimento inferior desnecessário no OverlaySheets (erro 390032)
- Limpeza da aparência do ToolBarApplicationHeader
- mostrar um botão de fecho no ecrã (erro 387815)
- não é possível fechar a folha com a roda do rato
- Só multiplicar o tamanho do ícone se o Qt não o estiver já a fazer (erro 390076)
- ter o rodapé global em conta na posição das pegas
- compressão dos eventos de criação e destruição das barras de deslocamento
- ScrollView: Tornar a política da barra de deslocamento pública e correcção da mesma

### KNewStuff

- Adição do 'vokoscreen' às KMoreTools e adição ao grupo "screenrecorder" (gravador do ecrã)

### KNotification

- Uso do QWidget para ver se a janela está visível

### Plataforma KPackage

- Ajuda ao 'automoc' para encontrar os ficheiros JSON de meta-dados referenciados no código

### KParts

- Limpeza de código antigo e inacessível

### KRunner

- Actualização do modelo de 'plugin' do KRunner

### KTextEditor

- Adição de ícones para o 'Documento->Exportar', 'Favorito->Remover' e das 'Maiúsculas', 'Minúsculas' e 'Capitalizar' da 'Formatação de Texto' do KTextEditor

### KWayland

- Implementação da libertação do resultado libertado pelo cliente
- [servidor] Tratamento adequado da situação em que a DataSource de um arrastamento é destruída (erro 389221)
- [servidor] Não estoirar quando uma sub-superfície for enviada e onde a superfície-mãe foi destruída (erro 389231)

### KXMLGUI

- Limpeza dos dados internos do QLocale quando tiver uma língua personalizada para a aplicação
- Não permitir configurar acções de separador com o menu de contexto
- Não mostrar o menu de contexto se carregar com o botão direito fora (erro 373653)
- Melhoria no KSwitchLanguageDialogPrivate::fillApplicationLanguages

### Ícones do Oxygen

- adição do ícone do Artikulate (erro 317527)
- adição do ícone do 'folder-games' (erro 318993)
- correcção do ícone de 48px incorrecto para o 'calc.template' (erro 299504)
- adição dos ícones 'media-playlist-repeat' e 'shuffle' (erro 339666)
- Oxygen: adição de ícones de marcas como no Brisa (erro 332210)
- ligação do 'emblem-mount' ao 'media-mount' (erro 373654)
- adição de ícones de rede que estão disponíveis nos ícones do Brisa (erro 374673)
- sincronização dos ícones do Oxygen com o Brisa no plasmóide de áudio
- Adição do 'edit-select-none' ao Oxygen para o Krusader (erro 388691)
- Adição do ícone 'rating-unrated' (erro 339863)

### Plataforma do Plasma

- uso do novo valor do 'largeSpacing' no Kirigami
- Redução da visibilidade do texto de substituição do TextField do PC3
- Não tornar também os títulos 20% transparentes
- [PackageUrlInterceptor] Não escrever de novo "inline"
- Não tornar os cabeçalhos 20% transparentes para corresponder ao Kirigami
- não colocar no 'fullrep' se o 'popup' não estiver fechado
- Ajuda ao 'automoc' para encontrar os ficheiros JSON de meta-dados referenciados no código
- [AppletQuickItem] Pré-carregamento da expansão da 'applet' se não estiver já expandido
- outras micro-optimizações do pré-carregamento
- Configuração do IconItem por omissão como 'smooth=true'
- pré-carregamento também da expansão (a janela)
- [AppletQuickItem] Correcção da definição da política de pré-carregamento, caso não esteja definida nenhuma variável de ambiente
- correcção da aparência RTL do ComboBox (erro https://bugreports.qt.io/browse/QTBUG-66446)
- tentar pré-carregar certas 'applets' de forma inteligente
- [Item de Ícones] Definição de filtragem na textura FadingNode
- Inicialização do 'm_actualGroup' como NormalColorGroup
- Validação de que as instâncias de FrameSvg e Svg têm o mesmo 'devicePixelRatio'

### Prisão

- Actualização das ligações às dependências e marcação do Android como oficialmente suportado
- Tornar a dependência do DMTX opcional
- Adição do suporte de QML para o Prison
- Definição do tamanho mínimo também nos códigos de barras 1D

### Purpose

- Correcção do nível, acomodação para o KIO

### QQC2StyleBridge

- Correcção de erro de sintaxe na modificação anterior, detectado ao lançar o 'ruqola'
- Apresentação de um botão de opção exclusiva quando aparece um controlo exclusivo (erro 391144)
- implementação do MenuBarItem
- implementação do DelayButton
- Novo componente: botão arredondado
- ter em conta a posição da barra de ferramentas
- suportar cores para os ícones nos botões
- suporte do '--reverse'
- ícones no Menu completamente funcionais
- sombras consistentes com o novo estilo Brisa
- Alguns QStyles parecem não devolver métricas de pixels adequadas aqui
- suporte para os primeiros ícones preliminares
- não mudar de linha com a roda do rato

### Solid

- correcção de uma fuga e uma verificação incorrecta de 'nullptr' no DADictionary
- [UDisks] Correcção de regressão na montagem automática (erro 389479)
- [UDisksDeviceBackend] Evitar uma pesquisa múltipla
- Infra-estrutura do Mac/IOKit: suporte para unidades, discos e volumes

### Sonnet

- Uso do Locale::name() em vez do Locale::bcp47Name()
- Pesquisa da libhunspell compilada pelo MSVC

### Realce de Sintaxe

- Suporte básico para os blocos de código em PHP e Python delimitados no Markdown
- Suporte para o WordDetect sem distinção de maiúsculas/minúsculas
- Realce de Scheme: Remoção de cores fixas
- Adição do realce de sintaxe para as Políticas CIL &amp; Contextos de Ficheiros do SELinux
- Adição do suporte aos ficheiros .ctp no realce de sintaxe do PHP
- Yacc/Bison: Correcção do símbolo '$' e actualização da sintaxe para o Bison
- awk.xml: adição das palavras-chave de extensão do 'gawk' (erro 389590)
- Adição do APKBUILD para ser realçado como um ficheiro do Bash
- Reversão da "Adição do APKBUILD para ser realçado como um ficheiro do Bash"
- Adição do APKBUILD para ser realçado como um ficheiro do Bash

### Informação de segurança

O código lançado foi assinado com GPG, usando a seguinte chave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Impressão digital da chave primária: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB

Poderá discutir e partilhar ideias sobre esta versão na secção de comentários do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-'>artigo do Dot</a>.
