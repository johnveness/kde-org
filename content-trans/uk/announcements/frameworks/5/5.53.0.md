---
aliases:
- ../../kde-frameworks-5.53.0
date: 2018-12-09
layout: framework
libCount: 70
---
### Baloo

- Виправлено пошук для оцінки 10 (5 зірок) (виправлено 357960)
- Усунено непотрібне записування незмінених даних до бази даних термінів
- Усунено подвійне додавання Type::Document/Presentation/Spreadsheet для документів MS Office
- Реалізовано належну ініціалізацію kcrash
- Забезпечено зберігання лише одного значення MTime на документ у MTimeDB
- [Засіб видобування] Реалізовано використання серіалізації QDataStream замість власного варіанта
- [Засіб видобування] Власний обробник введення-виведення замінено на QDataStream із перехопленням HUP

### Піктограми Breeze

- Додано піктограми application-vnd.appimage/x-iso9660-appimage
- Додано піктограму dialog-warning розміром 22 пікселі (виправлено ваду 397983)
- Виправлено кут повороту і поле для розміру 32 пікселі піктограми dialog-ok-apply (виправлено ваду 393608)
- Змінено основні монохроматичні кольори піктограм відповідно до нових кольорів HIG
- Змінено піктограми дій archive-* для показу файлів архівів (виправлено ваду 399253)
- Додано символічне посилання help-browser на піктограми розмірами 16 пікселів та 22 пікселі для каталогів (виправлено ваду 400852)
- Додано нові загальні піктограми упорядковування; перейменовано наявні піктограми упорядковування
- Додано версію root піктограми drive-harddisk (виправлено ваду 399307)

### Додаткові модулі CMake

- Новий модуль: FindLibExiv2.cmake

### Інтеграція бібліотек

- Додано параметр BUILD_KPACKAGE_INSTALL_HANDLERS, який призначено для пропускання збирання засобів для встановлення

### Інструменти Doxygen KDE

- Додано індикатор стану зайнятості під час дослідження та реалізовано асинхронне дослідження (виправлено ваду 379281)
- Усі шлях вхідних даних нормалізовано за допомогою функції os.path.normpath (виправлено ваду 392428)

### KCMUtils

- Реалізовано ідеальне вирівнювання між заголовками модулів центру керування, створених на основі QML і QWidget
- Додано контекст до з'єднання модуля центру керування із лямбдами (виправлено ваду 397894)

### KCoreAddons

- Уможливлено використання KAboutData/License/Person у QML
- Усунено аварійне завершення роботи, якщо каталог XDG_CACHE_HOME є надто малим або у ньому скінчилося вільне місце (виправлено ваду 339829)

### KDE DNS-SD

- Реалізовано встановлення kdnssd_version.h для перевірки версії бібліотеки
- Усунено витік пам'яті у resolver у remoteservice
- Реалізовано запобігання конкуренції за сигнал із avahi
- Виправлено версію для macOS

### KFileMetaData

- Відновлено властивість «Description» у метаданих DublinCore
- До KFileMetaData додано властивість опису (description)
- [KFileMetaData] Додано засіб видобування для сумісного із DSC (вбудованого) коду Postscript
- [Збірка засобів видобування даних] Усунено залежність від kcoreaddons для перевірок служби неперервної інтеграції
- До taglibextractor додано підтримку файлів speex
- Додано ще два джерела даних для міток у інтернеті
- Спрощено обробку міток id3
- [XmlExtractor] Використано QXmlStreamReader для поліпшення швидкодії

### KIO

- Виправлено оцінку при вилученні символічних посилань у PreviewJob
- Додано можливість створення комбінації клавіш для створення файла
- [KUrlNavigator] Реалізовано повторну активацію при клацанні середньою кнопкою миші (виправлено ваду 386453)
- Вилучено записи непрацездатних служб пошуку
- На HTTPS портовано ще декілька засобів пошуку
- Знову експортовано KFilePlaceEditDialog (виправлено ваду 376619)
- Відновлено підтримку надсилання файлів
- [ioslaves/trash] Реалізовано обробку пошкоджених символічних посилань на вилучені каталоги (виправлено ваду 400990)
- [Вікно перейменування] Виправлено компонування, якщо використовується прапорець NoRename
- Додано пропущений @since у KFilePlacesModel::TagsType
- [KDirOperator] Використано нову піктограму <code>view-sort</code> для засобу вибору критерію упорядковування
- Для усіх служб пошуку, де передбачено відповідну підтримку, використано HTTPS
- Вимкнено пункт демонтування для / або /home (виправлено ваду 399659)
- [KFilePlaceEditDialog] Виправлено захист від повторного включення
- [Панель «Місця»] Використано нову піктограму <code>folder-root</code> для пункту «Корінь»
- [KSambaShare] Уможливлено тестування засобу обробки «net usershare info»
- У діалогові вікна для роботи з файлами на панель інструментів додано кнопку «Критерій упорядковування»

### Kirigami

- DelegateRecycler: усунено створення нового propertiesTracker для кожного делегування
- Сторінку «Про програму» пересунуто з Discover до Kirigami
- Реалізовано приховування контекстної висувної панелі, якщо видимою є загальна панель інструментів
- Забезпечено належне компонування усіх пунктів (виправлено ваду 400671)
- Реалізовано зміну індексу у відповідь на натискання, а не на клацання (виправлено ваду 400518)
- Додано нові розміри тексту для заголовків
- Тепер бічні висувні панелі не пересувають загальних верхніх і нижніх колонтитулів

### KNewStuff

- Додано корисні для програмування сигнали щодо помилок

### KNotification

- NotifyByFlatpak перейменовано на NotifyByPortal
- Портал сповіщень: реалізовано підтримку растрових зображень у сповіщеннях

### Набір бібліотек KPackage

- Усунено створення даних appstream для файлів, у яких не міститься опис (виправлено ваду 400431)
- Реалізовано перехоплення метаданих пакунка до початку встановлення

### KRunner

- При повторному використанні засобів запуску при перезавантаженні реалізовано перезавантаження налаштувань (виправлено ваду 399621)

### KTextEditor

- Уможливлено використання від'ємних значень пріоритетності визначення синтаксичних конструкцій
- Можливість «Увімкнути/Вимкнути коментування» отримала свій пункту меню інструментів і типове клавіатурне скорочення (виправлено ваду 387654)
- Усунено приховування пунктів мов у меню режимів
- SpellCheckBar: реалізовано використання DictionaryComboBox замість звичайного QComboBox
- KTextEditor::ViewPrivate: усунено причину попередження «Запит щодо тексту для некоректного діапазону»
- Android: вилучено непотрібне визначення log2
- Контекстне меню від'єднано від усіх засобів отримання даних aboutToXXContextMenu (виправлено ваду 401069)
- Впроваджено AbstractAnnotationItemDelegate для забезпечення додаткового контролю засобами споживання

### KUnitConversion

- Оновлено одиниці нафтової промисловості (виправлено ваду 388074)

### KWayland

- Реалізовано автоматичне створення файла журналу + виправлено файл категорій
- До PlasmaWindowModel додано VirtualDesktops
- Додано перевірку PlasmaWindowModel відповідно до змін у VirtualDesktop
- Реалізовано спорожнення windowInterface у тестах до знищення windowManagement
- Реалізовано вилучення належного запису у removeDesktop
- Реалізовано очищення запису списку керування віртуальними стільницями у деструкторі PlasmaVirtualDesktop
- Використано належну версію нового доданого інтерфейсу PlasmaVirtualDesktop
- [сервер] Реалізовано підказку щодо введення тексту та мети для окремих версій протоколу
- [сервер] Зворотні виклики активації та деактивації, вмикання та вимикання текстового введення розміщено у дочірніх класах
- [сервер] Зворотний виклик із uint для навколишнього тексту розташовано у класі v0
- [сервер] Деякі виключні зворотні виклики щодо введення тексту v0 розташовано у класі v0

### KWidgetsAddons

- До Kirigami.Heading додано програмний інтерфейс рівня

### KXMLGUI

- Оновлено текст «Про KDE»

### NetworkManagerQt

- Виправлено ваду у параметрах ipv4 і; ipv6
- Додано параметри ovs-bridge і ovs-interface
- Оновлено параметри IP-тунелювання
- Додано параметри проксі-сервера і користувача
- Додано параметр IpTunnel
- Уможливлено збирання перевірки параметрів tun за будь-яких умов
- Додано пропущені параметри IPv6
- Реалізовано очікування на дані на інтерфейсах DBus замість зареєстрованих служб (виправлено ваду 400359)

### Бібліотеки Plasma

- Реалізовано паритетність меню зі стилем стільниці
- Мінімальною можливою версією Qt тепер є 5.9
- Знову додано випадково вилучений рядок у CMakeLists.txt
- Реалізовано 100%c відповідність розмірам заголовків у kirigami
- Реалізовано кращу сумісність за заголовками із Kirigami
- Реалізовано встановлення обробленої версії приватних імпортувань
- Реалізовано засоби керування форматуванням тексту для мобільних пристроїв
- Оновлено схеми кольорів breeze-light та breeze-dark
- Виправлено низку витоків пам'яті (завдяки ASAN)

### Purpose

- Додаток phabricator: реалізовано використання порядку запису diff.rev. Arcanist (виправлено ваду 401565)
- Додано заголовок для JobDialog
- Поліпшено початкові розміри JobDialog (виправлено ваду 400873)
- Уможливлено оприлюднення різних адрес у menudemo
- Реалізовано використання QQC2 для JobDialog (виправлено ваду 400997)

### QQC2StyleBridge

- Реалізовано однакові розміри пунктів із QWidgets
- Виправлено встановлення розмірів меню
- Забезпечено вирівнювання елементів, які блимають, за пікселями
- Реалізовано підтримку заснованих на QGuiApplication програм (виправлено ваду 396287)
- Додано засоби керування текстом для сенсорних екранів
- Реалізовано визначення розміру за вказаними шириною та висотою піктограм
- Уможливлено використання властивості flat для кнопок
- Виправлено проблему, яка виникала, якщо у меню лише один елемент (виправлено ваду 400517)

### Solid

- Виправлено зміну піктограми кореневого диска. Тепер ця зміна не призводить до помилкової зміни інших піктограм.

### Sonnet

- DictionaryComboBoxTest: додано розтяжку з метою уникнути розтягування кнопки «Дамп»

### Підсвічування синтаксису

- BrightScript: уможливлено використання sub без назв
- Додано файл підсвічування для даних трасування Wayland
- Додано підсвічування синтаксичних конструкцій для TypeScript і TypeScript React
- Rust і Yacc/Bison: поліпшено обробку коментарів
- Prolog і Lua: виправлено вступний запис
- Виправлено завантаження даних мови після включення ключових слів з цієї мови до іншого файла
- Додано синтаксис BrightScript
- debchangelog: додано Disco Dingo

### Відомості щодо безпеки

Випущений код підписано за допомогою GPG з використанням такого ключа: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Відбиток основного ключа: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB

Обговорити цей випуск та поділитися ідеями можна у розділі коментарів до <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>статті з новиною</a>.
