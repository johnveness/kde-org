---
aliases:
- ../announce-applications-18.08.1
changelog: true
date: 2018-09-06
description: KDE, KDE Uygulamalar 18.08.1'i Gönderdi
layout: application
title: KDE, KDE Uygulamalar 18.08.1'i Gönderdi
version: 18.08.1
---
September 6, 2018. Today KDE released the first stability update for <a href='../18.08.0'>KDE Applications 18.08</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Kaydedilen bir düzineden fazla hata düzeltmesi, diğerlerinin yanı sıra Kontak, Cantor, Gwenview, Okular, Umbrello'daki iyileştirmeleri içerir.

İyileştirmeler şunları içerir:

- KIO-MTP bileşeni, cihaza farklı bir uygulama tarafından zaten erişildiğinde artık çökmez
- K Posta'da posta göndermek artık parola istemi ile belirtildiğinde parolayı kullanıyor
- Okular artık PDF belgelerini kaydettikten sonra kenar çubuğu modunu hatırlıyor
