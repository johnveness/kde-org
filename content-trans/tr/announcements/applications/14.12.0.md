---
aliases:
- ../announce-applications-14.12.0
changelog: true
date: '2014-12-17'
description: KDE Uygulamalar 14.12'yi Gönderdi.
layout: application
title: KDE, KDE Uygulamaları 14.12'yi Gönderdi
version: 14.12.0
---
December 17, 2014. Today KDE released KDE Applications 14.12. This release brings new features and bug fixes to more than a hundred applications. Most of these applications are based on the KDE Development Platform 4; some have been converted to the new <a href='https://dot.kde.org/2013/09/25/frameworks-5'>KDE Frameworks 5</a>, a set of modularized libraries that are based on Qt5, the latest version of this popular cross-platform application framework.

<a href='http://api.kde.org/4.x-api/kdegraphics-apidocs/libs/libkface/libkface/html/index.html'>Libkface</a> bu sürümde yenidir; fotoğraflarda yüz tanıma ve yüz tanıma sağlayan bir kitaplıktır.

The release includes the first KDE Frameworks 5-based versions of <a href='http://www.kate-editor.org'>Kate</a> and <a href='https://www.kde.org/applications/utilities/kwrite/'>KWrite</a>, <a href='https://www.kde.org/applications/system/konsole/s'>Konsole</a>, <a href='https://www.kde.org/applications/graphics/gwenview/s'>Gwenview</a>, <a href='http://edu.kde.org/kalgebras'>KAlgebra</a>, <a href='http://edu.kde.org/kanagram'>Kanagram</a>, <a href='http://edu.kde.org/khangman'>KHangman</a>, <a href='http://edu.kde.org/kig'>Kig</a>, <a href='http://edu.kde.org/parley'>Parley</a>, <a href='https://www.kde.org/applications/development/kapptemplate/'>KApptemplate</a> and <a href='https://www.kde.org/applications/utilities/okteta/'>Okteta</a>. Some libraries are also ready for KDE Frameworks 5 use: analitza and libkeduvocdocument.

Geliştiriciler yeni enerjilerini KDE Frameworks 5'e taşımak için kullanırken <a href='http://kontact.kde.org'>Kontak Suite</a> artık 4.14 sürümünde Uzun Süreli Destek kapsamındadır.

Bu sürümdeki yeni özelliklerden bazıları şunlardır:

+ <a href='http://edu.kde.org/kalgebra'>KAlgebra</a> has a new Android version thanks to KDE Frameworks 5 and is now able to <a href='http://www.proli.net/2014/09/18/touching-mathematics/'>print its graphs in 3D</a>
+ <a href='http://edu.kde.org/kgeography'>K Coğrafya</a>'nın Bihar için yeni bir haritası var.
+ Belge görüntüleyici <a href='http://okular.kde.org'>Okular</a> artık dvi'de lateks-synctex ters arama için desteğe ve ePub desteğinde bazı küçük iyileştirmelere sahiptir.
+ <a href='http://umbrello.kde.org'>Umbrello</a> --the UML modeller-- has many new features too numerous to list here.

KDE Applications 15.04'ün Nisan sürümü, birçok yeni özelliğin yanı sıra modüler KDE Frameworks 5'e dayalı daha fazla uygulama içerecek.
