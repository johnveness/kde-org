---
aliases:
- ../../kde-frameworks-5.34.0
date: 2017-05-13
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- balooctl, baloosearch, balooshow: QCoreApplication'i objekti loomise järjekorra parandus (veateade 378539)

### Breeze'i ikoonid

- hotspot'i (https://github.com/KDAB/hotspot)ikoonide lisamine
- Paremad versioohihalduse ikoonid (veateade 377380=
- Plasmate'i ikooni lisamine (veateade 376780)
- microphone-sensitivity ikoonide uuendamine (veateade 377012)
- Paneeliikoonide vaikesuuruse tõstmine 48 peale

### CMake'i lisamoodulid

- Korrastajad: GCC-laadis lippe ei kasutata näiteks MSVC peal
- KDEPackageAppTemplates: dokumentatsiooni täiustused
- KDECompilerSettings: -Wvla &amp; -Wdate-time edastamine
- Vanemate qmlplugindump'i versioonide toetamine
- ecm_generate_qmltypes pakkumine
- Projektidel lubatakse faili kaasata kaks korda
- rx parandus, mis sobib projekti nimedega git-i uri põhjal
- Ehitamiskäsu fetch-translations pakkumine
- -Wno-gnu-zero-variadic-macro-arguments'i sagedasem kasutamine

### KActivities

- Me kasutame ainult 1. kihi raamistikke, nii et liigume aga 2. kihti
- KIO eemaldamine sõltuvustest

### KAuth

- Turvaparandus: kontroll, et kes iganes meid välja kutsub, on tõepoolest see, kellena esineb

### KConfig

- relativePath'i arvutamise parandus KDesktopFile::locateLocal()-s (veateade 345100)

### KConfigWidgets

- Annetamistoimingule ikooni määramine
- QGroupBoxes'i töötlemise piirangute leevendamine

### KDeclarative

- ItemHasContents'i ei määrata DropArea's
- Hiirealuseid sündmusi ei tunnistata DragArea's

### KDocTools

- Hädalahendus kataloogi laadimiseks MSVC-s
- meinproc5 nähtavuse konflikti lahendamine (veateade 379142)
- Veel mõne asukohaga muutuja panemine jutumärkidesse (vältimaks probleeme tühikutega)
- Mõne asukohaga muutuja panemine jutumärkidesse (vältimaks probleeme tühikutega)
- Kohaliku dokumentatsiooni ajutine keelamine Windowsis
- FindDocBookXML4.cmake, FindDocBookXSL.cmake - otsing homebrew paigaldustes

### KFileMetaData

- KArchive ei ole enam kohustuslik ja seda vajavaid ekstraktoreid enam ei ehitata
- topeltsümbolite kompillerimistõrke parandus mingw peal Windowsis

### KGlobalAccel

- ehitamine: KService'i sõltuvuse eemaldamine

### KI18n

- po-failide baasnime käitlemise parandus (veateade 379116)
- ki18n eellaadimise parandus

### KIconThemes

- Isegi ei püüta luua tühja suurusega ikoone

### KIO

- KDirSortFilterProxyModel: loomuliku sortimise tagasitoomine (veateade 343452)
- UDS_CREATION_TIME'i täitmine st_birthtime'i väärtusega FreeBSD peal
- http slave: vealehekülje saatmine pärast autentimise nurjumist (veateade 373323)
- kioexec: üleslaadimise delegeerimine kded moodulile (veateade 370532)
- KDirlister'i Gui testi parandus, mis määras URL-skeemi kaks korda
- Kiod mooduli kustutamine väljumisel
- Faili moc_predefs.h genereerimine KIOCore'ile (veateade 371721)
- kioexec: --suggestedfilename'i toetuse parandus

### KNewStuff

- Mitme sama nimega kategooria lubamine
- KNewStuff: faili suuruseteabe näitamine grid'i delegaadis
- Kui kirje suurus ei ole teada, näidatakse seda loendivaates
- KNSCore::EntryInternal::List'i registreerimine ja deklareerimine metatüübina
- Lülitist läbikukkumist ei lubata. Topeltkirjed? Tänan, ei
- allalaaditud fail suletakse alati pärast allalaadimist

### KPackage raamistik

- Kaasatu asukoha parandus KF5PackageMacros.cmake's
- Hoiatuste eiramine appdata genereerimisel (veateade 378529)

### KRunner

- Mall: tipptaseme malli kategooriaks määratakse "Plasma"

### KTextEditor

- KAuth'i lõimimine dokumentide salvestamisel - 2. peatükk
- Eelduse parandus koodi voltimise rakendamisel, mis muudab kursori asukohta
- Mitteiganenud &lt;gui&gt; juurelemendi kasutamine ui.rc-failis
- Kerimisriba tähiste lisamine ka sisseehitatud otsimisse ja asendamisse
- KAuth'i lõimimine dokumentide salvestamisel

### KWayland

- Pinna kehtivuse kontrollimine TextInput'i lahkumissündmuse saatmisel

### KWidgetsAddons

- KNewPasswordWidget: nähtavustoimingut ei peideta lihtteksti režiimis (veateade 378276)
- KPasswordDialog: nähtavustoimingut ei peideta lihtteksti režiimis (veateade 378276)
- KActionSelectorPrivate::insertionIndex() parandus

### KXMLGUI

- kcm_useraccount on surnud, elagu ja õitsegu user_manager
- Korratavad ehitamised: XMLGUI_COMPILING_OS'i versiooni hülgamine
- Parandus: DOCTYPE'i nimi peab sobima juurelemendi tüübiga
- ANY vale kasutuse parandus kpartgui.dtd's
- Mitteiganenud &lt;gui&gt; juurelemendi kasutamine
- API dox'i parandused: 0 asendamine nullptr'iga või üldse eemaldamine, kui ei kasutata

### NetworkManagerQt

- Krahhi vältimine aktiivsete ühenduste loendi hankimisel (veateade 373993)
- Automaatsete läbirääkimiste vaikeväärtuse määramine töötava NM versiooni alusel

### Oxygeni ikoonid

- hotspot'i (https://github.com/KDAB/hotspot) ikooni lisamine
- Paneeliikoonide vaikesuuruse tõstmine 48 peale

### Plasma raamistik

- ikooni taaslaadimine, kui usesPlasmaTheme muutub
- Plasma Components 3 paigaldamine, et neid saaks kasutada
- units.iconSizeHints.* lisamine pakkumaks kasutaja seadistatavaid ikoonisuuruse vihjeid (veateade 378443)
- [TextFieldStyle] tõrke textField ei ole defineeritud parandus
- ungrabMouse'i häki uuendamine Qt 5.8 peale
- Kaitse "aplett ei laadi AppletInterface'i" eest (veateade 377050)
- Kalender: korrektse keele kasutamine kuu- ja päevanimedes
- plugins.qmltypes-failide genereerimine pluginatele, mida me paigaldame
- Kui kasutaja määras otseselt suuruse, see säilitatakse

### Solid

- Kaasatu lisamine, mida vajab msys2

### Süntaksi esiletõstmine

- Arduino laienduse lisamine
- LaTeX: Fix Incorrect termination of iffalse comments (bug 378487)

### Turbeteave

The released code has been GPG-signed using the following key: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Väljalaske üle arutada ja mõtteid jagada saab <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>meie uudistelehekülje artikli</a> kommentaarides.
