---
aliases:
- ../../kde-frameworks-5.33.0
date: 2017-04-08
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Käskude kirjelduse lisamine (balooctl)
- Ka nimeviidaga osutatud kataloogides otsimine (veateade 333678)

### BluezQt

- Seadmetüübi pakkumine madalenergiaseadmetele

### CMake'i lisamoodulid

- qml-root-path'i määramine jagatud kataloogina prefiksis
- ecm_generate_pkgconfig_file'i uue cmake'iga ühilduvuse parandus
- APPLE_* options registreeritakse ainult, kui if(APPLE)

### KActivitiesStats

- Eelseadistuste lisamine testirakendusele
- Elementide kohane liigutamine soovitud asukohta
- Järjekorra muutmise sünkroonimine teiste töötavate mudelitega
- Kui järjekord ei ole defineeritud, sorditakse kirjed ID järgi

### KDE Doxygeni tööriistad

- [Meta] hooldaja muutmine setup.py's

### KAuth

- Mac'i taustaprogramm
- KAuth::ExecuteJob'i tapmise toetuse lisamine

### KConfig

- Kiirklahvide loendi korrastamine kdeglobals'i lugemisel/kirjutamisel
- Tarbetute taasomistamiste vältimine squeeze'i väljakutse eemaldamisega ajutisest puhvrist

### KDBusAddons

- KDBusService: aksessori lisamine dbus'i teenuse nimele, mis on registreeritud

### KDeclarative

- Alates Qt &gt;= 5.8 kasutatakse stseenigraafi taustaprogrammi määramiseks uut API-t
- acceptHoverEvents'i ei määrata DragArea's, sest me ei kasuta neid

### KDocTools

- meinproc5: linkimine failide, mitte teegia (veateade 377406)

### KFileMetaData

- PlainTextExtractor vastab jälle "text/plain"-ile

### KHTML

- Vealeheküljel laaditakse korrektne pilt (koos tõelise URL-iga)

### KIO

- Kaugfaili file:// URL-i ümbersuunamine smb:// peale toimib taas
- Päringu kodeering säilitatakse HTTP puhverserveri kasutamisel
- Identifikaatorite uuendamine (Firefox 52ESR, Chromium 57)
- Töö kirjeldusele omistatud näidatava stringi url-i käitlemine/kärpimine. Väldib suurte andme-url-ide sattumist kasutajaliidese märguannetesse
- KFileWidget::setSelectedUrl() lisamine (veateade 376365)
- KUrlRequester'i salvestusrežiimi parandus setAcceptMode'i lisamisega

### KItemModels

- Uue QSFPM::setRecursiveFiltering(true) mainimine, mis muudab KRecursiveFilterProxyModel'i iganenuks

### KNotification

- Järjekorras märguandeid ei eemaldata fd.o teenuse käivitamisel
- Mac'i platvormi kohandused

### KParts

- API dox: puuduva märkuse parandus kutsuda setXMLFile välja KParts::MainWindow'ga

### KService

- Terminaliteadete 'Ei leitud: ""' parandus

### KTextEditor

- Vaate sisemise lisafunktsionaalsuse paljastamine avalikule API-le
- Rohke omistamise salvestamine setPen'is
- KTextEditor::Document'i ConfigInterface'i parandus
- Fondi ja kirjutamise ajal õigekirja kontrollimise valikute lisamine ConfigInterface'i

### KWayland

- wl_shell_surface::set_popup ja popup_done toetuse lisamine

### KWidgetsAddons

- qt peal ehitamise toetus ka ilma a11y lubamata
- Vale suurusevihje parandus, kui animatedShow kutsutakse välja peidetud eellasega (veateade 377676)
- Sümbolite kärpimise vältimine KCharSelectTable'is
- Kõigi tasandite lubamine kcharselect'i testdialoogis

### NetworkManagerQt

- WiredSetting: automaatse läbirääkimise tagastamine, isegi kui see on keelatud
- Takistatakse glib2 signaalide defineerimist Qt poolt
- WiredSetting: kiirust ja dupleksit saab määrata ainult siis, kui automaatne läbirääkimine on välja lülitatud (veateade 376018)
- võrguseadistuse automaatse läbirääkimise väärtus peab olema false

### Plasma raamistik

- [ModelContextMenu] Instantiator'i kasutamine Repeater'i-ja-reparent-hack'i asemel
- [Kalender] nädalanimede kärpimine nagu seda tehakse päevadelegaadi korral (veateade 378020)
- [Icon Item] omadus "smooth" pannakse ka tegelikult midagi tegema
- Tuletatud suuruse määramine lähtesuuruse põhjal pildi/SVG URL-i allikatele
- uue omaduse lisamine konteinerisse redigeerimisrežiimi tarbeks
- maskRequestedPrefix'i korrigeerimine, kui prefiksit ei kasutata (veateade 377893)
- [Menüü] openRelative asetuse harmoneerimine
- Enamikul (konteksti)menüüdel on nüüd kiirklahvid (Alt+ltäht) (veateade 361915)
- Plasma juhtelemendid QtQuickControls2 alusel
- applyPrefixes'i käitlemine tühja stringiga (veateade 377441)
- vana teemapuhvri tegelik kustutamine
- [Konteineriliides] kontekstimenüü käivitamine klahvile "Menu" vajutades
- [Breeze'i Plasma Teema] action-overlay ikoonide täiustamine (veateade 376321)

### Süntaksi esiletõstmine

- TOML: stringi paojadade esiletõstmise parandus
- Clojure süntaksi esiletõstmise uuendamine
- Mõned uuendused OCaml'i süntaksis
- *.sbt-failide esiletõstmine scala koodina
- Ka .qmltypes-failide korral kasutatakse QML-i esiletõstmist

### Turbeteave

The released code has been GPG-signed using the following key: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Väljalaske üle arutada ja mõtteid jagada saab <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>meie uudistelehekülje artikli</a> kommentaarides.
