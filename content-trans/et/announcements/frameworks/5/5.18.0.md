---
aliases:
- ../../kde-frameworks-5.18.0
date: 2016-01-09
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Mitme mtime'iga seotud otsingu probleemi parandus
- PostingDB Iter: ei eeldata MDB_NOTFOUND
- Balooctl status: "sisu indekseerimise" näitamise vältimine kataloogide korral
- StatusCommand: kataloogide korrektse oleku näitamine
- SearchStore: tühjade otsinguväärtuste viisakas käitlemine (veateade 356176)

### Breeze'i ikoonid

- ikoonide uuendamine ja lisamine
- Ka 32px olekuikoonidele 22px variandid, mida läheb tarvis süsteemisalves
- Breeze Darki 32px kataloogide väärtuse muutmine fikseeritult skaleeritavale

### CMake'i lisamoodulid

- KAppTemplate CMake'i mooduli muutmine globaalseks
- CMP0063 hoiatuste vaigistamine KDECompilerSettings'is
- ECMQtDeclareLoggingCategory: &lt;QDebug&gt; kaasamine genereeritud failiga
- CMP0054 hoiatuste parandus

### KActivities

- QML-i laadimise muutmine sujuvaks juhtimismoodulites (veateade 356832)
- Hädalahendus Qt SQL-i veale, mis ei luba ühendusi korralikult puhastada (veateade 348194)
- Plugina ühendamine, mis käivitab tegevuse oleku muutumisel rakendused
- Portimine KService'i pealt KPluginLoader'i peale
- Pluginate portimine kasutama kcoreaddons_desktop_to_json()

### KBookmarks

- DynMenuInfo täielik initsialiseerimine tagastuväärtuses

### KCMUtils

- KPluginSelector::addPlugins: eelduse parandus, kui parameeter 'config' on vaikeväärtus (veateade 352471)

### KCodecs

- Täis puhvri tahtliku ületäite vältimine

### KConfig

- grupi korraliku varjestamatuse tagamine kconf_update'is

### KCoreAddons

- KAboutData::fromPluginMetaData(const KPluginMetaData &amp;plugin) lisamine
- KPluginMetaData::copyrightText(), extraInformation() ja otherContributors() lisamine
- KPluginMetaData::translators() ja KAboutPerson::fromJson() lisamine
- after-free kasutuse parandus töölauafaili parseris
- KPluginMetaData on nüüd võimalik luua json'i asukoha põhjal
- desktoptojson: puuduvat teenusetüübi faili peetakse nüüd binaarfaili puhul veaks
- kcoreaddons_add_plugin'i väljakutset ilma SOURCES'ita peetakse nüüd veaks

### KDBusAddons

- Kohandamine Qt 5.6 dbus-in-secondary-thread'i peale

### KDeclarative

- [DragArea] omaduse dragActive lisamine
- [KQuickControlsAddons MimeDatabase] QMimeType kommentaari esiletoomine

### KDED

- kded: kohandamine Qt 5.6 lõimedega dbus'i peale: messageFilter peab käivitama mooduli laadimise põhilõimes

### KDELibs 4 toetus

- kdelibs4support nõuab kded'd (kdedmodule.desktop'i jaoks)
- CMP0064 hoiatuse parandus reegli CMP0054 määramisega väärtusele NEW
- Ei ekspordita sümboleid, mida juba sisaldab KWidgetsAddons

### KDESU

- fd lekitamise vältimine pesa loomisel

### KHTML

- Windows: kdewin'i sõltuvuse eemaldamine

### KI18n

- Mitmuse esimese argumendi reegli dokumenteeerimine QML-is
- Soovimatute tüübimuutuste vähendamine
- Make it possible to use doubles as index for i18np*() calls in QML

### KIO

- kiod parandus Qt 5.6 lõimedega dbus'is: messageFilter peab tagastamist ootama mooduli laadimise järele
- Veakoodi muutmine asetamisel/liigutamisel alamkataloogi
- emptyTrash'i blokkimise probleemi parandus
- Võrgu-URL-ide vale nupu parandus KUrlNavigator'is
- KUrlComboBox: urls() põhjal absoluutse asukoha tagastamise parandus
- kiod: seansihalduse keelamine
- Sisendi '.' automaatse lõpetamise lisamine, mis pakub kõiki peidetud faile/katalooge  (veateade 354981)
- ktelnetservice: ühe võrra nihkes argc kontrolli parandus, paiga pakkus Steven Bromley

### KNotification

- [Notify By Popup] saatmine vastavalt sündmuse ID-le
- Vaikimisi näidatakse ekraanisäästja keelamise põhjust (veateade 334525)
- Vihje lisamine märguannete rühmitamise vältimiseks (veateade 356653)

### KNotifyConfig

- [KNotifyConfigWidget] konkreetse sündmuse valimise lubamine

### Paketiraamistik

- Metaandmete esitamise võimaldamine json'is

### KPeople

- Võimaliku topeltkustutamise vältimine DeclarativePersonData's

### KTextEditor

- pli süntaksi esiletõstmine: lisati sisseehitatud funktsioonid, lisati laiendatavad piirkonnad

### KWalleti raamistik

- kwalletd: FILE* lekke parandus

### KWindowSystem

- xcb variandi lisamine staatilistele KStartupInfo::sendFoo meetoditele

### NetworkManagerQt

- töötamine ka vanemate NM versioonidega

### Plasma raamistik

- [ToolButtonStyle] alati näidatakse activeFocus't
- SkipGrouping lipu kasutamine märguandes "vidin kustutatud" (veateade 356653)
- Nimeviitade kohane käitlemine pakettide asukohtades
- HiddenStatus'e lisamine plasmoidide omal algatusel peitmiseks
- Aknaid ei suunata ümber, kui element on keelatud või peidetud (veateade 356938)
- statusChanged jäetakse väljastamata, kui olek ei ole muutunud
- Idasuunaliste elementide ID-de parandus
- Containment: appletCreated'i väljastamata jätmine null apleti korral (veateade 356428)
- [Containment Interface] hüpleva ülitäpse kerimine parandus
- KPluginMetada omaduse X-Plasma-ComponentTypes lugemine stringiloendina
- [Akna pisipildid] krahhi vältimine, kui komposiit on keelatud
- Konteinerid võivad tühistada CompactAppleti.qml'i

Väljalaske üle arutada ja mõtteid jagada saab <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>meie uudistelehekülje artikli</a> kommentaarides.
