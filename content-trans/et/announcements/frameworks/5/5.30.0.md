---
aliases:
- ../../kde-frameworks-5.30.0
date: 2017-01-14
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Breeze'i ikoonid

- Haguichi ikoonide lisamine tänu Stephem Brandtile
- calligra ikooni toetuse viimistlemine
- cups'i ikooni lisamine tänu colinile (veateade 373126)
- kalarm'i ikooni uuendamine (veateade 362631)
- krfb rakenduseikooni lisamine (veateade 373362)
- r'i MIME tüübi toetuse lisamine (veateade 371811)
- ja veel palju rohkem

### CMake'i lisamoodulid

- appstreamtest: paigaldamata programmide käitlemine
- Värviliste hoiatuste lubamine ninja väljundis
- Parandus: puuduv :: API dokumentatsioonis käivitas koodile stiili rakendamise
- Masina libs/includes/cmakeconfig-failide eiramine Androidi tööriistaahelas
- gnustl_shared'i kasutuse dokumenteerimine Androidi tööriistaahelas
- -Wl,--no-undefined'it ei kasutata kunagi Mac'i (APPLE) peal

### Raamistike lõimimine

- KPackage'i KNSHandler'i täiustamine
- AppstreamQt täpsema nõutud versiooni otsimine
- KNewStuff'i toetuse lisamine KPackage'ile

### KActivitiesStats

- Kompileerimise tagamine -fno-operator-names'iga
- Rohkem lingitud ressursse ei tõmmata, kui üks neist kaob
- Mudeli rolli lisamine ressursi lingitud tegevuste loendi hankimiseks

### KDE Doxygeni tööriistad

- Androidi lisamine saadaolevate platvormide loendisse
- ObjC++ lähtefailide kaasamine

### KConfig

- Isendi genereerimine KSharedConfig'i abil singletonile ja argumendile
- kconfig_compiler: nullptr kasutamine genereeritud koodis

### KConfigWidgets

- Toimingu "Näita menüüriba" peitmine, kui kõik menüüribad on omaribad
- KConfigDialogManager: kdelibs3 klasside hülgamine

### KCoreAddons

- stringlist/boolean tüüpide tagastamine KPluginMetaData::value's
- DesktopFileParser: välja ServiceTypes arvestamine

### KDBusAddons

- Pythoni seoste lisamine KDbusAddons'ile

### KDeclarative

- org.kde.kconfig'i QML-impordi pakkumine KAuthorized'iga

### KDocTools

- kdoctools_install: programmi täieliku asukoha vastavus (veateade 374435)

### KGlobalAccel

- [runtime] keskkonnamuutuja KGLOBALACCEL_TEST_MODE pakkumine

### KDE GUI Addons

- Pythoni seoste lisamine KGuiAddons'ile

### KHTML

- Plugina andmete määramine, et põimitud pildinäitaja saaks töötada

### KIconThemes

- Töölaua ikooniteema muumisest teavitatakse ka QIconLoader'it (veateade 365363)

### KInit

- Rakenduse käivitamisel klauncher'i kaudu võetalse arvesse omadust X-KDE-RunOnDiscreteGpu

### KIO

- KIO::iconNameForUrl tagastab nüüd xdg asukohtade, näiteks kasutaja kataloogi Pildid puhul eriikoonid
- ForwardingSlaveBase: lipu Overwrite kio_desktop'ile edastamise parandus (vateade 360487)
- Klassi KPasswdServerClient, mis on kpasswdserver'i kliendi API, täiustamine ja importimine
- [KPropertiesDialog] Valiku "Näitamine süsteemisalves" kaotamine
- [KPropertiesDialog] .desktop-failide "Lingi" "Nime" ei muudeta, kui failinimi on kirjutuskaitstud
- [KFileWidget] urlFromString'i kasutamine QUrl(QString) asemel setSelection'is (veateade 369216)
- KPropertiesDialog'i valiku lisamine käivitada rakendus diskreetse graafikakaardi peal
- DropJob'i kohane hävitamine pärast täitmisfaili hülgamist
- Logimiskategooria kasutuse parandus Winsowsi peal
- DropJob: kopeerimistöö väljastamine loomise järel
- Toetuse lisamine suspend() väljakutsumiseks CopyJob'ile enne selle käivitumist
- Tööde kohese peatamise toetuse lisamine vähemalt SimpleJob'is ja FileCopyJob'is

### KItemModels

- Hiljuti realiseeritud veaklasside puhverserverite uuendamine (layoutChanged'i käitlemine)
- KConcatenateRowsProxyModel võib nüüd töötada koos QML-iga
- KExtraColumnsProxyModel: Persist mudel indekseerib pärast layoutChange'i väljastamist, mitte enne
- Eelduse parandus (beginRemoveRows'is) valitud alama tühja alama valimise tühistamisel korganizer'is

### KJobWidgets

- Fookust ei viida edenemisknale (veateade 333934)

### KNewStuff

- [GHNS nupp] peidetakse KIOSK-i piirangu rakendamisel
- ::Engine seadistamise parandus, kui see loodi seadistusfaili absoluutse asukohaga

### KNotification

- Unity käivitajate käsitsi testi lisamine
- [KNotificationRestrictions] kasutaja võib määrata piirangu põhjuse stringi

### KPackage raamistik

- [PackageLoader] vigast KPluginMetadata't ei kasutata (veateade 374541)
- Valiku -t kirjelduse parandus manuaalileheküljel
- Tõrketeate täiustus
- --type abiteate parandus
- KPackage'i kimpude paigaldamise täiustus
- Faili kpackage-generic.desktop paigaldamine
- qmlc-failiid jäetakse paigaldamata
- Plasmoide ei loetleta eraldi failidest metadata.desktop ja .json
- Eri tüüpi, aga sama ID-ga pakettide lisaparandus
- CMake'i nurjumise parandus, kui kahel eri tüüpi paketil oli sama ID

### KParts

- Uue checkAmbiguousShortcuts() väljakutsumine MainWindow::createShellGUI'st

### KService

- KSycoca: nimeviitu ei järgita kataloogideni, mis võib põhjustada rekursiivsuse ohu

### KTextEditor

- Parandus: tekstitulemuste edasilohistus vales valikus (veateade 374163)

### KWalleti raamistik

- GpgME++ versiooni 1.7.0 nõutava miinimumi määramine
- "Kui Gpgmepp'i ei leita, püütakse kasutada KF5Gpgmepp'i" tagasivõtmine

### KWidgetsAddons

- Pythoni seoste lisamine
- Teist vidinat sisaldava kohtspikri KToolTipWidget lisamine
- KDateComboBox'i kontrollide parandus sisestatud kehtivate kuupäevade korral
- KMessageWidget: tumedama punase värvi kasutamine, kui tüübiks on Error (veateade 357210)

### KXMLGUI

- Käivitamisel hoiatatakse mitmeti tõlgendatavate kiirklahvide eest (erandiks tõstuklahv ja Delete)
- MSWin ja Mac on sarnase automaatsalvestuse aknasuuruse käitumisega
- Rakenduse versiooni näitamine teabedialoogi päises (veateade 372367)

### Oxygeni ikoonid

- sünkroonimine breeze'i ikoonidega

### Plasma raamistik

- Mitme komponendi omaduse renderType parandus
- [ToolTipDialog] puhverdatud KWindowSystem::isPlatformX11() kasutamine
- [Icon Item] vihjatava suuruse uuendamise parandus ikoonisuuruste muutumisel
- [Dialog] setPosition / setSize kasutamine kõige ükshaaval määramise asemel
- Units] Omaduse iconSizes muutmine sobivalt konstantseks
- Nüüd on olemas globaalne isend "Units", mis vähendab mälutarvet ja SVG-elementide loomisaega
- [Icon Item] muude kui ruudukujuliste ikoonide toetus (veateade 355592)
- plasmapkg2 vanade koodi kirjutatud tüüpide otsimine ja asendamine (veateade 374463)
- X-Plasma-Drop* tüüpide parandus (veateade 374418)
- [Plasma ScrollViewStyle] kerimisriba tausta näitamine ainult kursori all
- Veateate 374127 parandus: hüpikute vale asetus dokiakendes
- Plasma::Package API märkimine iganenuks PluginLoader'is
- Ülekontrollimine, millist esindatust tuleks kasutada setPreferredRepresentation'is
- [declarativeimports] QtRendering'i kasutamine telefoniseadmetes
- tühja paneeli hinnatakse alati kui "apletid laaditud" (veateade 373836)
- toolTipMainTextChanged'i väljastamine, kui see muutub tiitli muutumise tõttu
- [TextField] parooli näitamise nupu keelamise lubamine KIOSK-i piiranguga
- [AppletQuickItem] käivitamise tõrketeate toetus
- Noole käitlemise loogika parandus RTL-keeltes (veateade 373749)
- TextFieldStyle: implicitHeight'i väärtuse parandus, et tekstikursor oleks tsentreeritud

### Sonnet

- cmake: ka hunspell-1.6 otsimine

### Süntaksi esiletõstmine

- Makefile.inc'i esiletõstmise parandus makefile.xml'ile prioriteeti lisades
- SCXML-failide esiletõstmine XML-ina
- makefile.xml: palju täiustusi, mida läheks siin pikaks üles lugeda
- Pythoni süntaks: lisati f-literaalid ja täiustati stringide käitlemist

### Turbeteave

The released code has been GPG-signed using the following key: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Väljalaske üle arutada ja mõtteid jagada saab <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>meie uudistelehekülje artikli</a> kommentaarides.
