---
aliases:
- ../../kde-frameworks-5.28.0
date: 2016-11-15
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Uus raamistik: süntaksi esiletõstmine

Kate süntaksidefinitsioonide süntaksi esiletõstmise mootor

See on Kate süntaksi esiletõstmise mootori autonoomne teostus. See on mõeldud tarvilise ehituskivina nii tekstiredaktoritele kui ka lihtsaks esiletõstetud teksti renderdamiseks (nt. HTML), mis toetab nii lõimimist kohandatud redaktoriga kui ka allklassi QSyntaxHighlighter kohest kasutamist.

### Breeze'i ikoonid

- kstars'i toiminguikoonide uuendamine (veateade 364981)
- Breeze Dark on Süsteemi seadistustes esitatud kui Breeze, vale .themes-failiga (veateade 370213)

### CMake'i lisamoodulid

- KDECMakeSettings pandi tööle KDE_INSTALL_DIRS_NO_DEPRECATED'iga
- ECM-ile ei nõuta sõltuvusena pythoni seoseid
- Mooduli PythonModuleGeneration lisamine

### KActivitiesStats

- Lingi oleku eiramine UsedResources'i ja LinkedResources'i mudeli sortimisel

### KDE Doxygeni tööriistad

- [CSS] doxygen 1.8.12 muutuste tagasivõtmine
- Faili doxygenlayout lisamine
- Rühmanimede defineerimise viisi uuendamine

### KAuth

- Tagamine, et meil on võimalik teha üle päringu
- Tagamine, et me saame programmi väljundi põhjal teada edenemisest

### KConfig

- Tagamine, et me ei purusta kompileerimist varasemate katkiste ühikutega
- Failivälja ebatäielikku parsimist ei peeta saatuslikuks tõrkeks

### KCoreAddons

- Halva url-i näitamine
- Kasutaja avataride laadimine AccountsServicePath'ist, kui neid on (veateade 370362)

### KDeclarative

- [QtQuickRendererSettings] vaikeväärtuse parandus "false" pealt tühjaks

### KDELibs 4 toetus

- Prantsusmaa lipp kasutab ka tegelikult kogu pixmap'i

### KDocTools

- "checkXML5 genereerib html-faile kehtivate docbook'ide töökataloogis" parandus (veateade 371987)

### KIconThemes

- Mittetäisarvulise skaleerimisteguri toetus kiconengine'is (veateade 366451)

### KIdleTime

- Keelatakse konsooliväljundi ummistamine "oodatakse" teadetega

### KImageFormats

- imageformats/kra.h - KraPlugin capabilities() ja create() tühistamine

### KIO

- kio_http saadetud HTTP kuupäevavormingu parandus, et alati kasutaks C lokaati (veateade 372005)
- KACL: ASAN-i avastatud mälulekete parandus
- ASAN-i avastatud KIO::Scheduler'i mälulekete parandus
- Topelt puhastamisnupu eemaldamine (veateade 369377)
- Automaatkäivituse kirjete muutmise parandus, kui /usr/local/share/applications puudub (veateade 371194)
- [KOpenWithDialog] TreeView päise peitmine
- Nimeviitade nimepuhvri suuruse muutmine mõistlikuks (veateade 369275)
- Kohane DropJobs'i lõpetamine, kui signaali triggered ei ole väljastatud (veateade 363936)
- ClipboardUpdater: veel ühe krahhi parandus Wayland'is (veateade 359883)
- ClipboardUpdater: krahhi parandus Wayland'is (veateade 370520)
- Mittetäisarvulise skaleerimisteguri toetus KFileDelegate'is (veateade 366451)
- kntlm: NULL ja tühja domeeni eristamine
- Ülekirjutamisdialoogi ei näidata, kui failinimi on tühi
- kioexec: sõbralike failinimede kasutamine
- Fookuse omaniku parandus, kui url-i muudeti enne vidina näitamist
- Jõudluse tuntav suurendamine eelvaatluste väljalülitamisel failidialoogis (veateade 346403)

### KItemModels

- Pythoni seoste lisamine

### KJS

- FunctionObjectImp'i eksportimine, mida tarvitab khtml'i silur

### KNewStuff

- Rollide ja filtrite eraldi sortimine
- Võimaldamine pärida paigaldatud kirjeid

### KNotification

- Objekti, mida ei ole referentsitud, ei dereferentsita, kui märguandel puudub toiming
- KNotification'i ei taba enam krahh, kui seda kasutatakse QGuiApplication'is ja ükski märguandeteenus ei tööta (veateade 370667)
- Krahhi parandamine NotifyByAudio's

### KPackage raamistik

- Tagamine, et otsitakse nii json- kui ka töölaua metaandmeid
- Kaitse Q_GLOBAL_STATIC'i häivitamise vastu rakenduse töö lõpetamisel
- Ripneva viida parandus KPackageJob'is (veateade 369935)
- Võtmega seotud avastuse eemaldamine definitsiooni eemaldamisel
- Ikooni genereerimine appstream-failile

### KPty

- utempter'i asemel ulog-helper'i kasutamine FreeBSD-s
- utempter'i visam otsimine ka cmake'i põhilist prefiksit kasutades
- find_program ( utempter ...) nurjumiste hädaväljapääs
- ECM asukoha kasutamine utempter'i binaarfaili leidmiseks, mis on palju usaldusväärsem kui lihtsalt cmake'i prefiks

### KRunner

- i18n: stringide käitlemine kdevtemplate'i failides

### KTextEditor

- Breeze Dark: aktiivse rea taustavärvi tumendamine parema loetavuse huvides (veateade 371042)
- Sorditud Dockerfile'i juhised
- Breeze (Dark): kommentaaride muutmine veidi heledamaks parema loetavuse huvides (veateade 371042)
- CStyle'i ja C++/boost'i treppijate parandus, kui automaatsulud on lubatud (veateade 370715)
- Režiimirea "qautomaatsulgude" lisamine
- Teksti pärast faililõppu lisamise parandus (väga harv juhus)
- Vigaste xml'i esiletõstmise failide parandus
- Maxima: koodi kirjutatud värvide eemaldamine, itemData pealdise parandus
- OBJ, PLY ja STL'i süntaksi definitsioonide lisamine
- Praat'i süntaksi esiletõstmise toetuse lisamine

### KUnitConversion

- Uued soojus- ja elektriühikud ning ühikute mugavusfunktsioon

### KWalleti raamistik

- Kui Gpgmepp'i ei leita, püütakse kasutada KF5Gpgmepp'i
- Gpgmepp'i kasutamine GpgME-1.7.0-st

### KWayland

- Cmake'i ekspordi ümberpaigutatavuse täiustus
- [tööriistad] wayland_pointer_p.h genereerimise parandus
- [tööriistad] eventQueue meetodite genereerimine ainult globaalsetes klassides
- [server] Krahhi parandus fokuseeritud klaviatuuripinna uuendamisel
- [server] võimaliku krahhi parandus DataDevice'i loomisel
- [server] tagamine, et meil oleks DataSource DataDevice'il setSelection'is
- [tööriistad/generaator] ressursi hävitamise täiustus serveri poolel
- Nõude lisamine fookuse hoidmiseks rollipaneeli PlasmaShellSurface'il
- Paneeli automaatse peitmise toetuse lisamine PlasmaShellSurface'i liidesele
- Üldise QIcon'i läbi PlasmaWindow liidese edastamise toetus
- [server] üldise akna omaduse teostus QtSurfaceExtension'is
- [klient] meetodite lisamine ShellSurface'i saamiseks QWindow'st
- [server] viidasündmused saadetakse kliendi kõigile wl_pointer'i ressurssidele
- [server] wl_data_source_send_send'i ei kutsuta välja, kui DataSource ei ole seotud
- [server] deleteLater'i kasutamine, kui ClientConnection hävitatakse (veateade 370232)
- Suhtelise viida protokolli toetuse teostus
- [server] varasemast SeatInterface::setSelection'i valikust loobumine
- [server] klahvisündmused saadetakse kliendi kõigile wl_keyboard ressurssidele

### KWidgetsAddons

- kcharselect-generate-datafile.py liigutamine alamkataloogi
- kcharselect-generate-datafile.py skripti importimine ajalooga
- Iganenud sektsiooni eemaldamine
- Unicode'i autoriõiguse ja õiguste märkme lisamine
- Hoiatuse parandus: puuduv tühistus
- Sümboli SMP plokkide lisamine
- "Vaata ka" viidete parandus
- Puuduvate Unicode'i plokkide lisamine, järjekorra täiustamine (veateade 298010)
- märgikategooriate lisamine andmefaili
- Unicode'i kategooriate uuendamine andmefaili genereerimise skriptis
- andmefaili genereerimise faili kohandamine, et see suudaks parsida unicode 5.2.0 andmefaile
- tõlgete genereerimise paranduse edasiport
- Skriptil lastakse genereerida kcharselect'i andmefail ning kohatäiteks ka tõlge
- Skripti lisamine KCharSelect'i andmefaili genereerimiseks
- uus rakendus KCharSelect (praegu kdelibs'i vidinat lcharselect kasutades)

### KWindowSystem

- Cmake'i ekspordi ümberpaigutatavuse täiustus
- desktopFileName'i toetuse lisamine NETWinInfo'sse

### KXMLGUI

- Uues stiilis ühenduse KActionCollection::add<a href="">Action</a> kasutamise lubamine

### ModemManagerQt

- Kataloogi include parandus pri-failis

### NetworkManagerQt

- Kataloogi include parandus pri-failis
- moc'i tõrke parandus, mida põhjustas Q_ENUMS'i kasutamine nimeruumis Qt 5.8 korral

### Plasma raamistik

- tagamine, et OSD oleks ilma liputa Dialog (veateade 370433)
- konteksti omaduste määramine enne qml-i taaslaadimist (veateade 371763)
- Metaandmete faili ei parsita uuesti, kui see on juba laaditud
- Krahhi parandus qmlplugindump'is, kui QApplication puudub
- "Alternatiivide" menüüd vaikimisi ei näidata
- Uus tõeväärtus aktiveeritud signaali kasutamiseks laiendatu lülitamisel (veateade 367685)
- Plasma raamistiku ehitamise parandus Qt 5.5 peal
- [PluginLoader] operator&lt;&lt; kasutamine finalArgs'i korral initsialiseerimisloendi asemel
- kwayland'i kasutamine varjude ja dialoogi paigutamise tarvis
- Viimased puuduvad ikoonid ja võrgutäiustused
- availableScreenRect/Region'i liigutamine AppletInterface'i
- Konteineritoiminguid ei laadita põimitud konteinerites (süsteemisalves)
- Apleti alternatiivse menüü kirje nähtavuse uuendamine nõudmisel

### Solid

- Päringu tulemuste ebapüsiva järjekordne uus parandus
- CMake'i valiku lisamine HAL-i ja UDisks'i haldurite vahel lülitamiseks FreeBSD peal
- UDisks2 taustaprogrammi muutmine kompileeritavaks FreeBSD (ja võib-olla ka teiste UNIX-ite) peal
- Windows: tõrkedialooge ei näidata (veateade 371012)

### Turbeteave

The released code has been GPG-signed using the following key: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Väljalaske üle arutada ja mõtteid jagada saab <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>meie uudistelehekülje artikli</a> kommentaarides.
