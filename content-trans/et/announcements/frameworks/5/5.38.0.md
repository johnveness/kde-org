---
aliases:
- ../../kde-frameworks-5.38.0
date: 2017-09-09
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
Missing in last month's announcement: KF5 includes a new framework, Kirigami, a set of QtQuick components to build user interfaces based on the KDE UX guidelines.

### Baloo

- Kataloogipõhise otsingu parandus

### CMake'i lisamoodulid

- CMAKE_*_OUTPUT_5.38 määramine testide käitamiseks ilma paigaldamata
- Mooduli kaasamine qml-i importide leidmiseks käitusaja sõltuvustena

### Raamistike lõimimine

- Kõrglahutusega redigeerimisrea puhastamise ikooni tagasitoomine
- Dialoogidega Ctrl+Enter abil nõustumise parandus, kui nupud on ümber nimetatud

### KActivitiesStats

- Lingitud ja kasutatud ressursse hõlmavate päringute ümberkorraldamine
- Mudeli taaslaadimine, kui ressurss kaotab lingi
- Päringu parandus lingitud ja kasutatud ressursside ühendamisel

### KConfig

- DeleteFile/RenameFile toimingute pealdiste parandus (veateade 382450)
- kconfigini: alustava tühimärgi kõrvalejätmine kirje väärtuste lugemisel (veateade 310674)

### KConfigWidgets

- KStandardAction::Help'i ja KStandardAction::SaveOptions'i märkimine iganenuks
- DeleteFile/RenameFile toimingute pealdiste parandus (veateade 382450)
- "document-close" kasutamine KStandardAction::close'i ikoonina

### KCoreAddons

- DesktopFileParser: tagavaraotsingu lisamine ":/kservicetypes5/*"-s
- Paigaldamata pluginate toetuse lisamine kcoreaddons_add_plugin'is
- desktopfileparser: reeglivastase võtme/väärtuse parsimise parandus (veateade 310674)

### KDED

- X-KDE-OnlyShowOnQtPlatforms'i toetus

### KDocTools

- CMake: sihtmärgi nime lühendamise parandus, kui ehitamiskataloogil on erisümboleid (veateade 377573)
- CC BY-SA 4.0 International'i lisamine ja määramine vaikeväärtuseks

### KGlobalAccel

- KGlobalAccel: portimine KKeyServer'i uue meetodi symXModXToKeyQt peale parandamaks numbriklahvistiku klahve (veateade 183458)

### KInit

- klauncher: appId flatpak'i rakendustega vastendamise parandus

### KIO

- Veebikiirklahvide juhtimismooduli portimine KServiceTypeTrader'i pealt KPluginLoader::findPlugins'i peale
- [KFilePropsPlugin] totalSize'i vormindamine arvutamise ajal lokaadi alusel
- KIO: ammuse mälulekke parandus väljumisel
- MIME tüübi järgi filtreerimise võime lisamine KUrlCompletion'ile
- KIO: URI filtripluginate portimine KServiceTypeTrader'i pealt json'i+KPluginMetaData peale
- [KUrlNavigator] tabRequested'i väljastamine, kui menüüs tehakse keskklõps (veateade 304589)
- [KUrlNavigator] tabRequested'i väljastamine, kui asukohtade valijale tehakse keskklõps (veateade 304589)
- [KACLEditWidget] topeltklõpsu lubamine redigeerimiskirjel
- [kiocore] eelmise sissekande loogikavea parandus
- [kiocore] kontroll, kas klauncher töötab või mitt
- INF_PROCESSED_SIZE'i teadete tegelik piiramine (veateade 383843)
- Qt SSL SK sertifikaadihoidlat ei puhastata
- [KDesktopPropsPlugin] sihtkataloogi loomine, kui seda veel ei ole
- [Faili KIO-moodul] faili eriatribuutide rakendamise parandus (veateade 365795)
- Hõivatud silmuse kontrolli eemaldamine TransferJobPrivate::slotDataReqFromDevice'is
- kiod5 muutmine "agendiks"  Mac'i peal
- Parandus: puhverserveri juhtimismoodul ei laadinud puhverservereid käsitsi korrektselt

### Kirigami

- parajasti kasutute kerimisribade peitmine
- Baasnäite lisamine veeru laiuse lohistatava pideme kohandamiseks
- Laiemad kihid pidemete asetamisel
- pideme asetuse parandus, kui see kattub viimase leheküljega
- viimasel veerul ei näidata võltspidet
- delegaatidesse ei salvestata igasuguseid asju )veateade 38374₎
- kuna me oleme juba määranud keyNavigationEnabled'i, määrakem ka mähkimised
- tagasinupu parem vasakjoondus (veateade 383751)
- kerimisel ei võeta päist kaks korda arvesse (veateade 383725)
- päisepealdise ridu ei murta kunagi
- FIXME lahendamine: resetTimer'i eemaldamine (veateade 383772)
- mujal kui mobiilis ei kerita rakendusepäist eest ära
- Omaduse lisamine AbstractListItem'iga  sobiva PageRow eraldaja peitmiseks
- originY ja alt-üles vooga kerimise parandus
- Hoiatustest vabanemine nii piksli- kui ka punktisuuruse määramisel
- teistpidi vaadetes ei käivitata kättesaadavat režiimi
- arvestatakse lehekülje jalust
- vestlusrakenduse veidi keerulisema näite lisamine
- turvalisem viis leida õige jalus
- Elemendi kehtivuse kontroll enne selle kasutamist
- Kihi asukoha austamine isCurrentPage'is
- animation'i kasutamine animator'i asemel (veateade 383761)
- lehekülje jalusele vajaliku ruumi jätmine, kui võimalik
- applicationitem'i parem tumendamine
- applicationitem'i tausta tumendamine
- tagasinupu veeriste kohane parandus
- tagasinupu kohased veerised
- vähem hoiatusi ApplicationHeader'is
- ikoonisuuruste puhul ei kasutata plasma skaleerimist
- pidemete uus välimus

### KItemViews

### KJobWidgets

- Nupu "Paus" oleku initsialiseerimine vidinajälgijas

### KNotification

- Märguandeteenuse käivitamist ei blokita (veateade 382444)

### KPackage raamistik

- kpackagetool'i ümberkorraldamine stringy valikute pealt ära

### KRunner

- Varasemate toimingute puhastamine uuendamisel
- Kaugkäivitite (üle DBus'i) lisamine

### KTextEditor

- Document/View skriptimise API portimine QJSValue-põhise lahenduse peale
- Ikoonide näitamine ikoonipiirde kontekstimenüüs
- KStandardAction::PasteText'i asendamine KStandardAction::Paste'iga
- Murdarvulise skaleerimise toetus külgriba eelvaaluse genereerimisel
- Lülitumine QtScript'i pealt QtQml'i peale

### KWayland

- RGB sisendpuhvrite tõlgendamine eelmitmekordistatuna
- SurfaceInterface'i väljundite uuendamine globaalse väljundi hävitamisel
- KWayland::Client::Surface jälgib väljundi hävitamist
- Välditakse andmepakkumiste saatmist vigasest allikast (veateade 383054)

### KWidgetsAddons

- setContents'i lihtsustamine, lastes suure osa tööd ära teha Qt'l
- KSqueezedTextLabel: mugavuse huvides isSqueezed() lisamine
- KSqueezedTextLabel: väikesed täiustused API dokumentatsioonis
- [KPasswordLineEdit] fookusepuhvri määramine redigeerimisreale (veateade 383653)
- [KPasswordDialog] omaduse geometry lähtestamine

### KWindowSystem

- KKeyServer: KeypadModifier'i käitlemise parandus (veateade 183458)

### KXMLGUI

- Terve hulga stat() väljakutsete salvestamine rakenduse käivitamisel
- KHelpMenu asukoha parandus Wayland'is (veateade 384193)
- Katkise keskklõpsu käitlemise hülgamine (veateade 383162)
- KUndoActions: actionCollection'i kasutamine kiirklahvi määramiseks

### Plasma raamistik

- [ConfigModel] Kaitse null ConfigCategory lisamise eest
- [ConfigModel] ConfigCategory programmeeritava lisamise ja eemaldamise lubamine (veateade 372090)
- [EventPluginsManager] pluginPath'i avalikustamine mudelis
- [Icon Item] Ei tühistata tarbetult imagePath'i määramist
- [FrameSvg] QPixmap::mask() kasutamine iganenud segadusse ajava viisi asemel alphaChannel() vahendusel
- [FrameSvgItem] objekti margins/fixedMargins loomine nõudmisel
- menüüelementide oleku kontrolli parandus
- Jõuga Plasma stiil QQC2-le aplettides
- Kataloogi PlasmaComponents.3/private paigaldamine
- "locolor" teemade jäänuste hülgamine
- [Teema] KConfig'i SimpleConfig'i kasutamine
- Mõningate tarbetute teemasisu otsingute vältimine
- õigustamatute suuruse nullsuuruse peale muutmise sündmuste eiramine (veateade 382340)

### Süntaksi esiletõstmine

- Adblock Plus'i filtriloendite süntaksi definitsiooni lisamine
- Sieve süntaksi definitsiooni ümberkirjutamine
- QDoc'i seadistusfailide esiletõstmise lisamine
- Tiger'i esiletõstmise definitsiooni lisamine
- Poolituskriipsu varjestamine rest.xml regulaaravaldistes (veateade 383632)
- Parandus: lihtteks tõstetakse esile powershell'ina
- Metamath'i süntaksi esiletõstmise lisamine
- Less'i süntaksi esiletõstmise aluseks võeti SCSS (veateade 369277)
- Pony esiletõstmise lisamine
- e-kirja süntaksi definitsiooni ümberkirjutamine

### Turbeteave

The released code has been GPG-signed using the following key: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Väljalaske üle arutada ja mõtteid jagada saab <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>meie uudistelehekülje artikli</a> kommentaarides.
