---
aliases:
- ../announce-applications-19.08.0
changelog: true
date: 2019-08-15
description: KDE Ships Applications 19.08.
layout: application
release: applications-19.08.0
title: KDE toob välja KDE rakendused 19.08.0
version: '19.08'
version_number: 19.08.0
---
{{< peertube "/6443ce38-0a96-4b49-8fc5-a50832ed93ce" >}}

{{% i18n_date %}}

The KDE community is happy to announce the release of KDE Applications 19.08.

See väljalase näitab KDE jätkuvat pühendumist pakkuda meie loodavate programmide aina veatumaid ja täiustatumaid versioone. Rakenduste uued versioonid toovad kaasa uusi omadusi ja etema ülesehitusega tarkvara, mis suurendab selliste rakenduste kasutushõlpsust ja stabiilsust, nagu Dolphin, Konsool, Kate, Okular ja muud sinu lemmikud KDE tööriistad. Meie eesmärk on tagada kasutajate võimalikult muretu loomeprotsess ning muuta KDE tarkvara kasutamine võimalikult lihtsaks ja maksimaalselt nauditavaks.

We hope you enjoy all the new enhancements you'll find in 19.08!

## What's new in KDE Applications 19.08

More than 170 bugs have been resolved. These fixes re-implement disabled features, normalize shortcuts, and solve crashes, making your applications friendlier and allowing you to work and play smarter.

### Dolphin

Dolphin on KDE failide ja kataloogide haldur, mida saab nüüd sõltumata sellest, kus parasjagu viibite, käivitada uue globaalse kiirklahviga <keycap>Meta+E</keycap>. Üks uus omadus aitab vähendada segadikku töölaual. Kui Dolphin juba töötab ja avad katalooge teiste rakendustega, siis avatakse need kataloogid olemasoleva akna uuel kaardil, mitte Dolphini uues aknas. Pane tähele, et see on nüüd vaikimisi nii, aga soovi korral saab selle keelata.

Infopaneeli (asub vaikimisi Dolphini põhipaneelist paremal) on mitmeti täiustatud. Näiteks saab panna meediafaile automaatselt esitama, kui need põhipaneelil esile tõsta, samuti saab nüüd valida ja kopeerida paneelil näidatavat teksti. Kui aga tahad muuta, mida infopaneel näitab, saab seda teha otse paneelil, sest Dolphin ei ava enam eraldi akent paneeli seadistamiseks.

Me oleme lahendanud hulga väiksemaid vigu ja tõrkeid, mis peaks muutma Dolphini kasutamise veel sujuvamaks.

{{<figure src="/announcements/applications/19.08.0/dolphin_bookmark.png" alt=`Dolphini uus järjehoidjate võimalus` caption=`Dolphini uus järjehoidjate võimalus` width="600px" >}}

### Gwenview

Gwenview is KDE's image viewer, and in this release the developers have improved its thumbnail viewing feature across the board. Gwenview can now use a \"Low resource usage mode\" that loads low-resolution thumbnails (when available). This new mode is much faster and more resource-efficient when loading thumbnails for JPEG images and RAW files. In cases when Gwenview cannot generate a thumbnail for an image, it now displays a placeholder image rather than re-using the thumbnail of the previous image. The problems Gwenview had with displaying thumbnails from Sony and Canon cameras have also been solved.

Lisaks muutustele pisipiltide juures pakub Gwenview nüüd uut menüüd jagamiseks, mis lubab saata pilte väga mitmele poole, ning suudab laadida ja näidata KIO-moodulite vahendusel võrguasukohtade faile. Samuti näitab Gwenview uus versioon tohutult rohkem toorfailide EXIF-i metaandmeid.

{{<figure src="/announcements/applications/19.08.0/gwenview_share.png" alt=`Gwenview uus jagamismenüü` caption=`Gwenview uus jagamismenüü` width="600px" >}}

### Okular

Arendajad täiustasid omajagu KDE dokumendinäitaja Okular annotatsioone. Lisaks annotatsioonide seadistamisdialoogide kasutajaliidese viimistlemisele saab nüüd joonannotatsioonide otstesse lisada mitmesuguseid visuaalseid elemente, mis lubab need näiteks noolteks muuta. Lisaks saab nüüd annotatsioone vajaduse ja soovi korral lahti ja kokku kerida.

Okulari ePubi dokumentide toetust parandati selles väljalaskes oluliselt. Okulari ei taba enam krahh vigases vormingus ePubi faili laadimisel ning jõudlus suurte ePubi failide käitlemisel on märgatavalt parem. Teiste selle väljalaske muudatuste hulka kuuluvad lehekülje täiustatud piirded ja esitlusrežiimi markeri tööriista parem toimetulek kõrglahutusega DPI korral.

{{<figure src="/announcements/applications/19.08.0/okular_line_end.png" alt=`Okulari annoteerimistööriista seadistused uue joonelõpu valikuga` caption=`Okulari annoteerimistööriista seadistused uue joonelõpu valikuga` width="600px" >}}

### Kate

Thanks to our developers, three annoying bugs have been squashed in this version of KDE's advanced text editor. Kate once again brings its existing window to the front when asked to open a new document from another app. The \"Quick Open\" feature sorts items by most recently used, and pre-selects the top item. The third change is in the \"Recent documents\" feature, which now works when the current configuration is set up not to save individual windows’ settings.

### Konsool

KDE terminaliemulaatori Konsool kõige silmatorkavam muudatus puudutab poolitamisvõimalust. Nüüd saab põhipaneeli poolitada soovikohaselt nii püst- kui ka rõhtsuunas. Ja ka alampaneele saab omakorda poolitada. Selleest väljalaskest peale saab paneele ka lohistada, mis muudab nende ümberkorraldamise palju lihtsamaks.

Lisaks muule on seadistusteakent viimistletud, et see oleks selgemini mõistetav ja hõlpsam kasutada.

{{< video src-webm="/announcements/applications/19.08.0/konsole-tabs.webm" >}}

### Spectacle

Spectacle on KDE ekraanipiltide tegemise rakendus, mis on iga väljalaskega saanud aina uusi huvitavaid omadusi. Erand pole seegi väljalase, mis pakub nüüd mitmeid uusi omadusi viivitusvõimaluse täiustamiseks. Viivitusega ekraanipildi tegemisel näitab Spectacle jäänud aega akna tiitliribal. Samuti näeb seda rakenduse ikoonil tegumihalduril.

Still on the Delay feature, Spectacle's Task Manager button will also show a progress bar, so you can keep track of when the snap will be taken. And, finally, if you un-minimize Spectacle while waiting, you will see that the “Take a new Screenshot” button has turned into a \"Cancel\" button. This button also contains a progress bar, giving you the chance to stop the countdown.

Ekraanipiltide salvestamisel on samuti uus võimalus. Pildi salvestamisel näitab Spectacle rakenduses sõnumit, mis lubab avada ekraanipildi või seda sisaldava kataloogi.

{{< video src-webm="/announcements/applications/19.08.0/spectacle_progress.webm" >}}

### Kontact

KDE e-kirjade, kalendrite, kontaktide ja grupitöö rakendustekomplekt Kontact pakub kirjade koostamisel Unicode värviliste emojide ja Markdowni toetust. Nii laseb KMail sinu kirjadel kaunid välja näha, kuid lisaks pakub rakendus veel lõiminist selliste grammatika kontrollimise programmidega nagu LanguageTool ja Grammalecte, et su kirjade õigekeel oleks samuti parimas korras.

{{<figure src="/announcements/applications/19.08.0/kontact_emoji.png" alt=`Emoji valija` caption=`Emoji valija` width="600px" >}}

{{<figure src="/announcements/applications/19.08.0/kmail_grammar.png" alt=`KMaili grammatikakontrolli lõimimine` caption=`KMaili grammatikakontrolli lõimimine` width="600px" >}}

Sündmuste planeerimisel ei kustuta KMail enam kutsekirju, kui neile on vastatud. Sündmust saab nüüd KOrganizeri sündmuste redaktoris liigutada ühest kalendrist teise.

KDE aadressiraamat on nüüd suuteline saatma kontaktidele SMS-e KDE Connecti vahendusel, muutes nii töölaua ja mobiilseadmete lõimituse veel sügavamaks.

### Kdenlive

KDE videoredaktori Kdenlive uus versioon pakub välja uue valiku klaviatuuri-hiire ühendtoiminguid, mis lubavad loomekiirust tõsta. Näiteks saab klipi kiirust ajateljel muuta klahvi Ctrl all hoides ja klippi lohistades, videoklippide pisipiltide eelvaatluse aga aktiveerida tõstuklahvi all hoides ja hiirega projektipuus klipi pisipildi peale liikudes. Arendajad nägid palju vaeva ka kolmepunkti-redigeerimise toimingute ühtlustamisega muude videoredaktoritega, mis on kahtlemata tänuväärt täiustus, eriti kui tulla Kdenlive'i kasutama varasema muu videoredaktori kogemuse pealt.

{{<figure src="https://cdn.kde.org/screenshots/kdenlive/19-08.png" alt=`Kdenlive 19.08.0` caption=`Kdenlive 19.08.0` width="600px" >}}
