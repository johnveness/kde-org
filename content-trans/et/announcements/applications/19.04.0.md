---
aliases:
- ../announce-applications-19.04.0
changelog: true
date: 2019-04-18
description: KDE Ships Applications 19.04.
layout: application
release: applications-19.04.0
title: KDE toob välja KDE rakendused 19.04.0
version: 19.04.0
version_number: 19.04.0
version_text: '19.04'
---
{{%youtube id="1keUASEvIvE"%}}

{{% i18n_date %}}

The KDE community is happy to announce the release of KDE Applications 19.04.

Our community continuously works on improving the software included in our KDE Application series. Along with new features, we improve the design, usability and stability of all our utilities, games, and creativity tools. Our aim is to make your life easier by making KDE software more enjoyable to use. We hope you like all the new enhancements and bug fixes you'll find in 19.04!

## What's new in KDE Applications 19.04

Parandatud on üle 150 vea. Parandustega on tagasi toodud vahepeal välja lülitatud omadusi, kiirklahve mõistetavamaks defineeritud ja krahhe kõrvaldatud, mis muudab KDE rakenduste kasutamise mugavamaks ja mõnusamaks ning lubab kasutajal vähema vaevaga rohkem saavutada.

### Failihaldus

{{<figure src="/announcements/applications/19.04.0/app1904_dolphin01.png" width="600px" >}}

<a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a> is KDE's file manager. It also connects to network services, such as SSH, FTP, and Samba servers, and comes with advanced tools to find and organize your data.

Uued omadused:

+ We have expanded thumbnail support, so Dolphin can now display thumbnails for several new file types: <a href='https://phabricator.kde.org/D18768'>Microsoft Office</a> files, <a href='https://phabricator.kde.org/D18738'>.epub and .fb2 eBook</a> files, <a href='https://bugs.kde.org/show_bug.cgi?id=246050'>Blender</a> files, and <a href='https://phabricator.kde.org/D19679'>PCX</a> files. Additionally, thumbnails for text files now show <a href='https://phabricator.kde.org/D19432'>syntax highlighting</a> for the text inside the thumbnail. </li>
+ You can now choose <a href='https://bugs.kde.org/show_bug.cgi?id=312834'>which split view pane to close</a> when clicking the 'Close split' button. </li>
+ This version of Dolphin introduces <a href='https://bugs.kde.org/show_bug.cgi?id=403690'>smarter tab placement</a>. When you open a folder in a new tab, the new tab will now be placed immediately to the right of the current one, instead of always at the end of the tab bar. </li>
+ <a href='https://phabricator.kde.org/D16872'>Tagging items</a> is now much more practical, as tags can be added or removed using the context menu. </li>
+ We have <a href='https://phabricator.kde.org/D18697'>improved the default sorting</a> parameters for some commonly-used folders. By default, the Downloads folder is now sorted by date with grouping enabled, and the Recent Documents view (accessible by navigating to recentdocuments:/) is sorted by date with a list view selected. </li>

Tähtsamad veaparandused:

+ When using a modern version of the SMB protocol, you can now <a href='https://phabricator.kde.org/D16299'>discover Samba shares</a> for Mac and Linux machines. </li>
+ <a href='https://bugs.kde.org/show_bug.cgi?id=399430'>Re-arranging items in the Places panel</a> once again works properly when some items are hidden. </li>
+ After opening a new tab in Dolphin, that new tab's view now automatically gains <a href='https://bugs.kde.org/show_bug.cgi?id=401899'>keyboard focus</a>. </li>
+ Dolphin now warns you if you try to quit while the <a href='https://bugs.kde.org/show_bug.cgi?id=304816'>terminal panel is open</a> with a program running in it. </li>
+ We fixed many memory leaks, improving Dolphin's overall performance.</li>

The <a href='https://cgit.kde.org/audiocd-kio.git/'>AudioCD-KIO</a> allows other KDE applications to read audio from CDs and automatically convert it into other formats.

+ The AudioCD-KIO now supports ripping into <a href='https://bugs.kde.org/show_bug.cgi?id=313768'>Opus</a>. </li>
+ We made <a href='https://bugs.kde.org/show_bug.cgi?id=400849'>CD info text</a> really transparent for viewing. </li>

### Video redigeerimine

{{<figure src="/announcements/applications/19.04.0/app1904_kdenlive.png" width="600px" >}}

This is a landmark version for KDE's video editor. <a href='https://kde.org/applications/multimedia/kdenlive/'>Kdenlive</a> has gone through an extensive re-write of its core code as more than 60%% of its internals has changed, improving its overall architecture.

Täiustused sisaldavad muu hulgas:

+ Ajatelg kirjutati ümber QML-i peale.
+ Klipi asetamisel ajateljele, asetatakse heli ja video alati eraldi rajale.
+ Ajatelg toetab nüüd liikumist klaviatuuri abil: klippe, kompositsioone ja võtmekaadreid saab kõiki liigutada klaviatuuri kasutades. Samuti saab kohandada radade kõrgust.
+ In this version of Kdenlive, the in-track audio recording comes with a new <a href='https://bugs.kde.org/show_bug.cgi?id=367676'>voice-over</a> feature.
+ Parandatud on kopeerimist/asetamist: see toimib nüüd ka erinevate projektiakende vahel. Samuti on täiustatud asendusklippide haldust: klippe saab nüüd ükshaaval kustutada.
+ Versioonis 19.04 on tagasi välise BlackMagici monitori toetus, samuti pakume uusi monitoripõhiseid valmisseadistusi. 
+ We have improved the keyframe handling, giving it a more consistent look and workflow. The titler has also been improved by making the align buttons snap to safe zones, adding configurable guides and background colors, and displaying missing items.
+ We fixed the timeline corruption bug that misplaced or missed clips which was triggered when you moved a group of clips.
+ Parandasime ära JPG-piltide vea, mis renderdas pilte Windowsis valgena. Samuti parandasime vea, mis mõjutas ekraanipildi tegemist Windowsis.
+ Apart from all of the above, we have added many small usability enhancements that will make using Kdenlive easier and smoother.

### Kontoritöö

{{<figure src="/announcements/applications/19.04.0/app1904_okular.png" width="600px" >}}

<a href='https://kde.org/applications/graphics/okular/'>Okular</a> is KDE's multipurpose document viewer. Ideal for reading and annotating PDFs, it can also open ODF files (as used by LibreOffice and OpenOffice), ebooks published as ePub files, most common comic book files, PostScript files, and many more.

Täiustused sisaldavad muu hulgas:

+ To help you ensure your documents are always a perfect fit, we've added <a href='https://bugs.kde.org/show_bug.cgi?id=348172'>scaling options</a> to Okular's Print dialog.
+ Okular now supports viewing and verifying <a href='https://cgit.kde.org/okular.git/commit/?id=a234a902dcfff249d8f1d41dfbc257496c81d84e'>digital signatures</a> in PDF files.
+ Thanks to improved cross-application integration, Okular now supports editing LaTeX documents in <a href='https://bugs.kde.org/show_bug.cgi?id=404120'>TexStudio</a>.
+ Improved support for <a href='https://phabricator.kde.org/D18118'>touchscreen navigation</a> means you will be able to move backwards and forwards using a touchscreen when in Presentation mode.
+ Users who prefer manipulating documents from the command-line will be able to perform smart <a href='https://bugs.kde.org/show_bug.cgi?id=362038'>text search</a> with the new command-line flag that lets you open a document and highlight all occurrences of a given piece of text.
+ Okular now properly displays links in <a href='https://bugs.kde.org/show_bug.cgi?id=403247'>Markdown documents</a> that span more than one line.
+ The <a href='https://bugs.kde.org/show_bug.cgi?id=397768'>trim tools</a> have fancy new icons.

{{<figure src="/announcements/applications/19.04.0/app1904_kmail.png" width="600px" >}}

<a href='https://kde.org/applications/internet/kmail/'>KMail</a> is KDE's privacy-protecting email client. Part of the <a href='https://kde.org/applications/office/kontact/'>Kontact groupware suite</a>, KMail supports all email systems and allows you to organize your messages into a shared virtual inbox or into separate accounts (your choice). It supports all kinds of message encryption and signing, and lets you share data such as contacts, meeting dates, and travel information with other Kontact applications.

Täiustused sisaldavad muu hulgas:

+ Edasi parema õigekirja poole! KMail toetab nüüd tööriistu languagetools (grammatika kontrollija) ja grammalecte (ainult prantsuse keele grammatika kontrollija).
+ Phone numbers in emails are now detected and can be dialed directly via <a href='https://community.kde.org/KDEConnect'>KDE Connect</a>.
+ KMail now has an option to <a href='https://phabricator.kde.org/D19189'>start directly in system tray</a> without opening the main window.
+ Me täiustasime Markdowni plugina toetust.
+ Kirjade tõmbamine IMAP kaudu enam ei hangu, kui sisselogimine nurjub.
+ Samuti sai rohkelt parandusi KMaili taustaprogramm Akonadi, mis parandavad jõudlust ja usaldusväärsust.

<a href='https://kde.org/applications/office/korganizer/'>KOrganizer</a> is Kontact's calendar component, managing all your events.

+ Recurrent events from <a href='https://bugs.kde.org/show_bug.cgi?id=334569'>Google Calendar</a> are again synchronized correctly.
+ The event reminder window now remembers to <a href='https://phabricator.kde.org/D16247'>show on all desktops</a>.
+ We modernized the look of the <a href='https://phabricator.kde.org/T9420'>event views</a>.

<a href='https://cgit.kde.org/kitinerary.git/'>Kitinerary</a> is Kontact's brand new travel assistant that will help you get to your location and advise you on your way.

- Sel on uus üldine RCT2 piletite ekstraktor (selliseid pileteid kasutavad näiteks raudteekompaniid DSB, ÖBB, SBB, NS)
- Lennujaamade täpse nime tuvastamine on tunduvalt paranenud.
- Lisasime uusi kohandatud ekstraktoreid varem toetamata teenusepakkujate kasutamiseks (näiteks BCD Travel, NH Group) ja täiustasime juba toetatud teenusepakkujate vormingu- ja keelevariantide toetust (näiteks SNCF, Booking.com, Hertz).

### Arendus

{{<figure src="/announcements/applications/19.04.0/app1904_kate.png" width="600px" >}}

<a href='https://kde.org/applications/utilities/kate/'>Kate</a> is KDE's full-featured text editor, ideal for programming thanks to features such as tabs, split-view mode, syntax highlighting, a built-in terminal panel, word completion, regular expressions search and substitution, and many more via the flexible plugin infrastructure.

Täiustused sisaldavad muu hulgas:

- Kate suudab nüüd näidata kõiki nähtamatuid tühimärke, mitte kõigest mõningaid.
- Staatilist reamurdmist saab nüüd vähese vaevaga sisse ja välja lülitada eraldi menüükirjega igas dokumendis eraldi ilma globaalset vaikeseadistust muutmata.
- Faili ja kaardi kontekstimenüü sisaldab nüüd mitmeid kasulikke käske, näiteks Muuda nime, Kustuta, Ava sisaldav kataloog, Kopeeri faili asukoht, Võrdle (teise avatud failiga) ja Omadused.
- Kate selles versioonis on veel rohkem pluginaid vaikimisi lubatud, kaasa arvatud menukas ja kasulik terminal.
- Väljumisel ei soovi Kate enam lasta kasutajal kinnitada faile, mida on kettal muutnud mõni teine protsess (näiteks lähtekoodi kontrollmärkide muutused).
- Plugina puuvaade näitab nüüd korralikult kõiki menüüelemente nende giti kirjete puhul, milles nimes on täpitähti.
- Mitme faili avamisel käsurealt avatakse failid nüüd uutel kaartidel samas järjekorras, nagu failid olid määratud käsureal.

{{<figure src="/announcements/applications/19.04.0/app1904_konsole.png" width="600px" >}}

<a href='https://kde.org/applications/system/konsole/'>Konsole</a> is KDE's terminal emulator. It supports tabs, translucent backgrounds, split-view mode, customizable color schemes and keyboard shortcuts, directory and SSH bookmarking, and many other features.

Täiustused sisaldavad muu hulgas:

+ Kaartide haldamist on mitmeti täiustatud. Uusi kaarte saab luua keskklõpsuga kaardiriba tühjas osas ja on ka valik, mis lubab kaarte keskklõpsuga sulgeda. Kaartide sakkidel näidatakse vaikimisi sulgemisnuppu, ikoone aga ainult siis, kui kasutatakse kohandatud ikooniga profiili. Samuti saab nüüd kiirklahviga Ctrl+tabeldusklahv kiiresti lülituda praeguse ja eelmise kaardi vahel.
+ The Edit Profile dialog received a huge <a href='https://phabricator.kde.org/D17244'>user interface overhaul</a>.
+ Konsooli vaikimisi värviskeemina kasutatakse nüüd Breeze värviskeemi, samuti oleme täiustanud selle kontrasti ja ühtlustanud seda süsteemse Breeze teemaga.
+ Me lahendasime probleemid rasvase kirja esitamisega.
+ Konsool näitab nüüd korrektselt kursorit, millel on allajoonimise kriipsu kuju.
+ We have improved the display of <a href='https://bugs.kde.org/show_bug.cgi?id=402415'>box and line characters</a>, as well as of <a href='https://bugs.kde.org/show_bug.cgi?id=401298'>Emoji characters</a>.
+ Profiili vahetamise kiirklahvi vahetavad nüüd aktiivse kaardi profiili, mitte ei ava uut kaarti teise profiiliga.
+ Mitteaktiivsed kaardid, mis saavad märguande ja muudavad selle peale saki teksti värvi, lähtestavad nüüd tekstivärvi tagasi normaalseks. kui kaart aktiveeritakse.
+ "Eraldi taust igal kaardil" töötab nüüd korralikult ka siis, kui baastaustavärv on väga tume või must.

<a href='https://kde.org/applications/development/lokalize/'>Lokalize</a> is a computer-aided translation system that focuses on productivity and quality assurance. It is targeted at software translation, but also integrates external conversion tools for translating office documents.

Täiustused sisaldavad muu hulgas:

- Lokalize toetab nüüd tõlkeallika näitamist kohandatud redaktoris.
- Me oleme täiustanud dokividinate asukohta  ning seadistuse salvestamise ja taastamise viise.
- Tõlkeüksuste filtreerimisel säilitatakse nüüd asukoht .po-failides.
- Parandasime hulk kasutajaliidese vigu (arendajate kommentaarid, regulaaravaldise lülitamine massilise asendamise ajal, probleemid tühjade kirjete arvuga jms).

### Tööriistad

{{<figure src="/announcements/applications/19.04.0/app1904_gwenview.png" width="600px" >}}

<a href='https://kde.org/applications/graphics/gwenview/'>Gwenview</a> is an advanced image viewer and organizer with intuitive and easy-to-use editing tools.

Täiustused sisaldavad muu hulgas:

+ The version of Gwenview that ships with Applications 19.04 includes full <a href='https://phabricator.kde.org/D13901'>touchscreen support</a>, with gestures for swiping, zooming, panning, and more.
+ Another enhancement added to Gwenview is full <a href='https://bugs.kde.org/show_bug.cgi?id=373178'>High DPI support</a>, which will make images look great on high-resolution screens.
+ Improved support for <a href='https://phabricator.kde.org/D14583'>back and forward mouse buttons</a> allows you to navigate between images by pressing those buttons.
+ You can now use Gwenview to open image files created with <a href='https://krita.org/'>Krita</a> – everyone’s favorite digital painting tool.
+ Gwenview now supports large <a href='https://phabricator.kde.org/D6083'>512 px thumbnails</a>, allowing you to preview your images more easily.
+ Gwenview now uses the <a href='https://bugs.kde.org/show_bug.cgi?id=395184'>standard Ctrl+L keyboard shortcut</a> to move focus to the URL field.
+ You can now use the <a href='https://bugs.kde.org/show_bug.cgi?id=386531'>Filter-by-name feature</a> with the Ctrl+I shortcut, just like in Dolphin.

{{<figure src="/announcements/applications/19.04.0/app1904_spectacle.jpg" width="600px" >}}

<a href='https://kde.org/applications/graphics/spectacle/'>Spectacle</a> is Plasma's screenshot application. You can grab full desktops spanning several screens, individual screens, windows, sections of windows, or custom regions using the rectangle selection feature.

Täiustused sisaldavad muu hulgas:

+ We have extended the Rectangular Region mode with a few new options. It can be configured to <a href='https://bugs.kde.org/show_bug.cgi?id=404829'>auto-accept</a> the dragged box instead of asking you to adjust it first. There is also a new default option to remember the current <a href='https://phabricator.kde.org/D19117'>rectangular region</a> selection box, but only until the program is closed.
+ You can configure what happens when the screenshot shortcut is pressed <a href='https://phabricator.kde.org/T9855'>while Spectacle is already running</a>.
+ Spectacle allows you to change the <a href='https://bugs.kde.org/show_bug.cgi?id=63151'>compression level</a> for lossy image formats.
+ Save settings shows you what the <a href='https://bugs.kde.org/show_bug.cgi?id=381175'>filename of a screenshot</a> will look like. You can also easily tweak the <a href='https://bugs.kde.org/show_bug.cgi?id=390856'>filename template</a> to your preferences by simply clicking on placeholders.
+ Spectacle ei näita enam valikuid "Täisekraan (kõigil monitoridel)" ja "Aktiivne ekraan", kui arvutil on ainult üks ekraan.
+ Ristkülikukujulise ala abiteksti näidatakse nüüd esimese ekraani keskel, mitte ekraanide vahekohas.
+ Waylandis kasutab Spectacle ainult neid võimalusi, mis tõeliselt töötavad.

### Mängud ja õpirakendused

{{<figure src="/announcements/applications/19.04.0/app1904_kmplot.png" width="600px" >}}

Our application series includes numerous <a href='https://games.kde.org/'>games</a> and <a href='https://edu.kde.org/'>educational applications</a>.

<a href='https://kde.org/applications/education/kmplot'>KmPlot</a> is a mathematical function plotter. It has a powerful built-in parser. The graphs can be colorized and the view is scalable, allowing you to zoom to the level you need. Users can plot different functions simultaneously and combine them to build new functions.

+ You can now zoom in by holding down Ctrl and using the <a href='https://bugs.kde.org/show_bug.cgi?id=159772'>mouse wheel</a>.
+ This version of Kmplot introduces the <a href='https://phabricator.kde.org/D17626'>print preview</a> option.
+ Root value or (x,y) pair can now be copied to <a href='https://bugs.kde.org/show_bug.cgi?id=308168%'>clipboard</a>.

<a href='https://kde.org/applications/games/kolf/'>Kolf</a> is a miniature golf game.

+ We have restored <a href='https://phabricator.kde.org/D16978'>sound support</a>.
+ Kolf on edukalt porditud ära kdelibs4 pealt.
