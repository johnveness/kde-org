---
aliases:
- ../announce-applications-14.12.3
changelog: true
date: '2015-03-03'
description: KDE Ships Applications 14.12.3.
layout: application
title: KDE toob välja KDE rakendused 14.12.3
version: 14.12.3
---
March 3, 2015. Today KDE released the third stability update for <a href='../14.12.0'>KDE Applications 14.12</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

19 teadaoleva veaparanduse hulka kuuluvad anagrammimängu Kanagram, UML-i tööriista Umbrello, dokumendinäitaja Okular ja geomeetriarakenduse Kig täiustused.

This release also includes Long Term Support versions of Plasma Workspaces 4.11.17, KDE Development Platform 4.14.6 and the Kontact Suite 4.14.6.
