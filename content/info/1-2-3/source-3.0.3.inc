    <table border="0" cellpadding="4" cellspacing="0">
      <tr valign="top">
        <th align="left">Location</th>

        <th align="left">Size</th>

        <th align="left">MD5&nbsp;Sum</th>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.0.3/src/arts-1.0.3.tar.bz2">arts-1.0.3</a></td>

        <td align="right">997kB</td>

        <td><tt>dba1c36a3e8bad05ccda981ef00fcb99</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.0.3/src/kde-i18n-3.0.3.tar.bz2">kde-i18n-3.0.3</a></td>

        <td align="right">93MB</td>

        <td><tt>4e0da12a8cfd78f0fd2935b93bdf78d1</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.0.3/src/kdeaddons-3.0.3.tar.bz2">kdeaddons-3.0.3</a></td>

        <td align="right">901kB</td>

        <td><tt>05b29f1c944fa57dfcbf214563ed2006</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.0.3/src/kdeadmin-3.0.3.tar.bz2">kdeadmin-3.0.3</a></td>

        <td align="right">1.3MB</td>

        <td><tt>90141493c24bc3c9c424cb1b83c32a62</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.0.3/src/kdeartwork-3.0.3.tar.bz2">kdeartwork-3.0.3</a></td>

        <td align="right">11MB</td>

        <td><tt>7c368d16dc0a933e649cb7f7c12f7a84</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.0.3/src/kdebase-3.0.3.tar.bz2">kdebase-3.0.3</a></td>

        <td align="right">13MB</td>

        <td><tt>a1c6cb06468608318c5e59e362773360</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.0.3/src/kdebindings-3.0.3.tar.bz2">kdebindings-3.0.3</a></td>

        <td align="right">4.9MB</td>

        <td><tt>b641cbc106e221ab9a071bcc9e06b14b</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.0.3/src/kdeedu-3.0.3.tar.bz2">kdeedu-3.0.3</a></td>

        <td align="right">8.7MB</td>

        <td><tt>44917139bc3d4199df080509de8f4207</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.0.3/src/kdegames-3.0.3.tar.bz2">kdegames-3.0.3</a></td>

        <td align="right">7.0MB</td>

        <td><tt>906b5c4ecb808b853d1e5ea7a337a208</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.0.3/src/kdegraphics-3.0.3.tar.bz2">kdegraphics-3.0.3</a></td>

        <td align="right">2.6MB</td>

        <td><tt>9c3b99cec43dccd3957158ee32573da1</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.0.3/src/kdelibs-3.0.3.tar.bz2">kdelibs-3.0.3</a></td>

        <td align="right">7.3MB</td>

        <td><tt>f26acfafbd3a00451b6e344a7d75386d</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.0.3/src/kdemultimedia-3.0.3.tar.bz2">kdemultimedia-3.0.3</a></td>

        <td align="right">5.6MB</td>

        <td><tt>f191cc8476fb67fd1ca0c240f620d2b1</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.0.3/src/kdenetwork-3.0.3.tar.bz2">kdenetwork-3.0.3</a></td>

        <td align="right">3.7MB</td>

        <td><tt>11262498861b445190ad6d66eb3d7193</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.0.3/src/kdepim-3.0.3.tar.bz2">kdepim-3.0.3</a></td>

        <td align="right">3.1MB</td>

        <td><tt>1cf00b8d2c4742e79569c2c43142a3e3</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.0.3/src/kdesdk-3.0.3.tar.bz2">kdesdk-3.0.3</a></td>

        <td align="right">1.8MB</td>

        <td><tt>f7a7ae3118849636b123ebf813863e19</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.0.3/src/kdetoys-3.0.3.tar.bz2">kdetoys-3.0.3</a></td>

        <td align="right">1.4MB</td>

        <td><tt>99a0c42d4ed0a677f9bc0d8230203c6a</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.0.3/src/kdeutils-3.0.3.tar.bz2">kdeutils-3.0.3</a></td>

        <td align="right">1.5MB</td>

        <td><tt>e2b492fb02f51a6e807be724e56dfc12</tt></td>
      </tr>
    </table>

