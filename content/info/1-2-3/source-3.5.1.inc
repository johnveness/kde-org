<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top">
  <th align="left">Location</th>
  <th align="left">Size</th>
  <th align="left">MD5&nbsp;Sum</th>
</tr>
<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.1/src/arts-1.5.1.tar.bz2">arts-1.5.1</a></td>
   <td align="right">927kB</td>
   <td><tt>71c4996de36aa7f8726e54bb3d5bf059</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.1/src/kdeaccessibility-3.5.1.tar.bz2">kdeaccessibility-3.5.1</a></td>
   <td align="right">8.0MB</td>
   <td><tt>cc0773103053a6800b3d35e6021b6719</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.1/src/kdeaddons-3.5.1.tar.bz2">kdeaddons-3.5.1</a></td>
   <td align="right">1.5MB</td>
   <td><tt>cbd2a6f65ae7338736d93b72bfdf5ae3</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.1/src/kdeadmin-3.5.1.tar.bz2">kdeadmin-3.5.1</a></td>
   <td align="right">2.0MB</td>
   <td><tt>08e9bddd2b42e0783d7f79d08cdb031b</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.1/src/kdeartwork-3.5.1.tar.bz2">kdeartwork-3.5.1</a></td>
   <td align="right">15MB</td>
   <td><tt>8053bb1a1b2a5844b29f5f96328b1d96</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.1/src/kdebase-3.5.1.tar.bz2">kdebase-3.5.1</a></td>
   <td align="right">22MB</td>
   <td><tt>484c7b3895ce4f95173f4789571eb1cc</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.1/src/kdebindings-3.5.1.tar.bz2">kdebindings-3.5.1</a></td>
   <td align="right">5.1MB</td>
   <td><tt>302f6a842b5a29701846c49a24a64039</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.1/src/kdeedu-3.5.1.tar.bz2">kdeedu-3.5.1</a></td>
   <td align="right">28MB</td>
   <td><tt>fe31b9a2ec4e299531c6b7beeab09534</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.1/src/kdegames-3.5.1.tar.bz2">kdegames-3.5.1</a></td>
   <td align="right">10MB</td>
   <td><tt>15a4bb4e193e19be7decd4f147a31396</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.1/src/kdegraphics-3.5.1.tar.bz2">kdegraphics-3.5.1</a></td>
   <td align="right">6.9MB</td>
   <td><tt>2cd1c5348b7df46cf7f9d91e1dbfebd2</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.1/src/kdelibs-3.5.1.tar.bz2">kdelibs-3.5.1</a></td>
   <td align="right">14MB</td>
   <td><tt>2a22193ae7c23f3759b02723dab02d30</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.1/src/kdemultimedia-3.5.1.tar.bz2">kdemultimedia-3.5.1</a></td>
   <td align="right">5.2MB</td>
   <td><tt>595f637c637987a92f6dac9d9cd6667d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.1/src/kdenetwork-3.5.1.tar.bz2">kdenetwork-3.5.1</a></td>
   <td align="right">7.1MB</td>
   <td><tt>02ced8c14c80f28635056488949d56d7</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.1/src/kdepim-3.5.1.tar.bz2">kdepim-3.5.1</a></td>
   <td align="right">12MB</td>
   <td><tt>8e87c9cbe1f0c0f983f94d804a5bc8f9</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.1/src/kdesdk-3.5.1.tar.bz2">kdesdk-3.5.1</a></td>
   <td align="right">4.7MB</td>
   <td><tt>93199b8bf5793681fdb2e5cbcfcfbd93</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.1/src/kdetoys-3.5.1.tar.bz2">kdetoys-3.5.1</a></td>
   <td align="right">3.0MB</td>
   <td><tt>d98d4f30a8aa6d43b0af06421d4d6586</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.1/src/kdeutils-3.5.1.tar.bz2">kdeutils-3.5.1</a></td>
   <td align="right">2.8MB</td>
   <td><tt>1286c6a09b04452adfe492de2fad79bd</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.1/src/kdevelop-3.3.1.tar.bz2">kdevelop-3.3.1</a></td>
   <td align="right">7.7MB</td>
   <td><tt>e5e3d3ce60a0dcafb99721f6304f87ef</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.1/src/kdewebdev-3.5.1.tar.bz2">kdewebdev-3.5.1</a></td>
   <td align="right">5.7MB</td>
   <td><tt>0faea4e8088005ae60f58b21c60b32ea</tt></td>
</tr>

</table>
