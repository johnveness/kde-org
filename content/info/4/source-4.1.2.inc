<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top"><th align="left">Location</th><th align="left">Size</th><th align="left">MD5&nbsp;Sum</th></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.2/src/kdeaccessibility-4.1.2.tar.bz2">kdeaccessibility-4.1.2</a></td><td align="right">6,1MB</td><td><tt>41eee5a40d87c4b0917d28956a19f48a</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.2/src/kdeadmin-4.1.2.tar.bz2">kdeadmin-4.1.2</a></td><td align="right">1,8MB</td><td><tt>97a94526af42072da9e6aa959ca09afb</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.2/src/kdeartwork-4.1.2.tar.bz2">kdeartwork-4.1.2</a></td><td align="right">41MB</td><td><tt>43d2e6caa8796f70ecc49fe449a1f0aa</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.2/src/kdebase-4.1.2.tar.bz2">kdebase-4.1.2</a></td><td align="right">4,3MB</td><td><tt>705dc8a416cf472d04b9dc39d44652c6</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.2/src/kdebase-runtime-4.1.2.tar.bz2">kdebase-runtime-4.1.2</a></td><td align="right">50MB</td><td><tt>1f17bdfa4bee4bf1c7afc2bd5bdbae59</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.2/src/kdebase-workspace-4.1.2.tar.bz2">kdebase-workspace-4.1.2</a></td><td align="right">46MB</td><td><tt>b1f72c7c4e597e993669d625b26b4470</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.2/src/kdebindings-4.1.2.tar.bz2">kdebindings-4.1.2</a></td><td align="right">4,6MB</td><td><tt>f99b1fe747d6b4b9f444c1ca2f853172</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.2/src/kdeedu-4.1.2.tar.bz2">kdeedu-4.1.2</a></td><td align="right">55MB</td><td><tt>7319ea65180451d1ecfed158714a4fd3</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.2/src/kdegames-4.1.2.tar.bz2">kdegames-4.1.2</a></td><td align="right">31MB</td><td><tt>acb20df949ae44a55b72870a5640a629</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.2/src/kdegraphics-4.1.2.tar.bz2">kdegraphics-4.1.2</a></td><td align="right">3,3MB</td><td><tt>f29e68dfe91cc0d2a5fbe2e4f7920054</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.2/src/kdelibs-4.1.2.tar.bz2">kdelibs-4.1.2</a></td><td align="right">8,8MB</td><td><tt>cb5f43d2b11669fb17b0a1ecf3736c17</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.2/src/kdemultimedia-4.1.2.tar.bz2">kdemultimedia-4.1.2</a></td><td align="right">1,4MB</td><td><tt>232001bb20ad40d039c99a86360705db</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.2/src/kdenetwork-4.1.2.tar.bz2">kdenetwork-4.1.2</a></td><td align="right">7,1MB</td><td><tt>f1ba745dd89447904d5c1d567937627f</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.2/src/kdepim-4.1.2.tar.bz2">kdepim-4.1.2</a></td><td align="right">13MB</td><td><tt>484b3b9685ecd258d0c562c5b0fce297</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.2/src/kdepimlibs-4.1.2.tar.bz2">kdepimlibs-4.1.2</a></td><td align="right">1,9MB</td><td><tt>c5896119ce4aef2dd9d11495dd0dea6e</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.2/src/kdeplasma-addons-4.1.2.tar.bz2">kdeplasma-addons-4.1.2</a></td><td align="right">3,9MB</td><td><tt>84dc8f919d0a1e9bb1e1e2e499d80156</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.2/src/kdesdk-4.1.2.tar.bz2">kdesdk-4.1.2</a></td><td align="right">4,7MB</td><td><tt>29b29ed131db84a9abb2b4bf4d81c56e</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.2/src/kdetoys-4.1.2.tar.bz2">kdetoys-4.1.2</a></td><td align="right">1,3MB</td><td><tt>370d9469e3c6b087a7b7520f106c3d02</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.2/src/kdeutils-4.1.2.tar.bz2">kdeutils-4.1.2</a></td><td align="right">2,2MB</td><td><tt>cab195ea3a60d136706d00d66b504aa8</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.2/src/kdewebdev-4.1.2.tar.bz2">kdewebdev-4.1.2</a></td><td align="right">2,5MB</td><td><tt>7753591083d6211417b88d0a0f9ed120</tt></td></tr>
</table>
