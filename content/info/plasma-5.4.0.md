---
version: "5.4.0"
title: "Plasma 5.4.0 Source Information Page"
errata:
    link: https://community.kde.org/Plasma/5.4_Errata
    name: 5.4 Errata
type: info/plasma5
---

<p>This is a feature release of Plasma, featuring Plasma Desktop and
other essential software for your computer.  Details in the <a
href="/announcements/plasma-5.4.0">Plasma 5.4.0 announcement</a>.</p>
