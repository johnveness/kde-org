-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1


KDE Security Advisory: kpdf Buffer Overflow Vulnerability
Original Release Date: 2005-01-19
URL: http://www.kde.org/info/security/advisory-20050119-1.txt

0. References

        http://cve.mitre.org/cgi-bin/cvename.cgi?name=CAN-2005-0064
        http://www.idefense.com/application/poi/display?id=186&type=vulnerabilities


1. Systems affected:

        KDE 3.2 up to including KDE 3.2.3.
        KDE 3.3 up to including KDE 3.3.2.


2. Overview:

        kpdf, the KDE pdf viewer, shares code with xpdf. xpdf contains
        a buffer overflow that can be triggered by a specially 
        crafted PDF file.


3. Impact:

        Remotely supplied pdf files can be used to execute arbitrary
        code on the client machine.


4. Solution:

        Source code patches have been made available which fix these
        vulnerabilities. Contact your OS vendor / binary package provider
        for information about how to obtain updated binary packages.


5. Patch:

        Patch for KDE 3.2.3 is available from 
        ftp://ftp.kde.org/pub/kde/security_patches :

        fc6fc7fa6886d6ff19037e7547846990  post-3.2.3-kdegraphics-3.diff

        Patch for KDE 3.3.2 is available from 
        ftp://ftp.kde.org/pub/kde/security_patches :

        fc6fc7fa6886d6ff19037e7547846990  post-3.3.2-kdegraphics-3.diff


6. Time line and credits:

        19/01/2005 KDE Security Team alerted by Carsten Lohrke
        19/01/2005 Patches from xpdf 3.00pl3 applied to KDE CVS and patches
                   prepared. 
        19/01/2005 Public disclosure.


-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.2.5 (GNU/Linux)

iD8DBQFB7ofQvsXr+iuy1UoRAu7pAKCv5aD+QMuu7RpRmtRB8cjax/KzNwCeN+zP
BQEQ86e8UN+Xdr70GVH6A7c=
=soQl
-----END PGP SIGNATURE-----
