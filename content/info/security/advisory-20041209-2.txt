-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1


KDE Security Advisory: kfax libtiff vulnerabilities
Original Release Date: 2004-12-09
URL: http://www.kde.org/info/security/advisory-20041209-2.txt

0. References

        CAN-2004-0803
        CAN-2004-0804
        CAN-2004-0886


1. Systems affected:

        All KDE releases earlier than KDE 3.3.2.


2. Overview:

        Chris Evans and others discovered multiple vulnerabilities
        in the libtiff library. The Common Vulnerabilities and
        Exposures project assigned CAN-2004-0803 to this issue.

        kfax, a small utility for displaying fax files, contains
        for historic reasons a private copy of libtiff. Therefore
        it is vulnerable to these issues as well.

        kfax and the kfax KPart are invoked by KMail or Konqueror
        for viewing .g3 files.

        For the active KDE maintenance branches, which are
        KDE 3.2.x and KDE 3.3.x, this problem has been solved by
        removing the private copy of libtiff. In KDE 3.2.x, kfax
        will use the tiff2ps and fax2tiff utilities at runtime as
        backend. In KDE 3.3.x the code requiring libtiff or any other
        runtime dependencies has been replaced by a native solution
        that is unaffected by the mentioned vulnerabilities.

        Due to the complexity of the change, no simple diff is
        provided. The problems have been addressed in the KDE 3.3.2
        release.

        As a workaround, you can remove the kfax binary and the
        kfaxpart.la KPart from your system to be on the safe
        side.


3. Impact:

        Specially crafted fax files can trigger buffer overflows
        in libtiff and execute arbitrary code. 


4. Solution:

        Source code updates have been made available which fix these
        vulnerabilities. Contact your OS vendor / binary package provider
        for information about how to obtain updated binary packages.


5. Patch:

        No patches are being made available due to complexity of the change.


6. Time line and credits:

        13/10/2004 KDE Security Team alerted by Than Ngo
        28/10/2004 private libtiff fork removed from CVS, updated
                   packages finished.
        XX/11/2004 Regression fixing, several refinements.
        09/12/2004 Public announcement
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.2.5 (GNU/Linux)

iD8DBQFBt7AevsXr+iuy1UoRAqEDAJ93xLp8fP0xAcydL1OleyvOSl8PugCg54//
bH5iUgLqdW9+4XtV8FgFaOo=
=EydN
-----END PGP SIGNATURE-----
