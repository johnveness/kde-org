---
aliases:
- /info/releases-19.12.3
title: "19.12.3 Releases Source Info Page"
announcement: /announcements/releases/2020-03-apps-update
layout: releases
signer: Christoph Feck
signing_fingerprint: F23275E4BF10AFC1DF6914A6DBD2CE893E2D1C87
---
