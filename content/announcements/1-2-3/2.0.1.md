---
aliases:
- ../announce-2.0.1
custom_about: true
custom_contact: true
date: '2000-12-05'
description: The KDE Team today announced the release of KDE 2.0.1, KDE's powerful,
  modular, Internet-enabled desktop. KDE 2.0.1 is a translation, documentation and
  bug-fix release and follows six weeks after the release of KDE 2.0
title: KDE 2.0.1 Release Announcement
---

FOR IMMEDIATE RELEASE

<h3 ALIGN="center">New KDE Release for Linux Desktop</h3>

New Version of Leading Desktop for Linux and
Other UNIXes Ships

December 5, 2000 (The INTERNET). The <a href="/">KDE
Team</a> today announced the release of KDE 2.0.1, a powerful, modular,
Internet-enabled desktop. KDE 2.0.1 is a translation and bug-fix release
and follows six weeks after the release of KDE 2.0, which constitutes the
next generation of the
<a href="/awards">award-winning</a> KDE 1
series. KDE is the work product of hundreds of dedicated developers
originating from over 30 countries.

The primary goals of the 2.0.1 release are to improve documentation
and provide additional language translations for the user interface. As
a result of the dedicated efforts of hundreds of translators, KDE 2 is now
available in 33 languages and dialects, including the addition of Japanese in
this release. Code development is currently focused
on the branch that will lead to KDE 2.1, scheduled for release in first quarter
2001, so only a few significant code fixes and improvements are included in this
release. A
<a href="/announcements/changelogs/changelog2_0to2_0_1">list of
these changes</a> and a <a href="/info/1-2-3/2.0.1">FAQ about
the release</a> are available at the KDE
<a href="/">website</a>.

KDE 2.0.1 includes the core KDE libraries, the core desktop environment,
the KOffice suite, as well as the over 100 applications from the other
standard base KDE packages: Administration, Games, Graphics, Multimedia,
Network, Personal Information Management (PIM), Toys and Utilities.

All of KDE 2.0.1 is available for free under an Open Source license.
Likewise,
<a href="http://www.trolltech.com/">Trolltech's</a> Qt 2.2.2, the GUI
toolkit on which KDE is based,
is also available for free under two Open Source licenses: the
<a href="http://www.trolltech.com/products/download/freelicense/license.html">Q
Public License</a> and the <a href="http://www.gnu.org/copyleft/gpl.html">GNU
General Public License</a>.

More information about KDE 2 is available on the
<a href="/info/1-2-3/2.0.1">KDE 2.0.1 Info Page</a>, an
evolving FAQ about the release, in a
<a href="http://devel-home.kde.org/~granroth/LWE2000/index.html">slideshow
presentation</a> and on
<a href="/">KDE's web site</a>, including a number of
<a href="http://www.kde.org/screenshots/kde2shots.html">screenshots</a>, <a href="http://developer.kde.org/documentation/kde2arch.html">developer information</a> and
a developer's
<a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/~checkout~/kdelibs/KDE2PORTING.html?rev=2.3">KDE 1 - KDE 2 porting guide</a>.

#### Downloading and Compiling KDE

The source packages for KDE 2.0.1 are available for free download at
<a href="http://ftp.kde.org/stable/2.0.1/distribution/tar/generic/src/">http://ftp.kde.org/stable/2.0.1/distribution/tar/generic/src/</a> or in the
equivalent directory at one of the many KDE ftp server
<a href="/mirrors">mirrors</a>. KDE 2.0.1 requires
qt-2.2.1, which is available from the above locations under the name
<a href="http://ftp.kde.org/stable/2.0/distribution/tar/generic/src/qt-x11-2.2.1.tar.gz">qt-x11-2.2.1.tar.gz</a>,
although
<a href="ftp://ftp.trolltech.com/pub/qt/source/qt-x11-2.2.2.tar.gz">qt-2.2.2</a>
is recommended. KDE 2.0.1 will not work with versions of Qt older than 2.2.1.

For further instructions on compiling and installing KDE, please consult
the <a href="http://developer.kde.org/build/index.html">installation
instructions</a> and, if you encounter problems, the
<a href="http://developer.kde.org/build/index.html">compilation FAQ</a>.

<h4>Installing Binary Packages</h4>

Some distributors choose to provide binary packages of KDE for certain
versions of their distribution. Some of these binary packages for KDE 2.0.1
will be available for free download under
<a href="http://ftp.kde.org/stable/2.0.1/distribution/">http://ftp.kde.org/stable/2.0.1/distribution/</a>
or under the equivalent directory at one of the many KDE ftp server
<a href="/mirrors">mirrors</a>. Please note that the
KDE team is not responsible for these packages as they are provided by third
parties -- typically, but not always, the distributor of the relevant
distribution.

KDE 2.0.1 requires qt-2.2.1, the free version of which is available
from the above locations usually under the name qt-x11-2.2.1, although
qt-2.2.2 is recommended. KDE 2.0.1 will not work with versions of Qt
older than 2.2.1.

At the time of this release, pre-compiled packages are available for:

<ul>
<li><a href="http://kde.tdyc.com/debian/dists/potato/">Debian GNU/Linux 2.2 (potato)</a></li>
<li><a href="http://ftp.kde.org/stable/2.0.1/distribution/rpm/Mandrake/7.2/">Mandrake 7.2</a></li>
<li><a href="http://ftp.kde.org/stable/2.0.1/distribution/rpm/Caldera/">Caldera OpenLinux 2.4</a></li>
<li><a href="http://ftp.kde.org/stable/2.0.1/distribution/rpm/RedHat/7.0/i386/">RedHat Linux 7.0 (i386)</a>, <a href="http://ftp.kde.org/stable/2.0.1/distribution/rpm/RedHat/7.0/alpha/">RedHat Linux 7.0 (Alpha)</a>, <a href="http://ftp.kde.org/stable/2.0.1/distribution/rpm/RedHat/7.0/sparc/">RedHat Linux 7.0 (Sparc)</a>, <a href="http://ftp.kde.org/stable/2.0.1/distribution/rpm/RedHat/6.x/i386/">RedHat Linux 6.x (i386)</a>, <a href="http://ftp.kde.org/stable/2.0.1/distribution/rpm/RedHat/6.x/alpha/">RedHat Linux 6.x (Alpha)</a> and <a href="http://ftp.kde.org/stable/2.0.1/distribution/rpm/RedHat/6.x/sparc/">RedHat Linux 6.x (Sparc)</a></li>
<li><a href="http://ftp.kde.org/stable/2.0.1/distribution/rpm/SuSE/7.0-i386/">SuSE Linux 7.0 (i386)</a>, <a href="http://ftp.kde.org/stable/2.0.1/distribution/rpm/SuSE/7.0-sparc/">SuSE Linux 7.0 (Sparc)</a> and <a href="http://ftp.kde.org/stable/2.0.1/distribution/rpm/SuSE/6.4-i386/">SuSE Linux 6.4 (i386)</a></li>
<li><a href="http://ftp.kde.org/stable/2.0.1/distribution/tar/tru64/">Tru64 Systems</a></li>
</ul>

Please check the servers periodically for pre-compiled packages for other
distributions. More binary packages will become available over the
coming days and weeks.

#### About KDE

KDE is an independent, collaborative project by hundreds of developers
worldwide to create a sophisticated, customizable and stable desktop environment
employing a component-based, network-transparent architecture.
KDE is working proof of the power of the Open Source "Bazaar-style" software
development model to create first-rate technologies on par with
and superior to even the most complex commercial software.

For more information about KDE, please visit KDE's <a href="http://www.kde.org/whatiskde/">web site</a>.

<hr />
<FONT SIZE=2>

_Trademarks Notices._
Linux is a registered trademark of Linus Torvalds.
Unix is a registered trademark of The Open Group.
Trolltech and Qt are trademarks of Trolltech AS.
MS Windows, Internet Explorer and Windows Explorer are trademarks or registered trademarks of Microsoft Corporation.
Netscape and Netscape Communicator are trademarks or registered trademarks of Netscape Communications Corporation in the United States and other countries and JavaScript is a trademark of Netscape Communications Corporation.
Java is a trademark of Sun Microsystems, Inc.
Flash is a trademark or registered trademark of Macromedia, Inc. in the United States and/or other countries.
RealAudio and RealVideo are trademarks or registered trademarks of RealNetworks, Inc.
All other trademarks and copyrights referred to in this announcement are the property of their respective owners.</font>
<BR>

<table border=0 cellpadding=8 cellspacing=0 align="center">
<tr>
  <th colspan=2 align="left">
    Press Contacts:
  </th>
</tr>
<tr Valign="top">
  <td >
    United&nbsp;States:
  </td>
  <td >
    Kurt Granroth <br>
    
  [granroth@kde.org](mailto:granroth@kde.org)
    <br>
    (1) 480 732 1752<br>&nbsp;<br>
    Andreas Pour<br>
    [pour@kde.org](mailto:pour@kde.org)<br>
    (1) 718-456-1165
  </td>
</tr>
<tr valign="top"><TD>
Europe (French and English):
</TD><TD >
David Faure<BR>

[faure@kde.org](mailto:faure@kde.org)<BR>
(44) 1225 837409

</TD></tr>
<tr Valign="top">
  <td >
    Europe (English and German):
  </td>
  <td>
    Martin Konold<br>
    
  [konold@kde.org](mailto:konold@kde.org)
    <br>
    (49) 179 2252249
  </td>
</tr>
</table>
