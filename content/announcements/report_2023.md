---
description: "KDE's Annual Report for 2023 has been published"
authors:
  - SPDX-FileCopyrightText: 2024 KDE
SPDX-License-Identifier: CC-BY-4.0
date: 2024-09-05
scssFiles:
  - /scss/2023-gsoc-end.scss
title: "KDE’s Annual report for the year 2023 is out"
draft: false
---
Everything you wanted to know about the things we did last year is in this
report: the funds we raised, how we spent them, the sprints and events we
attended, the projects we took on, the milestones we hit, and much, much more.

[Read it here](https://ev.kde.org/reports/ev-2023/).

