---
qtversion: 5.15.2
date: 2023-09-09
layout: framework
libCount: 83
---

### Breeze Icons

* Add filled `bookmarks-bookmarked` icon (bug 473694)

### Extra CMake Modules

* ECMSetupVersion: do not handle SOVERSION value "0" as not set at all
* Drop outdated check for POLICY CMP0048
* No longer explicitly include CMakeParseArguments
* KDEInstallDirs6: use kxmlgui5 subdir as before, not implementation specific

### KConfig

* Fix deadlock when KConfigIni fails to acquire a file lock

### KCoreAddons

* KFileSystemType: recognize ntfs3

### KDeclarative

* GridDelegate: lazy load menu (bug 473798)

### KHolidays #

* Add holidays for Benin (bug 473315)
* Improve Norwegian names of the Sámi National Day
* Add holidays for Tanzania (bug 473279)

### KImageFormats

* QOI: Advertise write support in the desktop file
* qoi: write support backported from master
* xcf: format v12 support (kf5)
* Support libavif 1.0
* exr: multiple fixes (kf5)
* Fix missing qoi.desktop
* qoi: fix buffer overflow kf5
* Renamed qoi.h to qoi_p.h
* Add support for the QOI image format
* Set linear color space and round fix

### KIO

* Don't unlink + rename on CIFS mounts during copy operations (bug 454693)
* KFileWidget: better exclude string that may look like Urls (bug 473228)
* Fix Ask Jeeves search provider (bug 473297)

### Kirigami

* OverlayDrawer: Fix up property access
* Pass spacing on FlexColumn to it's inner ColumnLayout
* ShadowedRectangle: proceed itemChange in QQuickItem (bug 472748)

### KNotification

* Fix StatusNotifierItem checkVisibility on Windows

### KWayland

* Unbreak build where XLib's Bool definition harms moc generated code

### KWidgetsAddons

* Fix height of text after update

### ModemManagerQt

* Example: drop stand-alone CMake code, here part of normal build

### Plasma Framework

* Dialog: Fix positioning adjustments when dialog goes out of bounds
* Dialog: Factor out parts of expressions in positioning code
* SwitchIndicator: Enable layered rendering when semi-transparent
* add an option to disable installation of desktopthemes
* WindowThumbnail: proceed itemChange in QQuickItem (bug 472748)

### QQC2StyleBridge

* Don't connect the same PanelView to the same PlasmaDesktopTheme N times

### Security information

The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure <faure@kde.org>
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
