---
aliases:
- ../announce-frameworks5-alpha
date: 2014-02-14
description: KDE Ships Alpha 1 of Frameworks 5.
title: KDE Ships First Alpha of Frameworks 5
---

{{<figure src="/announcements/frameworks/5-tp/KDE_QT.jpg" class="text-center" caption="Collaboration between Qt and KDE" >}}

Today KDE released the first alpha of Frameworks 5, part of a series of releases leading up to the final version planned for June 2014. This release includes progress since the <a href='../5-tp'>Frameworks 5 Tech Preview</a> in the beginning of this year.

Improvements in this release include the addition of .pri files which make it easy for qmake based projects to use individual frameworks and two new frameworks: kactivities and plasma-framework. There has also been significant progress in getting frameworks to work on the Microsoft Windows platform.

For information about Frameworks 5, see <a href='http://dot.kde.org/2013/09/25/frameworks-5'>this article on the dot news site</a>. Those interested in following progress can check out the <a href='https://projects.kde.org/projects/frameworks'>git repositories</a>, follow the discussions on the <a href='https://mail.kde.org/mailman/listinfo/kde-frameworks-devel'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='https://git.reviewboard.kde.org/groups/kdeframeworks/'>review board</a>. Policies and the current state of the project and plans are available at the <a href='http://community.kde.org/Frameworks'>Frameworks wiki</a>. Real-time discussions take place on the <a href="irc://irc.libera.chat/#kde-devel">#kde-devel IRC channel on libera.chat</a>.

## Discuss, Spread the Word and See What's Happening: Tag as &quot;KDE&quot;

KDE encourages people to spread the word on the Social Web. Submit stories to news sites, use channels like delicious, digg, reddit, twitter, identi.ca. Upload screenshots to services like Facebook, Flickr, ipernity and Picasa, and post them to appropriate groups. Create screencasts and upload them to YouTube, Blip.tv, and Vimeo. Please tag posts and uploaded materials with &quot;KDE&quot;. This makes them easy to find, and gives the KDE Promo Team a way to analyze coverage for these releases of KDE software.

You can discuss this news story <a href='http://dot.kde.org/2014/02/14/kde-frameworks-5-alpha-out'>on the Dot</a>, KDE's news site.

#### Installing frameworks Alpha 1 Binary Packages

<em>Packages</em>.
A variety of distributions offers frequently updated packages of Frameworks 5. This includes Arch Linux, AOSC, Fedora, Kubuntu and openSUSE. See <a href='http://community.kde.org/Frameworks/Binary_Packages'>this wikipage</a> for an overview.

#### Compiling frameworks

The complete source code for frameworks 4.96.0 may be <a href='http://download.kde.org/unstable/frameworks/4.96.0/'>freely downloaded</a>.

#### {{< i18n "supporting-kde" >}}

{{% i18n "whatiskde" %}}
