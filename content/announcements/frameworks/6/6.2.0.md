---
qtversion: 6.5.0
date: 2024-05-10
layout: framework
libCount: 72
draft: false
---
{{< details title="Attica" href="https://commits.kde.org/attica" >}}
+ Modernize: don't else after return. [Commit.](http://commits.kde.org/attica/03da33028583ebbd0f70fa052797107c32b7c190) 
+ Provider: document default ctor. [Commit.](http://commits.kde.org/attica/9fd4e2e4139f78840078dc340256f152ea6e13cf) 
+ Providermanager: remove unused function with typo. [Commit.](http://commits.kde.org/attica/cd844b3e9d3abd1b30a0bc82a25347520348027f) 
+ Postfiledata: simplify private. [Commit.](http://commits.kde.org/attica/7026a673791836860ee81db15d1600bb6bd89f9c) 
+ Modernize: use unique_ptr for privates. [Commit.](http://commits.kde.org/attica/d4e25ff1d10830716c2361e283a18ee2c34b7e1e) 
+ Basejob: don't leave dangly pointers. [Commit.](http://commits.kde.org/attica/42f05b51aaadd7b881089deff40fefcc56d43812) 
+ Platformdependent: v3. [Commit.](http://commits.kde.org/attica/29fdc874236c36568c6dfe36b76dc91eeea7fe41) 
{{< /details >}}

{{< details title="Baloo" href="https://commits.kde.org/baloo" >}}
+ Don't kill the lock file, can lead to random corruption. [Commit.](http://commits.kde.org/baloo/15083afb9671ec79eb2dbee4ef41a92556d154af) Fixes bug [#389848](https://bugs.kde.org/389848)
{{< /details >}}

{{< details title="Bluez Qt" href="https://commits.kde.org/bluez-qt" >}}
+ Fixed min_bitpool and max_bitpool from capabilities where ignored. [Commit.](http://commits.kde.org/bluez-qt/3f815b7334b3ee00d5edc089867d13dfa87febb9) 
+ Port QML module to declarative type registration. [Commit.](http://commits.kde.org/bluez-qt/62cff9067c4860b8d208f11652833b823a84bfe8) 
{{< /details >}}

{{< details title="Breeze Icons" href="https://commits.kde.org/breeze-icons" >}}
+ Add support for media-playlist-no-shuffle icon name. [Commit.](http://commits.kde.org/breeze-icons/7e9102071f45cb1713642731df870aa30398cb9c) 
+ Add audio/ogg and audio/x-vorbis+ogg icons. [Commit.](http://commits.kde.org/breeze-icons/7c036574bab8a1d6dd94ca657208971cd6b05909) 
+ Add audio/vnd.wave MIME type. [Commit.](http://commits.kde.org/breeze-icons/810d3e93e82d06b3c87d452cf64c05e1385dfda9) 
+ Remove generic non-symbolic audio and video icons. [Commit.](http://commits.kde.org/breeze-icons/f948e46976ba26f4fa1f3a994a9fe19e280cb162) 
+ Add 16 and 22px symbolic versions of some Places icons that were missing. [Commit.](http://commits.kde.org/breeze-icons/e16423aa6332b4d49a2367b917eb4f70964e364f) Fixes bug [#486316](https://bugs.kde.org/486316)
+ Add zoom-in-map and zoom-out-map icons along with -symbolic versions. [Commit.](http://commits.kde.org/breeze-icons/5341ea2fca40581646d57110fff1f1c911c9abfc) 
+ Longer description for the ICONS_LIBRARY option. [Commit.](http://commits.kde.org/breeze-icons/b01914ba6603b50023a1f776bd2c17a9e9697251) 
+ Add symbolic versions for more USB device style icons. [Commit.](http://commits.kde.org/breeze-icons/a4bac8be79e33ff5f67e3a1e18b3f0c81103a789) 
+ Fix some scale errors. [Commit.](http://commits.kde.org/breeze-icons/678ff599bdc8721584164c69d6fa844b9116c5f9) Fixes bug [#485479](https://bugs.kde.org/485479)
+ Fixed input-combo-on.svg colour issue. [Commit.](http://commits.kde.org/breeze-icons/1844fb1ee2fa092df91b4ba523489c9add148bea) 
+ Add -symbolic symlinks for notification-* icons. [Commit.](http://commits.kde.org/breeze-icons/bb3f36428e7612adfdcbf9cfc43cd73bad51e39d) 
+ Use new `dev.suyu_emu.suyu` id, add symlink for old id. [Commit.](http://commits.kde.org/breeze-icons/73cf689ac95e815990d841942de64e9b1a267da4) 
+ Add accessories-screenshot-tool icon/symlink. [Commit.](http://commits.kde.org/breeze-icons/422925b10924eca2c10e72b66840873c4ab5d6cc) 
+ [webfont] enable ligatures. [Commit.](http://commits.kde.org/breeze-icons/30476e397613a762eded956f5d3cc2cc329518c8) 
+ Gitignore: add VS Code dir. [Commit.](http://commits.kde.org/breeze-icons/5013f5c53b6fc8845d6e9376f0be6addf8529931) 
+ Add input-keyboard-color 22px, 32 px. [Commit.](http://commits.kde.org/breeze-icons/7b082fd816ac3c5a3c53d5a9c5a2aa99455fd454) 
{{< /details >}}

{{< details title="Extra CMake Modules" href="https://commits.kde.org/extra-cmake-modules" >}}
+ API dox: KDEInstallDirs6: refer to qtpaths now as source of Qt paths. [Commit.](http://commits.kde.org/extra-cmake-modules/30f066c5f0295239b0b335a62c0c97e979313a7e) 
+ API dox: KDEInstallDirs6: drop outdated note about being in ALPHA state. [Commit.](http://commits.kde.org/extra-cmake-modules/4ed6070f830fff320a5ccce329c710460a44dcc6) 
+ Test: Increase minimum cmake version so that it works with Qt 6.7. [Commit.](http://commits.kde.org/extra-cmake-modules/534a7ffcb2e5a0feb024688b3f2092dfeae87323) 
+ ECMQmlModule6: group qml and resource file calls. [Commit.](http://commits.kde.org/extra-cmake-modules/eeac86c931199c85bff1fc50c5eae55735a1a22a) 
+ Modules/ECMAddTests.cmake - handle unset or empty QT_PLUGIN_PATH. [Commit.](http://commits.kde.org/extra-cmake-modules/eff69a1841fea1eefd34ebabd646418466f3a249) 
{{< /details >}}

{{< details title="KArchive" href="https://commits.kde.org/karchive" >}}
+ Gitignore: add VS Code dir. [Commit.](http://commits.kde.org/karchive/871b780195e4bf8b5535b69ee446a9fad2b5f68a) 
{{< /details >}}

{{< details title="KCMUtils" href="https://commits.kde.org/kcmutils" >}}
+ KQuickConfigModule: Don't crash on null object. [Commit.](http://commits.kde.org/kcmutils/a6fbb476b58253bf1fcbb907974cca11af8cf5ba) 
+ SimpleKCM: Remove remnants of footerParent. [Commit.](http://commits.kde.org/kcmutils/bda94f215cc6b340e26425f86c1a49bbd10f0370) 
+ Use ellipsis character instead of three dots. [Commit.](http://commits.kde.org/kcmutils/9217de987156b21806fdaf6ba01975f18037d99b) 
+ GridDelegate: Port to ComponentBehavior: Bound. [Commit.](http://commits.kde.org/kcmutils/76812a387e948a0e11d8d4b025a0f38f1307b678) 
+ GridDelegate: Consolidate code paths for opening a menu. [Commit.](http://commits.kde.org/kcmutils/2136436c61bf9fc7087f89d3ae9669ffbf11bf43) 
+ GridDelegate: Don't use qualified property access as appropriate. [Commit.](http://commits.kde.org/kcmutils/09b813afdb19edcac34b324727dae2ba3e1c9ece) 
+ GridDelegate: Use concrete type for the popup menu. [Commit.](http://commits.kde.org/kcmutils/ad40200406e31fcfaf6bb70c1c0d7270de74f5c0) 
+ GridDelegate: Use somewhat more consistent ToolTip bindings, remove timeout. [Commit.](http://commits.kde.org/kcmutils/2995223e2556c8f85632351541dcb755a3d6cc66) 
+ Components: Drop QML import versions, unify import aliases. [Commit.](http://commits.kde.org/kcmutils/4b6d9386763042da682c5a5357566d3e190d1054) 
+ Components: Guard nullable property access. [Commit.](http://commits.kde.org/kcmutils/bfd4b0c24f73343e6d681488ad571abefd61c17f) 
+ Components: Explicitly specify signal handler arguments. [Commit.](http://commits.kde.org/kcmutils/f0e9b969c8b59e0fda786fcf150865f2ff05e633) 
+ KCModuleQml: Provide a fallback in case a pushed page is not one of magical KCMUtils types. [Commit.](http://commits.kde.org/kcmutils/08786231388965cb339b24ad283f83bc870fac4b) 
+ Add API to make header and footer paddings optional. [Commit.](http://commits.kde.org/kcmutils/c2469298f6b743be90f95563ee837b8eca6e1430) 
+ Round all the things consistently. [Commit.](http://commits.kde.org/kcmutils/771faea95feb625cfd59446377ac973c43e25a0a) 
+ Pluginselector: cache delegates. [Commit.](http://commits.kde.org/kcmutils/3a9e665537cefe3946fe05f7b17c8256feaac1e2) 
{{< /details >}}

{{< details title="KCodecs" href="https://commits.kde.org/kcodecs" >}}
+ Gitignore: add VS Code dir. [Commit.](http://commits.kde.org/kcodecs/862b3c688f451c4edb3f3c63dcf1e819653c3d72) 
{{< /details >}}

{{< details title="KColorScheme" href="https://commits.kde.org/kcolorscheme" >}}
+ Gitignore: add VS Code dir. [Commit.](http://commits.kde.org/kcolorscheme/b4b41e41500dba29a63637c5eaa9e1b0b0cf605f) 
{{< /details >}}

{{< details title="KConfig" href="https://commits.kde.org/kconfig" >}}
+ Avoid allocations while parsing int/float lists. [Commit.](http://commits.kde.org/kconfig/4f9b34e7ffd4a84b76e507b99f568b02d75af241) 
+ Adapt kdesktopfiletest to QTemporaryFile behavior change. [Commit.](http://commits.kde.org/kconfig/b0ff19816a9efa2178d50ae5b73c262064168b21) 
+ Guard header with an ifndef and include moc generates sources in the cpp file. [Commit.](http://commits.kde.org/kconfig/8399ab9d9ab38a79553f21e75a14fbb7cf22f7fa) 
+ Autotests: Don't use a timeout in testLocalDeletion. [Commit.](http://commits.kde.org/kconfig/b05af56a418e8add360b954b3ce92e039803fbe1) 
+ Adjust kconfig_compiler autotests to include a version without kcfgc. [Commit.](http://commits.kde.org/kconfig/dc43b5483b3e71e173d461d12440745f9aa0be8b) 
+ Add a CMake function to add a kcfg file without kcfgc. [Commit.](http://commits.kde.org/kconfig/027a81e5d657163a61f7292608c91e923891e44b) 
+ Gitignore: add VS Code dir. [Commit.](http://commits.kde.org/kconfig/1aaacb22ce159044c15ebf60172f4c231ba47e14) 
{{< /details >}}

{{< details title="KConfigWidgets" href="https://commits.kde.org/kconfigwidgets" >}}
+ KRecentFilesAction: Load mimeType and action lazily. [Commit.](http://commits.kde.org/kconfigwidgets/b0dc12c8c24f36c2c68f82fa518a62423e2d1c4f) 
+ Remove forward declaration of KToggleAction. [Commit.](http://commits.kde.org/kconfigwidgets/af63935ba044d8c3e1a06425a5a15ae4412bc767) 
+ Test that an invalid language gives the empty string. [Commit.](http://commits.kde.org/kconfigwidgets/caf2c576ebfa1c34433a2694e62e707ca0dfa344) 
+ Gitignore: add VS Code dir. [Commit.](http://commits.kde.org/kconfigwidgets/9c7020553953717c704a52dafccb7a0a1d488fda) 
{{< /details >}}

{{< details title="KContacts" href="https://commits.kde.org/kcontacts" >}}
+ Restore country detection tests on FreeBSD. [Commit.](http://commits.kde.org/kcontacts/b9cdfa56cd9670217e52fa728ddb489f901d01d4) 
+ Disable FreeBSD tests that recently started to fail in the CI. [Commit.](http://commits.kde.org/kcontacts/258d759752cf8e03a9e252e495f26843c069f165) 
{{< /details >}}

{{< details title="KCoreAddons" href="https://commits.kde.org/kcoreaddons" >}}
+ Gitignore: add VS Code dir. [Commit.](http://commits.kde.org/kcoreaddons/b4090f7a903afcad47821ef27de1077a3ffdc782) 
{{< /details >}}

{{< details title="KCrash" href="https://commits.kde.org/kcrash" >}}
+ Gitignore: add VS Code dir. [Commit.](http://commits.kde.org/kcrash/978673da04c7f3a801fef013619ebce7bbbb011e) 
{{< /details >}}

{{< details title="KFileMetaData" href="https://commits.kde.org/kfilemetadata" >}}
+ API dox: use "MIME type" and "URL" spellings consistently. [Commit.](http://commits.kde.org/kfilemetadata/6429f0fb0167fcc1ed3ac5538a08a9044a1ef8ad) 
+ API dox: add some minimal info to undocumented classes. [Commit.](http://commits.kde.org/kfilemetadata/b938714c5b75677757d5a4e4350399063254ca2e) 
+ API dox: add empty line between @brief (ends by first period) and rest. [Commit.](http://commits.kde.org/kfilemetadata/e95aa4679eb1e8d963512bd400c7636dfb605132) 
+ API dox: add some dummy info to namespaces for doxygen to cover them also. [Commit.](http://commits.kde.org/kfilemetadata/6adf67e620379cb476863ecb2a3404f734452330) 
+ API dox: escape space after e.g. to work-around doxygen sentence end magic. [Commit.](http://commits.kde.org/kfilemetadata/800728f0c7646ce5372fa208ceb4a18f6622e1f0) 
+ UserMetadata: return errors when xattr ops fails. [Commit.](http://commits.kde.org/kfilemetadata/db0c9e27157eb9067a5d22bc37715b887a1f7532) 
+ Value is already default timeout value in QT6 framework. [Commit.](http://commits.kde.org/kfilemetadata/73f2728ec1ec8e2cf2cbe5885a80ec28f0913f7a) 
+ [XmlExtractor] Add support for compressed SVGs. [Commit.](http://commits.kde.org/kfilemetadata/e646db97622cc11c928867db5a02270b43161c89) 
+ [XmlExtractorTest] Move Test class declaration to source file. [Commit.](http://commits.kde.org/kfilemetadata/1d6f7fae059dc089cdc498a1ec38b78aa8a00003) 
+ [Office2007Extractor] Reuse DublinCoreExtractor, fix namespace handling. [Commit.](http://commits.kde.org/kfilemetadata/1ba34b6763e216f7c47262cccfad1cf0c268b770) 
+ [DublinCoreExtractor] Add CreationDate (dc::created) support. [Commit.](http://commits.kde.org/kfilemetadata/da14bb8efbc0c79c63477761ef95632e7fb6ab68) 
+ [DublinCoreExtractor] Skip properties from empty elements, cleanup. [Commit.](http://commits.kde.org/kfilemetadata/aaa9fc3d694a53640a4fa750b257e9750306f83c) 
+ Move date parser helper out of ExtractorPlugin, clean it up. [Commit.](http://commits.kde.org/kfilemetadata/fb3d7570cfab1871780bda2b4724b399cbbdc098) 
+ [TaglibExtractor] Include vnd.audible.aaxc audio books in supported types. [Commit.](http://commits.kde.org/kfilemetadata/a943f7e125eb4ae02768a83c16f3d3548999865e) 
+ [Test] Include vnd.audible.aax audio books in coverage tests. [Commit.](http://commits.kde.org/kfilemetadata/21d462a6df3ee75b1fd401b4e5f16ea276b60ced) 
+ [TaglibWriterTest] Move test class declaration to source file, cleanup. [Commit.](http://commits.kde.org/kfilemetadata/98db8a4f3f300f0b15cf4fdf8b47efd68cb9f52f) 
+ [TaglibExtractor|Writer] Fix mimetypes. [Commit.](http://commits.kde.org/kfilemetadata/cb205f0ac0ae891ea4e21e27307d762e1b2af3ff) 
{{< /details >}}

{{< details title="KGlobalAccel" href="https://commits.kde.org/kglobalaccel" >}}
+ Port q->connect to QObject::connect. [Commit.](http://commits.kde.org/kglobalaccel/110a37c30721dd41d0a46a1ea9fd621f268cc91d) 
+ Fix connection lifetime issue. [Commit.](http://commits.kde.org/kglobalaccel/8e8ef50b9faa3c2365dcf60f9d8dac71c55c0440) See bug [#454854](https://bugs.kde.org/454854). See bug [#485386](https://bugs.kde.org/485386)
{{< /details >}}

{{< details title="KGuiAddons" href="https://commits.kde.org/kguiaddons" >}}
+ Recorder/kkeysequencerecorder: conform to KKeyServer changes. [Commit.](http://commits.kde.org/kguiaddons/ea978ed0002045701457adddc7781c91ed9c01a1) 
+ Gitignore: add VS Code dir. [Commit.](http://commits.kde.org/kguiaddons/0efca934f0fd69e93f4339b4c73f85adb3e0f478) 
{{< /details >}}

{{< details title="KHolidays" href="https://commits.kde.org/kholidays" >}}
+ Update holiday_bd_en. [Commit.](http://commits.kde.org/kholidays/d8b4263903d328edf7be2844216b2b78374c4965) 
{{< /details >}}

{{< details title="KI18n" href="https://commits.kde.org/ki18n" >}}
+ Gitignore: add VS Code dir. [Commit.](http://commits.kde.org/ki18n/2bbf95ca85950d3756a874853cd3a2d6e53737d8) 
{{< /details >}}

{{< details title="KIconThemes" href="https://commits.kde.org/kiconthemes" >}}
+ Allow to configure if we register our icon plugin for SVGs. [Commit.](http://commits.kde.org/kiconthemes/54dce1c6f35f3c3d579c5e5041437f2ca13cd4fa) 
+ Port QML module to declarative type registration. [Commit.](http://commits.kde.org/kiconthemes/6ed9c41764e9cb413713db66fcce4632e2464946) 
+ Fix typo in BreezeIcons::initIcons loading. [Commit.](http://commits.kde.org/kiconthemes/d8ee0caf5b940a310020dd68ecb6905e67413ed4) 
+ Gitignore: add VS Code dir. [Commit.](http://commits.kde.org/kiconthemes/d592532a6ea6a2c3013a182fdec586a5e64a0610) 
{{< /details >}}

{{< details title="KImageformats" href="https://commits.kde.org/kimageformats" >}}
+ Fix build with Qt 6.7 on 32 bits. [Commit.](http://commits.kde.org/kimageformats/c2c12b1d7e38728e7533f1346deca0270973d22d) 
{{< /details >}}

{{< details title="KIO" href="https://commits.kde.org/kio" >}}
+ [kfilefilter] Consider mime globs valid. [Commit.](http://commits.kde.org/kio/1aba3b839b83b3c6dd9062274685b44179fd6762) 
+ Kfileitemtest: add basic tests for dir. [Commit.](http://commits.kde.org/kio/80ac681987320b795a89a4ade358e587ec805832) 
+ File_unix: don't call QMimeDatabase::mimeTypeForFile for directories. [Commit.](http://commits.kde.org/kio/b8cc0bb75e38a5cefd32d9bb6aab0b58fc4eaa59) 
+ Widgetsaskuseractionhandler prevent crash when job had no parent widget. [Commit.](http://commits.kde.org/kio/ec342c3fa60886ee9927d3c8b3acfc909d79e359) 
+ PreviewJob: Add file extension to thumbnail temp files. [Commit.](http://commits.kde.org/kio/5e0b6cbf1b9ef5c1e181caa2726a5591ff0d5fde) Fixes bug [#463989](https://bugs.kde.org/463989)
+ KFileItem: handle the case parent url path is empty. [Commit.](http://commits.kde.org/kio/8886810e20001144d59ac2d2f80f60520b4544c3) 
+ KFileItemActions: Add logic to order actions between separators. [Commit.](http://commits.kde.org/kio/b154c9d2f4fc86e6efdcf50bd11bba1f4a365033) Fixes bug [#466115](https://bugs.kde.org/466115)
+ KFileItem: fix getStatusBarInfo() displaying symlink target as "http:". [Commit.](http://commits.kde.org/kio/8ed2639ba846f79f2b7dcbd7dde84d4339b860fc) Fixes bug [#475422](https://bugs.kde.org/475422)
+ ScopedProcessRunner: support waitForStarted. [Commit.](http://commits.kde.org/kio/52c5d4da3447ec1eea0024e0a3c86a3256f5e1fa) 
+ ScopedProcessRunner: fix use of undeclared identifier 'close'. [Commit.](http://commits.kde.org/kio/22e43a44bde6c719d1138f3422acaeffd0e0471f) 
+ [ftp] Always use default timeout values. [Commit.](http://commits.kde.org/kio/f5c899cf5f32adfd82290310b6c09e3309ae265c) 
+ [http] Set error string for ERR_DOES_NOT_EXIST. [Commit.](http://commits.kde.org/kio/487cf0da974673e7db611ff805b3b84f69e5a889) 
+ Force test language to en. [Commit.](http://commits.kde.org/kio/3780b4d0eee6acd096c82a7d24f6d82505be4976) 
+ Knewfilemenu: Add @since 6.2 to the new signals and methods. [Commit.](http://commits.kde.org/kio/18c5962188990d382ef9dcfecd821ab6f66f1207) 
+ Kfilefilter.h: update API docs. [Commit.](http://commits.kde.org/kio/2ed8ad3723f7c0652c9b5681e4401fc68a59936e) 
+ Kencodingfiledialog dox: rewrite API dox that refer to removed functions. [Commit.](http://commits.kde.org/kio/721d86947f3f8abf83f7df621c3530005bcf1efe) 
+ Kfilewidget dox: replace references to setFilter() with setFilters(). [Commit.](http://commits.kde.org/kio/cb9fc9b282d25acf134f29afac42e74eb5407766) 
+ Fix a few warnings. [Commit.](http://commits.kde.org/kio/000bfc80a9edbe467fbba8c647ff4a7cca088f4a) 
+ Add `EnableRemoteFolderThumbnail` option checking. [Commit.](http://commits.kde.org/kio/c7d6ff8f218a0c063f762bd236453a7ae00b817e) 
+ PreviewJob: Display preview for locally mounted remote directories. [Commit.](http://commits.kde.org/kio/bf27e0b84406c83d090f72e691b641b4c2bd4b8f) 
+ Knewfilemenu: add isNewDirNameJobRunning. [Commit.](http://commits.kde.org/kio/ac2fa30d7b905fe7770ab03b1c249650633c851e) 
+ KFilePlacesItem: Show teardown busy indicator during optical media eject. [Commit.](http://commits.kde.org/kio/31c7df654cc699cbc8a574cf7246650c819c7908) 
+ KDirModelTest.testDeleteFiles: lower debug output. [Commit.](http://commits.kde.org/kio/bf83e7ee87321ce92db5c52e0312c95d38d5d38e) 
+ KUrlNavigator: allow adding a badge widget after the breadcrumb. [Commit.](http://commits.kde.org/kio/88123618b0276e1cf02b45ecc65e740cac39890a) 
+ Set ideal case for TwoVectors. [Commit.](http://commits.kde.org/kio/ff581c2707ac106d314e874d652d81bb5209aa6c) 
+ Udsentry_api_comparison_benchmark: update. [Commit.](http://commits.kde.org/kio/1521417f663c9bdad64323931415165ee6d47ea2) 
+ Kurlnavigatorbutton: prevent. [Commit.](http://commits.kde.org/kio/341a75c7e42ea8bcea07d0a0c0044a5a4f342e07) 
+ Add more explicit moc includes to sources for moc-covered headers. [Commit.](http://commits.kde.org/kio/8947060de90699d1ed3b59e530ae431bdd212980) 
+ Connection: don't queue tasks until OOM. [Commit.](http://commits.kde.org/kio/2629414e08c50f10dc8493fb7a0d58a7103ab980) 
+ Http: Fix parsing DAV:getlastmodified. [Commit.](http://commits.kde.org/kio/a4d7b335d61e32993c90fa1a6ee143b860219f4f) Fixes bug [#484671](https://bugs.kde.org/484671)
+ Kfileitem: Linux, use statx to refresh files. [Commit.](http://commits.kde.org/kio/db740854f3644fe47e8aec24dadf467aa5a1d7a2) Fixes bug [#446858](https://bugs.kde.org/446858)
{{< /details >}}

{{< details title="Kirigami" href="https://commits.kde.org/kirigami" >}}
+ Fixed wrong navigation and dialog header button colors. [Commit.](http://commits.kde.org/kirigami/18e2273d4536487a63e7ee6c5d67cd9aafd69798) Fixes bug [#486163](https://bugs.kde.org/486163)
+ Card: Remove the unnecessary "reality check" binding on footer, add test. [Commit.](http://commits.kde.org/kirigami/671ee8f03c6039aeb3cb4dc1d8937e5377dc547d) 
+ Card: Restrict actions type from arbitrary QObject to T.Action. [Commit.](http://commits.kde.org/kirigami/e6cd55c0ec85988df34961fe7ee55b472d8f0836) 
+ Card: Shuffle things around a bit to make them look nicer. [Commit.](http://commits.kde.org/kirigami/dba784b0a9c2229a4810e859c49b50de18832a85) 
+ Remove linkActivated/linkHovered from Delegate types. [Commit.](http://commits.kde.org/kirigami/510c6ab6a200bda7e29cdaef6d11ebce562dec27) 
+ InlineMessage: Improve examples in documentation, clean up QML. [Commit.](http://commits.kde.org/kirigami/4792b63f343ebdaa5504937786c46621c35ad6f0) 
+ OverlayDrawer: Rework separator's code, animate transitions. [Commit.](http://commits.kde.org/kirigami/ce9b44e25abed7b3e4a9699b42529c7847bebfee) 
+ OverlayDrawer: Hide segmented separator when the drawer is collapsed. [Commit.](http://commits.kde.org/kirigami/86398cbc80e6aed95dcc1187a6b2dab9f878b3de) 
+ OverlayDrawer: Rewrite visibility condition for segmented separator. [Commit.](http://commits.kde.org/kirigami/7aa971c8662dfc367e63c103aad30c04595c2fec) 
+ OverlayDrawer: Rewrite segmented separator positioning expression. [Commit.](http://commits.kde.org/kirigami/df581fa8c061eeee8dde28079090cc562ce31811) 
+ OverlayDrawer: Bind segmented separator's width to the real separator's width. [Commit.](http://commits.kde.org/kirigami/225a7d8756a1fac9a6039961f39cef841399bd38) 
+ GlobalDrawer: Set spacing on the default header. [Commit.](http://commits.kde.org/kirigami/a5415d9aaefc38db1b2347bca3fb6740a67be1b3) 
+ Fix null deref in OverlaySheet. [Commit.](http://commits.kde.org/kirigami/d409ba9a71ddaa0fa5c43352c6d940c4f219be8f) 
+ OverlaySheet: Port layout hacks to a simple Padding with its contentItem. [Commit.](http://commits.kde.org/kirigami/f036039910d6f20371d93bc3a065005a7ef6d173) 
+ Tst_pagepool: Port testing code from verify(==) to compare. [Commit.](http://commits.kde.org/kirigami/96500e33bf2722bdadb130c8027869cd63cd3e39) 
+ Tst_pagepool: Fix test properly. [Commit.](http://commits.kde.org/kirigami/00ecb996ca2037b483759f57353c28d94e1dcced) 
+ Revert "tst_pagepool: Fix test". [Commit.](http://commits.kde.org/kirigami/3af2cfcbe1895d3489c145037541449bc9ff2704) 
+ CardsGridView and CheckableListItem are not a thing anymore. [Commit.](http://commits.kde.org/kirigami/64038fdbbee93514b2272bc3a7b04e33cb7c0e0b) 
+ Actions.main is not a thing anymore. [Commit.](http://commits.kde.org/kirigami/36ee90d89a5e65db83f064cba96fa8845039f2e9) 
+ Tst_pagepool: Fix test. [Commit.](http://commits.kde.org/kirigami/0b4316c354f41648faa8c23f7b0009e431abfc39) 
+ Revert "PageRow: Fix parent of Component-based pages". [Commit.](http://commits.kde.org/kirigami/b2a41bdc53f49c271774a67e169944c4bed768af) 
+ PageRow: Fix parent of Component-based pages. [Commit.](http://commits.kde.org/kirigami/a133a5ca6fe718ef033ce22b7a566a257966c6ae) Fixes bug [#482753](https://bugs.kde.org/482753)
+ MnemonicAttached: Fix logic when pressing Alt. [Commit.](http://commits.kde.org/kirigami/9f42be5c0cdc6f700f230425b65c2b9c7273240d) 
+ Dialog: Always use an overlay as visual parent. [Commit.](http://commits.kde.org/kirigami/9332c1183e209eaf305d2f33d7fc0cd953364eab) 
+ Link Activation TitleSubtitle. [Commit.](http://commits.kde.org/kirigami/4fb8b8490e46d223d51389746f335a6f77ae5c67) 
+ ContextualHelpButton: remove excess space from tooltip. [Commit.](http://commits.kde.org/kirigami/9514177efafef9e8954b8329a7bd79e665b3b379) Fixes bug [#481817](https://bugs.kde.org/481817)
+ Fix crash on teardown when QML engine is already unset. [Commit.](http://commits.kde.org/kirigami/16cba5ecb2baaf70d917830da3b47f6678208d25) 
+ ListSectionHeader: Deprecate label property. [Commit.](http://commits.kde.org/kirigami/98bf3e4c35709814ff25bbbb1e2f8a19f1c1baf0) 
+ PageRow: Remove superfluous trailing semicolon from a property alias. [Commit.](http://commits.kde.org/kirigami/8b6cb897a09677381b95486f7119427f214f2e9e) 
+ PromptDialog: Create default contentItem dynamically on demand. [Commit.](http://commits.kde.org/kirigami/e53c748270746fb5b2a523dfcc4ee23972304aff) 
+ Padding: Remove old overridden contentItem from the visual hierarchy. [Commit.](http://commits.kde.org/kirigami/efb5b04f93da91961df105894c6a302772fde7d8) 
+ Gitignore: add VS Code dir. [Commit.](http://commits.kde.org/kirigami/3d1988fb1ce760e093f39e3fa76d608f6bd3ab38) 
+ Add a explicit page type check in AbstractApplictionHeader. [Commit.](http://commits.kde.org/kirigami/3ddc0d6fa8b910150ee2f0e078de6d6e074f5cf6) 
+ Add radius unit for rounded rectangles. [Commit.](http://commits.kde.org/kirigami/c59ef76198b2b50993bd4bda6be51d0f39790d80) 
{{< /details >}}

{{< details title="KItemModels" href="https://commits.kde.org/kitemmodels" >}}
+ Add dependency to QML module. [Commit.](http://commits.kde.org/kitemmodels/b88e99129961315dfcd17cf8b1170b7d7873c33f) 
+ Port to declarative type registration. [Commit.](http://commits.kde.org/kitemmodels/c6fc3685b706b560558332e130bfd7c6950980ca) 
{{< /details >}}

{{< details title="KItemViews" href="https://commits.kde.org/kitemviews" >}}
+ Add more explicit moc includes to sources for moc-covered headers. [Commit.](http://commits.kde.org/kitemviews/90ad262dfae24ed01b2322374ebf155ea43dc1bb) 
{{< /details >}}

{{< details title="KNewStuff" href="https://commits.kde.org/knewstuff" >}}
+ Show header warning message framelessly. [Commit.](http://commits.kde.org/knewstuff/2e9909d4b52eb8d6253f8243d75d33dbc1973955) Fixes bug [#485464](https://bugs.kde.org/485464)
+ DownloadItemsSheet: use alternating background colors for legibility. [Commit.](http://commits.kde.org/knewstuff/ca457c3710171ceb5d68ab55c517fe6f9bb67cc4) Fixes bug [#483724](https://bugs.kde.org/483724)
+ Document ContentWarning. [Commit.](http://commits.kde.org/knewstuff/a3abd403765ef42189d90c4c93eb6270f39e3c99) 
+ Page: conditionalize warning message based on riskiness. [Commit.](http://commits.kde.org/knewstuff/e951ac9f6f379f14c7971eeb03c161d40f9d1648) 
{{< /details >}}

{{< details title="KNotifications" href="https://commits.kde.org/knotifications" >}}
+ Enforce passing tests on Windows. [Commit.](http://commits.kde.org/knotifications/60944b37bbeaf51342416a04ec55c23b8886f126) 
{{< /details >}}

{{< details title="KParts" href="https://commits.kde.org/kparts" >}}
+ Enforce passing tests on Windows. [Commit.](http://commits.kde.org/kparts/1899fa5f18820f3c3807357db436a160dd71337e) 
{{< /details >}}

{{< details title="KRunner" href="https://commits.kde.org/krunner" >}}
+ Action: Make bool operator explicit. [Commit.](http://commits.kde.org/krunner/562a86335d374b1d2b00c8f2cc2230af1ad19b7e) 
+ Fix matchInternalFinished not being emitted in case of dbus errors. [Commit.](http://commits.kde.org/krunner/bbf910703685abf2662e1008d1d2189c146fe666) 
{{< /details >}}

{{< details title="KSVG" href="https://commits.kde.org/ksvg" >}}
+ Don't call update on missing marginObject. [Commit.](http://commits.kde.org/ksvg/81a7b3bbd43a4f1aae2e516380b0fd9f9b5f7ab0) 
+ Make property type fully qualified. [Commit.](http://commits.kde.org/ksvg/da5a4e3de9fb88b1db8e6058ac96c456a0e44102) 
{{< /details >}}

{{< details title="KTextEditor" href="https://commits.kde.org/ktexteditor" >}}
+ Fix caret painting for inline notes at the end of line. [Commit.](http://commits.kde.org/ktexteditor/f621c2e560f75dc1d1d1e9bbd5d8f3816479ef89) 
+ KateCompletionWidget: Mark function static. [Commit.](http://commits.kde.org/ktexteditor/14ff47517ce7d40356350faaec890578c92fcb99) 
+ KateCompletionModel: remove useless std::as_const. [Commit.](http://commits.kde.org/ktexteditor/7c84a30779cb9bc9ad41a23f7593c9a108569706) 
+ Run more tests offscreen. [Commit.](http://commits.kde.org/ktexteditor/38b7c653e61e82f14d3e737bd8e7097a0ec8c69a) 
+ Fix performance with many cursors in a large line. [Commit.](http://commits.kde.org/ktexteditor/af151f7977dbd8d5a7fb8c0000584f715b1f37d2) 
+ Fix test expectations. [Commit.](http://commits.kde.org/ktexteditor/033a4c112f69292db235f46e9bfd21419ed8756b) 
+ Fix crashs and OOM on load with encoding failures. [Commit.](http://commits.kde.org/ktexteditor/990d5a34a699b61bbf321b4081afc9deb2f00f9b) Fixes bug [#486195](https://bugs.kde.org/486195). Fixes bug [#486134](https://bugs.kde.org/486134)
+ A11y: Improve tab order for "Appeareance" -> "Borders". [Commit.](http://commits.kde.org/ktexteditor/98f897bbe93dc1b6acb6e9e428bdd9b41b3aaacd) 
+ A11y: Set "Line Height Multiplier" buddy. [Commit.](http://commits.kde.org/ktexteditor/fa9ecde71a20152c7f251c61d3db4bfacb79eb8d) 
+ Fix broken navigation in completion widget with multiple views. [Commit.](http://commits.kde.org/ktexteditor/51d909adb592f1e2606f2ad8f7a94b20817571ee) 
+ Fix clicking in completion. [Commit.](http://commits.kde.org/ktexteditor/ad69f43c6f65ac9afbb3c0bf7c88867550142528) 
+ Fix textInsertedRange signal for insertText behind last line. [Commit.](http://commits.kde.org/ktexteditor/5c4bc8d34f8c774350a90ba36961ed1a38b73fbc) Fixes bug [#483363](https://bugs.kde.org/483363)
{{< /details >}}

{{< details title="KTextTemplate" href="https://commits.kde.org/ktexttemplate" >}}
+ Enforce passing tests on all platforms. [Commit.](http://commits.kde.org/ktexttemplate/c927e326622964d1ed2e637cc13b125e0a9aedfe) 
{{< /details >}}

{{< details title="KUserFeedback" href="https://commits.kde.org/kuserfeedback" >}}
+ Fix logging category. [Commit.](http://commits.kde.org/kuserfeedback/9a2a7dd3b2d510ca963fd0e5e94e4f0e30406e2c) 
{{< /details >}}

{{< details title="KWallet" href="https://commits.kde.org/kwallet" >}}
+ Fix reply type in portal implementation. [Commit.](http://commits.kde.org/kwallet/def978402795e83593834646f87da8bcf2ff27c5) 
+ Kwalletportalsecrets.h: Add missing include. [Commit.](http://commits.kde.org/kwallet/0171c24def2fb7b643bbc402c9057ef5840638d7) 
+ Implement XDG Secrets Portal. [Commit.](http://commits.kde.org/kwallet/020427c8fb9d1dd6a41658131d1f80eee9171776) Fixes bug [#466197](https://bugs.kde.org/466197)
{{< /details >}}

{{< details title="KWidgetsAddons" href="https://commits.kde.org/kwidgetsaddons" >}}
+ Introduce KContextualHelpButton. [Commit.](http://commits.kde.org/kwidgetsaddons/367f035fc827082e1a3800f28b4879592684712a) 
+ KMessageWidget: Fix handling of palette changes. [Commit.](http://commits.kde.org/kwidgetsaddons/bffd3b79898b6cfd157d20de14bd921f205cfe73) 
+ KMessageWidget: Make sure icon label is always vertically centered. [Commit.](http://commits.kde.org/kwidgetsaddons/086892e51bab9eac04d3cdef2642027db46d1807) 
+ Gitignore: add VS Code dir. [Commit.](http://commits.kde.org/kwidgetsaddons/3dab2699f4bb7832a5ccb1a2b7d5901e09b7333a) 
{{< /details >}}

{{< details title="KWindowSystem" href="https://commits.kde.org/kwindowsystem" >}}
+ Port to QNativeInterface::Private::QWaylandWindow. [Commit.](http://commits.kde.org/kwindowsystem/05553f1ad5b1470c6df49da7ccaf49d411e7e09e) 
+ Remove an unused explicit moc include. [Commit.](http://commits.kde.org/kwindowsystem/1f8449f3f64c602b2acd8c6c4077c3ac89c8ffef) 
+ Add more explicit moc includes to sources for moc-covered headers. [Commit.](http://commits.kde.org/kwindowsystem/077f4169560167fca7b8166b148daf3b15b67a30) 
+ Introduce KXcbEvent to initialize the memory of sent XCB events. [Commit.](http://commits.kde.org/kwindowsystem/28c267286f5f950f669daeccfd71528c015dcdd9) 
{{< /details >}}

{{< details title="KXMLGUI" href="https://commits.kde.org/kxmlgui" >}}
+ Improve dbus disabling. [Commit.](http://commits.kde.org/kxmlgui/9275b9ecc384f23ae4c3afe0eea399dab5698f07) 
{{< /details >}}

{{< details title="Purpose" href="https://commits.kde.org/purpose" >}}
+ Add pre share hooks. [Commit.](http://commits.kde.org/purpose/a71ad5c17cf8731cf7159405042b03f04665ebae) 
+ Enforce passing tests on Windows. [Commit.](http://commits.kde.org/purpose/ad27425612c3b229738c7f4383da92549b50f7bf) 
{{< /details >}}

{{< details title="QQC2 Desktop Style" href="https://commits.kde.org/qqc2-desktop-style" >}}
+ TreeViewDelegate: Fix non-observable modelIndex property getting stuck. [Commit.](http://commits.kde.org/qqc2-desktop-style/a5591be7f4c559e41946fcf7f6713b1828598814) 
+ [CheckIndicator] Use control as AbstractButton. [Commit.](http://commits.kde.org/qqc2-desktop-style/0dc7b6a53b33884014e14c45bad0c90e4b0ee5f7) 
+ Add missing dependency to private module. [Commit.](http://commits.kde.org/qqc2-desktop-style/553ef599db25771a9ee2bbec19e6d59f7560b80d) 
+ Add QTBUG to comment. [Commit.](http://commits.kde.org/qqc2-desktop-style/044fd35c5f2cd488bd2a23fe7ac5b8f4f34f27d3) 
+ Make SwitchIndicator more compiler-friendly. [Commit.](http://commits.kde.org/qqc2-desktop-style/0ec6cee39c3a9909db86fc5ba25ea6f72fcb3971) 
+ ItemBranchIndicators: Fix uninitialized member variable m_selected. [Commit.](http://commits.kde.org/qqc2-desktop-style/9915117fb3afd12de88b5f68ae7915cbc7b3b8d6) 
+ StyleSingleton: Check whether object is qGuiApp. [Commit.](http://commits.kde.org/qqc2-desktop-style/e8d1d32b1fe2dd9ac5a709cb0f4a3293162397c9) 
+ [RadioButton] Use id instead of parent lookup. [Commit.](http://commits.kde.org/qqc2-desktop-style/dfb32d60caadbdb48201c00d176528859533f444) 
+ [TabButton] Fix property type. [Commit.](http://commits.kde.org/qqc2-desktop-style/eb73b2bb3b629e7c26e0db13359c4456700a2a4c) 
+ Apply Kirigami.Units.cornerRadius to default list item background too. [Commit.](http://commits.kde.org/qqc2-desktop-style/2563ff529b82ae16e0496140ada052cd496a42b6) 
+ Gitignore: add VS Code dir. [Commit.](http://commits.kde.org/qqc2-desktop-style/7aee34b59013e07ed7e2c2fbfa84cba66d6aa8f0) 
+ Use consistent radius value from Kirigami.Units.radius. [Commit.](http://commits.kde.org/qqc2-desktop-style/ac9258e748858362dfe5522db14dfd26dd81bba1) 
{{< /details >}}

{{< details title="Solid" href="https://commits.kde.org/solid" >}}
+ Remove too aggressive assert. [Commit.](http://commits.kde.org/solid/f099efda7a38e7f3651a802a44cd7e125b065dc1) Fixes bug [#464149](https://bugs.kde.org/464149)
+ [StorageAccess] Fix storageAccessFromPath returning unmounted filesystems. [Commit.](http://commits.kde.org/solid/f0ec9d388e59ec60303e66d57519f78e0e4fecb6) 
+ [SolidHwTest] Extend FakeHW description with encrypted volume, add tests. [Commit.](http://commits.kde.org/solid/0b56431c1f1949ad2c2f7552efee7aad28190d21) 
+ [SolidHwTest] Test Device::storageAccessFromPath. [Commit.](http://commits.kde.org/solid/03b4ce94ffacd0f6cbb3fe5b2e58b7c6b7a18e7a) 
+ [DeviceManager] Remove exists() check from storageAccessFromPath. [Commit.](http://commits.kde.org/solid/b67eaf92133bde674b43105ce4d3a01a8f1cf0bd) 
+ Udisks: Return empty string for "root" clearTextPath. [Commit.](http://commits.kde.org/solid/9ecaf388ccd41d2831ab3833c26627f72ab78f40) Fixes bug [#485507](https://bugs.kde.org/485507)
+ [SolidHwTest] Remove unnecessary slotPropertyChanged helper, fix bug. [Commit.](http://commits.kde.org/solid/59741508f6f27b520c1d4e557d0ffbce445e006f) 
+ [SolidHwTest] Remove setenv wrapper, unnecessary qt_windows.h include. [Commit.](http://commits.kde.org/solid/fcf44e6b30501848a67f2630b2f870f0b026722b) 
+ [SolidHwTest] Move test class declaration to source file. [Commit.](http://commits.kde.org/solid/f158663549e63f5744cfa997490314b6e69347a6) 
+ Udisks2: Add support CanCheck/Check/CanRepair/Repair. [Commit.](http://commits.kde.org/solid/71db7af2cf84f3c22032c4626192b8d80d4ae5ee) 
{{< /details >}}

{{< details title="Sonnet" href="https://commits.kde.org/sonnet" >}}
+ Add dependency to QML module. [Commit.](http://commits.kde.org/sonnet/e38702fb2a84cab2e2828c9d4ba4ee0010062409) 
+ Gitignore: add VS Code dir. [Commit.](http://commits.kde.org/sonnet/11bdb8f20b2836ac192ac6045064c2055e466872) 
{{< /details >}}

{{< details title="Syntax Highlighting" href="https://commits.kde.org/syntax-highlighting" >}}
+ Support single-quoted strings in MapCSS. [Commit.](http://commits.kde.org/syntax-highlighting/5a8eaad1e7ee5b6a7db0555076c901984d26a101) 
+ Add Syntax Highlighting for Vue Template Files. [Commit.](http://commits.kde.org/syntax-highlighting/3599a100c1e5efcbdd8d0bacb79e5feb8df2474c) 
+ Add syntax highlighting support for CashScript. [Commit.](http://commits.kde.org/syntax-highlighting/9f1cde41cf0bd2fd753fccf5d720a6723841b67a) 
{{< /details >}}

