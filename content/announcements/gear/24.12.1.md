---
date: 2025-01-09
appCount: 180
layout: gear
---
+ kasts: Retrieve duration from file as soon as it's downloaded ([Commit](http://commits.kde.org/kasts/e67b9ed3bbbfc52b5c835981cebbf4a2635d4d41)), fixes bug [#497448](https://bugs.kde.org/497448)
+ konversation:  Prevent crash when updating Watched Nicks ([Commit](http://commits.kde.org/konversation/5da1a32c95f24ea5743ff932f320a6cbc55beaab), fixes bug [#497799](https://bugs.kde.org/497799))
+ telly-skout:  Speed up program description update ([Commit](http://commits.kde.org/telly-skout/56fc3b93748ab23d67c6dfaca47a79e5bd9cfd7e), fixes bug [#497954](https://bugs.kde.org/497954))
