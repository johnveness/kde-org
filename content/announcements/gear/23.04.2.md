---
date: 2023-06-08
appCount: 120
image: true
layout: gear
---
+ konsole: Adjust selection point coords when scrollback shrinks ([Commit](http://commits.kde.org/konsole/af2f32c7af485d3fc96b13215435272e0d6adb7e), fixes bug [#470346](https://bugs.kde.org/470346))
+ neochat: Focus message search window's search field by default ([Commit](http://commits.kde.org/neochat/5a1a3fbd06ca485396aac493aa29629086411b98), fixes bug [#469879](https://bugs.kde.org/469879))
+ yakuake: Prevent unnecessary tab switching when closing a tab ([Commit](http://commits.kde.org/yakuake/baaac15a8f52d044dc6a712c96e2c68fc4209315), fixes bug [#392626](https://bugs.kde.org/392626))
