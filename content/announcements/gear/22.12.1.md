---
date: 2023-01-05
appCount: 120
image: true
layout: gear
---
+ Kitinerary now handles more NH booking confirmation variants ([Commit](http://commits.kde.org/kitinerary/1e320b05c685404553da18870245dda26a3262aa)) and supports international Trenitalia tickets ([Commit](http://commits.kde.org/kitinerary/c9d3a3ada9d376abbf5d83c2da0eec5014e17873)).
+ Konsole can save new profiles ([Commit](http://commits.kde.org/konsole/9566ed97d0d2e85f27790ab8f1d89ac0c9ad4373), fixes bug [#460383](https://bugs.kde.org/460383))
+ KDE PIM devs fixed the bug that crashed Kontact upon start ([Commit](http://commits.kde.org/kmail/9d37b837eca35bd7976709a9d25d3700c70e321e), fixes bug [#460747](https://bugs.kde.org/460747))
