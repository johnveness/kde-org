---
date: 2024-10-22
changelog: 6.2.1-6.2.2
layout: plasma
video: false
asBugfix: true
draft: false
---

+ KWin Backends/drm: leave all outputs disabled by default, including VR headsets. [Commit.](http://commits.kde.org/kwin/b7dbb2845bfe454efe75be8f7f1b1631f50be174) Fixes bug [#493148](https://bugs.kde.org/493148)
+ KWin Set WAYLAND_DISPLAY before starting wayland server. [Commit.](http://commits.kde.org/kwin/5275c31819c795f2095c211f87af9008c94d099f) 
+ Plasma Audio Volume Control: Fix text display for auto_null device. [Commit.](http://commits.kde.org/plasma-pa/ce9b240dc6e5efdb9f6b1a24ad4b38b2de87d15c) Fixes bug [#494324](https://bugs.kde.org/494324)
