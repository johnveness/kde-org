---
date: 2024-04-16
changelog: 6.0.3-6.0.4
layout: plasma
video: false
asBugfix: true
draft: false
---

+ Foldermodel: Export urls to the Desktop Portal on drag and copy. [Commit.](http://commits.kde.org/plasma-desktop/fb709dde5e97551da7448a6eb11d0c40678bd4f7)
+ System Monitor: Fix the column configuration dialog being too small on the overview page. [Commit.](http://commits.kde.org/plasma-systemmonitor/55dd85aa3fe81c6909e74ef644e12a731965501c) Fixes bug [#482008](https://bugs.kde.org/482008)
+ Applets/battery: Check actual battery for charge state workaround. [Commit.](http://commits.kde.org/plasma-workspace/2ea9868f9f1a798b6059ff6ad00ab39d6dd23941)
