---
date: 2024-03-06
changelog: 5.27.10-5.27.11
layout: plasma
video: false
asBugfix: true
draft: false
---

+ Plasma Browser Integration: 🍒🍒Download Job: Truncate excessively long URLs. [Commit.](http://commits.kde.org/plasma-browser-integration/84e738f1abb2dc1cc5f6d1541dc509951dd719b7) 
+ KWin: Tabbox: match Shift+Backtab against Shift+Tab. [Commit.](http://commits.kde.org/kwin/c8b7717953a28ac43f1fa5e69c9eceae4ea7e868) Fixes bug [#438991](https://bugs.kde.org/438991)
+ Powerdevil: Kbd backlight: Fix double brightness restore on LidOpen-resume. [Commit.](http://commits.kde.org/powerdevil/b30c584d6a724d350b7b8f6a30cc4bbbf9dbac3d) 
