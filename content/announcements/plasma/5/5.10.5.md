---
aliases:
- ../../plasma-5.10.5
changelog: 5.10.4-5.10.5
date: 2017-08-22
layout: plasma
youtube: VtdTC2Mh070
figure:
  src: /announcements/plasma/5/5.10.0/plasma-5.10.png
  class: text-center mt-4
asBugfix: true
---

- A Plasma crash when certain taskbar applications group in a specific way
- Excluding OSD's from the desktop grid kwin effect
- Discover handling URL links to packages
