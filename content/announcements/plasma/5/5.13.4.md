---
aliases:
- ../../plasma-5.13.4
changelog: 5.13.3-5.13.4
date: 2018-07-31
layout: plasma
youtube: C2kR1_n_d-g
figure:
  src: /announcements/plasma/5/5.13.0/plasma-5.13.png
  class: text-center mt-4
asBugfix: true
---

- Discover: When sorting by release date, show newer first. <a href="https://commits.kde.org/discover/b6a3d2bbf1a75bac6e48f5ef5e8ace8f770d535c">Commit.</a>
- Don't unintentionally change font rendering when rendering preview images. <a href="https://commits.kde.org/plasma-desktop/79a4bbc36cee399d71f3cfb05429939b0850db25">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D14480">D14480</a>
- Honor ghns KIOSK restriction in new KCMs. <a href="https://commits.kde.org/plasma-desktop/4e2a515bb34f6262e7d0c39c11ee35b6556a6146">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D14041">D14041</a>
