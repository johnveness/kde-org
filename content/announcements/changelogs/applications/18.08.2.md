---
aliases:
- ../../fulllog_applications-18.08.2
hidden: true
title: KDE Applications 18.08.2 Full Log Page
type: fulllog
version: 18.08.2
---

<h3><a name='akonadi' href='https://cgit.kde.org/akonadi.git'>akonadi</a> <a href='#akonadi' onclick='toggle("ulakonadi", this)'>[Hide]</a></h3>
<ul id='ulakonadi' style='display: block'>
<li>Job tracker: fix expected reply signature, improve error handling. <a href='http://commits.kde.org/akonadi/5ada337059fa8ca8a2a3ad097f4627d1ccd512ee'>Commit.</a> </li>
<li>Fix icon name. <a href='http://commits.kde.org/akonadi/def56f64c3324b0a7ac0c69210e3575374fe8072'>Commit.</a> </li>
<li>Fix "QCoreApplication::postEvent: Unexpected null receiver" warnings. <a href='http://commits.kde.org/akonadi/1a00b5a225cf04ac652a96770c4fb9470ef796c5'>Commit.</a> </li>
</ul>
<h3><a name='ark' href='https://cgit.kde.org/ark.git'>ark</a> <a href='#ark' onclick='toggle("ulark", this)'>[Hide]</a></h3>
<ul id='ulark' style='display: block'>
<li>Downgrade warning to debug. <a href='http://commits.kde.org/ark/27d71389921ffdf29c84234e22fe355e11b0f222'>Commit.</a> </li>
</ul>
<h3><a name='audiocd-kio' href='https://cgit.kde.org/audiocd-kio.git'>audiocd-kio</a> <a href='#audiocd-kio' onclick='toggle("ulaudiocd-kio", this)'>[Hide]</a></h3>
<ul id='ulaudiocd-kio' style='display: block'>
<li>Use the right version of kcmshell. <a href='http://commits.kde.org/audiocd-kio/9aa606bbc4c9012eb1f09fbc8c7db4194a81f18a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/398368'>#398368</a></li>
</ul>
<h3><a name='dolphin' href='https://cgit.kde.org/dolphin.git'>dolphin</a> <a href='#dolphin' onclick='toggle("uldolphin", this)'>[Hide]</a></h3>
<ul id='uldolphin' style='display: block'>
<li>[PlacesItemModelTest] More index-related fixes. <a href='http://commits.kde.org/dolphin/e2f93a28294fd0575cf8427f31a2fba4f8d6b099'>Commit.</a> </li>
<li>[PlacesItemModel] Fix testSystemItems(). <a href='http://commits.kde.org/dolphin/4a56d21fb1e98cd41e2e82a68ef3d23687c233e2'>Commit.</a> </li>
<li>[PlacesItemModelTest] Fix testHideItem(). <a href='http://commits.kde.org/dolphin/e99827293bea035b99e4f1e1881821ce378bd253'>Commit.</a> </li>
<li>[PlacesItemModelTest] Fix testTearDownDevice(). <a href='http://commits.kde.org/dolphin/26d02ecbdb8b95bff5371a4deb414b7076cc28b8'>Commit.</a> </li>
<li>[PlacesItemModelTest] Fix testDeletePlace(). <a href='http://commits.kde.org/dolphin/0abba76eea63d3939e9942c92a3377d401d3c9b5'>Commit.</a> </li>
<li>[PlacesItemModelTest] Fix testGroups() test case. <a href='http://commits.kde.org/dolphin/9204f3272bf7232df86d5da1824f7355c9c0b9c4'>Commit.</a> </li>
<li>Follow-up of commit 9760f9607d. <a href='http://commits.kde.org/dolphin/92157f6d7d1d6a2226ee3ae7db4bed2e50ee371e'>Commit.</a> </li>
<li>[PlacesItemModelTest] Check whether Desktop and Download folders exist. <a href='http://commits.kde.org/dolphin/9760f9607d48555452557f22749b291fd3981b76'>Commit.</a> </li>
<li>Fix disabling of DolphinNewFileMenu. <a href='http://commits.kde.org/dolphin/5e8b892a519b8d1fef58a747fafb5846b7fa3492'>Commit.</a> </li>
<li>Add test cases for enabled status of DolphinNewFileMenu. <a href='http://commits.kde.org/dolphin/dec7016c0d52129340a5b47f6de4df8af5115e21'>Commit.</a> </li>
<li>Don't assign twice the same key to the action New Tab. <a href='http://commits.kde.org/dolphin/5d180c5bb280f68cb98996044055e6891e29bab7'>Commit.</a> </li>
<li>Fix that dragging a file can trigger inline rename. <a href='http://commits.kde.org/dolphin/ddfc86ab8c207b81ec9ac83167d081b8d01c2750'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/398375'>#398375</a></li>
<li>Don't assign twice the same key to the action New Tab. <a href='http://commits.kde.org/dolphin/9991eb0f5eaf38aa7f0c61206937d4cd013e24c8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/398324'>#398324</a></li>
<li>Do use QIcon::Selected for non-icon view. <a href='http://commits.kde.org/dolphin/c5ce845e83d3d62ccf523d00ef7d414b0e2f819a'>Commit.</a> </li>
<li>[KStandardItemListWidget] Round to icon size before applying scaling. <a href='http://commits.kde.org/dolphin/1e7e4fad49e9a84113577abe98770055e9ed050a'>Commit.</a> </li>
<li>Correctly save behaviourOnLaunch. <a href='http://commits.kde.org/dolphin/5f252da8b58e03b623e56f788ff6d1056269c005'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/398022'>#398022</a></li>
<li>Fixed monochrome icons turning invisible when selected. <a href='http://commits.kde.org/dolphin/b734386cbc8436a239d20df31253a870c4e6c936'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/398014'>#398014</a></li>
</ul>
<h3><a name='ffmpegthumbs' href='https://cgit.kde.org/ffmpegthumbs.git'>ffmpegthumbs</a> <a href='#ffmpegthumbs' onclick='toggle("ulffmpegthumbs", this)'>[Hide]</a></h3>
<ul id='ulffmpegthumbs' style='display: block'>
<li>Fixed video thumbnailer crashing when trying to decode files without video stream. <a href='http://commits.kde.org/ffmpegthumbs/6fa26cddeff42831a6c6bf885bd8c905a413d3e4'>Commit.</a> </li>
</ul>
<h3><a name='gwenview' href='https://cgit.kde.org/gwenview.git'>gwenview</a> <a href='#gwenview' onclick='toggle("ulgwenview", this)'>[Hide]</a></h3>
<ul id='ulgwenview' style='display: block'>
<li>Fix orientation when saving/transforming JPEG images. <a href='http://commits.kde.org/gwenview/e6c60224defaa80db16636a799ac3ee99b1ae814'>Commit.</a> </li>
<li>Set file name on print job. <a href='http://commits.kde.org/gwenview/6e8d8facee8d31994351e5e8cd3f93f413838513'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/332207'>#332207</a></li>
</ul>
<h3><a name='juk' href='https://cgit.kde.org/juk.git'>juk</a> <a href='#juk' onclick='toggle("uljuk", this)'>[Hide]</a></h3>
<ul id='uljuk' style='display: block'>
<li>WebImageFetcher: properly destroy dialog after any request. <a href='http://commits.kde.org/juk/441cb735dd34c8b48c5af09a89b69564a49b7f1e'>Commit.</a> </li>
</ul>
<h3><a name='kalarm' href='https://cgit.kde.org/kalarm.git'>kalarm</a> <a href='#kalarm' onclick='toggle("ulkalarm", this)'>[Hide]</a></h3>
<ul id='ulkalarm' style='display: block'>
<li>Bug 398658: Fix Defer button being disabled for recurring alarms. <a href='http://commits.kde.org/kalarm/f16df03a593c59145f88c6c743ad1dd47e0201e4'>Commit.</a> </li>
</ul>
<h3><a name='kalarmcal' href='https://cgit.kde.org/kalarmcal.git'>kalarmcal</a> <a href='#kalarmcal' onclick='toggle("ulkalarmcal", this)'>[Hide]</a></h3>
<ul id='ulkalarmcal' style='display: block'>
<li>Add KADateTime::msecsTo(). <a href='http://commits.kde.org/kalarmcal/2692686754b53dcf86ab9cc525eb2fbda0ce5271'>Commit.</a> </li>
</ul>
<h3><a name='kcalc' href='https://cgit.kde.org/kcalc.git'>kcalc</a> <a href='#kcalc' onclick='toggle("ulkcalc", this)'>[Hide]</a></h3>
<ul id='ulkcalc' style='display: block'>
<li>Allow shortcut keys for both decimal separators point and comma. <a href='http://commits.kde.org/kcalc/f00e48b6a50fccba2e3a43d06752e2043fd20633'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/357824'>#357824</a></li>
</ul>
<h3><a name='kdenetwork-filesharing' href='https://cgit.kde.org/kdenetwork-filesharing.git'>kdenetwork-filesharing</a> <a href='#kdenetwork-filesharing' onclick='toggle("ulkdenetwork-filesharing", this)'>[Hide]</a></h3>
<ul id='ulkdenetwork-filesharing' style='display: block'>
<li>Allow building with packagekit-qt >= 1.0.0. <a href='http://commits.kde.org/kdenetwork-filesharing/3394a564ec62a40ed99c185324993205f9a72641'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/390599'>#390599</a></li>
</ul>
<h3><a name='kdenlive' href='https://cgit.kde.org/kdenlive.git'>kdenlive</a> <a href='#kdenlive' onclick='toggle("ulkdenlive", this)'>[Hide]</a></h3>
<ul id='ulkdenlive' style='display: block'>
<li>Improve missing luma detection (region transition) - fixes project crash. <a href='http://commits.kde.org/kdenlive/637d2115ac4503910d17825beab9407241015e1b'>Commit.</a> </li>
<li>Home/End now also seek in clip monitor. <a href='http://commits.kde.org/kdenlive/72c6a1715a9b88ae08c879f85e7a3f12f0b8bdd7'>Commit.</a> </li>
<li>Update AppData for upcoming release. <a href='http://commits.kde.org/kdenlive/17074d01a18cbb1bbeca3b68f820bd4bbdb72908'>Commit.</a> </li>
<li>Windows crash reports. <a href='http://commits.kde.org/kdenlive/51d34999ee1f6d90c0aaa5dd96f1e3f5c5b893ab'>Commit.</a> </li>
<li>Update authors. <a href='http://commits.kde.org/kdenlive/294cc23d842fcc948825e0c53e9b5b5c95423be0'>Commit.</a> </li>
<li>Fix double clicking a title clip in bin tries to rename instead of opening the title dialog. <a href='http://commits.kde.org/kdenlive/3148e662639e2845492bbcdff409ce998fce1767'>Commit.</a> </li>
</ul>
<h3><a name='kdepim-apps-libs' href='https://cgit.kde.org/kdepim-apps-libs.git'>kdepim-apps-libs</a> <a href='#kdepim-apps-libs' onclick='toggle("ulkdepim-apps-libs", this)'>[Hide]</a></h3>
<ul id='ulkdepim-apps-libs' style='display: block'>
<li>Fix Bug 399074 - Untranslatable strings in kdepim-apps-libs/kaddressbookimportexport/src/. <a href='http://commits.kde.org/kdepim-apps-libs/a41660ed8b5968e2b4edb26afef97dddc68f1234'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/399074'>#399074</a></li>
</ul>
<h3><a name='kdepim-runtime' href='https://cgit.kde.org/kdepim-runtime.git'>kdepim-runtime</a> <a href='#kdepim-runtime' onclick='toggle("ulkdepim-runtime", this)'>[Hide]</a></h3>
<ul id='ulkdepim-runtime' style='display: block'>
<li>Remove not necessary header when we send mail. <a href='http://commits.kde.org/kdepim-runtime/4a457479d4995b5e3279a7e096f891f2b7b79a08'>Commit.</a> </li>
<li>Maildispatcher: escape '<' and '>' in error messages. <a href='http://commits.kde.org/kdepim-runtime/1400d9ecd2fbc14122e16adf59e3fc231cbcc44f'>Commit.</a> </li>
<li>Maildispatcher: escape '<' and '>' in error messages. <a href='http://commits.kde.org/kdepim-runtime/305e12796bfed141090b16163abe57371cc40a9e'>Commit.</a> </li>
<li>Fix compilation with Qt 5.12. <a href='http://commits.kde.org/kdepim-runtime/5a95a7d084ccd8d5d863f51c415160a99212e5fd'>Commit.</a> </li>
</ul>
<h3><a name='kgoldrunner' href='https://cgit.kde.org/kgoldrunner.git'>kgoldrunner</a> <a href='#kgoldrunner' onclick='toggle("ulkgoldrunner", this)'>[Hide]</a></h3>
<ul id='ulkgoldrunner' style='display: block'>
<li>Drop unused dependencies, add implicit. <a href='http://commits.kde.org/kgoldrunner/b99310d021db27667ef8ec4cecb986ec7fa47f1b'>Commit.</a> </li>
</ul>
<h3><a name='kgpg' href='https://cgit.kde.org/kgpg.git'>kgpg</a> <a href='#kgpg' onclick='toggle("ulkgpg", this)'>[Hide]</a></h3>
<ul id='ulkgpg' style='display: block'>
<li>Add .editorconfig. <a href='http://commits.kde.org/kgpg/455f295ebcf2213e742ee7e10e96fe6eec34810c'>Commit.</a> </li>
<li>Add .arcconfig. <a href='http://commits.kde.org/kgpg/eb5ab0a8953374c1b7c45631a7cc76b6c693c542'>Commit.</a> </li>
<li>Use QTEST_GUILESS_MAIN. <a href='http://commits.kde.org/kgpg/39cf5dc730e41e62294a91471d6211a3345545d8'>Commit.</a> </li>
</ul>
<h3><a name='kidentitymanagement' href='https://cgit.kde.org/kidentitymanagement.git'>kidentitymanagement</a> <a href='#kidentitymanagement' onclick='toggle("ulkidentitymanagement", this)'>[Hide]</a></h3>
<ul id='ulkidentitymanagement' style='display: block'>
<li>Add signal when identity is not found. <a href='http://commits.kde.org/kidentitymanagement/48fd7b2ccaeda93c120064349ba87022e39a0a25'>Commit.</a> </li>
</ul>
<h3><a name='kldap' href='https://cgit.kde.org/kldap.git'>kldap</a> <a href='#kldap' onclick='toggle("ulkldap", this)'>[Hide]</a></h3>
<ul id='ulkldap' style='display: block'>
<li>Fix "finished() called after error()!" warning. <a href='http://commits.kde.org/kldap/525de77cd56c2e0d6228fc7dd321673356168698'>Commit.</a> </li>
</ul>
<h3><a name='klettres' href='https://cgit.kde.org/klettres.git'>klettres</a> <a href='#klettres' onclick='toggle("ulklettres", this)'>[Hide]</a></h3>
<ul id='ulklettres' style='display: block'>
<li>Mark phonon as required. <a href='http://commits.kde.org/klettres/7fe3cd3c6654f350836d6d2d7c374508ed5cbd2c'>Commit.</a> </li>
</ul>
<h3><a name='klickety' href='https://cgit.kde.org/klickety.git'>klickety</a> <a href='#klickety' onclick='toggle("ulklickety", this)'>[Hide]</a></h3>
<ul id='ulklickety' style='display: block'>
<li>Install the appdata files and fix few metadata. <a href='http://commits.kde.org/klickety/d1fbba470ff19f8b86f1bd7d921ff9df9182ca8c'>Commit.</a> </li>
</ul>
<h3><a name='kmail' href='https://cgit.kde.org/kmail.git'>kmail</a> <a href='#kmail' onclick='toggle("ulkmail", this)'>[Hide]</a></h3>
<ul id='ulkmail' style='display: block'>
<li>Don't show quota page when quota max < 0. <a href='http://commits.kde.org/kmail/f369e1f07bb5dc5c76162fa4d84cd10fa5d39f09'>Commit.</a> </li>
<li>Allow to restore with mailtransportname and identity name. <a href='http://commits.kde.org/kmail/23735c196825fab47f2f667c24f8ef7bb0eb3c28'>Commit.</a> </li>
<li>Initialize with identity. <a href='http://commits.kde.org/kmail/ab02e8822bad9e7e1693b8e6a65b9d5d3e4c6318'>Commit.</a> </li>
<li>Emit signal when identity is not found. <a href='http://commits.kde.org/kmail/8fdb8bc41fa3afb715e8555c471704f51ef5ed2a'>Commit.</a> </li>
<li>Fix use identity, it will select correct send folder now. <a href='http://commits.kde.org/kmail/916d3b81cd40856280c0c90c211f269dc18303fd'>Commit.</a> </li>
<li>Fix Bug 397937 - Kmail wrong trash icon. <a href='http://commits.kde.org/kmail/70158417737439afa2a2c026b311fccfb0d7f145'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/397937'>#397937</a></li>
</ul>
<h3><a name='kmailtransport' href='https://cgit.kde.org/kmailtransport.git'>kmailtransport</a> <a href='#kmailtransport' onclick='toggle("ulkmailtransport", this)'>[Hide]</a></h3>
<ul id='ulkmailtransport' style='display: block'>
<li>When instance is invalide return directly. <a href='http://commits.kde.org/kmailtransport/91f75ba639f6031e4269a4cd69dfe4cfdcdc15d1'>Commit.</a> </li>
</ul>
<h3><a name='kolf' href='https://cgit.kde.org/kolf.git'>kolf</a> <a href='#kolf' onclick='toggle("ulkolf", this)'>[Hide]</a></h3>
<ul id='ulkolf' style='display: block'>
<li>Fix dependencies: add implicit, drop unused. <a href='http://commits.kde.org/kolf/4c890e43ab57d59aae3024b4765280ed50ec952f'>Commit.</a> </li>
</ul>
<h3><a name='kontactinterface' href='https://cgit.kde.org/kontactinterface.git'>kontactinterface</a> <a href='#kontactinterface' onclick='toggle("ulkontactinterface", this)'>[Hide]</a></h3>
<ul id='ulkontactinterface' style='display: block'>
<li>Disable Chromium's crash handler. <a href='http://commits.kde.org/kontactinterface/62f97d9aca770c59514d1641c591b8de5542c49f'>Commit.</a> </li>
</ul>
<h3><a name='kpat' href='https://cgit.kde.org/kpat.git'>kpat</a> <a href='#kpat' onclick='toggle("ulkpat", this)'>[Hide]</a></h3>
<ul id='ulkpat' style='display: block'>
<li>Reduce wasted space for Klondike and Grandfather. <a href='http://commits.kde.org/kpat/eb5fa8dab3559bf008829154c4c17239af607da6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/307625'>#307625</a>. Fixes bug <a href='https://bugs.kde.org/378914'>#378914</a></li>
</ul>
<h3><a name='kpimtextedit' href='https://cgit.kde.org/kpimtextedit.git'>kpimtextedit</a> <a href='#kpimtextedit' onclick='toggle("ulkpimtextedit", this)'>[Hide]</a></h3>
<ul id='ulkpimtextedit' style='display: block'>
<li>Don't install this header. <a href='http://commits.kde.org/kpimtextedit/460e7fdcf30b30bb40be8cf66e5a5a7b6594b564'>Commit.</a> </li>
</ul>
<h3><a name='libkdegames' href='https://cgit.kde.org/libkdegames.git'>libkdegames</a> <a href='#libkdegames' onclick='toggle("ullibkdegames", this)'>[Hide]</a></h3>
<ul id='ullibkdegames' style='display: block'>
<li>Fix visual glitch in ace of clubs card in the Paris card deck. <a href='http://commits.kde.org/libkdegames/4650a53a865c07b7e634b9f3e86922b481ea3e6a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/158390'>#158390</a></li>
</ul>
<h3><a name='libkgapi' href='https://cgit.kde.org/libkgapi.git'>libkgapi</a> <a href='#libkgapi' onclick='toggle("ullibkgapi", this)'>[Hide]</a></h3>
<ul id='ullibkgapi' style='display: block'>
<li>Fix compilation with Qt 5.12. <a href='http://commits.kde.org/libkgapi/bcec77777463564d44b2d0bae30059fb8ec37c02'>Commit.</a> </li>
</ul>
<h3><a name='lokalize' href='https://cgit.kde.org/lokalize.git'>lokalize</a> <a href='#lokalize' onclick='toggle("ullokalize", this)'>[Hide]</a></h3>
<ul id='ullokalize' style='display: block'>
<li>Demote some warnings to debug messages. <a href='http://commits.kde.org/lokalize/07ad5894349feadf721e7754a27d4bf7fe4b0c2a'>Commit.</a> </li>
</ul>
<h3><a name='lskat' href='https://cgit.kde.org/lskat.git'>lskat</a> <a href='#lskat' onclick='toggle("ullskat", this)'>[Hide]</a></h3>
<ul id='ullskat' style='display: block'>
<li>Drop unused dependencies. <a href='http://commits.kde.org/lskat/e3ca875e15e441878759ca3fb76924299d93e501'>Commit.</a> </li>
</ul>
<h3><a name='marble' href='https://cgit.kde.org/marble.git'>marble</a> <a href='#marble' onclick='toggle("ulmarble", this)'>[Hide]</a></h3>
<ul id='ulmarble' style='display: block'>
<li>BUG: 394517. <a href='http://commits.kde.org/marble/25d873a1f6cb167105da7bf8824f79794982b3ba'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/394517'>#394517</a></li>
</ul>
<h3><a name='messagelib' href='https://cgit.kde.org/messagelib.git'>messagelib</a> <a href='#messagelib' onclick='toggle("ulmessagelib", this)'>[Hide]</a></h3>
<ul id='ulmessagelib' style='display: block'>
<li>Relicense my code from GPLv2 to GPLv2+. <a href='http://commits.kde.org/messagelib/4c360329969b63e2f2376d388669bb5c7b7c659d'>Commit.</a> </li>
<li>Define color for openpgp signature. <a href='http://commits.kde.org/messagelib/3125a19c250817e1e545dcb86dbdea8d52ca383c'>Commit.</a> </li>
<li>Store transport name and identity name. <a href='http://commits.kde.org/messagelib/8160119895a279788d315cf39425c857a79fccba'>Commit.</a> </li>
<li>Don't save X-KMail-Fcc it's redundant. <a href='http://commits.kde.org/messagelib/4ffcc577916b9780a43687b26dbd034ad7d505b2'>Commit.</a> </li>
<li>We need theses header when we resend file. <a href='http://commits.kde.org/messagelib/fab5f74cea7d1888bb784bb4e842f91bbdf9c9c7'>Commit.</a> </li>
</ul>
<h3><a name='okular' href='https://cgit.kde.org/okular.git'>okular</a> <a href='#okular' onclick='toggle("ulokular", this)'>[Hide]</a></h3>
<ul id='ulokular' style='display: block'>
<li>Fine tune opening of archives after last change. <a href='http://commits.kde.org/okular/ee5b830ced19077b859a0de96a08e51f1b97d8c2'>Commit.</a> </li>
<li>Fine tune opening files. <a href='http://commits.kde.org/okular/caf52d0ed92fd1a6ae85a73691269f2e8575bda8'>Commit.</a> </li>
<li>Avoid undefined behavior due to dangling file descriptor. <a href='http://commits.kde.org/okular/fa7a1b3d0674b234a61ac784dd879ad705a2f515'>Commit.</a> </li>
</ul>
<h3><a name='parley' href='https://cgit.kde.org/parley.git'>parley</a> <a href='#parley' onclick='toggle("ulparley", this)'>[Hide]</a></h3>
<ul id='ulparley' style='display: block'>
<li>Fix kns category name in kde-look. <a href='http://commits.kde.org/parley/bd9a57224eb953298f0f60e5bbabf8e4b765eb9d'>Commit.</a> </li>
</ul>
<h3><a name='pim-data-exporter' href='https://cgit.kde.org/pim-data-exporter.git'>pim-data-exporter</a> <a href='#pim-data-exporter' onclick='toggle("ulpim-data-exporter", this)'>[Hide]</a></h3>
<ul id='ulpim-data-exporter' style='display: block'>
<li>Fix encryption type. <a href='http://commits.kde.org/pim-data-exporter/787744a03c424bb21caf76639564f2ffbd908149'>Commit.</a> </li>
<li>Add warning. <a href='http://commits.kde.org/pim-data-exporter/886a5ec8ffdcc20fb8e78e5a899b1614ccc97592'>Commit.</a> </li>
<li>Add support for import sendmail/akonadi mailtransport. <a href='http://commits.kde.org/pim-data-exporter/0ec012de3f80f69dce748934aba711eb2fa03640'>Commit.</a> </li>
<li>Start to implement sendmail/akonadi support. <a href='http://commits.kde.org/pim-data-exporter/019ca1f2b2e4802e0501f33b1c77eed2ef1620d3'>Commit.</a> </li>
<li>Add warning when id is not defined. <a href='http://commits.kde.org/pim-data-exporter/272f1e316e1272abeb5f529c271ddad1f83916a9'>Commit.</a> </li>
</ul>
<h3><a name='umbrello' href='https://cgit.kde.org/umbrello.git'>umbrello</a> <a href='#umbrello' onclick='toggle("ulumbrello", this)'>[Hide]</a></h3>
<ul id='ulumbrello' style='display: block'>
<li>Implementation of dragging multiple entries from the tree view and inserting them into the diagram. <a href='http://commits.kde.org/umbrello/0c9b1b14120bbaa56b1dc7e5c95e861032e64dec'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/394606'>#394606</a></li>
<li>Fix 'Multiple creation of the same entity on an entity relationship diagram'. <a href='http://commits.kde.org/umbrello/76349a80c2074a2403474dfb44109c1149fb934d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/399475'>#399475</a></li>
<li>Fix 'changes on "entitätattribut" changes the attributes also on an copy'. <a href='http://commits.kde.org/umbrello/52c92044191f291a8503dd2c618381b80e010d92'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/158647'>#158647</a></li>
<li>Refactored UMLDragData::decodeClip4() to use better variable names. <a href='http://commits.kde.org/umbrello/36e8d4cabefd722ccccd3556168d0c629b3b8c13'>Commit.</a> See bug <a href='https://bugs.kde.org/158647'>#158647</a></li>
</ul>