---
aliases:
- ../../fulllog_applications-15.12.2
hidden: true
title: KDE Applications 15.12.2 Full Log Page
type: fulllog
version: 15.12.2
---

<h3><a name='akonadi' href='https://cgit.kde.org/akonadi.git'>akonadi</a> <a href='#akonadi' onclick='toggle("ulakonadi", this)'>[Show]</a></h3>
<ul id='ulakonadi' style='display: none'>
<li>Fix encoding of reading *.desktop files of Akonadi agents. <a href='http://commits.kde.org/akonadi/de588dc7fd459449ccc1d29fb30cee1774837140'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/358658'>#358658</a>. Code review <a href='https://git.reviewboard.kde.org/r/126911'>#126911</a></li>
</ul>
<h3><a name='ark' href='https://cgit.kde.org/ark.git'>ark</a> <a href='#ark' onclick='toggle("ulark", this)'>[Show]</a></h3>
<ul id='ulark' style='display: none'>
<li>Disable Save As action if no archive is open. <a href='http://commits.kde.org/ark/be4ab34e57ac1cdf5896a9c2cee5b0dd8eb40f65'>Commit.</a> </li>
<li>Properly finish AddToArchive jobs. <a href='http://commits.kde.org/ark/302f86db350aa270094f8db73c3eda8451fc2811'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/359147'>#359147</a></li>
<li>Always show the archive's name in window title. <a href='http://commits.kde.org/ark/c9de2b68460e7fbd7de34fafcd2197f83b72802b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/358830'>#358830</a></li>
<li>Fix opening of ISO files detected as text/plain. <a href='http://commits.kde.org/ark/1587a9f96ba7923505465ad598c1f053dbc2bdf2'>Commit.</a> See bug <a href='https://bugs.kde.org/354344'>#354344</a></li>
<li>Disable open-file actions if there are multiple selections. <a href='http://commits.kde.org/ark/a0a5270167d40fa7d04990790e05ef6834788dbc'>Commit.</a> </li>
</ul>
<h3><a name='dolphin' href='https://cgit.kde.org/dolphin.git'>dolphin</a> <a href='#dolphin' onclick='toggle("uldolphin", this)'>[Show]</a></h3>
<ul id='uldolphin' style='display: none'>
<li>Fix check whether window is closed by session management. <a href='http://commits.kde.org/dolphin/bf1dbee1d6a709983d9f5034c61e2f005fdc6aee'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/353548'>#353548</a>. Code review <a href='https://git.reviewboard.kde.org/r/126917'>#126917</a></li>
<li>Do not delete sub menus of the control menu explicitly. <a href='http://commits.kde.org/dolphin/ddc050f23596493e8debd2dfd523fd572c098d63'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/354558'>#354558</a>. Code review <a href='https://git.reviewboard.kde.org/r/126693'>#126693</a></li>
</ul>
<h3><a name='dragon' href='https://cgit.kde.org/dragon.git'>dragon</a> <a href='#dragon' onclick='toggle("uldragon", this)'>[Show]</a></h3>
<ul id='uldragon' style='display: none'>
<li>Bump Qt minimum to 5.4. <a href='http://commits.kde.org/dragon/ceb7aaf22f3c96b67405acfeb353e286214b8069'>Commit.</a> </li>
<li>When parsing user input urls, default to assume local files. <a href='http://commits.kde.org/dragon/ee333a83457450df771b458c299dce43a5500a35'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/357795'>#357795</a></li>
</ul>
<h3><a name='filelight' href='https://cgit.kde.org/filelight.git'>filelight</a> <a href='#filelight' onclick='toggle("ulfilelight", this)'>[Show]</a></h3>
<ul id='ulfilelight' style='display: none'>
<li>Explicitly construct QUrl for new (local) skipList entries. <a href='http://commits.kde.org/filelight/95c04b7ee437e89ca7d2e6aa409b43d314ae2106'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126985'>#126985</a></li>
<li>Do not use i18n().arg() but i18n(..., args). <a href='http://commits.kde.org/filelight/350e0043ef666de065159f4d403553e0fe476fcd'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126978'>#126978</a></li>
<li>Call KLocalizedString::setApplicationDomain. <a href='http://commits.kde.org/filelight/a811282ba3b9abfb079baaacc0e8a630d5549414'>Commit.</a> </li>
</ul>
<h3><a name='granatier' href='https://cgit.kde.org/granatier.git'>granatier</a> <a href='#granatier' onclick='toggle("ulgranatier", this)'>[Show]</a></h3>
<ul id='ulgranatier' style='display: none'>
<li>Fix occasional crash when starting new game. <a href='http://commits.kde.org/granatier/437c44dfc55276280b7dcda16a59f5196259ed7b'>Commit.</a> </li>
</ul>
<h3><a name='kalarmcal' href='https://cgit.kde.org/kalarmcal.git'>kalarmcal</a> <a href='#kalarmcal' onclick='toggle("ulkalarmcal", this)'>[Show]</a></h3>
<ul id='ulkalarmcal' style='display: none'>
<li>Bug 346060: fix deferral time of date-only recurring alarms. <a href='http://commits.kde.org/kalarmcal/fd5960c66c650ef42207e83b44e80e6e243474dd'>Commit.</a> </li>
</ul>
<h3><a name='kalgebra' href='https://cgit.kde.org/kalgebra.git'>kalgebra</a> <a href='#kalgebra' onclick='toggle("ulkalgebra", this)'>[Show]</a></h3>
<ul id='ulkalgebra' style='display: none'>
<li>Backport setting KAboutData from master. <a href='http://commits.kde.org/kalgebra/9ca540b667931ba7e84cf6f6a0435a2c6eceb4b9'>Commit.</a> </li>
</ul>
<h3><a name='kapman' href='https://cgit.kde.org/kapman.git'>kapman</a> <a href='#kapman' onclick='toggle("ulkapman", this)'>[Show]</a></h3>
<ul id='ulkapman' style='display: none'>
<li>Fix mummies game layout. <a href='http://commits.kde.org/kapman/7fa638d43ee9bd1caa2d4a4a2266901cd4816944'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/359048'>#359048</a></li>
</ul>
<h3><a name='kate' href='https://cgit.kde.org/kate.git'>kate</a> <a href='#kate' onclick='toggle("ulkate", this)'>[Show]</a></h3>
<ul id='ulkate' style='display: none'>
<li>S&R: Fix eternal loop on unexpected regular expression. <a href='http://commits.kde.org/kate/9b3f722ba514097aa4166c04f7a7167209c81584'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/358915'>#358915</a></li>
</ul>
<h3><a name='kbounce' href='https://cgit.kde.org/kbounce.git'>kbounce</a> <a href='#kbounce' onclick='toggle("ulkbounce", this)'>[Show]</a></h3>
<ul id='ulkbounce' style='display: none'>
<li>Emit the signal from a more reasonable place. <a href='http://commits.kde.org/kbounce/56b5fc1f4595efec7afcd1497bb0fafb33c133fc'>Commit.</a> </li>
<li>Set time to 0 if timeout occurred. <a href='http://commits.kde.org/kbounce/7bb125da1e324fc60cd3e6b445fd51ce068b0c7b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/357773'>#357773</a></li>
</ul>
<h3><a name='kcalcore' href='https://cgit.kde.org/kcalcore.git'>kcalcore</a> <a href='#kcalcore' onclick='toggle("ulkcalcore", this)'>[Show]</a></h3>
<ul id='ulkcalcore' style='display: none'>
<li>Increase version. <a href='http://commits.kde.org/kcalcore/10c83a30b4ba19f9288789b6e72fbf614a7314a9'>Commit.</a> </li>
<li>This patch breaks serialize/deserialize todo element (bug found by Kevin). <a href='http://commits.kde.org/kcalcore/c598e23f3a2b42df0bf168b1b423f801c8a28052'>Commit.</a> </li>
</ul>
<h3><a name='kdelibs' href='https://cgit.kde.org/kdelibs.git'>kdelibs</a> <a href='#kdelibs' onclick='toggle("ulkdelibs", this)'>[Show]</a></h3>
<ul id='ulkdelibs' style='display: none'>
<li>Fix detection of icon link type (aka "favicon"). <a href='http://commits.kde.org/kdelibs/022f1248f9b4db51d4d23a109568e24ef5481f61'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/193825'>#193825</a></li>
<li>Fix KDirListerCache crash with two listers for an empty dir in the cache. <a href='http://commits.kde.org/kdelibs/725523993e6037b94a99348fc3879a4070c3dc7d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/278431'>#278431</a></li>
<li>Fix KDirNotify signal emission in kio_file to send full URLs rather than local paths. <a href='http://commits.kde.org/kdelibs/32dc425f4ca616fef7a5f193bf79d38f0465f8bf'>Commit.</a> </li>
</ul>
<h3><a name='kdenlive' href='https://cgit.kde.org/kdenlive.git'>kdenlive</a> <a href='#kdenlive' onclick='toggle("ulkdenlive", this)'>[Show]</a></h3>
<ul id='ulkdenlive' style='display: none'>
<li>Fix old keyframes doubling effect on nodes. <a href='http://commits.kde.org/kdenlive/9838f0597d6f1092c95f79755741d6b7939783b3'>Commit.</a> </li>
<li>Fix custom effects deletion. <a href='http://commits.kde.org/kdenlive/47567f07dcfd0f68e0ec9ed31803dea250add203'>Commit.</a> See bug <a href='https://bugs.kde.org/358782'>#358782</a></li>
<li>Fix transition list search widget broken. <a href='http://commits.kde.org/kdenlive/233cad6ee3a0c83ccfd919516c4e332970329310'>Commit.</a> See bug <a href='https://bugs.kde.org/358449'>#358449</a></li>
<li>Stem export is always stem export also if only one track is selected. <a href='http://commits.kde.org/kdenlive/dc11bf42f826988b8787fdbca7a2ce76a160a465'>Commit.</a> </li>
<li>Fix titles from 0.9.x kdenlive versions broken on locales with comma separator. <a href='http://commits.kde.org/kdenlive/a98aac413302602505f632e895ea682e0f903c5f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/358033'>#358033</a></li>
<li>Follow Desktop style unless it is GTK+. <a href='http://commits.kde.org/kdenlive/e1bdc57f12fc193523ec656f46ae5cc1118044f0'>Commit.</a> See bug <a href='https://bugs.kde.org/357893'>#357893</a></li>
<li>Fix track effects cannot be disabled / deleted. <a href='http://commits.kde.org/kdenlive/8a5e4c3c7bda32a6d8dbfa074e5d39cfc3a0f0b5'>Commit.</a> See bug <a href='https://bugs.kde.org/357856'>#357856</a></li>
<li>Fix opacity parameter broken in composite transition/pan+zoom. <a href='http://commits.kde.org/kdenlive/efede114b6823bdf1603522cd52fd1d26906a4f2'>Commit.</a> See bug <a href='https://bugs.kde.org/357856'>#357856</a></li>
<li>Obsolete reference to trackInfoList removed. <a href='http://commits.kde.org/kdenlive/183681c3fd23179f7e57a294b7ec31774f58f55d'>Commit.</a> </li>
<li>Backport broken stem export fix. <a href='http://commits.kde.org/kdenlive/5346a545a2c2ace79766dfc2cf958638885251d6'>Commit.</a> </li>
</ul>
<h3><a name='kdepim' href='https://cgit.kde.org/kdepim.git'>kdepim</a> <a href='#kdepim' onclick='toggle("ulkdepim", this)'>[Show]</a></h3>
<ul id='ulkdepim' style='display: none'>
<li>Don't insert file when we canceled select file. <a href='http://commits.kde.org/kdepim/d8c05d0db1cc90faacbe7ee211a44fbfd5271e0c'>Commit.</a> </li>
<li>Align elements. <a href='http://commits.kde.org/kdepim/84488fad936729485ebdbaa2e1100d1d5c954006'>Commit.</a> </li>
<li>Fix Bug 359086 - Untranslatable string in Export Log File dialog. <a href='http://commits.kde.org/kdepim/9a9e8d4837ded18dcfa85e1d65d81447d10884cf'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/359086'>#359086</a></li>
<li>Fix [kmail2] [Bug 357557] New: Save attachment: Dialog does not show correct folder. <a href='http://commits.kde.org/kdepim/43c23ea9a360f1aafe3229975774464569d27ed2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/357557'>#357557</a></li>
<li>Fix Bug 358234 - Message view - Big icon in message with PDF attachment. <a href='http://commits.kde.org/kdepim/0151a72890d969b01734d3ddae1e7337cf471b25'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/358234'>#358234</a></li>
<li>Fix Bug 359018 - Pressing cancel does not undo changes made to "Empty local wastebin..." kmail2 v 5.1.1. <a href='http://commits.kde.org/kdepim/e6af484bdc1c3cc30e94fc93d8608fb1f756d710'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/359018'>#359018</a></li>
<li>Disable translate plugins. <a href='http://commits.kde.org/kdepim/0ba446aeb6436d2e4845f9689331ce7c201610c0'>Commit.</a> </li>
<li>Fix frequency edit field missing from recurrence editor. <a href='http://commits.kde.org/kdepim/6ee2c3baf980dd891a2717c69ab8c4172ff6ad77'>Commit.</a> </li>
<li>Remove double margin. <a href='http://commits.kde.org/kdepim/99e020af160cd9476062a0c7efe77906800415aa'>Commit.</a> </li>
<li>Update list each hour. <a href='http://commits.kde.org/kdepim/29bb4d4f6d17104102d7b227fe11e8c74dc5fe36'>Commit.</a> </li>
<li>Update. <a href='http://commits.kde.org/kdepim/19b1a4fcf706c08bdda445016ca8bc34f0658bfa'>Commit.</a> </li>
<li>Fix Bug 358840 - kmail composer: menu item "send" missing. <a href='http://commits.kde.org/kdepim/9e3989b0c37e3b869e9cf77f0d8339f98c80993b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/358840'>#358840</a></li>
<li>Fix Bug 358845 - Incorrect alignment of string with drop-down list. <a href='http://commits.kde.org/kdepim/a037495f3ed3791a5f6b1f6bfcabc75991de8891'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/358845'>#358845</a></li>
<li>Fix Bug 358802 - KMail settings - key expire warning doesn't enable apply button. <a href='http://commits.kde.org/kdepim/e9d1baf2595bab9976b483e1f0719bc6c1818f78'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/358802'>#358802</a></li>
<li>Bug 357018: Fix reminder time edit field being covered up. <a href='http://commits.kde.org/kdepim/a5c6b6dd5fb2b58676dcf18213510a1778dbc6de'>Commit.</a> </li>
<li>Fix specification on command line of a reminder after the alarm. <a href='http://commits.kde.org/kdepim/9678b1c491dc55f4fd31c13ed85ad62d8c790e1d'>Commit.</a> </li>
<li>Output function names in debug output; tidy. <a href='http://commits.kde.org/kdepim/7e8da39741d7c8f57ffe2a876f6ea23d6b7b6591'>Commit.</a> </li>
<li>Fix Bug 358568 - Kontact crash on closing Kontact. <a href='http://commits.kde.org/kdepim/f620805ebb9bb5a334a99e32c1628091f15c557e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/358568'>#358568</a></li>
<li>Fix crash when we restore from crashed file. <a href='http://commits.kde.org/kdepim/bb09ccb1a7319f744c2c0e39f86899babb6a8cf3'>Commit.</a> </li>
<li>Bug 358217: fix occasional crash on startup. <a href='http://commits.kde.org/kdepim/4c8ecf34c1d9432b692d4ead41fc4b879aa51b4d'>Commit.</a> </li>
<li>Bug 356048: fix crash on editing an alarm if spellcheck enabled. <a href='http://commits.kde.org/kdepim/6af5386719f01cc3a71f9486807c66b1b3da26d1'>Commit.</a> </li>
<li>Fix show items. <a href='http://commits.kde.org/kdepim/642dd9d4851acf3a0a96c946f81cc3fee353850e'>Commit.</a> </li>
<li>Fix tooltip. <a href='http://commits.kde.org/kdepim/7a6ab70facb9850e39acd5dc60e0fceac065ce2a'>Commit.</a> </li>
<li>Fix show total article. <a href='http://commits.kde.org/kdepim/289ab191d1f52cd64fb90169da492a034b330b04'>Commit.</a> </li>
<li>Set decryptMessage propperly. <a href='http://commits.kde.org/kdepim/8590e75e524b9dbd4ec481670313dea7364b3c38'>Commit.</a> </li>
<li>Fix translate. <a href='http://commits.kde.org/kdepim/14a432152d14bd90dd9a30e9bcaaaab2521d367f'>Commit.</a> </li>
<li>Fix Bug 358127 - Incorrect Hungarian and Spanish feeds address. <a href='http://commits.kde.org/kdepim/de8c3fd37945df8866f2edf3f85da719c2e8a9c6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/358127'>#358127</a></li>
<li>Make the translators tab of the About dialog appear. <a href='http://commits.kde.org/kdepim/91fb0b65ad149dc5ccb1ed1e2ed70d5c0c0934d0'>Commit.</a> </li>
<li>Make the translators tab of the About dialog appear. <a href='http://commits.kde.org/kdepim/e4bdbb3604853aa0760465cc1d0a47d84e41ea35'>Commit.</a> </li>
<li>We need to call it in kf5 otherwise we can't use dropEvent. <a href='http://commits.kde.org/kdepim/ade320565d3a58abea31867110fca6c1e4780644'>Commit.</a> </li>
<li>Increase version. <a href='http://commits.kde.org/kdepim/3dd35510029884d3865f9c7da865393051841047'>Commit.</a> </li>
<li>Fix dnd email. <a href='http://commits.kde.org/kdepim/e211f1f488cbf14e5f17f13805dc21cffe3eb744'>Commit.</a> </li>
<li>Fix dnd email. <a href='http://commits.kde.org/kdepim/779fa0fdcca4426a76941a38d22420aee911e162'>Commit.</a> </li>
<li>Update version number. <a href='http://commits.kde.org/kdepim/43a51b5578c8c18484207a9b27f18262a16a5b11'>Commit.</a> </li>
<li>Format. <a href='http://commits.kde.org/kdepim/11a7088238be279b1ad7c088e42e7159246cf3d0'>Commit.</a> </li>
<li>Bug 357018: Fix reminder time edit field being covered up. <a href='http://commits.kde.org/kdepim/90adafe8dfd2894f90098df1f3d9c04c5cb5b2dc'>Commit.</a> </li>
<li>Fix Bug 357777 - Incorrect characters in Subject field when I use Share On...-> Mail action. <a href='http://commits.kde.org/kdepim/24ab60180dd14d5ead7e8daaa2906ce520707767'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/357777'>#357777</a></li>
</ul>
<h3><a name='kdepim-runtime' href='https://cgit.kde.org/kdepim-runtime.git'>kdepim-runtime</a> <a href='#kdepim-runtime' onclick='toggle("ulkdepim-runtime", this)'>[Show]</a></h3>
<ul id='ulkdepim-runtime' style='display: none'>
<li>Check that we've been given a valid collection in retrieveItems(). <a href='http://commits.kde.org/kdepim-runtime/c94e23a452d410d2c2e4033fb76261d969f19b92'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/354994'>#354994</a></li>
<li>Build the caches as early as possible (before change replay at least). <a href='http://commits.kde.org/kdepim-runtime/fe23c35545d66bbf8fe6cae461de96663d5cf089'>Commit.</a> </li>
<li>Create the cache directory if it's missing /o\. <a href='http://commits.kde.org/kdepim-runtime/3426b206dbe93d2e3a784af9978f1d676696e7f9'>Commit.</a> </li>
<li>Increase version. <a href='http://commits.kde.org/kdepim-runtime/7c66691d9c2d96bf211b7e676eab99ca8fa1e8a0'>Commit.</a> </li>
</ul>
<h3><a name='kdepimlibs' href='https://cgit.kde.org/kdepimlibs.git'>kdepimlibs</a> <a href='#kdepimlibs' onclick='toggle("ulkdepimlibs", this)'>[Show]</a></h3>
<ul id='ulkdepimlibs' style='display: none'>
<li>Fix show icon. <a href='http://commits.kde.org/kdepimlibs/5c11b0c5d2fc6081374c6d0336f51006ed3ba6d8'>Commit.</a> </li>
<li>Generate knut resource too. <a href='http://commits.kde.org/kdepimlibs/54327cb87b7a4106b9918fb25ebb11caeb336981'>Commit.</a> </li>
<li>Generate akonaditest even if we use BUILD_TESTS=FALSE otherwise when we want to debug with it we can't when we use some distro package. <a href='http://commits.kde.org/kdepimlibs/21dc14a7dfcc56dc30f480d4dbdc1107bcf0aa66'>Commit.</a> </li>
</ul>
<h3><a name='kholidays' href='https://cgit.kde.org/kholidays.git'>kholidays</a> <a href='#kholidays' onclick='toggle("ulkholidays", this)'>[Show]</a></h3>
<ul id='ulkholidays' style='display: none'>
<li>Fix building with gcc6. <a href='http://commits.kde.org/kholidays/50bc8a280ad537ddb95ab756dc01fb9eedaa7c85'>Commit.</a> </li>
</ul>
<h3><a name='killbots' href='https://cgit.kde.org/killbots.git'>killbots</a> <a href='#killbots' onclick='toggle("ulkillbots", this)'>[Show]</a></h3>
<ul id='ulkillbots' style='display: none'>
<li>Add tooltips and what's this popups to ki18n. <a href='http://commits.kde.org/killbots/10aa66181d351b45115ed56b81412c773212a964'>Commit.</a> </li>
<li>Fix messages showing up untranslated. <a href='http://commits.kde.org/killbots/5f7e147f816eb853c41e289a24dff4c2a875a48f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/358235'>#358235</a></li>
</ul>
<h3><a name='kio-extras' href='https://cgit.kde.org/kio-extras.git'>kio-extras</a> <a href='#kio-extras' onclick='toggle("ulkio-extras", this)'>[Show]</a></h3>
<ul id='ulkio-extras' style='display: none'>
<li>Info: remove extra defined(@array). <a href='http://commits.kde.org/kio-extras/af92d6180d479d1e56bc549bbda30a3036687182'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/359015'>#359015</a></li>
<li>Unbreak thumbnail kioslave for plugins which reuse QWidget-tainted code. <a href='http://commits.kde.org/kio-extras/99cd8dadb0926ec58e82b01170fa7b5bedfbd889'>Commit.</a> </li>
</ul>
<h3><a name='klickety' href='https://cgit.kde.org/klickety.git'>klickety</a> <a href='#klickety' onclick='toggle("ulklickety", this)'>[Show]</a></h3>
<ul id='ulklickety' style='display: none'>
<li>Fix QFileDialog filter. <a href='http://commits.kde.org/klickety/160666cfed595e4eff9f48081d44d8fb27ea33dc'>Commit.</a> </li>
</ul>
<h3><a name='kmplot' href='https://cgit.kde.org/kmplot.git'>kmplot</a> <a href='#kmplot' onclick='toggle("ulkmplot", this)'>[Show]</a></h3>
<ul id='ulkmplot' style='display: none'>
<li>Call KLocalizedString::setApplicationDomain. <a href='http://commits.kde.org/kmplot/cc2ce64fcb15588f15d51a8b0e7178b696fbbf11'>Commit.</a> </li>
</ul>
<h3><a name='kolourpaint' href='https://cgit.kde.org/kolourpaint.git'>kolourpaint</a> <a href='#kolourpaint' onclick='toggle("ulkolourpaint", this)'>[Show]</a></h3>
<ul id='ulkolourpaint' style='display: none'>
<li>Rename appdata to match .desktop name. <a href='http://commits.kde.org/kolourpaint/e5eef4acee21a7c8fdb0dbe34bc18922ad75c9d0'>Commit.</a> </li>
</ul>
<h3><a name='konsole' href='https://cgit.kde.org/konsole.git'>konsole</a> <a href='#konsole' onclick='toggle("ulkonsole", this)'>[Show]</a></h3>
<ul id='ulkonsole' style='display: none'>
<li>Allow certain variable-width fonts to be used. <a href='http://commits.kde.org/konsole/4f60f65b097766d5f573d325e056490d4b765fa0'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126921'>#126921</a></li>
<li>Allow profile's termainal size to work again. <a href='http://commits.kde.org/konsole/1e211482566edb9194f34755b823bd98f15c6f25'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126924'>#126924</a>. See bug <a href='https://bugs.kde.org/345403'>#345403</a></li>
</ul>
<h3><a name='kopete' href='https://cgit.kde.org/kopete.git'>kopete</a> <a href='#kopete' onclick='toggle("ulkopete", this)'>[Show]</a></h3>
<ul id='ulkopete' style='display: none'>
<li>Prepare for 15.12.2. <a href='http://commits.kde.org/kopete/f68d6d1eecf80ba98b37a0a3ac1f4121f842293f'>Commit.</a> </li>
</ul>
<h3><a name='lokalize' href='https://cgit.kde.org/lokalize.git'>lokalize</a> <a href='#lokalize' onclick='toggle("ullokalize", this)'>[Show]</a></h3>
<ul id='ullokalize' style='display: none'>
<li>Fix duplicate install field and install lokalize.notifyrc to correct path. <a href='http://commits.kde.org/lokalize/c94e048e4be2679aac14d8396d0bb09b79aa413f'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126967'>#126967</a></li>
</ul>
<h3><a name='marble' href='https://cgit.kde.org/marble.git'>marble</a> <a href='#marble' onclick='toggle("ulmarble", this)'>[Show]</a></h3>
<ul id='ulmarble' style='display: none'>
<li>Call qmlRegisterUncreatableType for Marble::MarbleMap. <a href='http://commits.kde.org/marble/2d50edf5d06ccac613660baad9ce9c48af800d6d'>Commit.</a> </li>
<li>Change #include style. <a href='http://commits.kde.org/marble/183dd7531d0797e09948f3c822a7aa9d9910766f'>Commit.</a> </li>
</ul>
<h3><a name='okteta' href='https://cgit.kde.org/okteta.git'>okteta</a> <a href='#okteta' onclick='toggle("ulokteta", this)'>[Show]</a></h3>
<ul id='ulokteta' style='display: none'>
<li>Remove wrong DBusActivatable entry from okteta's app desktop file. <a href='http://commits.kde.org/okteta/6efd8baece75e8fd2b2c18e0866a33b46b0c1e84'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/353573'>#353573</a></li>
<li>No headers to be installed for libchecksum & libfilter. <a href='http://commits.kde.org/okteta/69243ed076b5379aafaf5c3b894b1fe029490c36'>Commit.</a> </li>
<li>Bump version to 0.18.2. <a href='http://commits.kde.org/okteta/651f24ce5aac6f73163b2afbf4dc615f19b41897'>Commit.</a> </li>
<li>Bump patchlevel version of OKTETAKASTEN libs to 0.3.1. <a href='http://commits.kde.org/okteta/92f96726d59624fa9be6f680b39e43835e3ac13f'>Commit.</a> </li>
<li>Fix: install header for KFindDirection. <a href='http://commits.kde.org/okteta/fc52ac0aea4545788803abfbcdf57dd3636d39ec'>Commit.</a> </li>
</ul>
<h3><a name='okular' href='https://cgit.kde.org/okular.git'>okular</a> <a href='#okular' onclick='toggle("ulokular", this)'>[Show]</a></h3>
<ul id='ulokular' style='display: none'>
<li>Increase version for KDE Applications 15.12.2. <a href='http://commits.kde.org/okular/060a93698f24635b5101270e5fc94b49baad4408'>Commit.</a> </li>
<li>SetAttribute with doubles is evil as it uses the current locale and we don't want that, use QString::number. <a href='http://commits.kde.org/okular/41006445a9d8369d3891ea1feed7c977a3d0f1a5'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/359016'>#359016</a></li>
<li>Forms: Let checkboxes be unchecked. <a href='http://commits.kde.org/okular/ab3a9072b097a9f3c92635aeb2e785f97078c72a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/357743'>#357743</a></li>
</ul>
<h3><a name='print-manager' href='https://cgit.kde.org/print-manager.git'>print-manager</a> <a href='#print-manager' onclick='toggle("ulprint-manager", this)'>[Show]</a></h3>
<ul id='ulprint-manager' style='display: none'>
<li>Fix printmanager.notifyrc is installed in a wrong path. <a href='http://commits.kde.org/print-manager/5bedcd8b791f59d7f162be96491b98832778e02d'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126965'>#126965</a></li>
</ul>
<h3><a name='spectacle' href='https://cgit.kde.org/spectacle.git'>spectacle</a> <a href='#spectacle' onclick='toggle("ulspectacle", this)'>[Show]</a></h3>
<ul id='ulspectacle' style='display: none'>
<li>DO NOT MERGE TO MASTER: Make save options dialog resizable. <a href='http://commits.kde.org/spectacle/688e1e2a4642f6f43e3ce7bfd2edff0cae106dcd'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/358103'>#358103</a></li>
</ul>
<h3><a name='umbrello' href='https://cgit.kde.org/umbrello.git'>umbrello</a> <a href='#umbrello' onclick='toggle("ulumbrello", this)'>[Show]</a></h3>
<ul id='ulumbrello' style='display: none'>
<li>Fix 'Broken use case clipboard import from another instance'. <a href='http://commits.kde.org/umbrello/5b3ff959e82367124e873ba54e8a3873e56b39bc'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/359055'>#359055</a></li>
<li>Fix initial setting of line color of an association widget. <a href='http://commits.kde.org/umbrello/8909fe46197efb45fd0a9263bf6afdf7b4a5fd34'>Commit.</a> See bug <a href='https://bugs.kde.org/358358'>#358358</a></li>
<li>Fix 'Sequence line color do not follow object widget color settings'. <a href='http://commits.kde.org/umbrello/8ffad46f0aefd9f4d9a6a6c4546f2386e5773dc9'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/358943'>#358943</a></li>
<li>Add more c++ import testcases for parsing comments. <a href='http://commits.kde.org/umbrello/dd45e245990253af19988268c3a599cad91d8719'>Commit.</a> See bug <a href='https://bugs.kde.org/358655'>#358655</a></li>
<li>Fix bug raised with testcase 'test/import/cxx/comments-file-variant3.h'. <a href='http://commits.kde.org/umbrello/1cecce63d777ed9378e89496ddf19ec9a5f6d14b'>Commit.</a> </li>
<li>Add c++ import testcases for parsing comments. <a href='http://commits.kde.org/umbrello/67a97004fc8353544326d4875e3c236813a1bb6a'>Commit.</a> See bug <a href='https://bugs.kde.org/358655'>#358655</a></li>
<li>Windows gcc compile fix. <a href='http://commits.kde.org/umbrello/4663b8ec7564cfeebb27d5b04045dc8024fd7450'>Commit.</a> </li>
<li>Fix 'Message "too big indentation amount" on windows'. <a href='http://commits.kde.org/umbrello/01b2b16339521c051049b53662ecf890ac0cbe49'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/358731'>#358731</a></li>
<li>Fix 'Impossible to add additional import include search pathes on windows'. <a href='http://commits.kde.org/umbrello/95d554165e1ccdb2798b736e19b5a2500f8080f2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/358687'>#358687</a></li>
<li>Fix wheel event position issue after selecting widgets on linux. <a href='http://commits.kde.org/umbrello/de1dce05bcf5afa1157d3baf1a2667dd54c45e32'>Commit.</a> See bug <a href='https://bugs.kde.org/358097'>#358097</a></li>
<li>Fix bug been locked near lower zoom limit on windows. <a href='http://commits.kde.org/umbrello/334d8488e1d5a36ab6cd7c979303919b60281bc0'>Commit.</a> See bug <a href='https://bugs.kde.org/358097'>#358097</a></li>
<li>Fix wheel mouse inaccuracy on zooming. <a href='http://commits.kde.org/umbrello/fbfbffea75726b5a2d329a791c896dfb809bbfde'>Commit.</a> See bug <a href='https://bugs.kde.org/358097'>#358097</a></li>
<li>Refactoring UMLView. <a href='http://commits.kde.org/umbrello/014319ab1d71b162a0b9bc9bd518317f9387becb'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/358097'>#358097</a>. Code review <a href='https://git.reviewboard.kde.org/r/126773'>#126773</a></li>
<li>Keep loading type of XMI file attributes "xclicked", "yclicked", "minY" and "maxY" in sync with saving. <a href='http://commits.kde.org/umbrello/15117e4ca041b5d3241cd75edf900b8dd5d5163d'>Commit.</a> See bug <a href='https://bugs.kde.org/357373'>#357373</a></li>
<li>Fix loading of XMI files on linux os generated with non english locale. <a href='http://commits.kde.org/umbrello/59ab483bac9104ec12dee4a98a0a9dcca4dd7636'>Commit.</a> See bug <a href='https://bugs.kde.org/357373'>#357373</a></li>
<li>Fixup of 90517660d6a14da833ea6cb7c1641bcdfe6c1d2b. <a href='http://commits.kde.org/umbrello/65296dc299319e0ad4675353100e582054987f81'>Commit.</a> See bug <a href='https://bugs.kde.org/357373'>#357373</a></li>
<li>Fix 'When opening a saved file, class positions are wrong'. <a href='http://commits.kde.org/umbrello/90517660d6a14da833ea6cb7c1641bcdfe6c1d2b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/357373'>#357373</a></li>
<li>Fix 'No change of association symbol line colour and width possible'. <a href='http://commits.kde.org/umbrello/c14b580a5f42358d9dfbd916b3afc315e12ffd51'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/358358'>#358358</a>. See bug <a href='https://bugs.kde.org/358365'>#358365</a></li>
<li>Fix 'Resize of sequence diagram messages is broken in horizontal orientation'. <a href='http://commits.kde.org/umbrello/401f94108f213cdd3859b9e0780527843ce1aa8e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/358287'>#358287</a></li>
<li>Fix 'Crash on removing diagram'. <a href='http://commits.kde.org/umbrello/972f7145563d1e89754b612bcc03d3f754e952ca'>Commit.</a> See bug <a href='https://bugs.kde.org/352633'>#352633</a>. Fixes bug <a href='https://bugs.kde.org/358286'>#358286</a></li>
<li>Fix 'Crash on removing association point'. <a href='http://commits.kde.org/umbrello/7fc95d9ba5a850ab882a4460cf44934738c06500'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/358276'>#358276</a></li>
</ul>