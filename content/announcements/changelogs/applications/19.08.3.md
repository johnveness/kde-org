---
aliases:
- ../../fulllog_applications-19.08.3
hidden: true
title: KDE Applications 19.08.3 Full Log Page
type: fulllog
version: 19.08.3
---

<h3><a name='ark' href='https://cgit.kde.org/ark.git'>ark</a> <a href='#ark' onclick='toggle("ulark", this)'>[Hide]</a></h3>
<ul id='ulark' style='display: block'>
<li>Prevent drops from non-local URLs. <a href='http://commits.kde.org/ark/d4801147cd4ddd40311a9b6a51f8de6324c2e2a3'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/409835'>#409835</a></li>
</ul>
<h3><a name='artikulate' href='https://cgit.kde.org/artikulate.git'>artikulate</a> <a href='#artikulate' onclick='toggle("ulartikulate", this)'>[Hide]</a></h3>
<ul id='ulartikulate' style='display: block'>
<li>Use registered categories in the desktop file. <a href='http://commits.kde.org/artikulate/be832997d451adab3306e2481ccb72e8c5194638'>Commit.</a> </li>
</ul>
<h3><a name='cantor' href='https://cgit.kde.org/cantor.git'>cantor</a> <a href='#cantor' onclick='toggle("ulcantor", this)'>[Hide]</a></h3>
<ul id='ulcantor' style='display: block'>
<li>Fix sagemath backend when there is no sage-env. <a href='http://commits.kde.org/cantor/13b8d731d501839ad4602232345baa4307d6d7e9'>Commit.</a> </li>
</ul>
<h3><a name='eventviews' href='https://cgit.kde.org/eventviews.git'>eventviews</a> <a href='#eventviews' onclick='toggle("uleventviews", this)'>[Hide]</a></h3>
<ul id='uleventviews' style='display: block'>
<li>Month/monthscene.cpp - remove comma in year (in the title). <a href='http://commits.kde.org/eventviews/a5c067fbc83c59d34166048a58fe12e5942f3e36'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/412833'>#412833</a></li>
</ul>
<h3><a name='granatier' href='https://cgit.kde.org/granatier.git'>granatier</a> <a href='#granatier' onclick='toggle("ulgranatier", this)'>[Hide]</a></h3>
<ul id='ulgranatier' style='display: block'>
<li>Restrict GNUGXX compiler options. <a href='http://commits.kde.org/granatier/b352814a2d58f894f8519f2a96cde6fd5c684b4d'>Commit.</a> </li>
</ul>
<h3><a name='incidenceeditor' href='https://cgit.kde.org/incidenceeditor.git'>incidenceeditor</a> <a href='#incidenceeditor' onclick='toggle("ulincidenceeditor", this)'>[Hide]</a></h3>
<ul id='ulincidenceeditor' style='display: block'>
<li>Incidencealarm.cpp - allow double-click to edit an alarm. <a href='http://commits.kde.org/incidenceeditor/dd6795cec40148a5d274eb01a5a82633dee4bbfa'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/412242'>#412242</a></li>
</ul>
<h3><a name='k3b' href='https://cgit.kde.org/k3b.git'>k3b</a> <a href='#k3b' onclick='toggle("ulk3b", this)'>[Hide]</a></h3>
<ul id='ulk3b' style='display: block'>
<li>Fix crash on exit while uninstalling a theme. <a href='http://commits.kde.org/k3b/a930be4876104ea4a0c5332b85b625240e45fd72'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/413278'>#413278</a></li>
</ul>
<h3><a name='kalarm' href='https://cgit.kde.org/kalarm.git'>kalarm</a> <a href='#kalarm' onclick='toggle("ulkalarm", this)'>[Hide]</a></h3>
<ul id='ulkalarm' style='display: block'>
<li>Add kdepim-runtime change. <a href='http://commits.kde.org/kalarm/ea402d73ff03297849b952b9208590d29cc4270d'>Commit.</a> </li>
<li>Remove stray comment terminator. <a href='http://commits.kde.org/kalarm/52645d69390b3ab2893de0da2e10bfe2d20e7225'>Commit.</a> </li>
<li>Make user settings changes take effect immediately. <a href='http://commits.kde.org/kalarm/02ccc337a24d02ca6ca427d8239a75274893febd'>Commit.</a> </li>
<li>Fix layout of defer alarm dialog. <a href='http://commits.kde.org/kalarm/3ef411b13d9c747f9218ada035094fa4b53d3c72'>Commit.</a> </li>
<li>Update version number. <a href='http://commits.kde.org/kalarm/7bf0706c203a9290cf2443def3fb0647b58d23c3'>Commit.</a> </li>
<li>Fix error on redo of an active alarm deletion. <a href='http://commits.kde.org/kalarm/62caadaf754c7cf4e18524e1c4eb2a7a0f759e5c'>Commit.</a> </li>
<li>Fix error on redo of an active alarm deletion. <a href='http://commits.kde.org/kalarm/7e6c9bf20fe93e5d111f1f33a7fa624e2ceddaee'>Commit.</a> </li>
<li>Archive repeat-at-login alarms if previously triggered, when deleted. <a href='http://commits.kde.org/kalarm/45d7e9978606da5c516d6d67c33e5e7a732a6dc8'>Commit.</a> </li>
</ul>
<h3><a name='kalzium' href='https://cgit.kde.org/kalzium.git'>kalzium</a> <a href='#kalzium' onclick='toggle("ulkalzium", this)'>[Hide]</a></h3>
<ul id='ulkalzium' style='display: block'>
<li>Correct formatting of year of discovery. <a href='http://commits.kde.org/kalzium/93c91c655bb15c3e6eb33c3a99cf91c1377ef50a'>Commit.</a> </li>
<li>Give correct information about the year of discovery. <a href='http://commits.kde.org/kalzium/242b7b7346c6f0086abd1f8919a31e847688c21b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/413180'>#413180</a></li>
</ul>
<h3><a name='kate' href='https://cgit.kde.org/kate.git'>kate</a> <a href='#kate' onclick='toggle("ulkate", this)'>[Hide]</a></h3>
<ul id='ulkate' style='display: block'>
<li>Ensure outer layout set. <a href='http://commits.kde.org/kate/255a72f5b6b98740b92c70e6bac3fa56218c9d57'>Commit.</a> See bug <a href='https://bugs.kde.org/412721'>#412721</a></li>
<li>Try to avoid nested layouts that we don't need. <a href='http://commits.kde.org/kate/d3e413fb7c9fcf31c97b90a09aa52fb59bbbd9b6'>Commit.</a> See bug <a href='https://bugs.kde.org/412721'>#412721</a></li>
</ul>
<h3><a name='kdenlive' href='https://cgit.kde.org/kdenlive.git'>kdenlive</a> <a href='#kdenlive' onclick='toggle("ulkdenlive", this)'>[Hide]</a></h3>
<ul id='ulkdenlive' style='display: block'>
<li>Try to make it compile with gcc 9. <a href='http://commits.kde.org/kdenlive/63f232560ec2fd98b266f2177b37c51c4ceb1428'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/413416'>#413416</a></li>
<li>Fix missing param name in avfilters. <a href='http://commits.kde.org/kdenlive/cd349721ba1c5e7cc4694e581b0cffab9c054bad'>Commit.</a> </li>
<li>Fix compositions disappear after reopening project with locked track. <a href='http://commits.kde.org/kdenlive/fd9a00fb52ba9d5e23e06bf8ab5eeafe3ca0c017'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/412369'>#412369</a></li>
<li>Fix favorite compositions broken. Fixes #361. <a href='http://commits.kde.org/kdenlive/32d8c095fa1ba15fe71263daac79c4fb2a558a09'>Commit.</a> </li>
<li>Fix razor tool cutting wrong clip. Fixes #380. <a href='http://commits.kde.org/kdenlive/e6075212548757fc7271056cc4f2088a395a3507'>Commit.</a> </li>
<li>Fix red track background on add track. <a href='http://commits.kde.org/kdenlive/fdfb1e517be8dc7e7d03352db395f43ce458c3ff'>Commit.</a> </li>
<li>Fix deprecated method. <a href='http://commits.kde.org/kdenlive/b4786590900add2a2f92f2aff9ac73f121def4dd'>Commit.</a> </li>
<li>Fix docked widgets losing title and decoration when undocked. <a href='http://commits.kde.org/kdenlive/45f88ccacf4e5b9e86ff6a87ec0d01f87befcf32'>Commit.</a> </li>
<li>Close favorite effect popup on activation. <a href='http://commits.kde.org/kdenlive/35496f4a9d043aa30462ff2c588e62ceadc5754c'>Commit.</a> </li>
<li>Fix fades handles sometimes not appearing. <a href='http://commits.kde.org/kdenlive/06047a8ca092500fb8b7978f0f475ddcefccc79b'>Commit.</a> </li>
<li>Fix seeking with wheel on ruler. <a href='http://commits.kde.org/kdenlive/a076cc4dc8bd44334dedd517bdb9da45c4f7774e'>Commit.</a> </li>
<li>Update appdata for 19.08.3. <a href='http://commits.kde.org/kdenlive/f062b0a842bcdb2ee6b0e186619c9e5dec847787'>Commit.</a> </li>
<li>Fix fad in control sometimes not visible. <a href='http://commits.kde.org/kdenlive/12fd26015c5c7d66c71ae5fa754e182e2a683f34'>Commit.</a> </li>
</ul>
<h3><a name='kdepim-runtime' href='https://cgit.kde.org/kdepim-runtime.git'>kdepim-runtime</a> <a href='#kdepim-runtime' onclick='toggle("ulkdepim-runtime", this)'>[Hide]</a></h3>
<ul id='ulkdepim-runtime' style='display: block'>
<li>Prevent writing to calendar file if not in current KAlarm format. <a href='http://commits.kde.org/kdepim-runtime/2c4cc6d96fc46f7622c5eba5e61641669b859b1c'>Commit.</a> </li>
</ul>
<h3><a name='kdesdk-thumbnailers' href='https://cgit.kde.org/kdesdk-thumbnailers.git'>kdesdk-thumbnailers</a> <a href='#kdesdk-thumbnailers' onclick='toggle("ulkdesdk-thumbnailers", this)'>[Hide]</a></h3>
<ul id='ulkdesdk-thumbnailers' style='display: block'>
<li>[pocreator] Avoid divide by 0. <a href='http://commits.kde.org/kdesdk-thumbnailers/de3c82950d9a9bdfb3cfb8897b1d12eaa200066e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/412585'>#412585</a></li>
</ul>
<h3><a name='kfourinline' href='https://cgit.kde.org/kfourinline.git'>kfourinline</a> <a href='#kfourinline' onclick='toggle("ulkfourinline", this)'>[Hide]</a></h3>
<ul id='ulkfourinline' style='display: block'>
<li>Remove Include of unistd.h. <a href='http://commits.kde.org/kfourinline/56645661660097d36e3e2f548c358c09e522613d'>Commit.</a> </li>
</ul>
<h3><a name='kio-extras' href='https://cgit.kde.org/kio-extras.git'>kio-extras</a> <a href='#kio-extras' onclick='toggle("ulkio-extras", this)'>[Hide]</a></h3>
<ul id='ulkio-extras' style='display: block'>
<li>Fix password error condition handling of smb mount. <a href='http://commits.kde.org/kio-extras/45b1327a6dacf2a260bbb9aa7cb73b1ad2549f96'>Commit.</a> </li>
<li>Improve error reporting for when kpasswdserver is unreachable. <a href='http://commits.kde.org/kio-extras/5a8705548fae88c777ea9a0edeb3f58facbf5d48'>Commit.</a> </li>
</ul>
<h3><a name='kitinerary' href='https://cgit.kde.org/kitinerary.git'>kitinerary</a> <a href='#kitinerary' onclick='toggle("ulkitinerary", this)'>[Hide]</a></h3>
<ul id='ulkitinerary' style='display: block'>
<li>Better end time default for restaurant reservations without an end time. <a href='http://commits.kde.org/kitinerary/7e0e66911e642a82b8807022926d24996479fc19'>Commit.</a> See bug <a href='https://bugs.kde.org/404451'>#404451</a></li>
<li>Fix compatibility with Poppler 0.82. <a href='http://commits.kde.org/kitinerary/0f8b2babcc69c490ae6548bda7ceeb1ffd27a9e3'>Commit.</a> </li>
</ul>
<h3><a name='kmahjongg' href='https://cgit.kde.org/kmahjongg.git'>kmahjongg</a> <a href='#kmahjongg' onclick='toggle("ulkmahjongg", this)'>[Hide]</a></h3>
<ul id='ulkmahjongg' style='display: block'>
<li>Add Icons To Executable. <a href='http://commits.kde.org/kmahjongg/2fc9e28f636f502b0d81ea7d646667acfb5790b2'>Commit.</a> </li>
</ul>
<h3><a name='kmail' href='https://cgit.kde.org/kmail.git'>kmail</a> <a href='#kmail' onclick='toggle("ulkmail", this)'>[Hide]</a></h3>
<ul id='ulkmail' style='display: block'>
<li>Fix Bug 412895 - Attachment detector should not trigger while saving as draft. <a href='http://commits.kde.org/kmail/96c0e3ed5a01a41264ecb215f1b8bb8b4fee324e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/412895'>#412895</a></li>
</ul>
<h3><a name='konqueror' href='https://cgit.kde.org/konqueror.git'>konqueror</a> <a href='#konqueror' onclick='toggle("ulkonqueror", this)'>[Hide]</a></h3>
<ul id='ulkonqueror' style='display: block'>
<li>Fix CMakeLists.txt to compile WebEnginePartHtmlEmbedder on Qt before Qt-5.12. <a href='http://commits.kde.org/konqueror/9119e90c00ebe75bd41223eee9b06cda99181a0c'>Commit.</a> </li>
</ul>
<h3><a name='konquest' href='https://cgit.kde.org/konquest.git'>konquest</a> <a href='#konquest' onclick='toggle("ulkonquest", this)'>[Hide]</a></h3>
<ul id='ulkonquest' style='display: block'>
<li>Fix struct Prototype of GameOptions. <a href='http://commits.kde.org/konquest/c0ffb9b3b014d561add3afa84769a223e4d69bb6'>Commit.</a> </li>
</ul>
<h3><a name='konsole' href='https://cgit.kde.org/konsole.git'>konsole</a> <a href='#konsole' onclick='toggle("ulkonsole", this)'>[Hide]</a></h3>
<ul id='ulkonsole' style='display: block'>
<li>Fix scrollbar position. <a href='http://commits.kde.org/konsole/77ce0888d82ef9ee185e935d10b8a8869db6f989'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/412348'>#412348</a></li>
</ul>
<h3><a name='kopete' href='https://cgit.kde.org/kopete.git'>kopete</a> <a href='#kopete' onclick='toggle("ulkopete", this)'>[Hide]</a></h3>
<ul id='ulkopete' style='display: block'>
<li>Libiris: Stop using qt_wrap_cpp(). <a href='http://commits.kde.org/kopete/5a36ec88687ae0b602914c91d6fb1631b799a899'>Commit.</a> </li>
<li>Fix missing parentheses around macro. <a href='http://commits.kde.org/kopete/22563ca5f946b6ed150b4cad68c46d9babfc1c69'>Commit.</a> </li>
</ul>
<h3><a name='korganizer' href='https://cgit.kde.org/korganizer.git'>korganizer</a> <a href='#korganizer' onclick='toggle("ulkorganizer", this)'>[Hide]</a></h3>
<ul id='ulkorganizer' style='display: block'>
<li>NavBar: Use standaloneMonthName instead of monthName. <a href='http://commits.kde.org/korganizer/9844247d8cce65f75ff0815f03fe4b065bb99cc6'>Commit.</a> </li>
<li>Make Get New Stuff work again. <a href='http://commits.kde.org/korganizer/d4b52772038ee52fd9a7217c0b900a2cdaa4fd10'>Commit.</a> </li>
</ul>
<h3><a name='kreversi' href='https://cgit.kde.org/kreversi.git'>kreversi</a> <a href='#kreversi' onclick='toggle("ulkreversi", this)'>[Hide]</a></h3>
<ul id='ulkreversi' style='display: block'>
<li>Remove Include of unistd.h. <a href='http://commits.kde.org/kreversi/b56d13ee8f7c8cd06f82013ae4b0073844783d43'>Commit.</a> </li>
<li>Add Icons To Executable. <a href='http://commits.kde.org/kreversi/9fd839bddede7620a6475ed446d1cd0336cddf47'>Commit.</a> </li>
</ul>
<h3><a name='kshisen' href='https://cgit.kde.org/kshisen.git'>kshisen</a> <a href='#kshisen' onclick='toggle("ulkshisen", this)'>[Hide]</a></h3>
<ul id='ulkshisen' style='display: block'>
<li>Add Icons To Executable. <a href='http://commits.kde.org/kshisen/8c3c4e9ed0fdfdcec592460868ecb9770d115369'>Commit.</a> </li>
</ul>
<h3><a name='ktouch' href='https://cgit.kde.org/ktouch.git'>ktouch</a> <a href='#ktouch' onclick='toggle("ulktouch", this)'>[Hide]</a></h3>
<ul id='ulktouch' style='display: block'>
<li>Disable QML Precompilation by Default. <a href='http://commits.kde.org/ktouch/cb5792c6e9c72d9cc02e93223c86c793c0157b79'>Commit.</a> See bug <a href='https://bugs.kde.org/412677'>#412677</a></li>
</ul>
<h3><a name='messagelib' href='https://cgit.kde.org/messagelib.git'>messagelib</a> <a href='#messagelib' onclick='toggle("ulmessagelib", this)'>[Hide]</a></h3>
<ul id='ulmessagelib' style='display: block'>
<li>Fix search external reference. <a href='http://commits.kde.org/messagelib/a6eacf6e17220576547ec52776c058762d2587ff'>Commit.</a> </li>
</ul>
<h3><a name='okular' href='https://cgit.kde.org/okular.git'>okular</a> <a href='#okular' onclick='toggle("ulokular", this)'>[Hide]</a></h3>
<ul id='ulokular' style='display: block'>
<li>AnnotWindow: Always show creation time in local time. <a href='http://commits.kde.org/okular/3e63353bfe77ffe390d00bcd15fcca52c5369d78'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/413591'>#413591</a></li>
<li>Don't crash on nullptr pdfOptionsPage. <a href='http://commits.kde.org/okular/568f11b0678e68ef4eeb9bf63b618e32396658db'>Commit.</a> </li>
</ul>
<h3><a name='poxml' href='https://cgit.kde.org/poxml.git'>poxml</a> <a href='#poxml' onclick='toggle("ulpoxml", this)'>[Hide]</a></h3>
<ul id='ulpoxml' style='display: block'>
<li>Cmake: build/run tests only when enabled. <a href='http://commits.kde.org/poxml/9bf4f87ea55d7a23ba871047b9218911243669ed'>Commit.</a> </li>
</ul>
<h3><a name='spectacle' href='https://cgit.kde.org/spectacle.git'>spectacle</a> <a href='#spectacle' onclick='toggle("ulspectacle", this)'>[Hide]</a></h3>
<ul id='ulspectacle' style='display: block'>
<li>Make all buttons in the main window activatable with enter. <a href='http://commits.kde.org/spectacle/34484c935c5c6ea91f11bc69791ca43e18022016'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/412184'>#412184</a></li>
</ul>
<h3><a name='umbrello' href='https://cgit.kde.org/umbrello.git'>umbrello</a> <a href='#umbrello' onclick='toggle("ulumbrello", this)'>[Hide]</a></h3>
<ul id='ulumbrello' style='display: block'>
<li>Cleanup of generated c++ code. <a href='http://commits.kde.org/umbrello/a6f00ce109f8b834eaa8aaefc7a9408a86ef6f33'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/413574'>#413574</a></li>
<li>Cppwriter: In case of interfaces, do not write a superfluous .cpp file. <a href='http://commits.kde.org/umbrello/aa52c15a8ff69434950abdbf220ddbf2113604d5'>Commit.</a> See bug <a href='https://bugs.kde.org/413519'>#413519</a></li>
<li>When creating class methods for an interface through the user interface set abstract and virtual attributes. <a href='http://commits.kde.org/umbrello/c3afcda6559537dfff238f6783d8f9a782b294e0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/413519'>#413519</a></li>
<li>Fixes missing initialisation of members m_inline and m_abstract of class UMLOperation. <a href='http://commits.kde.org/umbrello/0333988fc16e0a403cdd306e3e213f0b7a5c6dcb'>Commit.</a> See bug <a href='https://bugs.kde.org/413519'>#413519</a></li>
<li>Disable unrelated checkboxes in operation property dialog for interfaces. <a href='http://commits.kde.org/umbrello/869268b5edc00f96075d5304e84ec14ed867974a'>Commit.</a> See bug <a href='https://bugs.kde.org/413519'>#413519</a></li>
<li>Cppwriter: Add support for writing abstract methods. <a href='http://commits.kde.org/umbrello/ef47ab7b242aa87b5cd4d8e5abeb841e2b00d663'>Commit.</a> See bug <a href='https://bugs.kde.org/413519'>#413519</a></li>
</ul>