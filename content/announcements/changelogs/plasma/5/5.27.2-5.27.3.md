---
title: Plasma 5.27.3 complete changelog
version: 5.27.3
hidden: true
plasma: true
type: fulllog
---
{{< details title="Breeze" href="https://commits.kde.org/breeze" >}}
+ Setting height before adding margins. [Commit.](http://commits.kde.org/breeze/09ff7d1ce91b01d6beaf8d23e0257684b7440cb1) 
+ Calling expandSize in flat comboboxes too. [Commit.](http://commits.kde.org/breeze/950771cd2ce7ed27af49e6e8a836df76c99d1f1b) 
{{< /details >}}

{{< details title="breeze-gtk" href="https://commits.kde.org/breeze-gtk" >}}
+ Gtk3, gtk4: apply searchbar styles to the box inside the revealer inside the searchbar. [Commit.](http://commits.kde.org/breeze-gtk/f1c4f13b34d9c137a2cb7970648c709115b956f0) 
+ Gtk3, gtk4: Make image-buttons have min-height. [Commit.](http://commits.kde.org/breeze-gtk/75ada22f9ca82641d5032ebe19bc8fcab4187e42) 
+ Remove margins between linked buttons. [Commit.](http://commits.kde.org/breeze-gtk/27ed81401b29adc37999ce102f6c47b49407d498) 
{{< /details >}}

{{< details title="Discover" href="https://commits.kde.org/discover" >}}
+ Rpm-ostree/notifier: Setup a watcher to trigger reboot check. [Commit.](http://commits.kde.org/discover/e46bd6c391236c201ae92c2947205bee700326c0) 
+ Rpm-ostree/notifier: Fix update/reboot notification logic. [Commit.](http://commits.kde.org/discover/d08936b36ca016ad81d8ab42d3c243dc36da261b) 
+ ApplicationPage: Allow main app info column to grow with window. [Commit.](http://commits.kde.org/discover/67de8a17e0b2a3dc2bf84b83a51ab4dce5a380e6) 
+ ApplicationPage: off-by-one in stackedLayout calc. [Commit.](http://commits.kde.org/discover/04c40fbbcb3a0767cfef88eb216742692d2cf5e8) 
+ ApplicationResouceButton: place icon side-by-side to the title. [Commit.](http://commits.kde.org/discover/5fdc5b8444c603f1ea2a5ee0a8a0c91aae37a758) 
+ ApplicationResourceButton: attribute the left/right padding. [Commit.](http://commits.kde.org/discover/ff5c77f190b04507d13ee6aed81e38bbf8caa4a7) 
+ ApplicationPage: drop the ternary operator for buttonWidth. [Commit.](http://commits.kde.org/discover/f98b1a6dc6c337b86fed7c97c0cca316216db458) 
+ Flatpak: Use Downloading as the status for Flatpak transactions. [Commit.](http://commits.kde.org/discover/189ef41ac6a03836fa37577da49998c56c0773bf) 
+ Pk: Finish porting away from runservices. [Commit.](http://commits.kde.org/discover/08ed9e8c125aca3628c31b9abd624cd171fffb49) Fixes bug [#466742](https://bugs.kde.org/466742)
+ Pk: Don't forget to finish streams. [Commit.](http://commits.kde.org/discover/29ba6422a0321ed9ccef7abe3a0a47a688b936f4) Fixes bug [#466765](https://bugs.kde.org/466765)
+ Flatpak: Fix spacing in permissions view. [Commit.](http://commits.kde.org/discover/29be0971a834b898f9f095d014a82f6ae34a0485) 
+ Fwupd: Mark the backend as invalid if fwupd_client_connect() fails. [Commit.](http://commits.kde.org/discover/44e282b6630593b16a5e8802e9b096bf46970870) 
{{< /details >}}

{{< details title="Dr Konqi" href="https://commits.kde.org/drkonqi" >}}
+ Add emoji picker to mappings. [Commit.](http://commits.kde.org/drkonqi/105fac2d53ca03dae451922acce42e1f3cb04623) 
{{< /details >}}

{{< details title="Flatpak Permissions" href="https://commits.kde.org/flatpak-kcm" >}}
+ FlatpakPermissionModel: Mark permExists method as const. [Commit.](http://commits.kde.org/flatpak-kcm/b1877ed22b0ea2634362f7a7078e1d003b007d79) 
+ FlatpakPermissionModel: Remove unused role DefaultValue. [Commit.](http://commits.kde.org/flatpak-kcm/5ba728fba57732bbe734f79792f19e91198a57da) 
+ FlatpakPermission: Drop unused name parameter from constructor for dummy entries. [Commit.](http://commits.kde.org/flatpak-kcm/ff8870722b384a78d4a7e54078afadefc99ca14d) 
+ FlatpakPermissionModel: Turn if-else chain into proper switch-case. [Commit.](http://commits.kde.org/flatpak-kcm/220c3874e869a7c0d38ef4bb1a3cd7854141f9f4) 
+ FlatpakPermissionModel: Stop messing up with clang-format off/on. [Commit.](http://commits.kde.org/flatpak-kcm/f84350b96dc204d5508890ddc35c05c8860ab9cf) 
+ Fix highlight states for permission items. [Commit.](http://commits.kde.org/flatpak-kcm/214d9b9fa5575dd692a924a42eb4bd5eef0dbd27) 
+ FlatpakPermissionModel: Optimize memory allocation: replace QVector with on-stack std::array. [Commit.](http://commits.kde.org/flatpak-kcm/d9963ba9c1d82ebfb0720d32097b1dee7abf3a4f) 
+ FlatpakPermissionModel: Fix model index ranges in load & save. [Commit.](http://commits.kde.org/flatpak-kcm/e62f6c9acd6646bf92c2277bcf2778b45f815a34) 
+ FlatpakPermissionModel: Use const iterators and const-references. [Commit.](http://commits.kde.org/flatpak-kcm/6fc01cb189e9744e53fffc9e0259222dda0d822e) 
+ Reorder roles to sync between the enum, roleNames mapping and data method. [Commit.](http://commits.kde.org/flatpak-kcm/a75a82de4e5f46bd26a313c7829b2b51b19190f8) 
+ FlatpakPermissionModel: Remove unused role IsBasic. [Commit.](http://commits.kde.org/flatpak-kcm/710d5f27cffc410b084c62df6ee981634e301db0) 
+ Doc: Fix typo. [Commit.](http://commits.kde.org/flatpak-kcm/1cf85089fd1d7af5f54ef816f51d1fb8188bbfd0) 
+ Re-add default list item highlight. [Commit.](http://commits.kde.org/flatpak-kcm/09dc8ccce3dc1f2b57a7d4d38f9e4111b8af3c0a) Fixes bug [#465951](https://bugs.kde.org/465951)
+ FlatpakPermissionModel: Remove unused role Path. [Commit.](http://commits.kde.org/flatpak-kcm/a670d507a3e3758426e19b6f9da789aeb88f9615) 
+ FlatpakPermissionModel: Remove unused role Type / "valueType". [Commit.](http://commits.kde.org/flatpak-kcm/d00850701ae624717c206fe28ea8e2a18faabe57) 
+ FlatpakPermission: Mandate enum name when referring to enum values. [Commit.](http://commits.kde.org/flatpak-kcm/7770d9517482fa5d750edf6ac4c473988e688583) 
+ KCM: Re-evaluate state of the standard KCM buttons when unloading permissions model. [Commit.](http://commits.kde.org/flatpak-kcm/059036d8d4bdd800da14303b65920f04958262ee) 
+ Permissions: Move showAdvanced option inside FlatpakPermissionModel model. [Commit.](http://commits.kde.org/flatpak-kcm/4ed15397c6f8688564737a7313caa8aa9cd1180f) 
+ FlatpakPermissionModel: Reorder implementation of methods closer to declarations. [Commit.](http://commits.kde.org/flatpak-kcm/22a426c65e013e1d58edec7bd09928cecb7d5aba) 
+ FlatpakPermissionModel: Add missing const qualifier from ref getter. [Commit.](http://commits.kde.org/flatpak-kcm/e478cf24a0edc5213f2fcfa5d8b827e96bb70e37) 
+ FlatpakPermissionModel: Demote setReference from public slots. [Commit.](http://commits.kde.org/flatpak-kcm/83c0ba27a8266733667d8e8112cf5dab5db37e07) 
+ FlatpakPermission: Factor out initialization of dummy/advanced entries. [Commit.](http://commits.kde.org/flatpak-kcm/4e52d3b9aadd9a87a22e7e38ac0eadb306422480) 
+ KCM: Initialize state of the standard "Defaults" button. [Commit.](http://commits.kde.org/flatpak-kcm/2a49242fccea1ebb95661dcb9c98af1c809d66a5) 
+ KCM: Fix refreshing state of the standard "Defaults" button. [Commit.](http://commits.kde.org/flatpak-kcm/7e9b42ef1c775019aad90a4d3167f15f51bd3eed) 
+ FlatpakPermission: Slightly simplify logical code for comprehension. [Commit.](http://commits.kde.org/flatpak-kcm/88ca0fcce94fc19d6351f39d319fd047c12bcf13) 
+ FlatpakPermission: Reorder methods implementation to match declarations. [Commit.](http://commits.kde.org/flatpak-kcm/3d4767b6cba6c05fcf3951e9a55bb599f93a72ad) 
+ Use more const auto &references to avoid copies. [Commit.](http://commits.kde.org/flatpak-kcm/0f2935bb772a95f6cf5b43076611ba842aae8fb6) 
+ QML: Rename top level component id to something simpler and shorter. [Commit.](http://commits.kde.org/flatpak-kcm/70608a96b36824b0daa3522f7a597bfc1e51a8df) 
+ Factor out index argument for dataChanged() calls. [Commit.](http://commits.kde.org/flatpak-kcm/b5997020199f86043a22d79f16736e7379ee048e) 
+ FlatpakPermission: Rename parameters of methods in implementation. [Commit.](http://commits.kde.org/flatpak-kcm/469f152ad028bbb5a20fabee2ba5c98c750576fa) 
+ FlatpakPermission: Report dummy section as always being at defaults. [Commit.](http://commits.kde.org/flatpak-kcm/414e75bd363065951cd7781cfa952f1a95c2f3f8) 
+ Rename some variables for clarity. [Commit.](http://commits.kde.org/flatpak-kcm/4cfedc915662e41957312ea500499e12b314f814) 
+ Factor out QML module URI. [Commit.](http://commits.kde.org/flatpak-kcm/642dd0f9a9acd44bf01f353f97713a11d9085c65) 
+ Rename system `installation` variable to `systemInstallation`. [Commit.](http://commits.kde.org/flatpak-kcm/bee2879f9ce2e3cadceb91752619f55093dcb192) 
+ FlatpakPermissionModel: Rename method setPerm to togglePermissionAtIndex. [Commit.](http://commits.kde.org/flatpak-kcm/cf9f0c682801d8fa6d8c3d834552b7d2efa0c130) 
+ FlatpakPermission: Fix omitting dummy row for existing but empty groups. [Commit.](http://commits.kde.org/flatpak-kcm/450b89dbec630ecd8bce011b4b22073d67397c9a) 
+ Fix whitespaces. [Commit.](http://commits.kde.org/flatpak-kcm/d0f46239b76153152437756769b2d85f38e22907) 
+ FlatpakPermission: Disable loading environment from user overrides as well. [Commit.](http://commits.kde.org/flatpak-kcm/7e45e4931ea1a84000255c54ae62bf2d2b8bc2d2) See bug [#465502](https://bugs.kde.org/465502)
+ FlatpakPermission: Change the way how we disable environment variables. [Commit.](http://commits.kde.org/flatpak-kcm/25f846b54dbe9a674ec2876b404d53490b4d0a5a) 
+ FlatpakPermission: Rename members, so they make more sense (x2). [Commit.](http://commits.kde.org/flatpak-kcm/9a0be9f7bfcc5b0d3b00f59e6b91f8b452e8cf1e) 
+ FlatpakPermission: Rename members, so they make more sense. [Commit.](http://commits.kde.org/flatpak-kcm/f7fac732018ef23ccc550eb20811273059b4ce9b) 
+ FlatpakPermission: Add more docs. [Commit.](http://commits.kde.org/flatpak-kcm/f7aa7bad3ebdcae797fa088ce5b7f6ac0a1256f0) 
+ FlatpakPermission: Improve QString handling. [Commit.](http://commits.kde.org/flatpak-kcm/717f06bea8ec77829a05ce369250ea1885fb772a) 
+ FlatpakPermission: Mark second constructor as explicit. [Commit.](http://commits.kde.org/flatpak-kcm/813c0569de367ae2aaafd7568cc176b9bfa97ec9) 
+ QML: Drop non-existent argument. [Commit.](http://commits.kde.org/flatpak-kcm/430ee75021176342f68815c029950e5b2c1007c6) 
+ FlatpakPermission: Document enum and members, shuffle then in a better order. [Commit.](http://commits.kde.org/flatpak-kcm/215299b1ca65c1acd29daa4de865ad0f6b07df25) 
+ FlatpakPermission: Fix uninitialized enum variable. [Commit.](http://commits.kde.org/flatpak-kcm/df0e4715accf7f6e0b69254d3bd99b5142fbdeed) 
+ FlatpakPermission: Simplify constructors. [Commit.](http://commits.kde.org/flatpak-kcm/7f441bf154e8e084d21c06e1ae2da829ddb518bc) 
+ Rename one local variable for clarity. [Commit.](http://commits.kde.org/flatpak-kcm/eb21d936577ea2f4fad42aa28c88f45b443c0620) 
+ FlatpakPermission: Drop unused enum value. [Commit.](http://commits.kde.org/flatpak-kcm/a86b93675e27f58179a7b60b17990244e247445e) 
+ Unbreak i18n handling between section headers and backend code. [Commit.](http://commits.kde.org/flatpak-kcm/525e3c442c8b17836613d096212168353deb2b93) 
+ Replace with named constants: "Environment". [Commit.](http://commits.kde.org/flatpak-kcm/544bc3e2fcd85b015d11ab0fbef0b0d61cd62b5c) 
+ Replace with named constants: "System Bus Policy". [Commit.](http://commits.kde.org/flatpak-kcm/1f4737af8e4446a16e464926fa1b2e82226dc124) 
+ Replace with named constants: "Session Bus Policy". [Commit.](http://commits.kde.org/flatpak-kcm/ca7a8f280e4232b8927f2891f6088d7c80285a90) 
+ Replace with named constants: "features". [Commit.](http://commits.kde.org/flatpak-kcm/e23ab2b2e46f9d02ba7e301b0f772f09842fc5af) 
+ Replace with named constants: "devices". [Commit.](http://commits.kde.org/flatpak-kcm/86805b8d0239c5c69fc8d42123be15f4639fbc6a) 
+ Replace with named constants: "shared". [Commit.](http://commits.kde.org/flatpak-kcm/3652fd85660415f0c7862a5d70acbcbc4321d925) 
+ Replace with named constants: "sockets". [Commit.](http://commits.kde.org/flatpak-kcm/213999392e23ae85ad1b83899d8c102ca35dc253) 
+ Replace with named constants: "filesystems". [Commit.](http://commits.kde.org/flatpak-kcm/0f6c8754623a002970cc79baf88a0cd788fbaa5c) 
+ Replace with named constants: "Context". [Commit.](http://commits.kde.org/flatpak-kcm/9c1f4180f3a04583f5440ccef160d41fc45c6079) 
+ Add named constants from flatpak internals. [Commit.](http://commits.kde.org/flatpak-kcm/49eadfa686cb8a93db66af77746157f26b50ed10) 
+ CMake: List header files in sources. [Commit.](http://commits.kde.org/flatpak-kcm/5cd2a7141219eea2ae4eff63cfc866bf4fa1adb0) 
+ Doc: Add note about meaning of FlatpakPermission members. [Commit.](http://commits.kde.org/flatpak-kcm/7c9d3a3490854a0553455c278e17906eabc9a64b) 
+ Add missing i18n mappings for section headers. [Commit.](http://commits.kde.org/flatpak-kcm/0f1694a5431b4235862d8afe76a138876b7a45b8) 
+ Drop unused parameter in permIndex(). [Commit.](http://commits.kde.org/flatpak-kcm/a773e18a972a5dfd6a7efecaa68e993c6a7f8804) 
+ Fix inappropriately localized permission name "per-app-dev-shm". [Commit.](http://commits.kde.org/flatpak-kcm/c108ef2b53918265d113d2d97721f88a3f9ed486) See bug [#465818](https://bugs.kde.org/465818)
+ Fix GLib memory management issue. [Commit.](http://commits.kde.org/flatpak-kcm/f0bf57cdb8436f357d0eedd3a306b9c9487f11cb) 
+ Port from NULL to nullptr. [Commit.](http://commits.kde.org/flatpak-kcm/b4ea85ce701b3cac3f9e997d9cb391a34ff4d4bc) 
+ Avoid duplicating connections between ref and its ref model. [Commit.](http://commits.kde.org/flatpak-kcm/d852e44cb5e4ff7b49ba91921517cd0679431c28) 
+ Expose FlatpakReferencesModel to QML. [Commit.](http://commits.kde.org/flatpak-kcm/7315348966666340557f61bb0f25b3c9dc3a9f29) 
{{< /details >}}

{{< details title="kpipewire" href="https://commits.kde.org/kpipewire" >}}
+ Guard m_producer. [Commit.](http://commits.kde.org/kpipewire/48e51f1f555c883fb3fc3811f31dfa0572a83eb2) 
+ Stream: better fallback for BGR formats when downloading into a QImage. [Commit.](http://commits.kde.org/kpipewire/f481d24f15ccec93415c561c4dbb039f8f7e215c) 
+ Stream: Fix support of SPA_VIDEO_FORMAT_RGB. [Commit.](http://commits.kde.org/kpipewire/de766450e3c655775eefede3d41a2ff6c358289a) 
+ Recording: Drop unnecessary conditional. [Commit.](http://commits.kde.org/kpipewire/13f4ad398ac56f5690a0c30952676f5a070977f0) 
+ Recording: use "good" deadline rather than quality that is deprecated upstream. [Commit.](http://commits.kde.org/kpipewire/0894b7ab346dbc2dfc57a689528d441206463053) 
+ Recording: Make bitrate depend on the stream size. [Commit.](http://commits.kde.org/kpipewire/606d7b9ec6ecdde30c6bb85f0e6b6f6439a032c0) 
{{< /details >}}

{{< details title="KScreen" href="https://commits.kde.org/kscreen" >}}
+ Kded/output: with duplicate edid hashes, use different global config files. [Commit.](http://commits.kde.org/kscreen/8288ea632f344472241ef107732c62757a8af2c9) Fixes bug [#452614](https://bugs.kde.org/452614). Fixes bug [#448599](https://bugs.kde.org/448599)
{{< /details >}}

{{< details title="KWin" href="https://commits.kde.org/kwin" >}}
+ Colordevice: default the simple transformations to 1. [Commit.](http://commits.kde.org/kwin/0e480632bc6cef1715328ec6f5673492378b7658) 
+ Backends/drm: fail commits if nonexistent properties would be set. [Commit.](http://commits.kde.org/kwin/12eef8aee370ff5715098dc09620a0ef846ead04) 
+ Backends/drm: ignore opaque formats for the cursor plane. [Commit.](http://commits.kde.org/kwin/4126ebb7a52c7d12977eea89679227a119c0e4b9) 
+ Forward keymap and modifier change to input method keyboard grab when changed. [Commit.](http://commits.kde.org/kwin/3de1a5817ea82add9f32337f68f21d6c4f7b74bb) 
+ Inputmethod: Show the input method even if it was dismissed. [Commit.](http://commits.kde.org/kwin/8fefef61849c58ca74e50cf09a6fcc15639cf087) Fixes bug [#466969](https://bugs.kde.org/466969)
+ Backends/drm: support CTM for simple color transformations. [Commit.](http://commits.kde.org/kwin/114032d2ac4d2eb9433bf75c0e6a82ecce7fa558) Fixes bug [#455720](https://bugs.kde.org/455720)
+ Xwayland: Prevent potential file descriptor leak. [Commit.](http://commits.kde.org/kwin/0a176ce548442bc88331e4f9627e461d25ba6d9e) 
+ Wayland: Prevent leaking --wayland-fd and --xwayland-fd to child processes. [Commit.](http://commits.kde.org/kwin/f7c3a7459a8864ebbc6ca7ab629ea70ce0fc5c7d) 
+ Helper: Don't leak lock file to kwin_wayland. [Commit.](http://commits.kde.org/kwin/ac8a9fa6d269c3f6af70680dd55eea58d6da3255) 
+ Backends/wayland: Don't leak renderD128 fd. [Commit.](http://commits.kde.org/kwin/e6536441f1ea2ae2fcab2baf497d88401351f7ee) 
+ Backends/wayland: Don't leak WaylandEventThread's pipe fds. [Commit.](http://commits.kde.org/kwin/7197c450a4f7da3636104dadbd4a96c62c4ea8bb) 
+ Fix text-input-v1 compatibility with 111.0.5563.64-1. [Commit.](http://commits.kde.org/kwin/28c99efaf520e0fe65610ca5afc865b1b6527a2f) 
+ Input: Make sure input backends are initialised when the workspace is set up. [Commit.](http://commits.kde.org/kwin/d32dc2ea2456f4d97cbd38381b4272876a7e4657) Fixes bug [#466721](https://bugs.kde.org/466721)
+ Tabbox: Fix grouping windows by application. [Commit.](http://commits.kde.org/kwin/4b0cb60274259d5bec1c487332889bd43dcc87c9) 
+ Scene: Use correct scale when computing world transform. [Commit.](http://commits.kde.org/kwin/86b258d6682a638495a243e6cd6472032ec58462) 
+ Wayland: Fix interactive resize of debug console. [Commit.](http://commits.kde.org/kwin/e3f8b4f06ac81ffaf9849ced1de76c1259dd3875) 
+ Kscreenintegration: read global output data. [Commit.](http://commits.kde.org/kwin/3e664071c61c54b99be417710e67d25aabdf783e) 
+ Workspace: move kscreen integration into separate files. [Commit.](http://commits.kde.org/kwin/d11edd5329f977aa6f5e67b3bca75b67da012b2c) 
+ Screencast: Try harder to be compatible with the pipewire buffer format. [Commit.](http://commits.kde.org/kwin/4ed5638b84220589e43977730641811ec63f341e) 
+ Screencasting: on memfd, skip the QImage step. [Commit.](http://commits.kde.org/kwin/5ac5251674d910e3402885144dc6ce9479a960eb) Fixes bug [#466655](https://bugs.kde.org/466655)
+ TabBox: Avoid unnecesary resets of the client model. [Commit.](http://commits.kde.org/kwin/619e2863d2a8bff9a5e13b0d4cacca4a57872b22) Fixes bug [#466660](https://bugs.kde.org/466660)
+ Wayland: Cancel selections if set without focus. [Commit.](http://commits.kde.org/kwin/4bb1000377ca798544727a7fdae68b8cb4d3ff53) See bug [#459389](https://bugs.kde.org/459389)
+ Windowitem: properly handle sub-subsurfaces. [Commit.](http://commits.kde.org/kwin/83a5ea3dd37173e29af98c152c7b4d2fe0f48bce) Fixes bug [#466747](https://bugs.kde.org/466747)
+ Tabletmodemanager: properly export properties. [Commit.](http://commits.kde.org/kwin/442fd8ba0b7ff2bbf6ab72cc27d4f694e5e7f814) 
+ Enable GLSL for Mali (Lima) / PinePhone devices. [Commit.](http://commits.kde.org/kwin/64d682a646d111a0250bbe3bff77ef0cead91403) 
{{< /details >}}

{{< details title="libkscreen" href="https://commits.kde.org/libkscreen" >}}
+ Libdpms/wayland: Do not create dpms interfaces for placeholder QScreens. [Commit.](http://commits.kde.org/libkscreen/e1f3cb774435d11e8f68192eee17decdc6e2b661) Fixes bug [#466674](https://bugs.kde.org/466674)
+ Dpms/xcb: Make sure we are setting it as unsupported when it is. [Commit.](http://commits.kde.org/libkscreen/5cd9e09fb3075734ece547b65fe4039626891330) Fixes bug [#466181](https://bugs.kde.org/466181)
+ Backends/wayland: Round passed scale. [Commit.](http://commits.kde.org/libkscreen/43481ac90728ade84643dc83da108462e9951830) See bug [#465850](https://bugs.kde.org/465850)
{{< /details >}}

{{< details title="Plasma Desktop" href="https://commits.kde.org/plasma-desktop" >}}
+ Revert "Divide minimum panel size by two when not floating". [Commit.](http://commits.kde.org/plasma-desktop/5eb64073c887ca5b86aafd5324eccb814eba4429) 
+ Divide minimum panel size by two when not floating. [Commit.](http://commits.kde.org/plasma-desktop/6801a4bb4da1428afd3ae0273b43811842af6cf6) 
+ Partly revert "make sure screen numbers are consecutive". [Commit.](http://commits.kde.org/plasma-desktop/ea3ab420547dfbbe861275ca37dbf8e032a787fe) Fixes bug [#464873](https://bugs.kde.org/464873)
{{< /details >}}

{{< details title="plasma-integration" href="https://commits.kde.org/plasma-integration" >}}
+ Revert "extend kio with portal-based open-with implementation". [Commit.](http://commits.kde.org/plasma-integration/1ca959de772e949ca8ab55ed41827ea796be1da8) Fixes bug [#460741](https://bugs.kde.org/460741). See bug [#460985](https://bugs.kde.org/460985)
{{< /details >}}

{{< details title="Plasma Networkmanager (plasma-nm)" href="https://commits.kde.org/plasma-nm" >}}
+ Don't crash when importing VPN config with missing NetworkManager plugin. [Commit.](http://commits.kde.org/plasma-nm/9893c67bd4a7eda7006d5eb7c683e17c1cb49b98) Fixes bug [#465484](https://bugs.kde.org/465484)
+ [kcm] Show VPN import error in the UI. [Commit.](http://commits.kde.org/plasma-nm/9fa48340837a03ad7cb08d4eb23fcc9ca84ab7a2) See bug [#466336](https://bugs.kde.org/466336)
{{< /details >}}

{{< details title="Plasma Audio Volume Control" href="https://commits.kde.org/plasma-pa" >}}
+ Kcm: Fix visuals when testing non-standard channel names. [Commit.](http://commits.kde.org/plasma-pa/90a85cd689193cb82f915058d30e9f53b96c26cc) 
+ Kcm: Fix missing id and implicit parameter signal handler. [Commit.](http://commits.kde.org/plasma-pa/198225942686cfad8813b4c54cb66f3b0c4a7191) Fixes bug [#466075](https://bugs.kde.org/466075)
{{< /details >}}

{{< details title="Plasma Remotecontrollers" href="https://commits.kde.org/plasma-remotecontrollers" >}}
+ Fix registering CEC::cec_logical_address with Qt. [Commit.](http://commits.kde.org/plasma-remotecontrollers/e83590abf19f49c534b005c1f44ba062c5712d52) 
+ Improve recovery from missing input systems. [Commit.](http://commits.kde.org/plasma-remotecontrollers/f470733725a8b3880a5bffa21a59edadef78af24) Fixes bug [#463325](https://bugs.kde.org/463325)
+ Uinput: Fix initial init. [Commit.](http://commits.kde.org/plasma-remotecontrollers/8f0d0fcb9ff6cc778ff19b8400d1476972c5b62a) 
{{< /details >}}

{{< details title="plasma-welcome" href="https://commits.kde.org/plasma-welcome" >}}
+ Set "ShouldShow=false" when quitting the app using amy method. [Commit.](http://commits.kde.org/plasma-welcome/f4c50c5ee61a2d2eaf3511959b20f9fac87489e6) Fixes bug [#466475](https://bugs.kde.org/466475)
{{< /details >}}

{{< details title="Plasma Workspace" href="https://commits.kde.org/plasma-workspace" >}}
+ Klipper: remove duplicate items when loading from history. [Commit.](http://commits.kde.org/plasma-workspace/b34c60956fe858f123dcdde7ee6322b986a795f6) Fixes bug [#466236](https://bugs.kde.org/466236). See bug [#465225](https://bugs.kde.org/465225)
+ Kcms/region_language: set LC_PAPER, not LC_PAGE. [Commit.](http://commits.kde.org/plasma-workspace/27de75798ee19fc1629c99cc13ee08754e0c47bd) Fixes bug [#467269](https://bugs.kde.org/467269). See bug [#466861](https://bugs.kde.org/466861)
+ Screenpool: avoid uniqueConnection with lambda. [Commit.](http://commits.kde.org/plasma-workspace/3164e1367122245dda4a3a4d95cbcb623e8168a2) See bug [#466312](https://bugs.kde.org/466312). See bug [#466020](https://bugs.kde.org/466020)
+ Kcms/fonts: Enable change notifications for base fonts settings (forceFontDPI). [Commit.](http://commits.kde.org/plasma-workspace/89363939ffd762f3964952bc16c143e134a3ab12) 
+ Sddm-theme: Transfer the focus to the text field as we show the OSK. [Commit.](http://commits.kde.org/plasma-workspace/cab6b844422aab0072db840123cc3207b338564b) Fixes bug [#466969](https://bugs.kde.org/466969)
+ Appstreamtest: fix test failure. [Commit.](http://commits.kde.org/plasma-workspace/d93030ef68343bd74168a81c9c712289869bd4e0) 
+ Wallpapers/image: improve efficiency of ImageFinder. [Commit.](http://commits.kde.org/plasma-workspace/9b23a357d2045e679eb997c2d6ce8cbdbaaa55ad) 
+ Klipper: Make action menu Frameless. [Commit.](http://commits.kde.org/plasma-workspace/c1f07ffd30c8df263917d1fbd92ba1f36d7765c0) Fixes bug [#466406](https://bugs.kde.org/466406)
+ Dataengines/mpris2: tolerate non-standards compliant players like mpris-proxy. [Commit.](http://commits.kde.org/plasma-workspace/177cf79ba2e1a055823677a710399b63bb55efb5) Fixes bug [#466288](https://bugs.kde.org/466288)
+ Klipper: History test passes now. [Commit.](http://commits.kde.org/plasma-workspace/4648cd9499ae3f9635b9d8a1ddbdf004c2e77c5c) 
+ Klipper: Insert items before remove. [Commit.](http://commits.kde.org/plasma-workspace/0cbe7da157847e8c74c058a805f407bbefb763c7) Fixes bug [#466041](https://bugs.kde.org/466041)
+ Sddm: Focus something useful when switching between alternative login screens. [Commit.](http://commits.kde.org/plasma-workspace/fd67273f299f340c5e225f5f505c8559a0712066) 
{{< /details >}}

{{< details title="Powerdevil" href="https://commits.kde.org/powerdevil" >}}
+ Suspend by default on AC profile. [Commit.](http://commits.kde.org/powerdevil/734c0753e502245372db8b1eec4645acc0138764) 
+ Use correct tablet mode function to determine mobile-ness. [Commit.](http://commits.kde.org/powerdevil/29d6873af9fdd43ca52ab634a99973036fbbb458) 
{{< /details >}}

{{< details title="xdg-desktop-portal-kde" href="https://commits.kde.org/xdg-desktop-portal-kde" >}}
+ Fix cursor and borders selectors in screenshot dialog. [Commit.](http://commits.kde.org/xdg-desktop-portal-kde/7e0aa86498399662048e93fa7aa2c59bfc097798) 
{{< /details >}}

