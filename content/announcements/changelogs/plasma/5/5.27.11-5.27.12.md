---
title: Plasma 5.27.12 complete changelog
version: 5.27.12
hidden: true
plasma: true
type: fulllog
---
{{< details title="Breeze" href="https://commits.kde.org/breeze" >}}
+ Otherwise plasma-workspace is unhappy. [Commit.](http://commits.kde.org/breeze/578fe3c5d5a42c7ffb2ad51747fdb66837296e32) 
{{< /details >}}

{{< details title="breeze-gtk" href="https://commits.kde.org/breeze-gtk" >}}
+ Scrollbar: Don't use negative margin to counteract border size. [Commit.](http://commits.kde.org/breeze-gtk/54b2d5e8d16477e443cee78dd8afc9790de36b36) 
+ Scrollbar: Split margin between scrollbar and trough. [Commit.](http://commits.kde.org/breeze-gtk/353599c279a22169dd5aeb409d6b222ba357c27a) 
+ Checkbox: Apply indeterminate after checked. [Commit.](http://commits.kde.org/breeze-gtk/e50864cc3fe47bf806546edf59bb3bd2d5671ea8) 
+ Switch to new CI format. [Commit.](http://commits.kde.org/breeze-gtk/e233084f19fd8ad709307a2ed5b19559711efd44) 
{{< /details >}}

{{< details title="Discover" href="https://commits.kde.org/discover" >}}
+ FwupdBackend: Set user agent from client. [Commit.](http://commits.kde.org/discover/dfc07eea5aafd47ea213160c869d90dd9b0d0bba) Fixes bug [#486726](https://bugs.kde.org/486726)
+ Rpm-ostree: Fix version checks for container path. [Commit.](http://commits.kde.org/discover/48adb8ff5058f3d4b9a2b9b36f55449a5dc6179a) 
+ Rpm-ostree: Fix error paths in Transaction. [Commit.](http://commits.kde.org/discover/759593a97b5a881991ad970fd4cad0707e73cd5b) 
{{< /details >}}

{{< details title="kde-cli-tools" href="https://commits.kde.org/kde-cli-tools" >}}
+ Kde-open: show error message when the specified file doesn't exist. [Commit.](http://commits.kde.org/kde-cli-tools/72c2d3301e50743cfcb6b792ebc89c34c7e89647) 
{{< /details >}}

{{< details title="KDE GTK Config" href="https://commits.kde.org/kde-gtk-config" >}}
+ Revert "Round x11GlobalScaleFactor instead of flooring it". [Commit.](http://commits.kde.org/kde-gtk-config/6f324aabdd4f118a81bb5c898689a0151e09d2e0) 
{{< /details >}}

{{< details title="Plasma Desktop" href="https://commits.kde.org/plasma-desktop" >}}
+ Applets/taskmanager: Don't try to find service for application/octet-stream. [Commit.](http://commits.kde.org/plasma-desktop/6d745be0c217cdacad0848d58d5eb43dd3177e51) 
+ Applets/taskmanager: Skip querying KActivitiesStats if recentdocs is disabled. [Commit.](http://commits.kde.org/plasma-desktop/fd6e1829609b28dea95e9aaf20190ea6a19c0e1a) 
+ Kcms: Add Notifiers for kactivitymanagerd_plugins_settings. [Commit.](http://commits.kde.org/plasma-desktop/f1af30dd9504dbdae657c8a2ee84136a20f26c37) 
+ Folder Model: Handle invalid URL in desktop file. [Commit.](http://commits.kde.org/plasma-desktop/20e6c20de00e03ebb1c4cf61246f7fbcd1e3fbbc) Fixes bug [#482889](https://bugs.kde.org/482889)
+ Task Manager: Emit contextualActionsAboutToShow when context menu opens. [Commit.](http://commits.kde.org/plasma-desktop/af23abd9f4ddf82dd80e563b7af2a40826ee7448) 
+ Kcms/mouse: Fix m_dpy check. [Commit.](http://commits.kde.org/plasma-desktop/5e5262bb922c079142de37e19ff816568371dfa4) 
{{< /details >}}

{{< details title="Plasma Workspace" href="https://commits.kde.org/plasma-workspace" >}}
+ Applets/notifications: Be explicit about "when" binding on notificationAction. [Commit.](http://commits.kde.org/plasma-workspace/0623f6d794a85586ee8187b6ff544c98c2c981d7) 
+ Ksmserver: Fix copying of auth data. [Commit.](http://commits.kde.org/plasma-workspace/a336dd70bf51cf4d9be8cf1b66b4819b78850838) Fixes bug [#491130](https://bugs.kde.org/491130)
+ Lookandfeel: Explicitly set theme to Breeze in defaults. [Commit.](http://commits.kde.org/plasma-workspace/1e9651d6bea39ce7391fe6fc34a93366b41c6856) 
+ Dataengines/soliddevice: Remove "file system not responding" notification. [Commit.](http://commits.kde.org/plasma-workspace/5c9fe9100dd102833e564aa05f766094de100824) 
+ Applets/battery: Check actual battery for charge state workaround. [Commit.](http://commits.kde.org/plasma-workspace/f8041a8fe0115a873942d1fa709cc9f0870d0339) 
+ Fix writing ICEAuthority file. [Commit.](http://commits.kde.org/plasma-workspace/1181acfe30557d6646511df8d98d82589878a570) Fixes bug [#487912](https://bugs.kde.org/487912)
+ Remove iceauth dependency. [Commit.](http://commits.kde.org/plasma-workspace/10a7ddd00d6e6bd9eb7414fff0d4f36a8329f649) 
+ Authenticate local clients. [Commit.](http://commits.kde.org/plasma-workspace/e6f05ccc17cc728b1dcb0fd179a495650b513f0c) 
+ Libcolorcorrect: fix reading auto location enabled from config. [Commit.](http://commits.kde.org/plasma-workspace/ed8e8fd95a9420a2fe45fc3d65b25bed7772044b) Fixes bug [#485086](https://bugs.kde.org/485086)
+ Weather/dwd: don't crash on empty json objects. [Commit.](http://commits.kde.org/plasma-workspace/ce1ed1ce55470a68935b192d10ead9f3fd37c360) Fixes bug [#481596](https://bugs.kde.org/481596)
+ [kfontinst] Skip window parenting on Wayland. [Commit.](http://commits.kde.org/plasma-workspace/b23945618230634ae1564af0c5bd5a60d79078c6) Fixes bug [#484273](https://bugs.kde.org/484273)
{{< /details >}}

