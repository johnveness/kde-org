---
title: Plasma 6.1.1 complete changelog
version: 6.1.1
hidden: true
plasma: true
type: fulllog
---

{{< details title="Discover" href="https://commits.kde.org/discover" >}}
+ ApplicationsListPage: move busyIndicator inside container, center it. [Commit.](http://commits.kde.org/discover/ab4e6629877e18420795213abebae3eabf9d9284) Fixes bug [#488267](https://bugs.kde.org/488267)
+ Packagekit: Add support for new action info enum values. [Commit.](http://commits.kde.org/discover/8204a2fd9d0f1b848155bd91670933cb6994c0ae) Fixes bug [#488219](https://bugs.kde.org/488219)
+ KCM: move system updates "apply immediately" radio button into system updates layout. [Commit.](http://commits.kde.org/discover/b65e96d1b18e6a2313bde7fd438ab298aa842a1c) 
{{< /details >}}

{{< details title="Dr Konqi" href="https://commits.kde.org/drkonqi" >}}
+ Sentry: correctly use dbus interface name. [Commit.](http://commits.kde.org/drkonqi/2aeff3c425ed1e118d208a5e7f8af3294443bf7c) Fixes bug [#488555](https://bugs.kde.org/488555)
{{< /details >}}

{{< details title="Plasma Addons" href="https://commits.kde.org/kdeplasma-addons" >}}
+ Effects/cube: reference main delegate. [Commit.](http://commits.kde.org/kdeplasma-addons/90429d44f0957a65faa50b5cce23ddf9f359075f) Fixes bug [#488654](https://bugs.kde.org/488654)
{{< /details >}}

{{< details title="krdp" href="https://commits.kde.org/krdp" >}}
+ Ensure WinPR version matches FreeRDP version. [Commit.](http://commits.kde.org/krdp/d2e49c9904897d418930d2e014ad1c4d86df93c4) 
+ SessionController: add missing include for flatpak build. [Commit.](http://commits.kde.org/krdp/f184d8793d36aac408d4cebc5cdb980b5b43929e) 
+ Kcmkrdpserver: if properties change when server settings are open, check server running. [Commit.](http://commits.kde.org/krdp/06249cab9e166d62ed2f3c2025113960ce2d8e0f) Fixes bug [#488360](https://bugs.kde.org/488360)
+ SessionController: instead of using default quit action, implement our own. [Commit.](http://commits.kde.org/krdp/dd956081c8a3158366cc99410b70e6af2344276b) Fixes bug [#488359](https://bugs.kde.org/488359). Fixes bug [#488362](https://bugs.kde.org/488362)
+ PortalSession: If portal is closed, raise error. [Commit.](http://commits.kde.org/krdp/b98b334afdd710137ff3171e6b6a7e8c5b868a85) 
+ SessionController: remove SNI status; keep it always passive. [Commit.](http://commits.kde.org/krdp/e778e26588862e1cda0f40e70f92c21481b70675) Fixes bug [#488365](https://bugs.kde.org/488365)
{{< /details >}}

{{< details title="kscreenlocker" href="https://commits.kde.org/kscreenlocker" >}}
+ Greeter: Fix Shader Wallpaper plugin and possibly others. [Commit.](http://commits.kde.org/kscreenlocker/82db40779730d284bcf474e3f23dfc0573387bb6) 
+ Ksldapp: add a signal for when the greeter is (re)started. [Commit.](http://commits.kde.org/kscreenlocker/34c6ff3133d1eede13abd09700308ef5b94012a0) 
+ Greeter: don't start authenticating during the grace lock time. [Commit.](http://commits.kde.org/kscreenlocker/62bdb37a910866b39420d7111d522227519f670e) Fixes bug [#476567](https://bugs.kde.org/476567)
{{< /details >}}

{{< details title="KWayland" href="https://commits.kde.org/kwayland" >}}
+ Raise required plasma-wayland-procotols minimum version to 1.13. [Commit.](http://commits.kde.org/kwayland/58acf75be5514fd7d02d5dc59a7a3c4359883974) 
{{< /details >}}

{{< details title="KWin" href="https://commits.kde.org/kwin" >}}
+ Opengl/eglcontext: bail out early if we can't use complex shaders. [Commit.](http://commits.kde.org/kwin/b819606a740e44cf689781175fab7cf7731f162c) Fixes bug [#482868](https://bugs.kde.org/482868)
+ Scripting: Add a temporary workaround to fix build without global shortcuts. [Commit.](http://commits.kde.org/kwin/56dd3b6e56e58e60a026f3e6919d13dc60a4fa27) 
+ Scripting: Port gesture handlers to native gesture apis. [Commit.](http://commits.kde.org/kwin/8daf620cb521c8f9e69f4209750455e6ce82d769) 
+ Scene: Drop ItemRendererOpenGL::RenderNode::scale. [Commit.](http://commits.kde.org/kwin/ab94922e329f16f7c922abf894f21e0be8b23ad7) 
+ Tiling: Add fallback path for the first Polonium tile. [Commit.](http://commits.kde.org/kwin/7578f70b404acbceb53b057e8c483926badf7e21) Fixes bug [#488898](https://bugs.kde.org/488898)
+ Plugins/shakecursor: Harden cursor theme loading logic. [Commit.](http://commits.kde.org/kwin/04113ac0cfbf722d49cc3f8dfca348f2f7ac9157) 
+ Utils: Fix XCURSOR_PATH envvar parsing. [Commit.](http://commits.kde.org/kwin/d53689ed96ced527d9fd0bd5030c30e41364b2a7) 
+ Plugins/shakecursor: Include high resolution breeze cursor themes. [Commit.](http://commits.kde.org/kwin/78658d02ffa47aae4d670fffc4a5112abc7f0b0a) 
+ Utils: Allow specifying XCursor theme search paths. [Commit.](http://commits.kde.org/kwin/0754376c68c32570f2e113febb6e34f6c4006de2) 
+ Plugins/shakecursor: Display default cursor shape. [Commit.](http://commits.kde.org/kwin/d8b98ea98b1f2dd90ac3bbe7155a22e2141a6947) 
+ Make FocusChain ignore closed windows. [Commit.](http://commits.kde.org/kwin/a4313d6c4e7179a46964b5e0295fd808c53191c9) 
+ Opengl: Remove code that prints gl platform details. [Commit.](http://commits.kde.org/kwin/ccb651effecba8c9f85f347893dc059e30cf7b51) Fixes bug [#489000](https://bugs.kde.org/489000)
+ Core/renderloop: also log the predicted render time. [Commit.](http://commits.kde.org/kwin/9e01058663f6b79a720ec9e866ecc36c6e9520d1) 
+ Plugins/screencast: test creating a dmabuf with the real modifier list. [Commit.](http://commits.kde.org/kwin/bc677cdbe94cf43a8df3006cc33035a21b0285b9) 
+ Core/renderloop: log frame statistics into a file. [Commit.](http://commits.kde.org/kwin/060db4cc7c30451522a595d3f6ce6f320409f133) See bug [#488843](https://bugs.kde.org/488843)
+ Plugins/slidingpopups: adopt input panels from when they're added. [Commit.](http://commits.kde.org/kwin/3cb7642fed79b322c25b198caa943a799770d2b1) 
+ Inputpanelv1window: polish window states. [Commit.](http://commits.kde.org/kwin/c339df8d66f86d4b7b726e4f400094e33b150884) 
+ Backends/drm: Fix DrmCrtc::queryCurrentMode() accidentally resetting m_crtc to null. [Commit.](http://commits.kde.org/kwin/a95716c51e404bdfd8eb38e5cd4487e41a4b2c10) 
+ Plugins/glide: Subdivide window quad grid. [Commit.](http://commits.kde.org/kwin/24c9ed3bde8a7830e43a2527d05e9099e5e138bd) Fixes bug [#488840](https://bugs.kde.org/488840)
+ Plugins/zoom: do colorspace conversions between the screen textures. [Commit.](http://commits.kde.org/kwin/2cc826067a4058c8c04b5f79f7f5c75933ec250e) Fixes bug [#488839](https://bugs.kde.org/488839)
+ Add closed window guards in X11Window::doSetXYZ() methods. [Commit.](http://commits.kde.org/kwin/adcb15c4de70eb80b9649631f60822736a097855) 
+ Plugins/shakecursor: Ignore animation speed. [Commit.](http://commits.kde.org/kwin/64f1455b1153214e9825953609bd3b63bfd9c74a) Fixes bug [#488813](https://bugs.kde.org/488813)
+ Wayland_server: create a new screen locker connection when the greeter gets restarted. [Commit.](http://commits.kde.org/kwin/b90a5346dec869f1ba9aea3974579465a881360e) 
+ Plugins/eis: Make input capture activation ids unsigned. [Commit.](http://commits.kde.org/kwin/b33b6c19356a8516e702a6d1b1b8b5b0beed8315) 
+ Autotests: add Xwayland scale changes to the output changes test. [Commit.](http://commits.kde.org/kwin/ed1bb6c6f0cecba01d15b75b212fdc839b2756c4) See bug [#487409](https://bugs.kde.org/487409)
+ Workspace: also update xwayland scale when not changing the output order. [Commit.](http://commits.kde.org/kwin/c0e3b67107c64849d60097ac3b0ec66bda6eaf2f) 
+ Sync xwayland eavesdropping default in kwin.kcfg. [Commit.](http://commits.kde.org/kwin/fd1de65a8509dc9a1edc97aad410d11c9382b7d3) 
+ Remove code that updates the focus chain in Window::setSkipTaskbar(). [Commit.](http://commits.kde.org/kwin/5d94f3a99b84f396d34344fa40ac4f130c795929) 
+ Get rid of extra string allocations in src/inputmethod.cpp. [Commit.](http://commits.kde.org/kwin/2abba236baf3f1f85f18a71b768bf850ac83ff59) 
+ Wayland: Fix buffer ref'ing in org_kde_kwin_shadow.commit. [Commit.](http://commits.kde.org/kwin/28fd18e6422c79d9d759ef8964fd4b0c99a79426) 
+ Backends/drm: don't do direct scanout when HDR brightness isn't 1. [Commit.](http://commits.kde.org/kwin/a095407e7b414ae3662a939f2c431c9436551ee0) 
+ Plugins/nightlight: Guard against invalid timings in the config. [Commit.](http://commits.kde.org/kwin/f74d75d1e027b8802dade1349e9a8450a52b53d6) 
+ Plugins/nightlight: Remove premature optimization in updateTransitionTimings(). [Commit.](http://commits.kde.org/kwin/61ad593b9d447a8e78f52f50e4b6873714932084) 
+ Plugins/screencast: Check compositing type. [Commit.](http://commits.kde.org/kwin/c5bd3e059ea3208b88664519b79addda00fc6c84) 
+ Plugins/nightlight: Make Night Light more robust to QTimer firing slightly earlier. [Commit.](http://commits.kde.org/kwin/4aa024cdb204c03bf86ef11657159e2f36796a6e) 
+ Make Window::closeWindow() noop if the window is already closed. [Commit.](http://commits.kde.org/kwin/cc741eaf4d2b81f97a52baf7f99e3e0c2e49514b) 
+ Also wake up screens on tablet interactions. [Commit.](http://commits.kde.org/kwin/6745be3092882ea8dff05814217445a66ef6bd80) Fixes bug [#451359](https://bugs.kde.org/451359)
+ Plugins/nightlight: Ensure the target temperature remains within reasonable bounds. [Commit.](http://commits.kde.org/kwin/94d5925629e99c767cb71e55cbf9ba1f81e76a92) 
+ Avoid sending X11 sync request if new logical geometry doesn't change the device geometry. [Commit.](http://commits.kde.org/kwin/83fc98239120b544573b6c07084325c86e041dff) Fixes bug [#488223](https://bugs.kde.org/488223)
+ Autotests: Add a missing mock definition of Xcb::toXNative(QRectF). [Commit.](http://commits.kde.org/kwin/cb5dd01c0d32849e05a5ad1ebd86bcca152bbefd) 
{{< /details >}}

{{< details title="libkscreen" href="https://commits.kde.org/libkscreen" >}}
+ Cmake: increase minimum plasma wayland protocols version to 1.13. [Commit.](http://commits.kde.org/libkscreen/b00d21942a4fa02fefe36500dbaf50b29bcff362) 
{{< /details >}}

{{< details title="libplasma" href="https://commits.kde.org/libplasma" >}}
+ Plasma/pluginloader: Repair loading plasmoid plugins on add from explorer. [Commit.](http://commits.kde.org/libplasma/fd8ae40c995b7e9c1fa2e2ec6a1a4437beabccde) Fixes bug [#488592](https://bugs.kde.org/488592)
+ Don't delete containment if appletDelete is on one of children. [Commit.](http://commits.kde.org/libplasma/4c3541078eb41f56cdf4d88a397b4218dd7c2762) 
+ Don't double delete PlasmoidItems. [Commit.](http://commits.kde.org/libplasma/c4fecb5f28f3758b496ad4ac4577f8808a339b23) 
+ Init at componentComplete. [Commit.](http://commits.kde.org/libplasma/ec4065e23f45513d133d2da2c07164ae99d49168) 
{{< /details >}}

{{< details title="Plasma Desktop" href="https://commits.kde.org/plasma-desktop" >}}
+ Do not hide panel edit mode dialog when Widget Explorer is closed. [Commit.](http://commits.kde.org/plasma-desktop/9cc78130ca833bac58b3039ef65f2236679349fa) See bug [#488571](https://bugs.kde.org/488571)
+ Widget Explorer: ask for focus after drag and drop. [Commit.](http://commits.kde.org/plasma-desktop/6b7a54ba8434e0872c5b7a045a6ae4f7847e1f22) Fixes bug [#488571](https://bugs.kde.org/488571)
+ Applets/taskmanager: Properly size Task icons. [Commit.](http://commits.kde.org/plasma-desktop/800b9c47936acc3e29f60754639884a8263be831) Fixes bug [#488733](https://bugs.kde.org/488733)
+ Read "padding" instead of "margin" of dialog. [Commit.](http://commits.kde.org/plasma-desktop/eb28771ffcd27579d6f467ac7541fca7dc514cdd) Fixes bug [#488901](https://bugs.kde.org/488901)
+ [kcms/keyboard] tastenbrett: more correct handling of xkb_keysym_*. [Commit.](http://commits.kde.org/plasma-desktop/8d4d352d06959fa30691a7ba8bcd3583142cbf7c) 
+ Applets/taskmanager: Fix grid layout for forced multiple rows/columns. [Commit.](http://commits.kde.org/plasma-desktop/9b85bbab9227c6a988d4293e64f67be04067e2ea) Fixes bug [#488702](https://bugs.kde.org/488702)
+ Panel: fix adaptive opacity not reacting to maximized windows. [Commit.](http://commits.kde.org/plasma-desktop/dc62acf008ef0a48f00d57f6d163663a73edff7d) Fixes bug [#488736](https://bugs.kde.org/488736)
+ Use the proper translation domain. [Commit.](http://commits.kde.org/plasma-desktop/ea61045e528ee5d551eeb6ec14443bca8b52bd89) 
+ Sddm-theme: Make the thing translatable. [Commit.](http://commits.kde.org/plasma-desktop/e62e62b78d630c85ac90275653eb6a6573f1f8bc) 
+ Applets/kickoff: support styled text in label again. [Commit.](http://commits.kde.org/plasma-desktop/c974dee83048f21a2b7df52ce116c181bca3787e) 
+ Revert "Moved old wallpaper, added new one, updated previews". [Commit.](http://commits.kde.org/plasma-desktop/a79c3605489fa7a7eb3b0b87cc6aa8d1ec71b49d) 
+ Moved old wallpaper, added new one, updated previews. [Commit.](http://commits.kde.org/plasma-desktop/14145eb401d6b6bf1f11b034ad9c65910216b342) 
+ FolderView Positioner: fix dragging files across screens. [Commit.](http://commits.kde.org/plasma-desktop/97794807a9a974ba32ee160665f0b69795888268) Fixes bug [#481254](https://bugs.kde.org/481254). Fixes bug [#466337](https://bugs.kde.org/466337)
{{< /details >}}

{{< details title="plasma-mobile" href="https://commits.kde.org/plasma-mobile" >}}
+ Volumeosd: Fix missing import. [Commit.](http://commits.kde.org/plasma-mobile/af9229e76e5ac9855b807b87a3deeffb80854f9e) 
+ Statusbar: Fix volume indicator not being visible. [Commit.](http://commits.kde.org/plasma-mobile/ab5972856df0dd6ad466ba8042fb5c5309298679) 
+ Homescreens/folio: Fix favourites delegate dragging. [Commit.](http://commits.kde.org/plasma-mobile/419b9f81b3da131264a159c90541856a44447d5e) 
+ Homescreens/folio: Port away from singletons to support multi-display. [Commit.](http://commits.kde.org/plasma-mobile/1c88bde4d1c491561fd2d816a9745c1b167f5424) 
+ Fix: code error. [Commit.](http://commits.kde.org/plasma-mobile/286cfcc0bff1aaada98050151c595540b33ff3f1) 
+ Fix:  Changing the power settings does not take effect in time. [Commit.](http://commits.kde.org/plasma-mobile/10e5bea18d82796d581f1e04fe8f9182646036d0) 
+ Add setInitialized(true) after set ipv6 settings. [Commit.](http://commits.kde.org/plasma-mobile/d56caf565bf2aa86a8c69389c0b7252beb9b0ff0) 
+ Force to set ipv6 method to auto. [Commit.](http://commits.kde.org/plasma-mobile/3726bbcae912e3a935c8432b423271f1a6fd3a0a) 
{{< /details >}}

{{< details title="Plasma Audio Volume Control" href="https://commits.kde.org/plasma-pa" >}}
+ Don't install plasma-volume.so symlink. [Commit.](http://commits.kde.org/plasma-pa/bfbf5dd89dbaf43248e7d7e901c4c8a0c8d9b9de) 
{{< /details >}}

{{< details title="Plasma Systemmonitor" href="https://commits.kde.org/plasma-systemmonitor" >}}
+ EditablePage: position loading placeholder overlay correctly. [Commit.](http://commits.kde.org/plasma-systemmonitor/eee81c58dc0169753ef17a68c663600685c701f6) Fixes bug [#488044](https://bugs.kde.org/488044)
{{< /details >}}

{{< details title="plasma-welcome" href="https://commits.kde.org/plasma-welcome" >}}
+ Remove dup appstream versions. [Commit.](http://commits.kde.org/plasma-welcome/991cce9863b4b3c73364a0415d7bfda436b1fef4) 
+ Use snap:// URLs rather than appstream:// ones on snap only distros. [Commit.](http://commits.kde.org/plasma-welcome/c23401051d5f553989ff92d6ed01c1d187451fdc) 
+ Use non-standard UBUNTU_VARIANT key to detect snap only distro. [Commit.](http://commits.kde.org/plasma-welcome/bb22f8fe67b1336f19b2100ed13015e5ba02f311 
{{< /details >}}

{{< details title="Plasma Workspace" href="https://commits.kde.org/plasma-workspace" >}}
+ Don't emit appletDeleted 2 times. [Commit.](http://commits.kde.org/plasma-workspace/4127573d584cccb13750da668229332da867f0af) 
+ Applets/systemtray: Read "padding" instead of "margin" of dialog. [Commit.](http://commits.kde.org/plasma-workspace/82b5880155db8b3c6e9b0caa17aad59bf61b6926) See bug [#488901](https://bugs.kde.org/488901)
+ Mapping for mouse events considering transforms. [Commit.](http://commits.kde.org/plasma-workspace/dfd60e1db7be938096613ec8ea30887f0a537981) Fixes bug [#488173](https://bugs.kde.org/488173)
+ Enforce formfactor when changing location. [Commit.](http://commits.kde.org/plasma-workspace/69d12b716d3cdb06a3cf23630ea4493f0e13a30c) Fixes bug [#488174](https://bugs.kde.org/488174)
+ Kcm_regionandlang: fix incorrect paper size example for some locales. [Commit.](http://commits.kde.org/plasma-workspace/fb7f8557cc14a190f0ef5f8dd0c166b224446cc3) Fixes bug [#488083](https://bugs.kde.org/488083)
+ Revert "Moved old wallpaper, added new one, updated previews". [Commit.](http://commits.kde.org/plasma-workspace/dafa342425ec880bc15d0d847fabb95cf090e0a3) 
+ Moved old wallpaper, added new one, updated previews. [Commit.](http://commits.kde.org/plasma-workspace/5995f1140add216e06c1d09833c4860fa49ecde2) 
+ Shell/kconf_update: don't remove shortcut added in 6.1. [Commit.](http://commits.kde.org/plasma-workspace/21856807387cb4be21f996f618021e35325b6209) Fixes bug [#488494](https://bugs.kde.org/488494)
+ Revert "applets/systemtray: Fix shortcut activation for hidden applets". [Commit.](http://commits.kde.org/plasma-workspace/8cc8d8dd0707e62671d43f880a4a6a576476aaa0) 
+ Applets/systemtray: Fix shortcut activation for hidden applets. [Commit.](http://commits.kde.org/plasma-workspace/63e572e923a4684edb7e90c5ea7eb02d967b39fb) Fixes bug [#480173](https://bugs.kde.org/480173)
{{< /details >}}

{{< details title="plasma-workspace-wallpapers" href="https://commits.kde.org/plasma-workspace-wallpapers" >}}
+ Revert "Moved old wallpaper, added new one, updated previews". [Commit.](http://commits.kde.org/plasma-workspace-wallpapers/7fb12e1d8b9e50d0757252320cabd953425cbb1a) 
+ Moved old wallpaper, added new one, updated previews. [Commit.](http://commits.kde.org/plasma-workspace-wallpapers/0734c232392fe1b899c74c72241156605c01366a) 
{{< /details >}}

{{< details title="Powerdevil" href="https://commits.kde.org/powerdevil" >}}
+ Fix initial UI state of Battery protection setting. [Commit.](http://commits.kde.org/powerdevil/7da820f5ba64766b15c1451ad0fbed3b714ed307) Fixes bug [#488954](https://bugs.kde.org/488954)
+ Daemon: Limit KAuth backlighthelper calls to only one at a time. [Commit.](http://commits.kde.org/powerdevil/1017585fe5ef2263e02e6dc6f5227de15fb3ba5d) Fixes bug [#481003](https://bugs.kde.org/481003). See bug [#470106](https://bugs.kde.org/470106)
+ Skip ddcutil initialization when POWERDEVIL_NO_DDCUTIL is set. [Commit.](http://commits.kde.org/powerdevil/1aa9ea47962c5c46a960c55b45f05235fb32ca10) See bug [#488373](https://bugs.kde.org/488373)
{{< /details >}}

{{< details title="System Settings" href="https://commits.kde.org/systemsettings" >}}
+ Remove dup appstream version. [Commit.](http://commits.kde.org/systemsettings/0993e98d1b9c00bed2de6172364a269ce450d037) 
{{< /details >}}

{{< details title="wacomtablet" href="https://commits.kde.org/wacomtablet" >}}
+ Remove dup appstream version. [Commit.](http://commits.kde.org/wacomtablet/69288f3d478203ec1d193d66a30589e36b2b0903) 
{{< /details >}}

{{< details title="xdg-desktop-portal-kde" href="https://commits.kde.org/xdg-desktop-portal-kde" >}}
+ InputCapture: Fix retrieving of cursor position out of QVariantMap. [Commit.](http://commits.kde.org/xdg-desktop-portal-kde/fc266385176b462111afb098d0a670a1a877e16f) 
+ InputCapture: activation_id needs to be unsigned. [Commit.](http://commits.kde.org/xdg-desktop-portal-kde/e7c58b0d687fcdaacfed4cf1321ed4fb439cafd2) 
{{< /details >}}

