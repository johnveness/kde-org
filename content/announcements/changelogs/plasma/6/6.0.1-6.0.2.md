---
title: Plasma 6.0.2 complete changelog
version: 6.0.2
hidden: true
plasma: true
type: fulllog
---

{{< details title="Breeze" href="https://commits.kde.org/breeze" >}}
+ Remove scrollbar groove completely. [Commit.](http://commits.kde.org/breeze/85bca858bf6d7be5d238555b7f2eb63bf8f018ca) 
{{< /details >}}

{{< details title="breeze-gtk" href="https://commits.kde.org/breeze-gtk" >}}
+ Fix ecm dep version. [Commit.](http://commits.kde.org/breeze-gtk/ed18a0f1a89d2216d3f1ad4ed60ad20d50870ed5) 
+ Bump version ahead of next release. [Commit.](http://commits.kde.org/breeze-gtk/f9be11ef9f9cd7f412db5eb0801d236ecad5cc84) 
+ Respin 6.0.1 tar. [Commit.](http://commits.kde.org/breeze-gtk/b32647d04e470c78a7293f13f57af98fa6ef86b9) 
+ Don't require a non-existent ECM version. [Commit.](http://commits.kde.org/breeze-gtk/71013dcd82466e2958ed8945b7a7bc276084e0fd) 
{{< /details >}}

{{< details title="breeze-plymouth" href="https://commits.kde.org/breeze-plymouth" >}}
+ Fix ecm version. [Commit.](http://commits.kde.org/breeze-plymouth/c43ce0672b89f11fa751a0a4623498ad83526e4d) 
+ Bump version ahead of next release. [Commit.](http://commits.kde.org/breeze-plymouth/d985ca7c7872e7190a1c0e78167624a7e534a816) 
+ Fix ecm dep version. [Commit.](http://commits.kde.org/breeze-plymouth/a419191c3132c3f35011ad3920045f8b36a5e7bf) 
{{< /details >}}

{{< details title="Discover" href="https://commits.kde.org/discover" >}}
+ Fix appstream warnign url-homepage-missing. [Commit.](http://commits.kde.org/discover/6bb74f3bafea3737eaae35352b9c73f6fad03fcb) 
+ Kns: Make sure we don't emit twice the same resource. [Commit.](http://commits.kde.org/discover/6e64778eb517ed3fbd0ec440f4ed281443fe268e) Fixes bug [#482095](https://bugs.kde.org/482095)
{{< /details >}}

{{< details title="Plasma Addons" href="https://commits.kde.org/kdeplasma-addons" >}}
+ Runners/converter: start timer in main thread. [Commit.](http://commits.kde.org/kdeplasma-addons/48d44d13f488096ad5b8df54402751b399338e42) 
+ Kdeplasma-addons: add Website to metadata to fix appstreamtest. [Commit.](http://commits.kde.org/kdeplasma-addons/48f89754b6d764e85435142a04e58cdee78dfac5) 
+ Fix set_package_properties to the match find_package call. [Commit.](http://commits.kde.org/kdeplasma-addons/f636c25490ec943d0092591c6d070fc56a6dc98f) 
{{< /details >}}

{{< details title="kglobalacceld" href="https://commits.kde.org/kglobalacceld" >}}
+ Include all deprecated functions by default. [Commit.](http://commits.kde.org/kglobalacceld/0ba7196bafc4caf57c1845616a127c828149f0dc) Fixes bug [#482712](https://bugs.kde.org/482712)
{{< /details >}}

{{< details title="KMenuEdit" href="https://commits.kde.org/kmenuedit" >}}
+ Add URL to the appstream metadata. [Commit.](http://commits.kde.org/kmenuedit/4a628f6bb7b5d5e3d2ef9080529abe2b9bdcda2a) 
+ Move cmake_minimum_required to the top of CMakeLists.txt. [Commit.](http://commits.kde.org/kmenuedit/10008d08d935c073ed09aff375ae80f0f1c0b583) 
{{< /details >}}

{{< details title="kpipewire" href="https://commits.kde.org/kpipewire" >}}
+ Fix version. [Commit.](http://commits.kde.org/kpipewire/e337c4cedbc4b157b5970a9b525305417a8c3203) 
+ Bump version ahead of new release. [Commit.](http://commits.kde.org/kpipewire/2624495f5d2f92f9f314f8ae2e140fe71d0dc55b) 
+ Cmake: Add missing " to unbreak the build. [Commit.](http://commits.kde.org/kpipewire/df052bfa3c66d24109f40f18266ee057d1838b9b) 
{{< /details >}}

{{< details title="KScreen" href="https://commits.kde.org/kscreen" >}}
+ KCM: Make sure HDR UI elements aren't shown when HDR isn't available. [Commit.](http://commits.kde.org/kscreen/9780e2c9cb5669d5816985eb520cfeb5e750a769) Fixes bug [#482748](https://bugs.kde.org/482748)
{{< /details >}}

{{< details title="KWin" href="https://commits.kde.org/kwin" >}}
+ Plugins/screencast: fix the cursor being offset after changing the scale. [Commit.](http://commits.kde.org/kwin/e83e8503d6fa958199549b6507cf5e2e7415a6f7) 
+ Xkb: fix testing if on keypad. [Commit.](http://commits.kde.org/kwin/05ad71e9fcbab171112dfde70dc07d14285d5398) 
+ Wayland: Only send artificial mouse up events for xwayland drags. [Commit.](http://commits.kde.org/kwin/783a63e5941bf183bd284b041967580ceb44840b) 
+ Wayland: Only partially revert send pointer leave on drag. [Commit.](http://commits.kde.org/kwin/61f65ce98d5dd0e8956a06f9f5f88a1aa0592f5f) 
+ Properly intersect the shape with clipRect. [Commit.](http://commits.kde.org/kwin/29bfd3558543ffe2623750efc44bccf855d90e3e) 
+ Core/colorspace: fix ColorDescription comparisons. [Commit.](http://commits.kde.org/kwin/d549fee89eeb1d0d881de8973bc586bbd7d14c7f) Fixes bug [#482809](https://bugs.kde.org/482809)
+ Utils/xcbutils: Don't call toXNative with unsigned integer. [Commit.](http://commits.kde.org/kwin/369c3cbd7d843b6ce2901adf8b464158f84759a1) Fixes bug [#482687](https://bugs.kde.org/482687)
+ Backends/drm: handle dumb buffer target correctly. [Commit.](http://commits.kde.org/kwin/cc7d71047688bc432fce8747b262510b4fadf624) Fixes bug [#482859](https://bugs.kde.org/482859)
+ Port IdleDetector to QBasicTimer. [Commit.](http://commits.kde.org/kwin/aef8796c723e8ec40c19580b2147708aacfb59b0) See bug [#482077](https://bugs.kde.org/482077)
+ Add timeout assert in IdleDetector. [Commit.](http://commits.kde.org/kwin/18882eefdecf638e9421116b639bb16a2fca8ce3) See bug [#482077](https://bugs.kde.org/482077)
+ Xdgshellwindow: Always update window position and size along all axes when fully miximizing window. [Commit.](http://commits.kde.org/kwin/ab8345a24cc4276099143942776ae5dacbcfda00) Fixes bug [#482086](https://bugs.kde.org/482086)
+ Fix sending window to all desktops. [Commit.](http://commits.kde.org/kwin/dbf1edcc4185819aae95fcb3d078574ce2019f67) Fixes bug [#482670](https://bugs.kde.org/482670)
+ Backends/drm: also set legacy gamma after VT switches. [Commit.](http://commits.kde.org/kwin/673f9a0ac54444f4494894d2da6b6554324912e1) 
+ Backends/drm: don't set gamma with legacy unless really necessary. [Commit.](http://commits.kde.org/kwin/4a78a5417b7f641a60eb7d6aa5ec8311a5c4950c) 
+ Backends/drm: ignore ctm support on legacy. [Commit.](http://commits.kde.org/kwin/29838e1e5bf7514718b77ff4e666b846c0f2d320) Fixes bug [#482143](https://bugs.kde.org/482143)
+ Rename Workspace::updateClientArea as Workspace::rearrange. [Commit.](http://commits.kde.org/kwin/6d5a0ed4d26d520697f83781e149b713ea4c111b) See bug [#482361](https://bugs.kde.org/482361)
+ Wayland: Fix windows shrinking when output layout changes. [Commit.](http://commits.kde.org/kwin/b3925a94600d297bbe85268cf8e85b0993de0471) See bug [#482361](https://bugs.kde.org/482361)
+ X11window: Skip strict geometry checks in isFullScreenable(). [Commit.](http://commits.kde.org/kwin/7db7c97e410c796ec370c3dc12c45ef29bf77a14) 
+ Xwayland: Use correct key for key release events. [Commit.](http://commits.kde.org/kwin/9e4d7e54c5de56e4778feb35ec027f08f86ce144) 
+ Xwayland: Send to xwayland even when no window is focussed. [Commit.](http://commits.kde.org/kwin/18e6256dd6a3c97b57180319edbe5f8559892556) Fixes bug [#478705](https://bugs.kde.org/478705)
{{< /details >}}

{{< details title="libplasma" href="https://commits.kde.org/libplasma" >}}
+ ContainmentItem: set correct constraints when wallpaper is ready. [Commit.](http://commits.kde.org/libplasma/ccd45f0434294b12ca342505f753dd1976d7260e) Fixes bug [#482267](https://bugs.kde.org/482267)
+ ContainmentItem: report correct isLoading status. [Commit.](http://commits.kde.org/libplasma/7180113a873d340bdb8652edf14d82042d1d9306) 
+ Remove old focus workaround. [Commit.](http://commits.kde.org/libplasma/800ab725a6e72621ca81e0952d951b5a5ed1061a) Fixes bug [#481967](https://bugs.kde.org/481967)
{{< /details >}}

{{< details title="Milou" href="https://commits.kde.org/milou" >}}
+ Resultsmodel: Read plasma settings for KRunner favorites. [Commit.](http://commits.kde.org/milou/8e3252e338d4eb6a2f4848d74a434a0c70b76878) Fixes bug [#480750](https://bugs.kde.org/480750)
{{< /details >}}


{{< details title="Plasma Desktop" href="https://commits.kde.org/plasma-desktop" >}}
+ Filter out enter. [Commit.](http://commits.kde.org/plasma-desktop/16123dc4df00409c9d542ca8539c233ddc1e3505) Fixes bug [#482123](https://bugs.kde.org/482123)
+ Appiumtests: match screenshot to test wallpaper DBus API. [Commit.](http://commits.kde.org/plasma-desktop/6ddc106ca7f86045328d7638bda16cc0aeb8a54e) 
+ Kcm/keys: fix custom shortcut alignment. [Commit.](http://commits.kde.org/plasma-desktop/8b37a2e192a319186d0db47b4e7655bbef06aabc) Fixes bug [#483169](https://bugs.kde.org/483169)
+ Panel: Don't take keyboard focus when one of its widgets is activated. [Commit.](http://commits.kde.org/plasma-desktop/a98f5f84098723a8c0dffeeb4de75f6f240a3671) Fixes bug [#479084](https://bugs.kde.org/479084). Fixes bug [#482653](https://bugs.kde.org/482653)
+ Appiumtests: add test to make sure ShellCorona gives ready signal to KSplash. [Commit.](http://commits.kde.org/plasma-desktop/6ab0f22e76f3cd6af56988d63b6a8e51725b37b8) See bug [#482267](https://bugs.kde.org/482267)
+ Folder Model: Handle invalid URL in desktop file. [Commit.](http://commits.kde.org/plasma-desktop/0264726f8720d3093bd3ba10f6107197b4f90be3) Fixes bug [#482889](https://bugs.kde.org/482889)
+ Kcms/landingpage: load theme preview image asynchronously. [Commit.](http://commits.kde.org/plasma-desktop/6d8fa65953f4b564ad3372e777935b7d296260e2) 
+ Rework logic to know who is the first visible results view. [Commit.](http://commits.kde.org/plasma-desktop/f9f87a3c89c6082e8171c4e87df01fe8df8280c9) Fixes bug [#482736](https://bugs.kde.org/482736)
+ Do not let ConfigOverlay cover the toolbox. [Commit.](http://commits.kde.org/plasma-desktop/f5ad2bca024e92c54c11ddf5ed8142e6a88d09fa) Fixes bug [#482218](https://bugs.kde.org/482218)
+ Kcms/mouse: Don't crash when no supported backend is found. [Commit.](http://commits.kde.org/plasma-desktop/4a05fc95cb431562270db9acab6370202e329c15) Fixes bug [#482048](https://bugs.kde.org/482048)
+ Don't overwrite files in the model positions on drop. [Commit.](http://commits.kde.org/plasma-desktop/2d56a5a01fdc25d381784ae5c6f3c88674c568d5) Fixes bug [#482302](https://bugs.kde.org/482302)
{{< /details >}}

{{< details title="plasma-integration" href="https://commits.kde.org/plasma-integration" >}}
+ Don't truncate filter name containing parenthesis. [Commit.](http://commits.kde.org/plasma-integration/efa61e7de254dd1184b73499adea5aba177e3db4) Fixes bug [#483350](https://bugs.kde.org/483350)
{{< /details >}}

{{< details title="plasma-mobile" href="https://commits.kde.org/plasma-mobile" >}}
+ Widgets/mediacontrols: Fix base model reference. [Commit.](http://commits.kde.org/plasma-mobile/e95ac3765a8b78d181d4bf93ffc300d449ee3da7) 
+ Widgets/mediacontrols: Fix uncentered icon in buttons. [Commit.](http://commits.kde.org/plasma-mobile/2dc5f79c6dcdbd20b331f605b0913036a17a093a) 
+ Lookandfeel: Sync with breeze, fix default cursor theme. [Commit.](http://commits.kde.org/plasma-mobile/e5aa1854f3405a3200610ac67fef246b1905e09b) Fixes bug [#482863](https://bugs.kde.org/482863)
+ Widgets/mediacontrols: Ensure multiplexer does not show. [Commit.](http://commits.kde.org/plasma-mobile/e86ebcefa55b2479bd7a99ce411b04e2eba6a9bb) 
+ Revert "volumeosd: Experiment with porting to Plasma.Dialog". [Commit.](http://commits.kde.org/plasma-mobile/b1a2afcf7f0a2d37031c03fb85d7a5c7fbce8584) 
+ Revert "allow input on dialog". [Commit.](http://commits.kde.org/plasma-mobile/f392a8a8c8d6a00b9c0083c52a841e9c535d7d9f) 
+ Allow input on dialog. [Commit.](http://commits.kde.org/plasma-mobile/ef433653b80bf849d6b097f463186d9622eb836a) 
+ Volumeosd: Experiment with porting to Plasma.Dialog. [Commit.](http://commits.kde.org/plasma-mobile/08294a1a5e6035062f4bb58f6832789b6c65c03a) 
+ Envmanager: Set Placement mode for KWin to skip window maximize animation when not in docked mode. [Commit.](http://commits.kde.org/plasma-mobile/882763d4ac886d96cb671d40b4577821f86617c6) 
+ Taskswitcher: Fix navigation panel position when in landscape mode. [Commit.](http://commits.kde.org/plasma-mobile/f932d73d977ef7b73cde58963a3dc1ee3efc29dc) 
+ Initialstart/time: Fix search box getting unfocused on every key press. [Commit.](http://commits.kde.org/plasma-mobile/af8999a0d8bd5fe30827a3840d0ca370c45835ce) 
+ Homescreens/folio: Fix start button behaviour, ensure windows are minimized and it's not triggered in docked mode. [Commit.](http://commits.kde.org/plasma-mobile/dc410b91a99e2632b8b1f23d5a158eb7fa89fa07) Fixes bug [#482870](https://bugs.kde.org/482870)
+ Taskswitcher: Do not show apps marked as "skipSwitcher". [Commit.](http://commits.kde.org/plasma-mobile/729b8a99323b5bc92ec5196c958d0beeea96efe7) 
+ Kwin/convergentwindows: Don't maximize xwaylandvideobridge ghost window. [Commit.](http://commits.kde.org/plasma-mobile/2106133f173c94e6f5ca862a19766dfe1649c418) 
+ Homescreens/folio: Improve performance of wallpaper blur, and enable by default. [Commit.](http://commits.kde.org/plasma-mobile/05122f2ac036fe15344a6765d25e43d4210ead93) 
+ Homescreens/folio: Prevent items from being added to favourites area if it's full. [Commit.](http://commits.kde.org/plasma-mobile/f873ba2193b4754af1b407dcf28c00da4de4e611) 
+ Systemdialog: Update to match plasma-workspace implementation, and simplify UI. [Commit.](http://commits.kde.org/plasma-mobile/855b6d0fecaa2dd5e77425e4a2cb88efb96b7206) 
+ Homescreens/folio: Fix deleting delegates from folder not working. [Commit.](http://commits.kde.org/plasma-mobile/883582afc5749681089a849a8b2056bac8d14fa6) 
+ Fix low inertia scrolling with shell flickables. [Commit.](http://commits.kde.org/plasma-mobile/072298e8bceee8808448c9f6cecb9a6845c43927) 
+ Homescreens/folio: Fix widget full representation being shown when starting drag and drop. [Commit.](http://commits.kde.org/plasma-mobile/d8a172aded140c01c2241ee920fbb0bcb53ed1b3) 
+ Homescreens/folio: Delete empty page at end if last delegate is deleted. [Commit.](http://commits.kde.org/plasma-mobile/1f35a8c8e5817b650d0e54a432bba545faab504b) 
{{< /details >}}

{{< details title="Plasma Nano" href="https://commits.kde.org/plasma-nano" >}}
+ Move cmake_minimum_required to the top of CMakeLists.txt. [Commit.](http://commits.kde.org/plasma-nano/e71d44192d7e3e246fb3517a122b053f1e765081) 
{{< /details >}}

{{< details title="Plasma Audio Volume Control" href="https://commits.kde.org/plasma-pa" >}}
+ Add URL to the metadata. [Commit.](http://commits.kde.org/plasma-pa/494b2702f0e142f06665281260b2b5a1045bb5b5) 
{{< /details >}}

{{< details title="Plasma SDK" href="https://commits.kde.org/plasma-sdk" >}}
+ Add homepage here to. [Commit.](http://commits.kde.org/plasma-sdk/5e6a26176fc7748e6e34a5636bdf61103c30d486) 
+ Add URL to the appstream metadata. [Commit.](http://commits.kde.org/plasma-sdk/ee20618ef63f8afcfaa180917c9d2a98fba5ee32) 
{{< /details >}}

{{< details title="Plasma Workspace" href="https://commits.kde.org/plasma-workspace" >}}
+ Appiumtests: skip touch gesture tests in the CI. [Commit.](http://commits.kde.org/plasma-workspace/b465cdba82160d5ba7be47615500ab50b1239e74) 
+ Kcms/wallpaper: check if config ui has saveConfig. [Commit.](http://commits.kde.org/plasma-workspace/7173dc65a6b2cc558ffaaf4536c7a9352cb02113) 
+ Kcms/wallpaper: format main.qml. [Commit.](http://commits.kde.org/plasma-workspace/e4c09165f29a6f217364e3396b55d1afd30fe80b) 
+ Shell: properly demarshall DBus argument for color. [Commit.](http://commits.kde.org/plasma-workspace/5dd906f1f52bf9507b5b65ac07400761d4aadec1) 
+ Libkmpris: always update active player index on rows inserted. [Commit.](http://commits.kde.org/plasma-workspace/bb4926b60822fbe6dbae15db19c02b214928123d) Fixes bug [#483027](https://bugs.kde.org/483027)
+ Libkmpris: work around nonstandard players. [Commit.](http://commits.kde.org/plasma-workspace/f049ffd07b71ce9f3239c5bd8267281049b5f5f1) Fixes bug [#482603](https://bugs.kde.org/482603)
+ [applets/systemtray] Load icons with Plasma palette. [Commit.](http://commits.kde.org/plasma-workspace/d73c6c4bb938bbd252c5bd69cc93862cba554e32) See bug [#479712](https://bugs.kde.org/479712). Fixes bug [#482645](https://bugs.kde.org/482645)
+ Applets/battery: hide battery percentage option when no batteries available. [Commit.](http://commits.kde.org/plasma-workspace/58d9aa5abc17493263b59b0b10fb9e70ce98e854) Fixes bug [#482089](https://bugs.kde.org/482089)
+ Wallpapers/image: save the last position for slideshow on exit. [Commit.](http://commits.kde.org/plasma-workspace/d3b3c5384d688c0ba96017775d7e4909da033251) Fixes bug [#482543](https://bugs.kde.org/482543)
+ PanelView: Remove redundant reading of the same config key. [Commit.](http://commits.kde.org/plasma-workspace/a11b6ff8852f492af84a44156532940daaa808e8) 
+ Fix panels being set to floating by upgrades. [Commit.](http://commits.kde.org/plasma-workspace/7f0e8e43de4155f5fb138aae6379c3fbef5570e3) 
+ Libkmpris: assert active player always matches active index. [Commit.](http://commits.kde.org/plasma-workspace/89563d31627cbfb10c7a335e0e8c573a3694208d) See bug [#482756](https://bugs.kde.org/482756)
+ Libkmpris: set active player index to -1 when no player matches active player. [Commit.](http://commits.kde.org/plasma-workspace/67361dd1eff060bc2c93e7b822054e0d57e2d812) Fixes bug [#482756](https://bugs.kde.org/482756)
+ Libkmpris: fix container not being deleted when its identity is empty. [Commit.](http://commits.kde.org/plasma-workspace/83d802abb2246568032b64b449b93974db1e3941) 
+ Runners/services: Increase category relevance when service name starts with query. [Commit.](http://commits.kde.org/plasma-workspace/d5055b1f21f06fdcdcb731b4de84673953a4a18c) 
+ Libkmpris: prevent an empty icon from being used. [Commit.](http://commits.kde.org/plasma-workspace/afa9e3406ba23a87cf4773f84f917d6466a33fa3) Fixes bug [#482908](https://bugs.kde.org/482908)
+ Appiumtests: move kcm tests to their own folder. [Commit.](http://commits.kde.org/plasma-workspace/1de9567711fc422b7326b9ef78ce456889c3dd98) 
+ Shell: read wallpaper config from plugin instead of local file. [Commit.](http://commits.kde.org/plasma-workspace/c940d214b794d0ca4d822aa40cb64199101e76cf) Fixes bug [#481741](https://bugs.kde.org/481741)
+ Wallpapers/image: remove unused connection. [Commit.](http://commits.kde.org/plasma-workspace/9ed7576abbc7d28bf317fa30aee94fc3fc710345) 
+ Shell: Avoid reserving a screen edge if no surface role has been created yet. [Commit.](http://commits.kde.org/plasma-workspace/083b346d58d770b7ef9794bea1e88e75669d1bd6) Fixes bug [#482399](https://bugs.kde.org/482399)
+ Kcm/regionandlang: fix Setting LANG to C mangles preview strings. [Commit.](http://commits.kde.org/plasma-workspace/9581e9ae0313bda1107ea3430b36f95c37c8d19b) Fixes bug [#482234](https://bugs.kde.org/482234)
{{< /details >}}

{{< details title="plasma-workspace-wallpapers" href="https://commits.kde.org/plasma-workspace-wallpapers" >}}
+ Move cmake_minimum_required to the top of CMakeLists.txt. [Commit.](http://commits.kde.org/plasma-workspace-wallpapers/a97426126b4d89d24d76c6daaec39a0772513fa3) 
{{< /details >}}

{{< details title="Powerdevil" href="https://commits.kde.org/powerdevil" >}}
+ Actions/dpms: Ignore turn-off triggers when action is disabled. [Commit.](http://commits.kde.org/powerdevil/a150b9fa622b71420d3c231eb87be695d3f96483) Fixes bug [#481308](https://bugs.kde.org/481308). Fixes bug [#482077](https://bugs.kde.org/482077)
+ Actions/powerprofile: guard against empty profiles list in readProperties. [Commit.](http://commits.kde.org/powerdevil/ae9af8fe02e0ee2540b4c81c05e9758a5aca6cd2) Fixes bug [#483216](https://bugs.kde.org/483216)
+ Check if idle inhibitor is active before blanking the screen. [Commit.](http://commits.kde.org/powerdevil/37c4434511c0281b991ac3b9a6b39884f7e4e993) Fixes bug [#482141](https://bugs.kde.org/482141)
+ Make OSD labels translatable. [Commit.](http://commits.kde.org/powerdevil/9781332020a36434372b848a6d256ac8ab77c4a6) 
{{< /details >}}

{{< details title="qqc2-breeze-style" href="https://commits.kde.org/qqc2-breeze-style" >}}
+ Move cmake_minimum_required to the top of CMakeLists.txt. [Commit.](http://commits.kde.org/qqc2-breeze-style/3ff9b43fb79395d9559f02e390952a9344ad95e2) 
{{< /details >}}

{{< details title="xdg-desktop-portal-kde" href="https://commits.kde.org/xdg-desktop-portal-kde" >}}
+ GlobalShortcuts: Fix shortcut register on session load. [Commit.](http://commits.kde.org/xdg-desktop-portal-kde/282fe3cc39a4f12480e230515cad272c49bb5a74) 
+ GlobalShortcuts: Load existing shortcuts at session creation. [Commit.](http://commits.kde.org/xdg-desktop-portal-kde/2efee2c98845b95fd0b372c6b4580030f04dda96) 
+ GlobalShortcuts: Set shortcuts in BindShortcuts. [Commit.](http://commits.kde.org/xdg-desktop-portal-kde/726d22c71aac0f820773ce5bb3ab91f8ffc77e1b) Fixes bug [#474741](https://bugs.kde.org/474741)
{{< /details >}}

