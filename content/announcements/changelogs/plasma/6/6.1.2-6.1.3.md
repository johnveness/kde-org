---
title: Plasma 6.1.3 complete changelog
version: 6.1.3
hidden: true
plasma: true
type: fulllog
---

{{< details title="Discover" href="https://commits.kde.org/discover" >}}
+ Flatpak: also auto-handle rebases from runtimes. [Commit.](http://commits.kde.org/discover/482b6795dedec289b38ca0a59ffc4564356ac602) 
+ Flatpak: uninstall properly EOL refs that have no replacement. [Commit.](http://commits.kde.org/discover/510816d9cb6dc573696eba6304f8a77512719c6c) 
+ Flatpak: don't trip over null gerrors. [Commit.](http://commits.kde.org/discover/c5c61cf6da9badf7002a32f086ecb3ef457d747c) Fixes bug [#487526](https://bugs.kde.org/487526)
{{< /details >}}

{{< details title="kglobalacceld" href="https://commits.kde.org/kglobalacceld" >}}
+ Explicitly process invalid keycodes. [Commit.](http://commits.kde.org/kglobalacceld/8643077d0422c97ad12dc052202c9e1348e99b23) Fixes bug [#489001](https://bugs.kde.org/489001)
{{< /details >}}

{{< details title="kpipewire" href="https://commits.kde.org/kpipewire" >}}
+ Produce: Properly cleanup on deactivate in all cases. [Commit.](http://commits.kde.org/kpipewire/9f4c61d80bd01a92ebbcc0c89d7d7d12b9d12079) Fixes bug [#488687](https://bugs.kde.org/488687)
+ Produce: Destroy PipeWireSourceStream on the right thread. [Commit.](http://commits.kde.org/kpipewire/1977da38ed25aa15347eb9027cb1fde3d66b075f) Fixes bug [#489434](https://bugs.kde.org/489434)
{{< /details >}}

{{< details title="krdp" href="https://commits.kde.org/krdp" >}}
+ RdpConnection: Include new state when emitting state change signal. [Commit.](http://commits.kde.org/krdp/2d95ca34ac465936f274a54e733a93589cef14d7) 
+ Remove bitmap cache handling from VideoStream. [Commit.](http://commits.kde.org/krdp/786cd94f84e1d06f9ee3d4910b0e9eed6c5916cb) Fixes bug [#489097](https://bugs.kde.org/489097)
+ Server: Use QHostAddress::Any if no listening address is set. [Commit.](http://commits.kde.org/krdp/67aaa5a7e58eadec456d88feb11343dda22a90a3) Fixes bug [#488884](https://bugs.kde.org/488884)
+ Allow toggling server without users. [Commit.](http://commits.kde.org/krdp/6b64676ce6924af0c8ddf39837e5df58f4d93195) See bug [#489283](https://bugs.kde.org/489283)
{{< /details >}}

{{< details title="KScreen" href="https://commits.kde.org/kscreen" >}}
+ Kcm: use ContextualHelpButton from Kirigami. [Commit.](http://commits.kde.org/kscreen/77cdc8112ed4ea50fc039e4b081c3af657f49f5b) 
{{< /details >}}

{{< details title="kscreenlocker" href="https://commits.kde.org/kscreenlocker" >}}
+ Greeter/authenticators: add a property for if a prompt was shown in the past. [Commit.](http://commits.kde.org/kscreenlocker/6297d4d2e37abadfab0f7389aec9aa4af7f928bc) See bug [#485520](https://bugs.kde.org/485520)
{{< /details >}}

{{< details title="KWin" href="https://commits.kde.org/kwin" >}}
+ Plugins/nightlight: Relax custom times constraints. [Commit.](http://commits.kde.org/kwin/6ef1c5dfbbb523eef3b9923af5d448a50079b9e6) Fixes bug [#489366](https://bugs.kde.org/489366)
+ Wayland: Simplify XdgPopupWindow::sendRoleConfigure(). [Commit.](http://commits.kde.org/kwin/15fc83cf6ea1c02889c72cfbf87932fb0996f504) 
+ Wayland: Dismiss XdgPopupWindow when the parent window is closed. [Commit.](http://commits.kde.org/kwin/0258a59123f9e8937b8b569ec8ca72884b53e63e) Fixes bug [#472013](https://bugs.kde.org/472013)
+ Tiling: Don't put maximized windows in tile. [Commit.](http://commits.kde.org/kwin/634e8be469c94763940f938fa446daee552b0cdf) Fixes bug [#489463](https://bugs.kde.org/489463)
+ Input method window should not break showing desktop. [Commit.](http://commits.kde.org/kwin/7ab44a56e280b945a5d7af323c844ab0eabc986e) Fixes bug [#489057](https://bugs.kde.org/489057)
+ Plugins/fadingpopups: don't block direct scanout. [Commit.](http://commits.kde.org/kwin/07e9d09611d1de307b7ce9724fc5dcea5a9cc4d7) Fixes bug [#487780](https://bugs.kde.org/487780)
+ Backends/drm: test and apply all mode changes at once. [Commit.](http://commits.kde.org/kwin/afffd1b1db7f044003589e0265d66982a2fa3a49) 
+ Autotests/drm: add test for vrr capability changing without a hotunplug. [Commit.](http://commits.kde.org/kwin/3a658206bc14e26c4957f8c71798fe3b8211ab4d) 
+ Backends/drm: update output properties after they're created too. [Commit.](http://commits.kde.org/kwin/28e8d4da159763e19962a98a3a13a8d5b6927970) Fixes bug [#486149](https://bugs.kde.org/486149)
+ Use separation dep_version to build against, updated by release scripts. [Commit.](http://commits.kde.org/kwin/0c2153b768e21121eb5282981573d85001bb0259) 
+ Plugins/screencast: Don't download texture data if target size and texture size mismatch. [Commit.](http://commits.kde.org/kwin/8d23766d103368dd809d74ce11b374db5c5f3a54) See bug [#489764](https://bugs.kde.org/489764)
+ Plugins/screencast: Allocate offscreen texture in WindowScreenCastSource::render(QImage) as big as the memfd buffer. [Commit.](http://commits.kde.org/kwin/e72f88042873db7b4d98065fc14f1e3eb5a2189e) Fixes bug [#489764](https://bugs.kde.org/489764)
+ Autotests: Skip testScreencasting in CI. [Commit.](http://commits.kde.org/kwin/42123a4d642db5231cfcf3d7af29c89ea39d54df) 
+ Foward modifiers after disabling sticky keys. [Commit.](http://commits.kde.org/kwin/aed2c5b55b27b1502632c7b87b07271232fbf9c7) 
+ Plugins/screenshot: Port blitScreenshot() to glReadnPixels(). [Commit.](http://commits.kde.org/kwin/64f75267024e1bdf602ffda8f37ddb2923ca630e) 
+ Wayland: add error handling for QFile::open failure in org_kde_plasma_window_get_icon. [Commit.](http://commits.kde.org/kwin/9dfa660bb9433faf27dcb0933cd56cbc8112a1e0) 
+ Placement: ignore the active output with place under mouse. [Commit.](http://commits.kde.org/kwin/429a6ab28a9e48d71cf69ecfb2989bb57ac47463) Fixes bug [#488110](https://bugs.kde.org/488110)
+ Opengl: Add OpenGlContext::glGetnTexImage(). [Commit.](http://commits.kde.org/kwin/ac4a0cec59609131ba22931c97ee6ec2aca5cac2) 
+ Plugins/screencast: Prefer glReadnPixels() and glGetnTexImage(). [Commit.](http://commits.kde.org/kwin/83f7db0a4f4cc41c1a3753ac5b06ba5771f0701c) 
+ WindowHeapDelegate: label topMargin to small, remove height padding. [Commit.](http://commits.kde.org/kwin/3837c027a87471e5abbca07b31e2874d818785ac) Fixes bug [#489595](https://bugs.kde.org/489595)
+ Plugins/glide: drop references to closed windows if they're not animated. [Commit.](http://commits.kde.org/kwin/ac217c244db439fe3058222c6555f8f3587bae7f) See bug [#485425](https://bugs.kde.org/485425)
+ Plugins/screencast: Handle frame rate throttling timer firing a bit earlier. [Commit.](http://commits.kde.org/kwin/2b4068673944b40d3ffa223ea6ae04f379f9ec93) See bug [#489602](https://bugs.kde.org/489602)
+ Backends/drm: disable triple buffering on NVidia. [Commit.](http://commits.kde.org/kwin/b437c65815b2898564748f3be48c4145671a750f) Fixes bug [#487833](https://bugs.kde.org/487833)
{{< /details >}}

{{< details title="libkscreen" href="https://commits.kde.org/libkscreen" >}}
+ Destroy the registry when WaylandConfig gets deleted. [Commit.](http://commits.kde.org/libkscreen/d35c1657f4c533ae15ddbc8a41d799087557a342) Fixes bug [#482768](https://bugs.kde.org/482768)
{{< /details >}}

{{< details title="libplasma" href="https://commits.kde.org/libplasma" >}}
+ Containmentitem.cpp: Do not set dropJob parent to m_dropMenu. [Commit.](http://commits.kde.org/libplasma/832a31c49ba90ac060a0f3c57bf3b459d7df7f46) Fixes bug [#484674](https://bugs.kde.org/484674)
{{< /details >}}

{{< details title="Plasma Desktop" href="https://commits.kde.org/plasma-desktop" >}}
+ Fix kcm_clock save on non-systemd distros. [Commit.](http://commits.kde.org/plasma-desktop/f8c880fe7eed319553e35a739acb5a9ed142ccb0) 
+ KeyboardButton: Check if longName is undefined. [Commit.](http://commits.kde.org/plasma-desktop/ca69eab14b11caf0a223e2aed337f787f78b3aa6) 
+ Use a shared QQmlEngine when possible. [Commit.](http://commits.kde.org/plasma-desktop/f6921a5415c5db0f5acfa9439bf33fbffe8026c9) Fixes bug [#488326](https://bugs.kde.org/488326)
+ Lockscreen: fix the check for authentication prompts. [Commit.](http://commits.kde.org/plasma-desktop/46910cb6a0a6cb8ad6b60ba2b8dfb224624df2ec) Fixes bug [#485520](https://bugs.kde.org/485520)
+ Edit Mode: Fix for blurry icons. [Commit.](http://commits.kde.org/plasma-desktop/6626ad46caaaf8b5147fbdf1ddc539788f41f98e) Fixes bug [#488920](https://bugs.kde.org/488920)
{{< /details >}}

{{< details title="plasma-mobile" href="https://commits.kde.org/plasma-mobile" >}}
+ Homescreens/folio: Close settings view when home button is pressed. [Commit.](http://commits.kde.org/plasma-mobile/5f33a1d347327c45b1550c1634f1cdf3d227860e) 
+ Homescreens/folio: Make it easier to click on delegate context menus without starting drag. [Commit.](http://commits.kde.org/plasma-mobile/bff496dc9c868f60b2f71781c693e147b3dc29f9) 
+ Homescreens/folio: Fix cube effect typo. [Commit.](http://commits.kde.org/plasma-mobile/e88d6334b20829b5089153c456f94c78ce2aad46) 
+ Adding translation domains to some dynamic libraries to resolve untranslated issues. [Commit.](http://commits.kde.org/plasma-mobile/5b03722aeb2e93c04e047943c3d2c97a37ccc7b2) 
+ Homescreens/folio: Halve the needed swipe distance for swipe detection. [Commit.](http://commits.kde.org/plasma-mobile/e9d1fba0ba24554dbc9082ddbe4267bf0455e225) 
+ Homescreens/folio: Fix widget config dialogs not having a background. [Commit.](http://commits.kde.org/plasma-mobile/b53bb6089849fb1a145c940659870b2afddb2f30) 
+ Homescreen: Add button to wallpaper selector to go to advanced settings. [Commit.](http://commits.kde.org/plasma-mobile/291748f871dcb84f83cc60c43041aa44cee132dd) 
+ Homescreen: Don't animate after unlock. [Commit.](http://commits.kde.org/plasma-mobile/855d9ac321bf858afb1b5760b0a876c16df5bfc1) 
+ Shellsettingsplugin: Never affect panels when not in Plasma Mobile. [Commit.](http://commits.kde.org/plasma-mobile/c34c5067615eec25507be5b1ce8e284a0029130c) 
+ Taskswitcher: Only enable KWin effect in mobile, have it be managed by envmanager. [Commit.](http://commits.kde.org/plasma-mobile/9e46508bb07669c3e070ad481982117a6c9d49b0) 
{{< /details >}}

{{< details title="Plasma Networkmanager (plasma-nm)" href="https://commits.kde.org/plasma-nm" >}}
+ Use a shared QQmlEngine when possible. [Commit.](http://commits.kde.org/plasma-nm/9e2ae48fb51cdb1a38e267c04a2ab5484e133d6e) Fixes bug [#488326](https://bugs.kde.org/488326)
{{< /details >}}

{{< details title="Plasma Workspace" href="https://commits.kde.org/plasma-workspace" >}}
+ Klipper: Avoid incorrect wayland roundtrips. [Commit.](http://commits.kde.org/plasma-workspace/ec617701e49b29422d246f23d1515670f1a9efa5) Fixes bug [#489096](https://bugs.kde.org/489096)
+ Klipper: Fix a potential null dereferencing. [Commit.](http://commits.kde.org/plasma-workspace/02d852d163ef98f42d341f4cdcf9a9c10d29115f) Fixes bug [#489096](https://bugs.kde.org/489096)
+ Kcm/users: decode URI-encoded file for avatar image. [Commit.](http://commits.kde.org/plasma-workspace/6b1e7af63dcf5f0ddb0dc86e8079e440b4be08bd) 
+ Kastatsfavoritesmodel.cpp: prefer .value() over operator[]. [Commit.](http://commits.kde.org/plasma-workspace/65c5bc469d53dc70921b10b8c4527536883a8bb0) Fixes bug [#482887](https://bugs.kde.org/482887)
+ Xembedsniproxy: Warp pointer to click location on wayland. [Commit.](http://commits.kde.org/plasma-workspace/e2cc2c2aab112edd29d1f713df1769c068f3f20f) Fixes bug [#489286](https://bugs.kde.org/489286)
+ Klipper: add missing static keyword in `SystemClipboard`. [Commit.](http://commits.kde.org/plasma-workspace/780a01a20fe293dfa7b136089ee1d5cd96df3391) 
{{< /details >}}

{{< details title="print-manager" href="https://commits.kde.org/print-manager" >}}
+ Plasmoid: Remove dead code (JobsModel). [Commit.](http://commits.kde.org/print-manager/6618b3709c80c28f618dc2dbbf3c060a77103c1f) 
{{< /details >}}

{{< details title="System Settings" href="https://commits.kde.org/systemsettings" >}}
+ Share the qml engine as a qApp property. [Commit.](http://commits.kde.org/systemsettings/09514cd3bfdfb1c6342cc98e25433537fb78cb3d) Fixes bug [#488326](https://bugs.kde.org/488326)
{{< /details >}}

