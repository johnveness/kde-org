---
title: Plasma 6.0.3 complete changelog
version: 6.0.3
hidden: true
plasma: true
type: fulllog
---

{{< details title="Bluedevil" href="https://commits.kde.org/bluedevil" >}}
+ Fix pairing bug. [Commit.](http://commits.kde.org/bluedevil/11689adb5866638e83bea22f4f2fbbf2963b692a) 
{{< /details >}}

{{< details title="Breeze" href="https://commits.kde.org/breeze" >}}
+ Kstyle/animations: Call QWidget::update in BusyIndicatorEngine. [Commit.](http://commits.kde.org/breeze/82dd358707bf44b641e2971ace3bd0cd8a6c1e87) 
+ Use QEvent::ApplicationPaletteChanged instead of QEvent::PaletteChanged. [Commit.](http://commits.kde.org/breeze/f794a82137a459cdf997941ca0216c74d96a5971) 
+ Kstyle: Don't send event to disabled scroll bar. [Commit.](http://commits.kde.org/breeze/c949bb4bd288bba5eda06e9a89ea0a054920c719) Fixes bug [#483487](https://bugs.kde.org/483487)
{{< /details >}}

{{< details title="Discover" href="https://commits.kde.org/discover" >}}
+ Revert duplicated appstream edit. [Commit.](http://commits.kde.org/discover/f288e5e63ac7392485d38030bfe0d1d897e12d04) 
+ Kns: Make sure we only launch requests once. [Commit.](http://commits.kde.org/discover/463f923bd4eca4d93b79709dc1a06eacf92538ab) 
+ Kns: fix entry deduplication. [Commit.](http://commits.kde.org/discover/0950381015ebed67bfe44608597ad533e7444df6) 
+ Packagekitbackend: de-thread the appstream loading hotfix. [Commit.](http://commits.kde.org/discover/a1ea1062ee5b5681bb2c5b4e462ad9ac8b66defe) See bug [#481993](https://bugs.kde.org/481993)
+ Fix build with c++17. [Commit.](http://commits.kde.org/discover/0f4827e3b563e8004ef4baece6bc755be9ae0598) 
+ Flatpak: Rescue local files that might exist before creating a new file. [Commit.](http://commits.kde.org/discover/25da333edd5ca86507b92ba04d5e93b85be24b28) 
{{< /details >}}

{{< details title="Dr Konqi" href="https://commits.kde.org/drkonqi" >}}
+ Revert "postman: differentiate error types". [Commit.](http://commits.kde.org/drkonqi/11a3e30481162427af3a9e1fcc78a963429a45c4) 
+ Sentry: add more tags. [Commit.](http://commits.kde.org/drkonqi/43024ce1029be83d1dd61f2b239ba91f4f43790d) 
+ Gdb: limit amount of frames in backtrace to 128. [Commit.](http://commits.kde.org/drkonqi/941d10ca40a02b57b69bf96f28913b2a6999ff07) 
+ Reportinterface: default log entries to priority 'info'. [Commit.](http://commits.kde.org/drkonqi/5ee5473382460b6190b29ccfdbb336846a63550e) 
+ Postman: differentiate error types. [Commit.](http://commits.kde.org/drkonqi/a8018da9ff49bc787c42abb2d60eacc06f9bf5f1) 
{{< /details >}}

{{< details title="KDE GTK Config" href="https://commits.kde.org/kde-gtk-config" >}}
+ Gtkconfig.upd: Set Version=6. [Commit.](http://commits.kde.org/kde-gtk-config/0760b1fd5f16a19475a25084be6e935a9ce7cbb5) Fixes bug [#482763](https://bugs.kde.org/482763)
{{< /details >}}

{{< details title="Plasma Addons" href="https://commits.kde.org/kdeplasma-addons" >}}
+ Wallpapers/potd: check network information backend is available before querying capabilities. [Commit.](http://commits.kde.org/kdeplasma-addons/9d74d8878161e15f87772289d5f5ef0161ae1810) 
+ Applets/weather: remove invalid bindings on Layout properties. [Commit.](http://commits.kde.org/kdeplasma-addons/0350e4468502f10a9530e41850cf929db20f5bb9) See bug [#483791](https://bugs.kde.org/483791). See bug [#483628](https://bugs.kde.org/483628)
+ Mark QtQuick3D as a runtime dependency. [Commit.](http://commits.kde.org/kdeplasma-addons/5b4f8161b0dd03659ec13b1d7f7f07003e535878) 
{{< /details >}}

{{< details title="kglobalacceld" href="https://commits.kde.org/kglobalacceld" >}}
+ Fix KHotkeys migration. [Commit.](http://commits.kde.org/kglobalacceld/2ddcc42067f9104f90cbd69bcfa7347f81023185) Fixes bug [#484063](https://bugs.kde.org/484063)
+ Also consider actions when querying for applications with shortcuts. [Commit.](http://commits.kde.org/kglobalacceld/cca0bbeb4600a6b06c821044b924c4b8ab53059c) Fixes bug [#483214](https://bugs.kde.org/483214)
{{< /details >}}

{{< details title="kpipewire" href="https://commits.kde.org/kpipewire" >}}
+ Fix typo. [Commit.](http://commits.kde.org/kpipewire/71d0d4cb5c1cdefd19dcff5ff44f810654c979a2) 
+ Don't set PipeWireSourceItem::enabled to true when the item becomes visible. [Commit.](http://commits.kde.org/kpipewire/11074fb2b1dd97f457b0aadc0d0becf89a89eb49) 
+ Drop redundant isComponentComplete() checks. [Commit.](http://commits.kde.org/kpipewire/f3d6583231025667dbe519a4af376e3b63ad1fc8) 
+ Remove redundant code. [Commit.](http://commits.kde.org/kpipewire/6273ac4d226354131b9591994c9f7cf566ca2096) 
+ Mark PipeWireSourceItem as enabled after receiving a frame. [Commit.](http://commits.kde.org/kpipewire/6a9f32007e169d7963e7652b7e9d11d24a749663) 
{{< /details >}}

{{< details title="KScreen" href="https://commits.kde.org/kscreen" >}}
+ Plasmoid: adapt to changed inhibition request handling. [Commit.](http://commits.kde.org/kscreen/cefc4921e24b0dbffa660911836c5a03bd5a8c21) Fixes bug [#477355](https://bugs.kde.org/477355)
+ OsdAction: fix finding best mode. [Commit.](http://commits.kde.org/kscreen/9dfe7381e88c119e9d4599d075f990669196246b) 
+ OsdAction: fix applying mode on X11. [Commit.](http://commits.kde.org/kscreen/7885a4887c0c3437bbe46e5cc03af79203f60fac) Fixes bug [#482642](https://bugs.kde.org/482642)
{{< /details >}}

{{< details title="ksystemstats" href="https://commits.kde.org/ksystemstats" >}}
+ Cpu: Store physical ID with core ID when looking up coretemp sensors. [Commit.](http://commits.kde.org/ksystemstats/5ab4ae0f82afbffd8d1a00ff8f1822e92ff9ba1b) Fixes bug [#474766](https://bugs.kde.org/474766)
{{< /details >}}

{{< details title="KWin" href="https://commits.kde.org/kwin" >}}
+ Plugins/screencast: Neutralize stopped streams. [Commit.](http://commits.kde.org/kwin/551100b4695aa60030c4e4378c882618eaa36a4b) 
+ Backends/wayland: don't clear the format map on tranche_done. [Commit.](http://commits.kde.org/kwin/a1d010583689d0ffac2a08c93c16325981601aa7) 
+ Xwayland: Allow pushing to the clipboard without focus. [Commit.](http://commits.kde.org/kwin/760e35065ed8b5f570f757cc464473b5f75a3c5b) 
+ Kcms/decoration: Fix crash when preview cannot be generated. [Commit.](http://commits.kde.org/kwin/d8019dbf168cf5330f65603ce7a8161443554e3d) Fixes bug [#456531](https://bugs.kde.org/456531)
+ Plugins/screencast: Prefer allocating 3 buffers per stream by default. [Commit.](http://commits.kde.org/kwin/f2a78761788b880300b9a03a8c7da0a0143defad) 
+ Plugins/screenshot: Fix a crash in ScreenShotSource2::marshal(). [Commit.](http://commits.kde.org/kwin/ac831f0b970298245254801b85c30c31d5df5ed7) 
+ Plugins/screencast: Enqueue buffers immediately. [Commit.](http://commits.kde.org/kwin/79fa595d2562b3926a04b0b2292155adf74afad9) 
+ Disable placeholder output when removing it. [Commit.](http://commits.kde.org/kwin/2d0117de9243b2ebecac0dab929b02e8ea948cd7) 
+ X11window: round border size to integral XNative units. [Commit.](http://commits.kde.org/kwin/5af86d18f406da03d194c464ac63bbb600aebc89) Fixes bug [#481460](https://bugs.kde.org/481460)
+ Backends/drm: Guard against null m_frame in DrmAbstractOutput::{frameFailed,pageFlipped}. [Commit.](http://commits.kde.org/kwin/045199f3a418f5a7a512b2af4948c17b44c2a617) 
+ Backends/drm: Reset m_frame when present fails. [Commit.](http://commits.kde.org/kwin/e378a35d05193c9306f4ee778c67838b837b6dd9) 
+ Fix warning about nullptr sender in QObject::connect(). [Commit.](http://commits.kde.org/kwin/ea8fa09e2db230f7d13d9c55ba8fa3644cc72933) 
+ Backends/drm: Handle failing to reopen drm node. [Commit.](http://commits.kde.org/kwin/0dbbcd83fefe23679a3b30d516fc484c6ce479d2) 
+ Add more guards for closed windows. [Commit.](http://commits.kde.org/kwin/e2aec38015929dccae70963ea1ac97cbfecb1271) 
+ Wayland: Remove zombie ClientConnection from Display later. [Commit.](http://commits.kde.org/kwin/c1c7de07aa5ce94febf1dc2eb29d815ffffa1e52) 
+ Don't trigger screen edge if the pointer is constrained. [Commit.](http://commits.kde.org/kwin/d38a8d48a91bfc51d608fe179951d3ceb9f52981) 
+ Pointer input: handle warp events differently from absolute motion events. [Commit.](http://commits.kde.org/kwin/0b2d8901ab086bf5119077911ba39f63b1bdecfe) Fixes bug [#458233](https://bugs.kde.org/458233). See bug [#482476](https://bugs.kde.org/482476)
+ Plugins/screencast: Improve code readability. [Commit.](http://commits.kde.org/kwin/fe1c814ee10df2f681c4c76605b6806cf9de1e2c) 
+ Plugins/screencast: Guard against having no dmabuf data for particular buffer. [Commit.](http://commits.kde.org/kwin/bb1507eadfba4ee385ba3bf1f0013d690ab4ce58) 
+ Plugins/screencast: Properly mark pw buffers as corrupted. [Commit.](http://commits.kde.org/kwin/5521920d31d0afa60e19400aa14358a201d2a094) 
+ Plugins/screencast: Clean up how pw_buffer is initialized. [Commit.](http://commits.kde.org/kwin/6000cca75640595385d4627735669be23119224f) 
+ Plugins/kpackage: Fix mainscript for declarative effects. [Commit.](http://commits.kde.org/kwin/5164f6969e8dc465a92bcca05f5cc2ac8a401b37) 
+ Window: fix interactiveMove exit condition. [Commit.](http://commits.kde.org/kwin/90e070d004ef57f2ec5acb3f1dd3807e3d1a038f) Fixes bug [#481610](https://bugs.kde.org/481610)
+ Plugins/screencast: Add missing Q_OBJECT. [Commit.](http://commits.kde.org/kwin/46d6ad9e94aae93f568bb9c0ecf9b066ff67d715) 
+ Fix the titlebar visibility check for small windows. [Commit.](http://commits.kde.org/kwin/bd681c3c72e4b00df079aca0be0bf1ad2cada1b4) 
+ Plugins/screencast: Avoid closing dmabuf fds twice. [Commit.](http://commits.kde.org/kwin/808ff015ad2c2de26e8d23f1e2aad59afa8dca14) 
+ Effects: Do not take ownership of QuickEffect::delegate. [Commit.](http://commits.kde.org/kwin/745625f04d7d46ed114f7a9fc3ac4ebb126c8b11) 
+ Platformsupport/scenes/opengl: don't access std::nullopt. [Commit.](http://commits.kde.org/kwin/e8f567b055f7abbc534c6128173acddffed76080) 
+ Platformsupport/scenes/opengl: advertise formats unnknown to KWin too. [Commit.](http://commits.kde.org/kwin/f1c132d329f22fb804ff357c21293825e7f5ba9f) 
+ Keyboard_layout: always expose dbus interface. [Commit.](http://commits.kde.org/kwin/24610e67a339d5bce637e060debb6a401026d70a) Fixes bug [#449531](https://bugs.kde.org/449531)
+ Wayland: DrmLeaseDevice, use Q_OBJECT macro. [Commit.](http://commits.kde.org/kwin/84412b22df01c5b00074e486cad3a432b04c953d) Fixes bug [#483008](https://bugs.kde.org/483008)
{{< /details >}}

{{< details title="libksysguard" href="https://commits.kde.org/libksysguard" >}}
+ Colorgrid: Use the same background color as pie/bar charts. [Commit.](http://commits.kde.org/libksysguard/8b9ace2a5c9e8c26af13ee67476a049f7f26d159) Fixes bug [#482664](https://bugs.kde.org/482664)
+ Faces/piechart: Explicitly set the thickness of the chart. [Commit.](http://commits.kde.org/libksysguard/f62f849ba63d507b0ecbc22def3eff9f91fb22da) 
+ Faces/piechart: Don't hardcode background color but base it on theme. [Commit.](http://commits.kde.org/libksysguard/40e44cd70878e8e34f1f986712466bc339563747) 
+ Faces/horizontalbars: Repair compact representation. [Commit.](http://commits.kde.org/libksysguard/8069ebec90254ed72abfcd1306d1420283981695) Fixes bug [#481949](https://bugs.kde.org/481949)
+ Faces/horizontalbars: Use a fixed background rather than relying on style. [Commit.](http://commits.kde.org/libksysguard/10d51f8f637bae322b018fe00951508f59a123ec) Fixes bug [#480415](https://bugs.kde.org/480415)
{{< /details >}}

{{< details title="libplasma" href="https://commits.kde.org/libplasma" >}}
+ Do not activate containment when one of its applet is activated. [Commit.](http://commits.kde.org/libplasma/abad7a55865f8ad2e936b34b33ffa5c2fe7d5598) Fixes bug [#483941](https://bugs.kde.org/483941). Fixes bug [#479084](https://bugs.kde.org/479084)
{{< /details >}}

{{< details title="Plasma Desktop" href="https://commits.kde.org/plasma-desktop" >}}
+ Applets/taskmanager: remove unnecessary Layout properties. [Commit.](http://commits.kde.org/plasma-desktop/22b45b588ed387a26258825beec8f3e74f90b9b8) 
+ Applets/taskmanager: Drop pipewire window thumbnail opacity animation. [Commit.](http://commits.kde.org/plasma-desktop/d0c56a1f1439d12ce8fe6da07bc67eba84abb796) 
+ Applets/taskmanager: Don't base the amount of space reserved for the thumbnail on the header. [Commit.](http://commits.kde.org/plasma-desktop/b40e3a8764dc4fbfdef9fa5efecb36f88db37ae1) Fixes bug [#481044](https://bugs.kde.org/481044)
+ Revert "Panel: Don't take keyboard focus when one of its widgets is activated". [Commit.](http://commits.kde.org/plasma-desktop/e53fd6b066d569ae8247e5565c51560ace7af078) See bug [#483941](https://bugs.kde.org/483941). See bug [#479084](https://bugs.kde.org/479084)
+ Folder View: fix label rendering for popup and widget views. [Commit.](http://commits.kde.org/plasma-desktop/95dbda1a15dd35903fcebaa1fc421dac79f3db11) Fixes bug [#484163](https://bugs.kde.org/484163)
+ Panel: don't round floating paddings to smoothen animation. [Commit.](http://commits.kde.org/plasma-desktop/8560cd25c2f4819b1dfa4532de29a35c55d0103e) 
+ Applets/kimpanel: Fix save font option. [Commit.](http://commits.kde.org/plasma-desktop/ceebb70f34667a79fb82e68dabfa553b27593909) 
+ Kicker: Fix submenus appearing in taskmanager. [Commit.](http://commits.kde.org/plasma-desktop/7cdeb427b59857ed0a69ad0fab6cc86a9cbbfc99) Fixes bug [#481955](https://bugs.kde.org/481955)
+ Cancel edit prior reparenting the applet. [Commit.](http://commits.kde.org/plasma-desktop/9d5b5040b4033e8ecfede2d56fe4bee30164b43e) See bug [#482371](https://bugs.kde.org/482371). See bug [#483287](https://bugs.kde.org/483287)
+ Solid-device-automounter/kcm: do not create proxy widget in initializer list. [Commit.](http://commits.kde.org/plasma-desktop/a4acebc1c6c705e65446c969091f4975e5fc1d66) 
+ Filter out enter. [Commit.](http://commits.kde.org/plasma-desktop/16123dc4df00409c9d542ca8539c233ddc1e3505) Fixes bug [#482123](https://bugs.kde.org/482123)
{{< /details >}}

{{< details title="plasma-mobile" href="https://commits.kde.org/plasma-mobile" >}}
+ Volumeosd: Properly bind volume keys. [Commit.](http://commits.kde.org/plasma-mobile/3192c0cab16954ee5fbd4a7482bf8709faa9ffa9)
+ Kcms/time: Fix timezone dialog constantly popping up and down the keyboard while typing. [Commit.](http://commits.kde.org/plasma-mobile/f8e7fa2c7744b8f07465c0aed14ef0cfa8d0e831) 
+ Components/mobileshell: Don't tie MarqueeLabel durations to kirigami duration lengths. [Commit.](http://commits.kde.org/plasma-mobile/b200200e1bdea007db6ac6b8228f464f588edd04) 
+ Homescreens/folio: Minimize all windows when pressing start key rather than activate "show desktop" mode. [Commit.](http://commits.kde.org/plasma-mobile/7ea2408cf942b8450b4bb7e3f06a13bba8b4207a) 
+ Navigationpanel: Fix remnants of gesture-only setting affecting panel height. [Commit.](http://commits.kde.org/plasma-mobile/466b93b67fb74e5dcd79c27aa4d8de18280ca920) Fixes bug [#483663](https://bugs.kde.org/483663)
+ Taskpanel: Fix close vkbd button not showing. [Commit.](http://commits.kde.org/plasma-mobile/fa1d94c72bc2307790c6d7c795adbeb8a480a6ed) 
+ Initialstart: Ensure initial value of 24 hour switch is obtained. [Commit.](http://commits.kde.org/plasma-mobile/f346f80820b2c161f1235a204f401d9c2271dd5a) 
{{< /details >}}

{{< details title="Plasma Networkmanager (plasma-nm)" href="https://commits.kde.org/plasma-nm" >}}
+ Use "double" for storing received/sent bytes. [Commit.](http://commits.kde.org/plasma-nm/9dd7b54936b76e6e06ca1318c2932bb7ea42007a) Fixes bug [#479230](https://bugs.kde.org/479230)
{{< /details >}}

{{< details title="Plasma SDK" href="https://commits.kde.org/plasma-sdk" >}}
+ Fix desktop file name for icon explorer. [Commit.](http://commits.kde.org/plasma-sdk/c77ff7bc6e0cb7bccf3a7f77366b8776cc6f7cde) 
{{< /details >}}

{{< details title="Plasma Systemmonitor" href="https://commits.kde.org/plasma-systemmonitor" >}}
+ GlobalMenu: Fix path to AboutPage.qml. [Commit.](http://commits.kde.org/plasma-systemmonitor/2ca66f8128dc3d7a6bd57d723a483258b311af4c) 
+ Page: Fix drag-sorting of pages in PageSortDialog. [Commit.](http://commits.kde.org/plasma-systemmonitor/8f590ae92e53fc8456fd360ca0491e0dde784f13) Fixes bug [#482377](https://bugs.kde.org/482377)
{{< /details >}}

{{< details title="Plasma Workspace" href="https://commits.kde.org/plasma-workspace" >}}
+ Avoid positioning the panel when it has no size yet. [Commit.](http://commits.kde.org/plasma-workspace/d03a98fc69c94f83f9410651d15a21ff41a72904) Fixes bug [#483102](https://bugs.kde.org/483102). Fixes bug [#483348](https://bugs.kde.org/483348). Fixes bug [#470434](https://bugs.kde.org/470434)
+ ScreenPool: use real display name to check fake output on X11. [Commit.](http://commits.kde.org/plasma-workspace/6fca2a116f919ff2c089fbb69262e293d3abd667) 
+ Update version for new release. [Commit.](http://commits.kde.org/plasma-workspace/b374e9d6666e91e9e2f1e45d7223fa57137a3e85) 
+ Plasma-shutdown: Correclty detect if we're using the legacy session. [Commit.](http://commits.kde.org/plasma-workspace/b0c3c66fc04a7743c6c61d54e1d825349de5d8b6) Fixes bug [#483066](https://bugs.kde.org/483066)
+ Always set a fixed size. [Commit.](http://commits.kde.org/plasma-workspace/6016a816d5788a7a621f9bdaef194979229081f4) Fixes bug [#477317](https://bugs.kde.org/477317)
+ Eventpluginsmanager: don't crash on double-delete of plugin objects. [Commit.](http://commits.kde.org/plasma-workspace/51239aa1033b6deeff0e382e43d4abaf9411a756) Fixes bug [#477165](https://bugs.kde.org/477165)
+ Plasma-apply-wallpaperimage: add a test. [Commit.](http://commits.kde.org/plasma-workspace/82a81bb8e3f831e58e12d83f8927c87f0f05837c) 
+ Dataengines/powermanagement: set result for inhibition request. [Commit.](http://commits.kde.org/plasma-workspace/ab792c02861b61e0af375e68eaf03d76ffd8a4d4) Fixes bug [#477355](https://bugs.kde.org/477355)
+ Libkmpris: demote nonstandard player warnings to debug. [Commit.](http://commits.kde.org/plasma-workspace/71580e164de22e4d9d189682ad0d54348652ec16) 
+ Appiumtests: add test for krunner main ui. [Commit.](http://commits.kde.org/plasma-workspace/1d95a92a6ca16e319c6cbd9c7f84b16499aabb07) 
+ [kfontinst] Skip window parenting on Wayland. [Commit.](http://commits.kde.org/plasma-workspace/f1734fd5cebffcce7cce9e67f6a082ec114ab4a5) Fixes bug [#484273](https://bugs.kde.org/484273)
+ Fix(notifications_kcm): ensure search terms work. [Commit.](http://commits.kde.org/plasma-workspace/d5f22823536f3847f1848fcbc4424dfd9799800d) 
+ Applets/appmenu: fix keeping button always visible in compact mode. [Commit.](http://commits.kde.org/plasma-workspace/94ef9514cc1cdaf568ae7aebd16716dcb78741f6) Fixes bug [#435928](https://bugs.kde.org/435928)
+ Revert "ci: add clang-format validation job". [Commit.](http://commits.kde.org/plasma-workspace/aec7a49d018d69d82c224f67f4ce9f9a7a28d15a) 
+ SystemTray/ExpandedRepresentation: Fix icon and checked state of primary action button. [Commit.](http://commits.kde.org/plasma-workspace/a7137813c3e9b158160b9bb9116a733e7d1e55ca) 
+ Appiumtests: skip mediacontrollertest for llvm 18.1.1-1.2. [Commit.](http://commits.kde.org/plasma-workspace/6983b7a654824c50b6290f279d338f419c6f8b7a) 
+ [interactiveconsole] Fix syntax in Messages.sh. [Commit.](http://commits.kde.org/plasma-workspace/61b623f16abd6258f104548343315d56ed269186) 
+ Shell: emit screenGeometryChanged when panel is ready. [Commit.](http://commits.kde.org/plasma-workspace/bd226094ccb3d143538553ec31add3133b9a5f34) Fixes bug [#482339](https://bugs.kde.org/482339). Fixes bug [#481736](https://bugs.kde.org/481736)
+ PanelView: fix rejecting click events near window edge with HiDPI. [Commit.](http://commits.kde.org/plasma-workspace/2288a0f1c576a0e096208caa195b5cbcb43c877b) Fixes bug [#482580](https://bugs.kde.org/482580)
+ Fix(soundtheme_kcm): ensure search terms work. [Commit.](http://commits.kde.org/plasma-workspace/34f6668dc6b49c7c62364552131f003fc51f3da4) 
+ [applets/systemtray] Don't needlessly update iconloader. [Commit.](http://commits.kde.org/plasma-workspace/00e2ee97a50a51f599fc6a7ca566991624e673a6) Fixes bug [#483900](https://bugs.kde.org/483900)
+ Interactiveconsole(i18n): set domain & add Messages.sh. [Commit.](http://commits.kde.org/plasma-workspace/f3296213ec75fa6f9aa5865163ca751aa532ff80) 
+ Libkmpris: check identity before emitting initialFetchFinished. [Commit.](http://commits.kde.org/plasma-workspace/de59f381d80e0c97564daa664215c186256482be) Fixes bug [#483818](https://bugs.kde.org/483818)
+ Krunner: Remove code for assigning favorites to model. [Commit.](http://commits.kde.org/plasma-workspace/2cb1a4bd536fe0fc0e14bc6936ddb69828d79e47) See bug [#480750](https://bugs.kde.org/480750)
+ Kcm_fonts: add subpixel and hinting previews. [Commit.](http://commits.kde.org/plasma-workspace/499e096d87d38ae4b9b194797520aaaf2450f33f) Fixes bug [#432320](https://bugs.kde.org/432320)
+ Add .vscode to gitignore. [Commit.](http://commits.kde.org/plasma-workspace/cfab35420b1fc614d6ded00604a5187e32aa62ce) 
+ Kcm_fonts: fix combobox item not taking up full width. [Commit.](http://commits.kde.org/plasma-workspace/53d1a09f414d3c5463860f451b70b72831df4bc5) 
+ Startplasma: Use the sound theme setting for startup sound. [Commit.](http://commits.kde.org/plasma-workspace/06ed14dfade3dd0195dfc3320e57dbd3226eb191) 
+ Add missing Qt::StringLiterals using. [Commit.](http://commits.kde.org/plasma-workspace/873c7e0f97fc811c10bf36494a6bd9f0d43fe1c4) 
+ Resolve startup sound against ocean theme. [Commit.](http://commits.kde.org/plasma-workspace/3ff5540efc259c9d428841d4038a54f2bf76df11) Fixes bug [#482716](https://bugs.kde.org/482716)
+ Applets/digital-clock: remove timezone highlight on click. [Commit.](http://commits.kde.org/plasma-workspace/676e93d8ef63c599d34da183cbb83e65b41f0012) Fixes bug [#483438](https://bugs.kde.org/483438)
+ Ungrab any drag before cancelling edit mode. [Commit.](http://commits.kde.org/plasma-workspace/b465e3c97031fe774e981e0feb1ef10694c7376e) See bug [#482371](https://bugs.kde.org/482371). See bug [#483287](https://bugs.kde.org/483287)
+ Kcms/nightcolor: show transition overlap error only on custom times. [Commit.](http://commits.kde.org/plasma-workspace/518ae854baab51b736ba4787920e3b0ca9d39bd0) 
+ Kcms/nightcolor: Make manual lat/lon respect locale decimal point. [Commit.](http://commits.kde.org/plasma-workspace/cd5111de2c7323c28a5295fe713868bf25e9fcdd) Fixes bug [#413956](https://bugs.kde.org/413956)
+ Appiumtests: move applet tests to their own folder. [Commit.](http://commits.kde.org/plasma-workspace/dd3f73da70f81f9c05f531ac5bf2883e87c5319a) 
+ Appiumtests: skip touch gesture tests in the CI. [Commit.](http://commits.kde.org/plasma-workspace/b465cdba82160d5ba7be47615500ab50b1239e74) 
{{< /details >}}

{{< details title="Powerdevil" href="https://commits.kde.org/powerdevil" >}}
+ Kcmodule: Don't crash when a configured button action is unsupported. [Commit.](http://commits.kde.org/powerdevil/aedb2529b31ec9b62ee5a8b3548be848769f4009) Fixes bug [#482668](https://bugs.kde.org/482668)
{{< /details >}}

{{< details title="SDDM KCM" href="https://commits.kde.org/sddm-kcm" >}}
+ Sync the current kwin output configuration. [Commit.](http://commits.kde.org/sddm-kcm/61ab30924b9b26dafbad9f4eb3aa6e2cefe73fc1) 
+ Reset cursor theme and size to defaults if they are unset in Plasma. [Commit.](http://commits.kde.org/sddm-kcm/8e1a4c1b74cb41479e87a7c663348a9d598f37fb) 
+ Allow unsetting values when syncing Plasma settings. [Commit.](http://commits.kde.org/sddm-kcm/434280230eaebe7565287732f0afa07eb7572b9a) 
+ Adapt to Qt6 QVariant::isNull() changes. [Commit.](http://commits.kde.org/sddm-kcm/b455da2a79c06d958e5b302b43da8c1c96a70211) 
{{< /details >}}

{{< details title="xdg-desktop-portal-kde" href="https://commits.kde.org/xdg-desktop-portal-kde" >}}
+ AppChooserDialog: Reload model when Sycoca database changes. [Commit.](http://commits.kde.org/xdg-desktop-portal-kde/eb5aaecedbe5b304423c7b8b49bca89e2968de3d) 
{{< /details >}}
