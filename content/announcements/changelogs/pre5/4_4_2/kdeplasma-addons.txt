------------------------------------------------------------------------
r1096698 | annma | 2010-02-28 01:59:09 +1300 (Sun, 28 Feb 2010) | 4 lines

Thanks Michael for the patch, too bad I did not see this before 4.4.1 tagging, will only be in 4.4.2.
Will fixe also in trunk
BUG=194302

------------------------------------------------------------------------
r1096761 | mart | 2010-02-28 04:17:00 +1300 (Sun, 28 Feb 2010) | 2 lines

backport dbus name fix

------------------------------------------------------------------------
r1097238 | mart | 2010-03-01 08:22:35 +1300 (Mon, 01 Mar 2010) | 4 lines

read all the text we can, including children, this makes the parser not skip user info of certain messages with geodata
patch by alexjp


------------------------------------------------------------------------
r1098934 | mdawson | 2010-03-05 06:53:15 +1300 (Fri, 05 Mar 2010) | 8 lines

Fix a series of bugs regarding the cpu usage not properly rendering, either with a floating bar or with a non-updating bar.

Also disables disconnecting unused cpu sources.

BUG: 227291
BUG: 221375
BUG: 227144

------------------------------------------------------------------------
r1101194 | asouza | 2010-03-10 03:49:02 +1300 (Wed, 10 Mar 2010) | 10 lines

Fix minimum size of weather plasmoid

There is no way to show all the information with a size smaller
than the one that is not setted as minimum. This seems to be the
best approach to fix this bug.

Backport to kde 4.4

BUG:201154

------------------------------------------------------------------------
r1101199 | asouza | 2010-03-10 04:06:57 +1300 (Wed, 10 Mar 2010) | 13 lines

Change weather behavior when it doesnt find icon condition

The correct fix would be to show the bigger version of the current
day condition. It's coming after this one.

This is just polishing as showing an invalid icon in front of
the name of the city is just not good: ugly icon in the face of
the user and broken usability (hard to read the name of the city).
Polishing++

Backport to 4.4


------------------------------------------------------------------------
r1101201 | asouza | 2010-03-10 04:11:09 +1300 (Wed, 10 Mar 2010) | 14 lines

Try a nicer icon if can't find a proper one

Just hide the icon if there is no way to get a nice icon.
We already have "today's weather icon" showing on the grid
so use that as it's much nicer and polished.

Hide the icon otherwise and avoid showing dummy icon that
covers the name of the city.

Backport to 4.4

Polish++ :)


------------------------------------------------------------------------
r1101204 | asouza | 2010-03-10 04:19:10 +1300 (Wed, 10 Mar 2010) | 8 lines

Avoid modal dialogs in Plasma

Otherwise the user is not able to use the taskbar, systray, etc..
While it's (sometimes) taking too much time to search for the city.
It's probably a good idea to avoid modals everywhere in Plasma...

Backport to 4.4

------------------------------------------------------------------------
r1101468 | asouza | 2010-03-10 16:19:26 +1300 (Wed, 10 Mar 2010) | 2 lines

Revert commit where it was set a hardcoded minimum size

------------------------------------------------------------------------
r1102625 | bjacob | 2010-03-13 15:36:52 +1300 (Sat, 13 Mar 2010) | 4 lines

backport bugfix to 4.4: only accept left and mid button events
(was preventing the right click menu from working)


------------------------------------------------------------------------
r1105636 | scripty | 2010-03-21 09:15:16 +1300 (Sun, 21 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1105829 | scripty | 2010-03-22 02:23:16 +1300 (Mon, 22 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1106168 | scripty | 2010-03-22 15:48:38 +1300 (Mon, 22 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1106447 | astromme | 2010-03-23 10:39:36 +1300 (Tue, 23 Mar 2010) | 5 lines

CCBUG: 231622
Backport to 4.4

Encode arguments in the request URL with percent encoding. Allows the addition of tasks with funky characters such as #,&,+

------------------------------------------------------------------------
r1106771 | alexmerry | 2010-03-24 11:17:02 +1300 (Wed, 24 Mar 2010) | 6 lines

Backport r1106768 to 4.4: Make sure calls over D-Bus to media players do not block the KRunner gui.

CCBUG: 203668



------------------------------------------------------------------------
r1106887 | scripty | 2010-03-24 15:29:44 +1300 (Wed, 24 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1107215 | scripty | 2010-03-25 15:30:44 +1300 (Thu, 25 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
