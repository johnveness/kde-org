------------------------------------------------------------------------
r1096784 | porten | 2010-02-28 05:46:13 +1300 (Sun, 28 Feb 2010) | 2 lines

Merged revision 1096783:
Lower max recursion limit. Was too much on my amd64 system.
------------------------------------------------------------------------
r1096853 | ahartmetz | 2010-02-28 09:35:15 +1300 (Sun, 28 Feb 2010) | 4 lines

Backport revisions 1074287, 1074338, 1074815, 1075312: cache entries scoreboard
to reduce disk seeks by not having to read gazillions of small files.


------------------------------------------------------------------------
r1096878 | fdekruijf | 2010-02-28 10:34:05 +1300 (Sun, 28 Feb 2010) | 1 line

documentation footer translatable
------------------------------------------------------------------------
r1096903 | aseigo | 2010-02-28 11:19:24 +1300 (Sun, 28 Feb 2010) | 2 lines

fix a regression with the link fix: we only want to go into text browser mode when text is selectable

------------------------------------------------------------------------
r1097020 | aseigo | 2010-02-28 21:32:00 +1300 (Sun, 28 Feb 2010) | 2 lines

use the accessor to get to the native widget

------------------------------------------------------------------------
r1097100 | ilic | 2010-03-01 02:05:45 +1300 (Mon, 01 Mar 2010) | 1 line

New mapping for dialect hybridization. (bport: 1097099)
------------------------------------------------------------------------
r1097412 | scripty | 2010-03-01 23:30:22 +1300 (Mon, 01 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1097544 | adawit | 2010-03-02 05:01:00 +1300 (Tue, 02 Mar 2010) | 1 line

Ensure unexpected deletion of KStatusBarItem's widget does not cause a crash
------------------------------------------------------------------------
r1097572 | trueg | 2010-03-02 06:09:49 +1300 (Tue, 02 Mar 2010) | 1 line

Backport: Added another example to sparqlQuery to make clear that one can basically query anything this way.
------------------------------------------------------------------------
r1097584 | aseigo | 2010-03-02 06:58:37 +1300 (Tue, 02 Mar 2010) | 2 lines

add the standard config pages in the AppletScript fallback method for showing the configuration

------------------------------------------------------------------------
r1097623 | dafre | 2010-03-02 08:20:31 +1300 (Tue, 02 Mar 2010) | 6 lines

This commit is a massive backport of: r1091455, r1091811, r1091516, r1091017, r1089968, r1089130 and maybe some others which I forgot.

Basically it makes KAuth's plugins dynamic, except for fake ones. Bottom line: less libraries linked to kdecore and no crashes upon installation problems.
Tested on a clean build of the branch, works great.


------------------------------------------------------------------------
r1097737 | mmrozowski | 2010-03-02 14:07:37 +1300 (Tue, 02 Mar 2010) | 1 line

SVN_SILENT fixed some minor spelling in comments
------------------------------------------------------------------------
r1097920 | sebsauer | 2010-03-03 01:35:24 +1300 (Wed, 03 Mar 2010) | 3 lines

backport r1097919
added include() function which allows to load qt.gui and other QtScriptExtension's from within scripts.

------------------------------------------------------------------------
r1097930 | mart | 2010-03-03 02:03:40 +1300 (Wed, 03 Mar 2010) | 2 lines

backport accelerator markers removal in tooltips

------------------------------------------------------------------------
r1097934 | lunakl | 2010-03-03 02:15:24 +1300 (Wed, 03 Mar 2010) | 6 lines

Backport:
With viewports, use cached geometry value instead of querying
X about it, avoids threading problems in KRunner.
Patch by Danny Baumann.


------------------------------------------------------------------------
r1097945 | dfaure | 2010-03-03 02:56:43 +1300 (Wed, 03 Mar 2010) | 3 lines

Resolve aliases when reading mimeapps.list, useful when shared-mime-info turns a real mimetype into an alias,
and matches the behavior in gio (according to Alexander Larsson).

------------------------------------------------------------------------
r1097954 | dfaure | 2010-03-03 03:19:38 +1300 (Wed, 03 Mar 2010) | 12 lines

As discussed with Pino: make alias resolution the default in KMimeType::mimeType.
There is just no good reason not to, aliases can be used anywhere, especially with
shared-mime-info replacing mimetypes with aliases over time (e.g. image/ico).

This fixes at least two bugs:
1) a bug noticed by Pino: if you set the old name as filter in kfiledialog, when
   updating s-m-i it won't work anymore (because aliases are not resolved)
2) the bug that nspluginscan would define mimetypes because "unknown" when they are
   in fact aliases.
BUG: 197346
Fixed for: 4.4.2

------------------------------------------------------------------------
r1097987 | mart | 2010-03-03 04:28:36 +1300 (Wed, 03 Mar 2010) | 2 lines

backport use of action->toolTip()

------------------------------------------------------------------------
r1098062 | grossard | 2010-03-03 08:18:04 +1300 (Wed, 03 Mar 2010) | 2 lines

added a new translator

------------------------------------------------------------------------
r1098376 | gladhorn | 2010-03-04 04:45:09 +1300 (Thu, 04 Mar 2010) | 3 lines

backport 1098372
use forward declaration so apps using knewstuff upload don't need attica headers

------------------------------------------------------------------------
r1099157 | pino | 2010-03-05 14:33:53 +1300 (Fri, 05 Mar 2010) | 2 lines

reduce few columns, so it gives less issues in manpages generation

------------------------------------------------------------------------
r1099482 | reed | 2010-03-06 06:19:22 +1300 (Sat, 06 Mar 2010) | 1 line

skip -psn_* on OSX in a less brittle way
------------------------------------------------------------------------
r1099487 | reed | 2010-03-06 06:28:12 +1300 (Sat, 06 Mar 2010) | 7 lines

work around OSX 10.6 fork .. exec restrictions

Qt's QFileInfo does a number of calls in it's constructor,
some of which trigger 10.6's draconian restrictions on
CoreFoundation calls after fork.  Do some simple stats
instead for this code.

------------------------------------------------------------------------
r1099489 | reed | 2010-03-06 06:31:47 +1300 (Sat, 06 Mar 2010) | 1 line

add a custom Info.plist bundle to make kded invisible on OSX
------------------------------------------------------------------------
r1099491 | reed | 2010-03-06 06:34:50 +1300 (Sat, 06 Mar 2010) | 1 line

refactor mac initialization in kuniqueapplication
------------------------------------------------------------------------
r1099494 | reed | 2010-03-06 06:40:12 +1300 (Sat, 06 Mar 2010) | 7 lines

hide kinit from the dock

Add a plist file to hide kinit from the dock.  Also
changes the "GUI" build of kinit from APPLE to
Q_WS_MAC, since Q_WS_X11 on OSX should still be
nogui.

------------------------------------------------------------------------
r1099497 | reed | 2010-03-06 06:42:20 +1300 (Sat, 06 Mar 2010) | 1 line

initialize dbus if necessary in klauncher on OSX
------------------------------------------------------------------------
r1099499 | reed | 2010-03-06 06:44:07 +1300 (Sat, 06 Mar 2010) | 1 line

OSX 10.4 needs Xlib.h included before the screensaver extension
------------------------------------------------------------------------
r1099634 | pletourn | 2010-03-06 10:31:08 +1300 (Sat, 06 Mar 2010) | 4 lines

Fix block selection

CCBUG:229454

------------------------------------------------------------------------
r1099656 | mart | 2010-03-06 11:32:09 +1300 (Sat, 06 Mar 2010) | 2 lines

backport mask fixing

------------------------------------------------------------------------
r1099660 | mart | 2010-03-06 11:36:57 +1300 (Sat, 06 Mar 2010) | 2 lines

backport mask deactivation on composite on

------------------------------------------------------------------------
r1100005 | annma | 2010-03-07 03:30:57 +1300 (Sun, 07 Mar 2010) | 3 lines

backport Aaron's fixe r1099727  for 229566
CCBUG=229566

------------------------------------------------------------------------
r1100529 | winterz | 2010-03-08 07:29:29 +1300 (Mon, 08 Mar 2010) | 7 lines

Backport r1100528 by winterz from trunk to the 4.4 branch:

fix compile on Sun Studio 12 U1 "Error: tm_gmtoff is not a member of std::tm"
based on a patch by tropikhajma
CCBUG: 217802


------------------------------------------------------------------------
r1100899 | mwolff | 2010-03-09 11:43:01 +1300 (Tue, 09 Mar 2010) | 3 lines

backport: also set text/plain data in clipboard to HTML version

CCBUG: 228204
------------------------------------------------------------------------
r1100900 | mwolff | 2010-03-09 11:44:13 +1300 (Tue, 09 Mar 2010) | 3 lines

backport: enable exporter plugin by default

CCBUG: 230012
------------------------------------------------------------------------
r1100905 | rkcosta | 2010-03-09 11:55:20 +1300 (Tue, 09 Mar 2010) | 4 lines

Backport r1100904.

Use toPrettyPercentEncoding() for the query part too, otherwise "%0A" ends up being interpreted as "\n", for example.

------------------------------------------------------------------------
r1100961 | rkcosta | 2010-03-09 16:21:59 +1300 (Tue, 09 Mar 2010) | 13 lines

Backport r1100959.

Revert r861021, which added QUrl::fromPercentEncoding() to QUrl::encodedQuery().

prettyUrl() did return a prettier version of the query string, however it was broken when
the original URL contained characters such as %A0 (should be %C2%A0 to be valid UTF-8),
which were converted to UTF-8 by fromPercentEncoding().

This ends up turning %A0 into some weird, unexpected UTF-8 character in Konqueror's
location bar as well, which would pass the wrong query string when the page was reloaded.

Test cases updated.

------------------------------------------------------------------------
r1100977 | aseigo | 2010-03-09 18:13:50 +1300 (Tue, 09 Mar 2010) | 2 lines

guard against multiple connections due to errant StartupCompleted constraints

------------------------------------------------------------------------
r1100982 | aseigo | 2010-03-09 18:58:35 +1300 (Tue, 09 Mar 2010) | 2 lines

allow arbitrary in-package paths with empty fileType

------------------------------------------------------------------------
r1101268 | aseigo | 2010-03-10 09:02:25 +1300 (Wed, 10 Mar 2010) | 3 lines

properties need to be fully qualified
CCBUG:229668

------------------------------------------------------------------------
r1101319 | aseigo | 2010-03-10 10:53:29 +1300 (Wed, 10 Mar 2010) | 2 lines

don't crash when failing multiple times

------------------------------------------------------------------------
r1101373 | ilic | 2010-03-10 13:48:26 +1300 (Wed, 10 Mar 2010) | 1 line

Language and scripts splitting now fully language-specific, on need-to-have basis.
------------------------------------------------------------------------
r1101387 | rkcosta | 2010-03-10 15:28:49 +1300 (Wed, 10 Mar 2010) | 6 lines

Backport r1101386.

For now, make all tests pass again after the latest commit to KUrl::prettyUrl().

Maybe the previous prettyUrl() behaviour should be restored and Konqueror "fixed" instead. Not really sure yet.

------------------------------------------------------------------------
r1101683 | trueg | 2010-03-11 04:14:55 +1300 (Thu, 11 Mar 2010) | 1 line

Backport: properly exclude the private classes
------------------------------------------------------------------------
r1101692 | dfaure | 2010-03-11 05:33:29 +1300 (Thu, 11 Mar 2010) | 4 lines

Fix compilation error with Qt-4.6.0, plasma/svgwidget.moc:53: error: 'staticMetaObject' is not a member of 'Plasma'
Apparently moc is more clever in Qt-4.6.3, but anyway http://bugreports.qt.nokia.com/browse/QTBUG-2151 is right,
the property is only usable if declared with Q_DECLARE_METATYPE, isn't it?

------------------------------------------------------------------------
r1101739 | winterz | 2010-03-11 08:44:29 +1300 (Thu, 11 Mar 2010) | 8 lines

add the QtCore and QtNetwork libraries to the CMAKE_REQUIRED_LIBRARIES
when doing try compiles. 
Might fix building with -g on Sun Studio12

tropikhajma, please test and let me know if this works.
if it does, we can forward port to trunk.
BUG: 204993

------------------------------------------------------------------------
r1101774 | aseigo | 2010-03-11 10:42:57 +1300 (Thu, 11 Mar 2010) | 2 lines

protect against stupid DataEngineScript implementations

------------------------------------------------------------------------
r1102048 | trueg | 2010-03-12 06:19:08 +1300 (Fri, 12 Mar 2010) | 1 line

Backport: Query optimization: we only use the one result anyway.
------------------------------------------------------------------------
r1102154 | pletourn | 2010-03-12 11:37:50 +1300 (Fri, 12 Mar 2010) | 4 lines

Fix block selection

CCBUG:229454

------------------------------------------------------------------------
r1102457 | ahartmetz | 2010-03-13 04:17:47 +1300 (Sat, 13 Mar 2010) | 2 lines

Backport r1097531: sort items by user-visible name, not internal name.

------------------------------------------------------------------------
r1102696 | ossi | 2010-03-13 23:18:15 +1300 (Sat, 13 Mar 2010) | 12 lines

backport: redirect stdout to stderr

if we are forked from a command line tool (say, ktraderclient) which is
used in a pipe, us keeping open the inherited stdout is pretty fatal for
the usefulness of the pipe.
as we have no use for a real stdout (if any at all), just redirect it to
stderr.
we can't just close/redirect stderr, as this is where all "interesting"
output is going. so if somebody tries to use some tool's stderr in a
pipe, he'd still be out of luck.


------------------------------------------------------------------------
r1102698 | ossi | 2010-03-13 23:22:12 +1300 (Sat, 13 Mar 2010) | 8 lines

backport: don't lock out the original owner when stealing a file

there should be no harm in applying the original file's permissions even
if the chown failed - after all, these permissions made it possible for
us to work with the file, so why would we suddenly have an interest in
disallowing others to do the same?


------------------------------------------------------------------------
r1102840 | orlovich | 2010-03-14 08:21:32 +1300 (Sun, 14 Mar 2010) | 3 lines

Merged revision:r1102835 | orlovich | 2010-03-13 14:11:37 -0500 (Sat, 13 Mar 2010) | 2 lines

Erk. Fix the null namespace constant.... in particular fixes creation of XML elements. 
------------------------------------------------------------------------
r1102935 | ehamberg | 2010-03-14 14:26:55 +1300 (Sun, 14 Mar 2010) | 7 lines

don't accept completion when user types a character as this breaks completion in kdevelop.

(also fixed in trunk.)

BUG: 230402


------------------------------------------------------------------------
r1103088 | mludwig | 2010-03-14 23:08:58 +1300 (Sun, 14 Mar 2010) | 4 lines

Ensure that no empty string is passed on to Sonnet as this can lead to a crash.

Back-port of revision 1103086.

------------------------------------------------------------------------
r1103162 | neundorf | 2010-03-15 03:15:04 +1300 (Mon, 15 Mar 2010) | 5 lines

-fix linking with OpenGL on OSX, same as in trunk

Alex


------------------------------------------------------------------------
r1103253 | rkcosta | 2010-03-15 06:42:48 +1300 (Mon, 15 Mar 2010) | 9 lines

Backport r1103252.

Do not strip of simplify an <option>'s value.

Simplifying is simply wrong, and trimming isn't required by the spec,
so we just follow other engines' behaviour here.

CCBUG: 60867

------------------------------------------------------------------------
r1103621 | trueg | 2010-03-16 03:05:12 +1300 (Tue, 16 Mar 2010) | 2 lines

Backport: Make sure newly generated URIs are not already used as graphs.
CCMAIL: nlecureuil@mandriva.com
------------------------------------------------------------------------
r1103855 | scripty | 2010-03-16 15:42:51 +1300 (Tue, 16 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1104042 | rkcosta | 2010-03-17 03:54:35 +1300 (Wed, 17 Mar 2010) | 9 lines

Backport r1104041.

Do not call RenderBox::relativePositionOffset() from RenderImage::nodeAtPoint().

That call, acording to Germain, is likely to be a remnant of the times when nodeAtPoint
didn't pass through RenderLayer.

CCBUG: 134388

------------------------------------------------------------------------
r1104130 | dfaure | 2010-03-17 09:07:21 +1300 (Wed, 17 Mar 2010) | 2 lines

Backport fix for 213876, crash when closing okularpart tab

------------------------------------------------------------------------
r1104322 | ereslibre | 2010-03-18 00:54:28 +1300 (Thu, 18 Mar 2010) | 4 lines

Backport revision 1104321 to 4.4. Fix infinite recursion under certain circumstances.

CCBUG: 213068

------------------------------------------------------------------------
r1104759 | gladhorn | 2010-03-19 03:48:50 +1300 (Fri, 19 Mar 2010) | 3 lines

backport 1104758 
fix connect for details dialog to properly update the state when an entry is changed (eg installed)

------------------------------------------------------------------------
r1104858 | gladhorn | 2010-03-19 08:01:34 +1300 (Fri, 19 Mar 2010) | 3 lines

backport r1104857
save the right urls for previews and payload in registry

------------------------------------------------------------------------
r1104867 | yurchor | 2010-03-19 08:16:07 +1300 (Fri, 19 Mar 2010) | 2 lines

Backporting commit 1104861
BUG: 231183
------------------------------------------------------------------------
r1105396 | dfaure | 2010-03-20 14:43:08 +1300 (Sat, 20 Mar 2010) | 4 lines

Backport r1105394: Fix the disabling of debug output when compiling apps in release mode.
Fixed for: 4.4.2
BUG: 227089

------------------------------------------------------------------------
r1105720 | modax | 2010-03-21 14:29:17 +1300 (Sun, 21 Mar 2010) | 9 lines

Backport 1105719 from trunk:

Fix typo in KDE4Macros.cmake::KDE4_INSTALL_AUTH_HELPER_FILES.

The name of the variable is KDE4_AUTH_HELPER_BACKEND_NAME rather than
KAUTH_HELPER_BACKEND_NAME.

CCMAIL: drf54321@gmail.com

------------------------------------------------------------------------
r1105852 | aacid | 2010-03-22 03:18:14 +1300 (Mon, 22 Mar 2010) | 3 lines

backport SVN commit 1105850 by mlaurent:
Add missing i18n

------------------------------------------------------------------------
r1106080 | mpyne | 2010-03-22 09:08:23 +1300 (Mon, 22 Mar 2010) | 13 lines

Backport bugfix for 182026 (KRunner/Plasma crashes due to
KPixmapCache::discard) to KDE 4.4.2. This has had several days to brew in trunk
and the reaction from testers is quite favorable.

If you continue to receive crashes in KPixmapCache that don't start from
KPixmapCache::discard, it's a different bug which may already be reported.

If merely loading a cached icon causes a crash (e.g. a crash in QPixmap) you may have a
corrupted cache -- try removing /var/tmp/kdecache-$USER/kpc/*.{index,data} and restarting
KDE.

BUG:182026

------------------------------------------------------------------------
r1106241 | dfaure | 2010-03-22 23:22:10 +1300 (Mon, 22 Mar 2010) | 2 lines

Fix I18N_ARGUMENT_MISSING in ssl info dialog

------------------------------------------------------------------------
r1106364 | orlovich | 2010-03-23 05:20:52 +1300 (Tue, 23 Mar 2010) | 6 lines

Merged revision:r1106363 | orlovich | 2010-03-22 12:19:21 -0400 (Mon, 22 Mar 2010) | 6 lines

Apply patch from pcc (username peter, host pcc.me, tld uk) that fixes 
updating of lastIndex in Regexp.prototype.test. 

Thanks!
------------------------------------------------------------------------
r1106409 | adawit | 2010-03-23 07:54:23 +1300 (Tue, 23 Mar 2010) | 1 line

Backport: Use proper debug area for debug/warning statements
------------------------------------------------------------------------
r1106446 | gladhorn | 2010-03-23 10:39:21 +1300 (Tue, 23 Mar 2010) | 3 lines

backport r1106445
make namespace of signal parameter fully qualified

------------------------------------------------------------------------
r1107047 | trueg | 2010-03-25 06:23:20 +1300 (Thu, 25 Mar 2010) | 4 lines

Backport: Use a REGEX FILTER instead of bif:contains full text query to match properties. Since we
have a rather limited set of properties (around 1000) a filter is much faster than a full
text query.

------------------------------------------------------------------------
r1107144 | mwolff | 2010-03-25 12:07:06 +1300 (Thu, 25 Mar 2010) | 2 lines

backport r1106483: merge two inserts into one undoSafePoint

------------------------------------------------------------------------
r1107146 | mwolff | 2010-03-25 12:07:36 +1300 (Thu, 25 Mar 2010) | 2 lines

backport r1106487: fix handling of escaped variables, fixes cases like \${foo${real template var}asdf}

------------------------------------------------------------------------
r1107147 | mwolff | 2010-03-25 12:08:07 +1300 (Thu, 25 Mar 2010) | 1 line

backport r1106486: --debug
------------------------------------------------------------------------
r1107148 | mwolff | 2010-03-25 12:10:48 +1300 (Thu, 25 Mar 2010) | 1 line

backport r1104181 by pletourn: Implement 'replace all' in block selection
------------------------------------------------------------------------
r1107150 | mwolff | 2010-03-25 12:12:40 +1300 (Thu, 25 Mar 2010) | 1 line

backport r1104222 by pletourn: Use the non-broken pattern syntax
------------------------------------------------------------------------
r1107210 | mpyne | 2010-03-25 15:24:54 +1300 (Thu, 25 Mar 2010) | 7 lines

Backport fix for a regression introduced in 4.4 branch to 4.4.2. (This bug was not in 4.4.1)
The issue is that my recent crash fix for the KPixmapCache Bug of Legend made the cache
stop working. It was caught by Parker Coates though so 4.4.2 should have the best of both
worlds.

CCBUG:182026

------------------------------------------------------------------------
r1107306 | trueg | 2010-03-25 22:32:42 +1300 (Thu, 25 Mar 2010) | 1 line

Backport: Yet another slight improvement in the property matching query.
------------------------------------------------------------------------
r1107354 | mwolff | 2010-03-26 01:59:58 +1300 (Fri, 26 Mar 2010) | 4 lines

fix compilation, apply rest of 1104181 - sorry I missed that yesterday (probably overlooked the conflict...)

CCMAIL: mseiwert@hbv.de
CCMAIL: faure@kde.org
------------------------------------------------------------------------
r1107447 | trueg | 2010-03-26 06:35:04 +1300 (Fri, 26 Mar 2010) | 1 line

Properly resolve fields in subterms of negation terms
------------------------------------------------------------------------
r1107636 | segato | 2010-03-26 23:45:28 +1300 (Fri, 26 Mar 2010) | 4 lines

backport r1107626
automatically create the emoticons directory if it's missing
CCBUG:231770

------------------------------------------------------------------------
r1107732 | adawit | 2010-03-27 05:26:03 +1300 (Sat, 27 Mar 2010) | 1 line

Added debug area 800 for kdewebkit
------------------------------------------------------------------------
r1107743 | mueller | 2010-03-27 06:18:40 +1300 (Sat, 27 Mar 2010) | 2 lines

KDE 4.4.2

------------------------------------------------------------------------
