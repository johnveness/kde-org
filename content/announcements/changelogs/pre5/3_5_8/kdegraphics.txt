2007-05-16 05:12 +0000 [r665177]  mm

	* branches/KDE/3.5/kdegraphics/kamera/kioslave/kamera.h,
	  branches/KDE/3.5/kdegraphics/kamera/kioslave/kamera.cpp: Reworked
	  to open/close logic to do some speed-ups: * We no longer close
	  the camera immediately after every request. * We close the camera
	  after: - There are no pending requests for this slave AND - The
	  idle timeout of 30 seconds has been reached. - Another slave
	  marks that it needs the camera. A lock file
	  (locate("tmp","kamera")) is used for marking "give up camera".
	  Did some fixes (gp_file_free -> gp_file_unref) to avoid crashes.
	  Also did some clean-ups in the header file.

2007-05-17 16:40 +0000 [r665684]  pino

	* branches/KDE/3.5/kdegraphics/kpdf/core/document.cpp: When getting
	  the result of a pixmap request for an observer that went away in
	  the meanwhile, don't create a memory entry for the pixmap, as we
	  assume the allocated pixmaps fifo holds only descriptors for
	  valid observers. BUG: 143951

2007-05-23 20:15 +0000 [r667745]  mm

	* branches/KDE/3.5/kdegraphics/kamera/kioslave/kamera.cpp: if just
	  statRoot() is done, set idletime to MAXIDLETIME. This helps kdemm
	  auto-detect the camera via media://camera and close it again if
	  nothing else happens (and not block other clients).

2007-05-25 18:35 +0000 [r668258]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/core/generator_pdf/generator_pdf.cpp:
	  changing APIs means you should TRIPLE check, double checking is
	  not enough :-/ BUGS: 145907

2007-05-27 12:38 +0000 [r668731]  mm

	* branches/KDE/3.5/kdegraphics/kamera/kcontrol/kamera.cpp: removed
	  tocstr

2007-05-28 12:22 +0000 [r669063]  mueller

	* branches/KDE/3.5/kdegraphics/kdvi/squeeze.c,
	  branches/KDE/3.5/kdegraphics/libkscan/kscanoption.cpp,
	  branches/KDE/3.5/kdegraphics/kghostview/kgv_miniwidget.cpp,
	  branches/KDE/3.5/kdegraphics/kviewshell/plugins/djvu/libdjvu/DataPool.cpp:
	  various uncommitted stuff

2007-06-06 13:56 +0000 [r672236]  mlaurent

	* branches/KDE/3.5/kdegraphics/ksnapshot/main.cpp: Backport minor
	  mem leak

2007-06-06 16:13 +0000 [r672280]  mlaurent

	* branches/KDE/3.5/kdegraphics/kghostview/main.cpp,
	  branches/KDE/3.5/kdegraphics/kooka/main.cpp,
	  branches/KDE/3.5/kdegraphics/kpovmodeler/main.cpp: Fix mem leak

2007-06-14 06:50 +0000 [r675383]  dang

	* branches/KDE/3.5/kdegraphics/kolourpaint/kpmainwindow_file.cpp:
	  Forget that string, it's not really important

2007-06-14 07:07 +0000 [r675386]  dang

	* branches/KDE/3.5/kdegraphics/kolourpaint/README,
	  branches/KDE/3.5/kdegraphics/kolourpaint/BUGS,
	  branches/KDE/3.5/kdegraphics/kolourpaint/VERSION: * Up ver to
	  1.4.8_relight-pre * Don't send bugs and wishes to
	  http://www.kolourpaint.org/support anymore

2007-06-14 09:29 +0000 [r675446]  dang

	* branches/KDE/3.5/kdegraphics/kolourpaint/kolourpaint.cpp,
	  branches/KDE/3.5/kdegraphics/kolourpaint/README: Drop references
	  to to-be-killed http://www.kolourpaint.org/support

2007-06-15 07:20 +0000 [r675802]  dang

	* branches/KDE/3.5/kdegraphics/kolourpaint/NEWS,
	  branches/KDE/3.5/kdegraphics/kolourpaint/kpmainwindow_file.cpp: *
	  Add error dialogs for: - if scanning support is unavailable -
	  running out of graphics memory during a scan

2007-06-20 20:25 +0000 [r678167]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/ui/pageview.cpp: Check we are
	  on PageView::MouseNormal on updateCursor and maybeTip, if not,
	  weird things happen and the user gets confused Thanks to Josep
	  Perarnau for discovering the bug, turning his head to my table
	  and telling "Albert i think i found a bug on KPDF" CCMAIL:
	  josep.perarnau@gmail.com

2007-06-21 17:27 +0000 [r678559]  binner

	* branches/KDE/3.5/kdegraphics/kfaxview/kfaxview.desktop,
	  branches/KDE/3.5/kdegraphics/kview/kview.desktop,
	  branches/KDE/3.5/kdegraphics/kolourpaint/kolourpaint.desktop,
	  branches/KDE/3.5/kdegraphics/kpdf/shell/kpdf.desktop,
	  branches/KDE/3.5/kdegraphics/kghostview/kghostview.desktop,
	  branches/KDE/3.5/kdegraphics/kuickshow/src/kuickshow.desktop: fix
	  invalid .desktop files

2007-06-27 08:40 +0000 [r680874]  savernik

	* branches/KDE/3.5/kdegraphics/kpdf/ui/toc.cpp,
	  branches/KDE/3.5/kdegraphics/kpdf/part.cpp: Provide horizontal
	  scrollbar in table of contents sidebar instead of cropping the
	  entries. This improves usability esp. for deeply nested TOCs.
	  BUG: 147233

2007-07-04 00:33 +0000 [r683031]  dang

	* branches/KDE/3.5/kdegraphics/kolourpaint/kpdocument.cpp: +comment

2007-07-12 05:48 +0000 [r686784]  dang

	* branches/KDE/3.5/kdegraphics/kolourpaint/kpdocument.cpp:
	  +explanation [BACKPORT FROM
	  branches/work/~dang/kdegraphics/kolourpaint]

2007-07-12 11:12 +0000 [r686888]  pino

	* branches/KDE/3.5/kdegraphics/kpdf/part.cpp: Sets the filename as
	  title also when reloading the document. CCBUG: 103362

2007-07-14 12:13 +0000 [r687806]  mueller

	* branches/KDE/3.5/kdegraphics/kmrml/kmrml/lib/kmrml_config.cpp:
	  fix comparison with string literal

2007-07-17 21:09 +0000 [r689188]  mkoller

	* branches/KDE/3.5/kdegraphics/libkscan/kscandevice.cpp: BUG:
	  119110 Correctly retrieve the x/y scan resolution and store it
	  into the scanned image

2007-07-21 12:03 +0000 [r690551]  dang

	* branches/KDE/3.5/kdegraphics/kolourpaint/pixmapfx/kpeffectbalance.cpp:
	  +TODO - I get this with KolourPaint in KDE 3.4.0, Fedora Core 4
	  CCMAIL: dang@kde.org

2007-07-25 09:02 +0000 [r692291]  pino

	* branches/KDE/3.5/kdegraphics/kpdf/ui/pagepainter.cpp: Take into
	  account the accessibility settings when drawing an empty page
	  waiting for the real page to be rendered. BUG: 148188

2007-07-30 13:59 +0000 [r694346]  mueller

	* branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/Stream.cc: newest
	  round of xpdf fixes (CVE-2007-3387)

2007-08-15 18:56 +0000 [r700519]  pino

	* branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/JBIG2Stream.cc:
	  Albert's fix for poppler bug 12014.

2007-08-22 12:19 +0000 [r703387]  bhards

	* branches/KDE/3.5/kdegraphics/kfile-plugins/exr/kfile_exr.cpp: One
	  of the "standard" attribute names changed between OpenEXR 1.4.0
	  and OpenEXR 1.6.0. At this stage I'm assuming that the change was
	  intentional, and will remain for future releases - there is an
	  open query on the EXR mailing list about this. This still needs
	  to get fixed for trunk, when the strigi porting is done, so the
	  bug remains open. CCBUG: 148865

2007-08-22 20:59 +0000 [r703564]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/part.cpp: Use fromPathOrURL so
	  people with strange dirs with # in them still get watching
	  working BUGS: 149102

2007-08-26 14:26 +0000 [r704875]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/dcop.h,
	  branches/KDE/3.5/kdegraphics/kpdf/part.h,
	  branches/KDE/3.5/kdegraphics/kpdf/part.cpp: got the necessary 2
	  seconds to review the patch. Thanks and sorry for taking so long.
	  Changed method's name to one i like better I'm not sure what are
	  we supposed to do with the profile for IRKick, i guess just
	  ignoring it :D BUGS: 148026

2007-08-26 14:44 +0000 [r704884]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/part.cpp: so much for reviewing
	  :-& Can't open presentation mode if no document is open, if you
	  do, ugly things happen

2007-08-26 18:22 +0000 [r704964]  pino

	* branches/KDE/3.5/kdegraphics/kpdf/part.cpp: Save the case
	  sensitivity of a search across multiple search sessions each time
	  a document is open. BUG: 149164

2007-08-31 11:32 +0000 [r706829]  pino

	* branches/KDE/3.5/kdegraphics/kpdf/ui/Makefile.am: pagepainter.cpp
	  requires the generated settings.h

2007-09-02 15:40 +0000 [r707656]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/GfxFont.cc: Don't
	  crash when we don't have a ctu. Fix imported from poppler BUGS:
	  149416

2007-09-05 18:19 +0000 [r708815]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/core/generator_pdf/generator_pdf.cpp,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/PSOutputDev.cc:
	  Sesame Street Class #1: Left is left, Right is right Sesame
	  Street Class #2: If you want to print parameters 0 and 1, use 0
	  and 1, not 0 and 0 This fixes some printing problems with margins
	  BUGS: 149560

2007-09-15 22:24 +0000 [r712984]  pino

	* branches/KDE/3.5/kdegraphics/kpdf/ui/searchwidget.cpp: give the
	  search lineedit text the usual color when it has no text

2007-09-15 23:23 +0000 [r712996]  pino

	* branches/KDE/3.5/kdegraphics/kpdf/ui/presentationwidget.cpp,
	  branches/KDE/3.5/kdegraphics/kpdf/ui/presentationwidget.h,
	  branches/KDE/3.5/kdegraphics/kpdf/part.cpp: Properly pass the
	  standard accelerators to the presentation mode, and make the
	  presentation mode able to handle those. BUG: 138451

2007-09-19 12:36 +0000 [r714382]  lunakl

	* branches/KDE/3.5/kdegraphics/kghostview/kgv_view.cpp: When
	  slotZoom() expects the zoom value in certain format, create it in
	  that format (like, don't do some translation that's useless
	  anyway).

2007-10-01 15:47 +0000 [r719657]  lunakl

	* branches/KDE/3.5/kdegraphics/ksvg/plugin/svgcreator.cpp: KURL(
	  path ) -> KURL::fromPathOrURL( path ) (bnc:328949)

2007-10-03 17:47 +0000 [r720782]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/shell/main.cpp,
	  branches/KDE/3.5/kdegraphics/kpdf/VERSION: bump version

2007-10-06 12:09 +0000 [r721994]  pino

	* branches/KDE/3.5/kdegraphics/kpdf/core/document.cpp: Wait at
	  least 2 seconds before reading /proc/memory again. BUG: 150325

2007-10-06 12:37 +0000 [r722007]  dang

	* branches/KDE/3.5/kdegraphics/kolourpaint/NEWS,
	  branches/KDE/3.5/kdegraphics/kolourpaint/kpmainwindow_edit.cpp: *
	  Paste transparent pixels as white instead of uninitialized
	  colors, when the app does not support pasting transparent pixels
	  (such as OpenOffice.org)

2007-10-06 13:17 +0000 [r722030]  dang

	* branches/KDE/3.5/kdegraphics/kolourpaint/README,
	  branches/KDE/3.5/kdegraphics/kolourpaint/VERSION: Prematurely up
	  ver and mark for KDE 3.5.8 freeze (already done in NEWS). More
	  commits to come tomorrow.

2007-10-07 02:03 +0000 [r722256]  dang

	* branches/KDE/3.5/kdegraphics/kolourpaint/NEWS,
	  branches/KDE/3.5/kdegraphics/kolourpaint/kpmainwindow_edit.cpp: *
	  Make "Edit / Paste in New Window" always paste white pixels as
	  white (it used to paste them as transparent when the selection
	  transparency mode was set to Transparent)

2007-10-07 05:37 +0000 [r722283]  dang

	* branches/KDE/3.5/kdegraphics/kolourpaint/tools/kptoolrotate.cpp,
	  branches/KDE/3.5/kdegraphics/kolourpaint/tools/kptoolresizescale.cpp,
	  branches/KDE/3.5/kdegraphics/kolourpaint/tools/kptoolskew.cpp:
	  Focus KNumInput's on creation instead of the OK button for the
	  Skew and Rotate dialogs. This allows the user to edit values
	  without an extra mouse click. For the Resize / Scale dialog,
	  focus the "Width:" KNumInput instead of the Operation Group Box
	  because: Users probably want to start editing the dimensions
	  straight away rather than the operation (the buttons inside the
	  group box), which were obscure to change via keyboard anyway (did
	  you know that you had to use the Left and Right arrow keys,
	  rather than Tab?). It is easy to select the operation afterwards
	  with the mouse since the buttons are huge. [BACKPORT of trunk/
	  -r720346 2007-10-03 and -r722282 2007-10-07]

2007-10-07 05:44 +0000 [r722284]  dang

	* branches/KDE/3.5/kdegraphics/kolourpaint/NEWS: NEWS for last
	  commit; it's really too minor to mention in trunk's NEWS though,
	  given the number of changes there.

2007-10-07 06:01 +0000 [r722290]  dang

	* branches/KDE/3.5/kdegraphics/kolourpaint/kpmainwindow_settings.cpp,
	  branches/KDE/3.5/kdegraphics/kolourpaint/kpmainwindow_help.cpp,
	  branches/KDE/3.5/kdegraphics/kolourpaint/kpmainwindow_file.cpp:
	  End the current shape in more kpMainWindow slots. [BACKPORT from
	  trunk/]

2007-10-07 07:34 +0000 [r722387]  dang

	* branches/KDE/3.5/kdegraphics/kolourpaint/tools/kptoolautocrop.cpp:
	  Explain that this code is flimsy but it's a bit risky to change
	  it now.

2007-10-07 08:23 +0000 [r722410]  dang

	* branches/KDE/3.5/kdegraphics/kolourpaint/kpdocument.cpp,
	  branches/KDE/3.5/kdegraphics/kolourpaint/NEWS: * Saving,
	  exporting and printing a document with an active text box, that
	  has opaque text and a transparent background, antialiases the
	  text with the document below

2007-10-07 09:30 +0000 [r722423]  dang

	* branches/KDE/3.5/kdegraphics/kolourpaint/kpdocument.cpp:
	  kpDocument::selectionCopyOntoDocument() should mark the document
	  as modified. By sheer luck, KolourPaint works perfectly without
	  this commit but it was asking for trouble. [BACKPORT from trunk/]

2007-10-07 13:18 +0000 [r722501]  dang

	* branches/KDE/3.5/kdegraphics/kolourpaint/NEWS: Finish doc for KDE
	  3.5.8, I think.

2007-10-08 00:07 +0000 [r722727-722725]  dang

	* branches/KDE/3.5/kdegraphics/kolourpaint/NEWS,
	  branches/KDE/3.5/kdegraphics/kolourpaint/kpmainwindow_edit.cpp: *
	  Edit / Paste from File..." respects the "Transparent" selection
	  mode [2007-10-08; not sure this patch made it into 1.4.8_relight]
	  [BACKPORT or trunk:-r722722 because I decided the possible
	  Undo/Redo oddities might cause bugs (I don't think there were any
	  though) or a maintenance hassle at least] It's a minor fix but
	  late by one day -- I'm hoping coolo hasn't tagged KDE 3.5.8 yet
	  :)

	* branches/KDE/3.5/kdegraphics/kolourpaint/NEWS,
	  branches/KDE/3.5/kdegraphics/kolourpaint/README: Pretend KDE
	  3.5.8 was tagged a day late in case it is :)

2007-10-08 04:31 +0000 [r722777]  dang

	* branches/KDE/3.5/kdegraphics/kolourpaint/NEWS,
	  branches/KDE/3.5/kdegraphics/kolourpaint/kpmainwindow_edit.cpp: *
	  Always enable the paste actions to guarantee that pasting from
	  non-Qt applications is always allowed (non-Qt applications do not
	  notify KolourPaint when they place objects into the clipboard)
	  [BACKPORT from trunk/ with some extra modifications I will put
	  back into trunk/]

2007-10-08 11:05 +0000 [r722966]  coolo

	* branches/KDE/3.5/kdegraphics/kdegraphics.lsm: updating lsm

