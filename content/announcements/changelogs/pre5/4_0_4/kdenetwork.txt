2008-03-28 16:42 +0000 [r791180]  rjarosz

	* branches/KDE/4.0/kdenetwork/kopete/libkopete/kopeteidentity.h,
	  branches/KDE/4.0/kdenetwork/kopete/kopete/kopetewindow.cpp,
	  branches/KDE/4.0/kdenetwork/kopete/kopete/kopetewindow.h,
	  branches/KDE/4.0/kdenetwork/kopete/libkopete/kopeteidentity.cpp:
	  Backport commit 791176. Correctly update tooltip of systray and
	  identities. CCBUG: 156113 CCBUG: 157444

2008-04-01 19:09 +0000 [r792654]  rjarosz

	* branches/KDE/4.0/kdenetwork/kopete/protocols/jabber/libiris/iris/xmpp-core/xmlprotocol.cpp,
	  branches/KDE/4.0/kdenetwork/kopete/protocols/jabber/libiris/022_fix_crash.patch
	  (added): Backport fix for bug 160000: kopete segfault in Jabber
	  protocol at exit It crashes because "CoreProtocol client;" and
	  "CoreProtocol srv;" in ClientStream::Private are deleted twice.
	  CCBUG: 156057 CCBUG: 160000

2008-04-03 23:38 +0000 [r793427]  rjarosz

	* branches/KDE/4.0/kdenetwork/kopete/kopete/chatwindow/kopetechatwindow.cpp:
	  Backport commit 793426. First create tabbar and add views into it
	  and then connect signals, otherwise active view will be changed.

2008-04-04 19:41 +0000 [r793680]  rjarosz

	* branches/KDE/4.0/kdenetwork/kopete/protocols/oscar/icq/xtrazstatus.cpp,
	  branches/KDE/4.0/kdenetwork/kopete/protocols/oscar/icq/icqaccount.h,
	  branches/KDE/4.0/kdenetwork/kopete/protocols/oscar/icq/xtrazstatusaction.h,
	  branches/KDE/4.0/kdenetwork/kopete/protocols/oscar/icq/xtrazstatus.h,
	  branches/KDE/4.0/kdenetwork/kopete/protocols/oscar/icq/icqaccount.cpp,
	  branches/KDE/4.0/kdenetwork/kopete/protocols/oscar/icq/xtrazstatusaction.cpp:
	  Backport commit 793679. Keep main status (Online/Away/etc.) and
	  invisible flag when user has changed XStatus. Novel bug 375289.
	  CCBUG: 159908

2008-04-06 16:13 +0000 [r794122]  rjarosz

	* branches/KDE/4.0/kdenetwork/kopete/protocols/oscar/aim/aimuserinfo.cpp,
	  branches/KDE/4.0/kdenetwork/kopete/protocols/oscar/liboscar/userdetails.h,
	  branches/KDE/4.0/kdenetwork/kopete/protocols/oscar/aim/aimcontact.cpp,
	  branches/KDE/4.0/kdenetwork/kopete/protocols/oscar/oscarcontact.h,
	  branches/KDE/4.0/kdenetwork/kopete/protocols/oscar/aim/icqcontact.cpp,
	  branches/KDE/4.0/kdenetwork/kopete/protocols/oscar/icq/aimcontact.cpp,
	  branches/KDE/4.0/kdenetwork/kopete/protocols/oscar/liboscar/userdetails.cpp,
	  branches/KDE/4.0/kdenetwork/kopete/protocols/oscar/icqcontactbase.cpp,
	  branches/KDE/4.0/kdenetwork/kopete/protocols/oscar/oscarcontact.cpp:
	  Backport fix for bug 160380: AIM status message wasn't updated if
	  contact was already away. CCBUG: 160380

2008-04-08 23:13 +0000 [r794957]  rjarosz

	* branches/KDE/4.0/kdenetwork/kopete/protocols/oscar/liboscar/tasks/messagereceivertask.cpp:
	  Fix offline messages encoding.

2008-04-12 16:07 +0000 [r796115]  kkofler

	* branches/KDE/4.0/kdenetwork/krdc/vnc/vncview.cpp: Prevent
	  painting an invalid image. Should hopefully fix a crash with
	  drawImage(). CCBUG:160728 (backport rev 796055 by uwolfer from
	  trunk)

2008-04-16 21:02 +0000 [r797799]  rjarosz

	* branches/KDE/4.0/kdenetwork/kopete/kopete/contactlist/kopetecontactlistview.cpp:
	  Backport commit 797798. Update context menu title.
	  KMenu::addTitle returns parent QWidgetAction and not the
	  QToolButton where the title text is.

2008-04-16 21:16 +0000 [r797826]  uwolfer

	* branches/KDE/4.0/kdenetwork/krdc/rdp/rdphostpreferences.cpp:
	  Backport: SVN commit 797822 by uwolfer: Respect the screen
	  resolution of the screen KRDC is running, not the first one.
	  #160853

2008-04-19 20:51 +0000 [r798901]  nienhueser

	* branches/KDE/4.0/kdenetwork/kopete/kopete/chatwindow/kopetechatwindowstylemanager.cpp:
	  Make sure there is a writable directory to install chat styles to
	  (backport of commit 798863) BUG: 160590

2008-04-20 10:27 +0000 [r799085]  nienhueser

	* branches/KDE/4.0/kdenetwork/kopete/kopete/chatwindow/chatview.cpp:
	  Focus proxy on message part; this way the text edit gets the
	  focus when I scroll over the message part. I think it was also
	  this way in the KDE 3 version. Backport of the fix introduced in
	  revision 778884 by uwolfer BUG: 158371

2008-04-20 11:14 +0000 [r799096]  ogoffart

	* branches/KDE/4.0/kdenetwork/kopete/protocols/jabber/jabbercapabilitiesmanager.cpp:
	  Backport r798338 by girko: Fix for a dangerous bug which can lead
	  to crash or remote DoS of Kopete when a Jabber contact has '#' in
	  Jabber client name or version returned in Disco response.

2008-04-20 11:56 +0000 [r799108]  nienhueser

	* branches/KDE/4.0/kdenetwork/kopete/libkopete/kopetepassword.cpp:
	  Show the "remember password" checkbox when prompting for a
	  password before account connection. This is more intuitive than
	  having to change settings (wrong password or password storing) in
	  the config dialog. Backport of revision 799106 BUG: 157697

2008-04-20 19:10 +0000 [r799200-799199]  nienhueser

	* branches/KDE/4.0/kdenetwork/kopete/plugins/latex/kopete_latexconvert.sh:
	  Increment variable using sh compatible arithmetic expansion $((
	  )), tested with dash and bash. Use -d parameter of mktemp to
	  avoid removing and recreating the temporary file as a directory.
	  Backport of revision 799167. CCBUG: 135997

	* branches/KDE/4.0/kdenetwork/kopete/plugins/latex/kopete_latexconvert.sh:
	  Use printf instead of echo to avoid escape sequence
	  interpretation by the underlying shell. This means that latex
	  commands don't get messed up by the shell anymore. Backport of
	  revision 799193. BUG: 135997

