------------------------------------------------------------------------
r1131739 | arnorehn | 2010-05-29 09:15:17 +1200 (Sat, 29 May 2010) | 2 lines

backport SynchronizedFree() fixes from trunk

------------------------------------------------------------------------
r1132352 | arnorehn | 2010-05-31 01:34:33 +1200 (Mon, 31 May 2010) | 2 lines

backport a fix from trunk

------------------------------------------------------------------------
r1135744 | sedwards | 2010-06-08 17:59:34 +1200 (Tue, 08 Jun 2010) | 2 lines

Added the semi-private, yet absolutely necessary results() signal to kjob.

------------------------------------------------------------------------
r1141031 | rdale | 2010-06-22 14:44:25 +1200 (Tue, 22 Jun 2010) | 11 lines

* Added snakecase versions of the methods in the Qt::DBusConnectionInterface
  class
* Fixed bug with missing QVector<double> marshaller. Fixes problem reported
  by Sylvain Sauvage http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=586672
  Thanks for the bug report.
* Add a version check when using rb_during_gc() as it was only added for 
  Ruby 1.8.7
* Added a setScriptOwnership= method for changing whether an instance is owned
  by the C++ or Ruby runtimes


------------------------------------------------------------------------
