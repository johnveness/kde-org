2006-05-24 11:20 +0000 [r544282]  howells

	* branches/KDE/3.5/kdeutils/kmilo/asus/asus.cpp: Work about buggy
	  asus_acpi module for bug 102374 by just disabling it for now. Is
	  it possible to get this into 3.5.3? CCMAIL: coolo@kde.org CCBUG:
	  102374

2006-05-26 16:41 +0000 [r545035]  staikos

	* branches/KDE/3.5/kdeutils/kwallet/kwalletmanager.cpp: don't show
	  the manager on session restore if there are no wallets open and
	  auto-close is enabled. BUG: 128022

2006-05-26 21:05 +0000 [r545122]  mueller

	* branches/KDE/3.5/kdeutils/kwallet/kwmapeditor.cpp: stop leaking
	  lonely widgets on the desktop

2006-06-03 10:31 +0000 [r547761]  lukas

	* branches/KDE/3.5/kdeutils/kgpg/listkeys.cpp,
	  branches/KDE/3.5/kdeutils/kgpg/keyinfowidget.cpp,
	  branches/KDE/3.5/kdeutils/kgpg/kgpg.cpp,
	  branches/KDE/3.5/kdeutils/kgpg/keygener.cpp,
	  branches/KDE/3.5/kdeutils/kgpg/kgpginterface.cpp,
	  branches/KDE/3.5/kdeutils/kgpg/kgpgeditor.cpp,
	  branches/KDE/3.5/kdeutils/kgpg/keyservers.cpp,
	  branches/KDE/3.5/kdeutils/kgpg/popuppublic.cpp,
	  branches/KDE/3.5/kdeutils/kgpg/kgpgview.cpp,
	  branches/KDE/3.5/kdeutils/kgpg/kgpgoptions.cpp: extensive fixes
	  to achieve UTF-8 compliance Comment doesn't work correctly due to
	  a bug in gpg, compare output of: gpg --list-secret-keys and gpg
	  --list-secret-keys --with-colon where the comment contains a
	  UTF-8 string Everything else seems to display and pass utf8
	  strings back to gpg correctly

2006-06-16 20:07 +0000 [r552170]  staikos

	* branches/KDE/3.5/kdeutils/kwallet/kwalletmanager.cpp: don't crash
	  when immediately exiting

2006-06-20 22:56 +0000 [r553412]  nickell

	* branches/KDE/3.5/kdeutils/superkaramba/src/input_python.h: API
	  geration fixes for make_api_doc.sh

2006-06-21 00:35 +0000 [r553426]  nickell

	* branches/KDE/3.5/kdeutils/superkaramba/src/karamba.cpp,
	  branches/KDE/3.5/kdeutils/superkaramba/src/karamba.h: Themes that
	  didn't have a python companion weren't able to update on their
	  specified intervals. We were relying on the size of a pixmap to
	  be the indicator when to start updating. This is not a good basis
	  for startup, so we now have a boolean flag to tell us when we've
	  gotten our images, backgrounds, etc. all up and ready.

2006-06-21 01:01 +0000 [r553433]  nickell

	* branches/KDE/3.5/kdeutils/superkaramba/src/karamba.cpp:
	  BUG:127217 Use the KeepBelow and KeepAbove arguments to
	  KWin::setState() instead of using KWin::clearState().

2006-06-26 20:40 +0000 [r555267]  teran

	* branches/KDE/3.5/kdeutils/kcalc/knumber/knumber_priv.cpp: fixed a
	  moderatly critical logic error in knumber_priv.cpp, there were 3
	  locations in which delete was being used on a resource allocated
	  by malloc (indirectly though a gmp call)

2006-06-29 22:44 +0000 [r556319]  kniederk

	* branches/KDE/3.5/kdeutils/kcalc/knumber/knumber_priv.cpp: Compile
	  error with gcc-4.1 fixed caused by const-pointer.

2006-07-10 22:42 +0000 [r560701]  mueller

	* branches/KDE/3.5/kdeutils/ksim/monitors/snmp/configure.in.in: fix
	  net-snmp detection

2006-07-15 23:16 +0000 [r562837]  nickell

	* branches/KDE/3.5/kdeutils/superkaramba/src/misc_python.h: API
	  documentation fixes.

2006-07-22 09:10 +0000 [r565041]  kniederk

	* branches/KDE/3.5/kdeutils/kcalc/kcalc_core.cpp,
	  branches/KDE/3.5/kdeutils/kcalc/version.h: Fixed BUG # 130880:
	  Crash on any function if parameter is infinity

2006-07-22 22:12 +0000 [r565262]  nickell

	* branches/KDE/3.5/kdeutils/superkaramba/src/main.cpp: Bump the
	  version since KDE 3.5.4 will be tagged shortly.

2006-07-23 14:02 +0000 [r565478]  coolo

	* branches/KDE/3.5/kdeutils/kdeutils.lsm: preparing KDE 3.5.4

