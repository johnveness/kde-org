2006-05-23 14:35 +0000 [r544061]  cartman

	* branches/KDE/3.5/kdebase/libkonq/konq_popupmenu.cc: support
	  X-KDE-Protocols in konqi RMB menu so kget can say
	  X-KDE-Protocols=ftp,http,https and not show up for local files

2006-05-23 17:17 +0000 [r544127]  mueller

	* branches/KDE/3.5/kdebase/ksysguard/gui/ksgrd/SensorShellAgent.cc,
	  branches/KDE/3.5/kdebase/ksysguard/gui/SensorBrowser.cc: really
	  fix the crashes BUG: 122271

2006-05-25 18:09 +0000 [r544663]  amantia

	* branches/KDE/3.5/kdebase/kicker/applets/systemtray/systemtrayapplet.cpp:
	  Fix system-tray related crashes in Kicker (most hated bug #66).
	  Coolo, if it's not too late, you might include in 3.5.3. BUG:
	  87613 CCMAIL: coolo@kde.org

2006-05-26 12:35 +0000 [r544911]  lunakl

	* branches/KDE/3.5/kdebase/kcontrol/krdb/ad/AAAXaw.ad: These
	  command translations apparently break some Motif apps. BUG:
	  127180

2006-05-26 15:04 +0000 [r544989]  lunakl

	* branches/KDE/3.5/kdebase/kicker/taskbar/taskbar.cpp: Revert
	  r521456 and attempt #2.

2006-05-26 20:27 +0000 [r545108]  deller

	* branches/KDE/3.5/kdebase/kcontrol/nics/nic.cpp: fix (hopefully)
	  nic information on AMD64 and others. Patch analysis and
	  suggestion by Daniel Eklöf. BUG: 125263 (can someone who is
	  working on HEAD please forward-port it ?)

2006-05-27 17:53 +0000 [r545528]  hindenburg

	* branches/KDE/3.5/kdebase/kcontrol/konsole/schemadialog.ui: Fix
	  accelerator key for Schema tab changes from S to M. Change the
	  'Save' accelerator key to V to correct this issue and match the
	  session dialog's Save accelerator. BUG: 128006

2006-05-27 21:43 +0000 [r545621]  troeder

	* branches/KDE/3.5/kdebase/kfind/kftabdlg.cpp,
	  branches/KDE/3.5/kdebase/kfind/kquery.cpp,
	  branches/KDE/3.5/kdebase/kfind/kquery.h: backport SVN commit
	  539481 from trunk for 3.5.4: - Allow kfind to search for files
	  greater than 2GB (INT_MAX), if a size range is specified. - Allow
	  size ranges greater than 2GB to be entered into the search form.
	  BUG: 123838

2006-05-28 19:57 +0000 [r546025]  lunakl

	* branches/KDE/3.5/kdebase/ksmserver/shutdown.cpp: I'm tired of
	  bugreports like #128130, make the message about apps cancelling
	  logout kdWarning() so that it's visible even in production
	  builds.

2006-05-29 13:59 +0000 [r546213]  mueller

	* branches/KDE/3.5/kdebase/kicker/taskbar/taskbar.cpp: also fix
	  taskbar size for right/left kicker bars.

2006-05-29 15:59 +0000 [r546264]  lunakl

	* branches/KDE/3.5/kdebase/kwin/geometry.cpp: Size can be empty
	  during attempted resize, avoid the warning.

2006-05-29 16:22 +0000 [r546271]  mueller

	* branches/KDE/3.5/kdebase/konsole/konsole/TEmulation.cpp: konsole
	  speedup patches part 1: the trivial stuff: - reduce redundant
	  memory allocations by factor 2

2006-05-30 11:36 +0000 [r546547]  lunakl

	* branches/KDE/3.5/kdebase/kwin/workspace.cpp,
	  branches/KDE/3.5/kdebase/kwin/kwin.kcfg,
	  branches/KDE/3.5/kdebase/kwin/options.h,
	  branches/KDE/3.5/kdebase/kwin/manage.cpp,
	  branches/KDE/3.5/kdebase/kicker/kicker/core/showdesktop.cpp,
	  branches/KDE/3.5/kdebase/kwin/options.cpp: For people who don't
	  see the difference between "show desktop" and "minimize all" add
	  config option ShowDesktopIsMinimizeAll in group [Windows] in
	  kwinrc. BUG: 67406

2006-05-30 22:44 +0000 [r546753]  dfaure

	* branches/KDE/3.5/kdebase/khotkeys/configure.in.in: Remove unused
	  configure check

2006-05-30 22:58 +0000 [r546756]  rysin

	* branches/KDE/3.5/kdebase/kxkb/kcmlayout.cpp: BUG: 127349

2006-05-31 15:59 +0000 [r546980]  hindenburg

	* branches/KDE/3.5/kdebase/konsole/konsole/konsole_part.cpp,
	  branches/KDE/3.5/kdebase/konsole/konsole/konsole_part.h: Fix
	  warnings dealing with updateTitle(TESession*). As far as I can
	  tell, updateTitle in konsole_part has no effect. CCBUG: 125796

2006-06-02 12:29 +0000 [r547516]  mueller

	* branches/KDE/3.5/kdebase/konsole/konsole/TEmulation.cpp: the
	  latin-1 decoder is a bit broken BUG:128488

2006-06-02 22:46 +0000 [r547673]  lunakl

	* branches/KDE/3.5/kdebase/kwin/manage.cpp: Allow activation from
	  session if there was user activity but there's no active window.

2006-06-04 12:42 +0000 [r548008]  coolo

	* branches/KDE/3.5/kdebase/kdesktop/lockeng.cc,
	  branches/KDE/3.5/kdebase/kdesktop/xautolock.cc: tricky. we can't
	  disable the X screensaver ourselves or we will think later some
	  program disabled it. BUG: 128610

2006-06-05 14:49 +0000 [r548428]  aseigo

	* branches/KDE/3.5/kdebase/kicker/libkicker/panelbutton.cpp: fix
	  arrow locations on vert panels patch by Maxilys
	  <maxilys@tele2.fr> BUGS:128622,128561

2006-06-05 19:00 +0000 [r548508]  chrsmrtn

	* branches/KDE/3.5/kdebase/kpager/windowdrag.cpp: Fix double-free
	  in kpager. This was caused by a fix to an apparent memory leak.
	  It appears that QByteArray does free up the memory it is
	  assigned. Then when 'tmp' goes out of scope, the memory is freed
	  again, hence the crash. I doubt that there was a problem to begin
	  with - at least in the 3.5 branch. A similar fix was checked into
	  HEAD, but QByteArray behaves differently in Qt4, so perhaps
	  things are now correct there; I'm not equipped to test that
	  situation. BUG: 128663

2006-06-05 19:55 +0000 [r548525]  chrsmrtn

	* branches/KDE/3.5/kdebase/kpager/windowdrag.cpp: On Monday 05 June
	  2006 15:22, Thiago Macieira wrote: > So, to fix the double-free
	  you leak memory? ...since QByteArray doesn't free everything,
	  just the latin1 stuff. Doh. Let's try again.

2006-06-06 01:31 +0000 [r548603]  chrsmrtn

	* branches/KDE/3.5/kdebase/kpager/windowdrag.cpp: As suggested,
	  this should be a better fix.

2006-06-06 15:56 +0000 [r548840]  lunakl

	* branches/KDE/3.5/kdebase/kwin/geometry.cpp: Do the
	  miscalculated-place-at-the-same-position hack only if it comes
	  from an application.

2006-06-06 16:12 +0000 [r548848]  hindenburg

	* branches/KDE/3.5/kdebase/konsole/konsole/konsole.h: update
	  version

2006-06-07 20:33 +0000 [r549226]  kling

	* branches/KDE/3.5/kdebase/kcontrol/kicker/extensionInfo.cpp: Use
	  correct defaults in configChanged() when values aren't present in
	  kickerrc. Fixes a minor itch -- the "Position" and "Screen" parts
	  didn't synchronize properly when moving the actual kicker bar to
	  its default position (screen bottom.) CCMAIL: aseigo@kde.org

2006-06-09 01:17 +0000 [r549551]  dgp

	* branches/KDE/3.5/kdebase/konsole/README.utmp (removed),
	  branches/KDE/3.5/kdebase/configure.in.in,
	  branches/KDE/3.5/kdebase/konsole/konsole/kwrited.cpp: Remove the
	  check for utempter, as it's no more valid here, the work is done
	  in kdelibs; also remove the README.utmp file for konsole that's
	  completely outdated and misleading for packagers; remove also the
	  note about kwrited working only without utempter, as it does work
	  without it just fine right now.

2006-06-09 16:25 +0000 [r549730]  hindenburg

	* branches/KDE/3.5/kdebase/konsole/konsole/sessioniface.h,
	  branches/KDE/3.5/kdebase/konsole/konsole/session.cpp,
	  branches/KDE/3.5/kdebase/konsole/konsole/session.h: Add DCOP
	  calls setFont() and font(). BUG: 123325

2006-06-09 16:29 +0000 [r549731]  hindenburg

	* branches/KDE/3.5/kdebase/konsole/konsole/kwrited.h: remove unsed
	  variable

2006-06-09 16:31 +0000 [r549732]  iastrubni

	* branches/KDE/3.5/kdebase/konqueror/about/tips.html: This fixes
	  bug 128909. Please when using CSS, don't align text to the left.
	  This is the default for most users, and it breaks RTL pages.
	  CCMAIL: kde-il@yahoogroups.com

2006-06-11 00:18 +0000 [r550095]  hindenburg

	* branches/KDE/3.5/kdebase/konsole/konsole/konsole.cpp: Upon
	  Prev/Next session, only activate new session when session.count >
	  1. Will forward port when I get trunk/kdebase compiling again...
	  BUG: 107197

2006-06-12 10:31 +0000 [r550578]  ervin

	* branches/KDE/3.5/kdebase/kioslave/media/propsdlgplugin (added),
	  branches/KDE/3.5/kdebase/kioslave/media/kcmodule/notifiermodule.cpp,
	  branches/KDE/3.5/kdebase/kioslave/media/Makefile.am,
	  branches/KDE/3.5/kdebase/kioslave/media/mediamanager/mediamanager.h,
	  branches/KDE/3.5/kdebase/kioslave/media/mounthelper/Makefile.am,
	  branches/KDE/3.5/kdebase/kioslave/media/mediamanager/halbackend.h,
	  branches/KDE/3.5/kdebase/kioslave/media/medianotifier/medianotifier.cpp,
	  branches/KDE/3.5/kdebase/kioslave/media/libmediacommon/medium.cpp,
	  branches/KDE/3.5/kdebase/kioslave/media/kfile-plugin/kfilemediaplugin.cpp,
	  branches/KDE/3.5/kdebase/kioslave/media/mediaimpl.cpp,
	  branches/KDE/3.5/kdebase/kioslave/media/libmediacommon/medium.h,
	  branches/KDE/3.5/kdebase/kioslave/media/mounthelper/kio_media_mounthelper.cpp,
	  branches/KDE/3.5/kdebase/kioslave/media/kfile-plugin/kfile_media.desktop,
	  branches/KDE/3.5/kdebase/kioslave/media/configure.in.in,
	  branches/KDE/3.5/kdebase/kioslave/media/kcmodule/managermodule.cpp,
	  branches/KDE/3.5/kdebase/kioslave/media/mediamanager/mediamanager.cpp,
	  branches/KDE/3.5/kdebase/kioslave/media/mediamanager/halbackend.cpp:
	  Merge coolo's branch, it contains quite some fixes and
	  interesting features. It'll be available in KDE 3.5.4, couldn't
	  be committed before, but thanks to the partial feature
	  unfreeze... FEATURE: BUG: 50185 BUG: 105482 BUG: 108823 BUG:
	  114854 BUG: 120619 BUG: 121833 BUG: 127788 CCMAIL: coolo@kde.org

2006-06-12 13:15 +0000 [r550619]  mueller

	* branches/KDE/3.5/kdebase/kioslave/thumbnail/configure.in.in:
	  remove duplicated configure check

2006-06-12 13:54 +0000 [r550638]  mueller

	* branches/KDE/3.5/kdebase/drkonqi/main.cpp,
	  branches/KDE/3.5/kdebase/ksysguard/ksysguardd/ksysguardd.c,
	  branches/KDE/3.5/kdebase/kdm/kfrontend/kgreeter.cpp: add various
	  missing return value checks

2006-06-13 15:18 +0000 [r551063]  bero

	* branches/KDE/3.5/kdebase/kioslave/media/mediamanager/mediamanager.cpp:
	  Make it compile again on systems that don't have HAL

2006-06-13 21:51 +0000 [r551191]  thiago

	* branches/KDE/3.5/kdebase/kdepasswd/kdepasswd.desktop: Remove the
	  duplicate NoDisplay line. Patch thanks to Jens Koerber.
	  BUG:128821

2006-06-13 22:29 +0000 [r551202]  craig

	* branches/KDE/3.5/kdebase/kcontrol/fonts/fonts.cpp,
	  branches/KDE/3.5/kdebase/kcontrol/krdb/krdb.cpp: Properly disable
	  antialiasing, by also setting the xrdb resource

2006-06-14 07:58 +0000 [r551288]  coolo

	* branches/KDE/3.5/kdebase/kioslave/media/mediamanager/mediamanager.cpp,
	  branches/KDE/3.5/kdebase/kioslave/media/mediamanager/halbackend.cpp:
	  indented the right branch

2006-06-14 08:49 +0000 [r551300]  coolo

	* branches/KDE/3.5/kdebase/kioslave/media/Makefile.am: the
	  properties are only useful in the hal backend

2006-06-14 09:09 +0000 [r551304]  coolo

	* branches/KDE/3.5/kdebase/kioslave/media/configure.in.in,
	  branches/KDE/3.5/kdebase/kioslave/media/mediamanager/halbackend.h,
	  branches/KDE/3.5/kdebase/kioslave/media/mediamanager/halbackend.cpp:
	  hal 0.4 simply won't work any more

2006-06-14 09:11 +0000 [r551306]  mueller

	* branches/KDE/3.5/kdebase/kioslave/media/mediamanager/fstabbackend.cpp,
	  branches/KDE/3.5/kdebase/kioslave/media/mediamanager/fstabbackend.h:
	  fix media handling regression caused by stat-avoidance patch
	  BUG:128757

2006-06-14 14:48 +0000 [r551391]  dhaumann

	* branches/KDE/3.5/kdebase/kate/app/katemdi.cpp: set sane default
	  sizes for sidebars if no configuration/group is available code
	  stolen from KTechLab - thanks ;) CCMAIL: david@bluehaze.org

2006-06-14 18:08 +0000 [r551475]  craig

	* branches/KDE/3.5/kdebase/kcontrol/fonts/fonts.cpp: When sub-pixel
	  set to none, also export this to xsettings.

2006-06-14 18:24 +0000 [r551490]  mueller

	* branches/KDE/3.5/kdebase/kdm/backend/client.c: avoid ~/.dmrc
	  symlink attack vulnerability (CVE-2006-2449)

2006-06-15 08:55 +0000 [r551657]  lunakl

	* branches/KDE/3.5/kdebase/kcontrol/background/bgdefaults.h,
	  branches/KDE/3.5/kdebase/kdesktop/kdesktop.kcfg: I guess #55795
	  is right - there's not much point in using a cache if it doesn't
	  really work by default. Make the backgrounds cache size unlimited
	  by default, people low on memory or whatever can change it
	  manually (or they shouldn't be using multiple wallpapers at all
	  in the first place). CCBUG: 55795

2006-06-15 12:32 +0000 [r551707]  deller

	* branches/KDE/3.5/kdebase/doc/kinfocenter/cdinfo/Makefile.am
	  (added), branches/KDE/3.5/kdebase/kcontrol/info/info.h,
	  branches/KDE/3.5/kdebase/kcontrol/info/main.cpp,
	  branches/KDE/3.5/kdebase/doc/kinfocenter/cdinfo (added),
	  branches/KDE/3.5/kdebase/kcontrol/info/info_generic.cpp,
	  branches/KDE/3.5/kdebase/kcontrol/info/Makefile.am,
	  branches/KDE/3.5/kdebase/kcontrol/info/cdinfo.desktop (added),
	  branches/KDE/3.5/kdebase/kcontrol/info/info_linux.cpp,
	  branches/KDE/3.5/kdebase/doc/kinfocenter/cdinfo/index.docbook
	  (added): submit patch by Jahshan Bhatti <jabhatti91@gmail.com> to
	  fix Bug 47242: JJ: CDROM info not available from Control Centre
	  Thanks a lot Jahshan !! FEATURE: 47242

2006-06-15 21:30 +0000 [r551883]  ervin

	* branches/KDE/3.5/kdebase/kioslave/media/mediamanager/fstabbackend.cpp:
	  Filesystem typ "fdescfs" is now blacklisted. That should be
	  enough to fix this one. Please reopen if it's still broken. BUG:
	  128754

2006-06-15 21:37 +0000 [r551884]  ervin

	* branches/KDE/3.5/kdebase/kioslave/media/mediamanager/halbackend.cpp:
	  Avoid to leak devices lists. Thanks for pointing this. BUG:
	  117623

2006-06-15 22:13 +0000 [r551890]  ervin

	* branches/KDE/3.5/kdebase/kioslave/media/mounthelper/kio_media_mounthelper.cpp:
	  It seems that letting "eject" handling the unmount doesn't
	  introduce undesired side-effects. So let's do it this way, to
	  avoid error on unmount for supermounted devices. BUG: 116209

2006-06-16 04:42 +0000 [r551942]  binner

	* branches/KDE/3.5/kdebase/klipper/configdialog.cpp: those are
	  global shortcuts

2006-06-16 10:41 +0000 [r551992]  coolo

	* branches/KDE/3.5/kdebase/kxkb/kcmlayout.cpp: as the libxklavier
	  port seems to be no longer an issue, we should at least make the
	  current xorg translatable

2006-06-17 23:08 +0000 [r552488]  dgp

	* branches/KDE/3.5/kdebase/kdm/kfrontend/kdm_config.c,
	  branches/KDE/3.5/kdebase/kdm/backend/Makefile.am,
	  branches/KDE/3.5/kdebase/kdm/backend/xdmcp.c: Fix strict-aliasing
	  breakages, avoid when possible or pass -fno-strict-aliasing when
	  needed.

2006-06-18 19:48 +0000 [r552682]  dgp

	* branches/KDE/3.5/kdebase/kdm/backend/Makefile.am: Revert
	  -fno-strict-aliasing for non-GCC compatibility, as asked by
	  Stephan Kulow.

2006-06-20 23:09 +0000 [r553414]  mueller

	* branches/KDE/3.5/kdebase/kcontrol/usbview/usbdevices.cpp,
	  branches/KDE/3.5/kdebase/kcontrol/usbview/usbdevices.h,
	  branches/KDE/3.5/kdebase/kcontrol/usbview/kcmusb.cpp: sysfs
	  parsing support. needed for distros that don't have /proc/bus/usb
	  anymore

2006-06-21 08:56 +0000 [r553501]  coolo

	* branches/KDE/3.5/kdebase/kioslave/media/libmediacommon/medium.cpp,
	  branches/KDE/3.5/kdebase/kioslave/media/mediamanager/halbackend.cpp:
	  better support for baseurl medias as discussed with Kevin

2006-06-21 13:09 +0000 [r553542]  coolo

	* branches/KDE/3.5/kdebase/konqueror/konq_mainwindow.cc: views of
	  media/hdd_mounted should have a way to create new folders too

2006-06-21 16:29 +0000 [r553664]  hindenburg

	* branches/KDE/3.5/kdebase/konsole/konsole/konsole.cpp,
	  branches/KDE/3.5/kdebase/konsole/konsole/konsole.h: RMB->Close
	  Session and tabbar menu->Close Session now uses the Close
	  Confirmation Dialog. Will forward port once I get KDE4 compiling
	  again. BUG: 129514

2006-06-21 20:21 +0000 [r553712]  mueller

	* branches/KDE/3.5/kdebase/kwin/workspace.cpp: fix strict aliasing
	  issue

2006-06-22 13:29 +0000 [r553910-553908]  lunakl

	* branches/KDE/3.5/kdebase/khotkeys/update/update.cpp,
	  branches/KDE/3.5/kdebase/khotkeys/shared/action_data.cpp,
	  branches/KDE/3.5/kdebase/khotkeys/data/Makefile.am: Fix adding of
	  new actions using kconf_update when KDE is running. Make it
	  possible to merge them into already existing action groups. Add
	  PrintScrn -> ksnapshot action by default.

	* branches/KDE/3.5/kdebase/khotkeys/data/printscreen.khotkeys
	  (added),
	  branches/KDE/3.5/kdebase/khotkeys/data/khotkeys_printscreen.upd
	  (added): for( int i = 0; i < 1000; ++i ) std::cout << "I will not
	  forget to do svn add." << std::endl;

2006-06-22 14:21 +0000 [r553927]  lunakl

	* branches/KDE/3.5/kdebase/kcontrol/keys/win3.kksrc,
	  branches/KDE/3.5/kdebase/kcontrol/keys/win4.kksrc: Alt+F4 is not
	  a shortcut for quit.

2006-06-23 00:14 +0000 [r554047]  mueller

	* branches/KDE/3.5/kdebase/kdesktop/desktop.cc: fix align-to-grid
	  option which regressed due to the startup performance patches.
	  BUG: 122001 BUG: 127672 BUG: 114766 BUG: 117952

2006-06-23 11:05 +0000 [r554157]  carewolf

	* branches/KDE/3.5/kdebase/kcontrol/kio/uasproviders/Makefile.am,
	  branches/KDE/3.5/kdebase/kcontrol/kio/uasproviders/firefoxoncurrent.desktop
	  (removed),
	  branches/KDE/3.5/kdebase/kcontrol/kio/uasproviders/firefox10oncurrent.desktop
	  (added),
	  branches/KDE/3.5/kdebase/kcontrol/kio/uasproviders/ie60oncurrent.desktop,
	  branches/KDE/3.5/kdebase/kcontrol/kio/uasproviders/safari.desktop
	  (removed),
	  branches/KDE/3.5/kdebase/kcontrol/kio/uasproviders/safari20.desktop
	  (added),
	  branches/KDE/3.5/kdebase/kcontrol/kio/uasproviders/firefox15oncurrent.desktop
	  (added),
	  branches/KDE/3.5/kdebase/kcontrol/kio/uasproviders/safari12.desktop
	  (added): Use feature thaw to get Safari 2.0 and Firefox 1.5
	  user-agent strings in

2006-06-24 01:09 +0000 [r554408]  mpyne

	* branches/KDE/3.5/kdebase/kioslave/cgi/cgi.cpp: Fix bug 118550
	  (CGI kioslave randomly adds extra spaces) in KDE 3.5. I still
	  need to port the patch to kdebase, I will do so shortly.

2006-06-26 18:21 +0000 [r555223]  mueller

	* branches/KDE/3.5/kdebase/ksysguard/ksysguardd/Linux/netdev.c: use
	  long long in reading network transfer sizes

2006-06-30 12:24 +0000 [r556482]  mueller

	* branches/KDE/3.5/kdebase/kicker/applets/media/Makefile.am: this
	  directory doesn't exist. and if its a core file, you get pretty
	  funky compile errors

2006-07-02 00:06 +0000 [r556955]  hindenburg

	* branches/KDE/3.5/kdebase/konsole/README.moreColors (added),
	  branches/KDE/3.5/kdebase/konsole/konsole/TECommon.h,
	  branches/KDE/3.5/kdebase/konsole/konsole/TEmuVt102.cpp,
	  branches/KDE/3.5/kdebase/konsole/konsole/TEScreen.cpp,
	  branches/KDE/3.5/kdebase/konsole/tests/color-spaces.pl (added),
	  branches/KDE/3.5/kdebase/konsole/konsole/TEWidget.cpp,
	  branches/KDE/3.5/kdebase/konsole/konsole/TEScreen.h: Add 256
	  color support. Patch by Lars Doelle. Thanks! Will forward port
	  shortly. BUG: 107487

2006-07-02 01:42 +0000 [r556978]  hindenburg

	* branches/KDE/3.5/kdebase/konsole/other/linux.desktop: Font= is
	  not used currently

2006-07-04 12:35 +0000 [r557931]  hindenburg

	* branches/KDE/3.5/kdebase/konsole/konsole/konsole_part.cpp: Fix
	  extra character (^L) in konsolepart when using bash vi mode. This
	  has been in since at least KDE 3.3.0, which is the earliest I
	  have. BUG: 127540

2006-07-05 21:52 +0000 [r558718]  dfaure

	* branches/KDE/3.5/kdebase/konqueror/client/kfmclient.cc: icefox is
	  right, "kfmclient exec http://www.koffice.org" shouldn't show a
	  progress dialog

2006-07-08 14:04 +0000 [r559809]  reitelbach

	* branches/KDE/3.5/kdebase/l10n/de/entry.desktop: change default
	  for EUR-sign in german localization: use currency symbol as
	  suffix by default, not as prefix. CCMAIL:kde-i18n-de@kde.org

2006-07-08 22:58 +0000 [r560010]  hindenburg

	* branches/KDE/3.5/kdebase/konsole/konsole/TEWidget.cpp: Patch to
	  reduce flickering by Andreas Kling. Already forwarded port Qt4
	  version to /trunk. BUG: 54230

2006-07-09 11:58 +0000 [r560144]  lueck

	* branches/KDE/3.5/kdebase/doc/userguide/removable-disks.docbook,
	  branches/KDE/3.5/kdebase/doc/kcontrol/kcmtaskbar/index.docbook,
	  branches/KDE/3.5/kdebase/doc/userguide/programs-and-documents.docbook,
	  branches/KDE/3.5/kdebase/doc/userguide/control-center.docbook,
	  branches/KDE/3.5/kdebase/doc/userguide/kde-for-admins.docbook,
	  branches/KDE/3.5/kdebase/doc/kcontrol/kcmaccess/index.docbook,
	  branches/KDE/3.5/kdebase/doc/userguide/usenet.docbook,
	  branches/KDE/3.5/kdebase/doc/userguide/panel-and-desktop.docbook,
	  branches/KDE/3.5/kdebase/doc/userguide/index.docbook,
	  branches/KDE/3.5/kdebase/doc/userguide/file-sharing.docbook,
	  branches/KDE/3.5/kdebase/doc/userguide/standard-menu-entries.docbook,
	  branches/KDE/3.5/kdebase/doc/kcontrol/kcmlaunch/index.docbook,
	  branches/KDE/3.5/kdebase/doc/userguide/net-connection-setup.docbook,
	  branches/KDE/3.5/kdebase/doc/userguide/under-the-hood.docbook,
	  branches/KDE/3.5/kdebase/doc/kicker/index.docbook,
	  branches/KDE/3.5/kdebase/doc/userguide/internet-shortcuts.docbook:
	  documentation backport from trunk CCMAIL:kde-doc-english

2006-07-10 14:19 +0000 [r560516]  hindenburg

	* branches/KDE/3.5/kdebase/konsole/konsole/main.cpp: Update
	  copyright years

2006-07-10 18:00 +0000 [r560584]  lunakl

	* branches/KDE/3.5/kdebase/kioslave/media/mediamanager/mediamanager.desktop:
	  Load mediamanager kded module soon in the startup. It used to do
	  notification about new media after startup, but that was removed
	  (r468233) even before the startup reorder because it was a
	  noticeable part of the startup time for some reason. The module
	  should be loaded soon anyway because media icons can be shown on
	  the desktop. BUG: 127971 BUG: 129534

2006-07-12 15:29 +0000 [r561580]  lunakl

	* branches/KDE/3.5/kdebase/ksmserver/server.cpp,
	  branches/KDE/3.5/kdebase/ksmserver/server.h,
	  branches/KDE/3.5/kdebase/ksmserver/shutdown.cpp: kill the WM
	  first, so that it doesn't track changes that happen as a result
	  of other clients going away (e.g. if KWin is set to remember
	  position of a window, it could shift because of Kicker going away
	  and KWin would remember wrong position)

2006-07-13 23:26 +0000 [r562022]  tyrerj

	* branches/KDE/3.5/kdebase/kdeprint/printmgr/printers.desktop:
	  Restoring "Printers" to the "Settings" menu.

2006-07-14 17:55 +0000 [r562411]  entriken

	* branches/KDE/3.5/kdebase/ksplashml/wndmain.cpp,
	  branches/KDE/3.5/kdebase/ksplashml/ksplashiface.h,
	  branches/KDE/3.5/kdebase/ksplashml/wndmain.h: added hide() and
	  show() to allow some ksmserver features

2006-07-18 21:12 +0000 [r563926]  deller

	* branches/KDE/3.5/kdebase/kcontrol/kcontrol/aboutwidget.cpp: fix
	  Bug 130774: Strange lonesome icon in KInfocenter's start page
	  BUG: 130774

2006-07-21 20:18 +0000 [r564948]  kling

	* branches/KDE/3.5/kdebase/kcontrol/background/bgsettings.cpp:
	  Check that m_CurrentWallpaper is >= 0 in currentWallpaper().
	  Fixes a long-standing freeze/crash in kdesktop, among other
	  things. BUG: 124633

2006-07-22 22:02 +0000 [r565258]  aseigo

	* branches/KDE/3.5/kdebase/konqueror/konq_mainwindow.cc: get
	  show/hide menubar action correct on full screened windows patch
	  by Tadeusz Andrzej Kadlubowski

2006-07-23 13:49 +0000 [r565446-565445]  coolo

	* branches/KDE/3.5/kdebase/konqueror/version.h: preparing KDE 3.5.4

	* branches/KDE/3.5/kdebase/startkde: preparing KDE 3.5.4

2006-07-23 14:01 +0000 [r565466]  coolo

	* branches/KDE/3.5/kdebase/kdebase.lsm: preparing KDE 3.5.4

2006-07-23 17:15 +0000 [r565544]  aseigo

	* branches/KDE/3.5/kdebase/kicker/applets/minipager/pagerapplet.cpp:
	  allow manually turning on of previews in the pager at low screen
	  resolutions thanks to vinay shastry for testing the patch.
	  BUG:119560

2006-07-24 04:44 +0000 [r565679]  aseigo

	* branches/KDE/3.5/kdebase/kicker/applets/minipager/pagerapplet.cpp:
	  change both instances of ItemInt to ItemBool, not just one of
	  them ;)

