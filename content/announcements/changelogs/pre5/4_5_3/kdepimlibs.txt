------------------------------------------------------------------------
r1182934 | scripty | 2010-10-06 16:02:12 +1300 (Wed, 06 Oct 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1183852 | tokoe | 2010-10-09 03:06:35 +1300 (Sat, 09 Oct 2010) | 18 lines

Merged revisions 1181945,1182205 via svnmerge from 
svn+ssh://tokoe@svn.kde.org/home/kde/trunk/KDE/kdepimlibs

........
  r1181945 | vkrause | 2010-10-02 21:42:07 +0200 (Sat, 02 Oct 2010) | 5 lines
  
  Introduce SharedValuePool, a little helper class that restores implicit
  sharing on objects that are received in great numbers but only with a
  small set of possible values. Use it for item mimetypes and flags,
  reducing their memory use from O(n) to O(1) for larger folder listings.
........
  r1182205 | vkrause | 2010-10-03 21:27:53 +0200 (Sun, 03 Oct 2010) | 4 lines
  
  Comment out the set version of SharedValuePool, currently unused and the
  template code to switch between the set and vector version fails to
  compile on Mac OS and Win CE at least.
........

------------------------------------------------------------------------
r1184261 | smartins | 2010-10-10 05:24:03 +1300 (Sun, 10 Oct 2010) | 10 lines

Fwdport r1182559 from e35 to branch 4.5:

Fix kolab/issue4579.

The schedulingId was getting lost inside the kolab resource.

CCMAIL: winter@kde.org
Backport to 4.4 if you have a chance.


------------------------------------------------------------------------
r1185655 | scripty | 2010-10-14 15:43:14 +1300 (Thu, 14 Oct 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1186570 | tokoe | 2010-10-17 11:35:22 +1300 (Sun, 17 Oct 2010) | 2 lines

Backport performance improvements done by Volker

------------------------------------------------------------------------
r1186571 | tokoe | 2010-10-17 11:36:35 +1300 (Sun, 17 Oct 2010) | 2 lines

Cache QRegExp objects, because their ctor is expensive

------------------------------------------------------------------------
r1186739 | tokoe | 2010-10-18 00:45:30 +1300 (Mon, 18 Oct 2010) | 2 lines

Backport performance improvements

------------------------------------------------------------------------
r1186756 | tokoe | 2010-10-18 02:14:55 +1300 (Mon, 18 Oct 2010) | 2 lines

Backport performance improvements

------------------------------------------------------------------------
r1186759 | tokoe | 2010-10-18 02:21:25 +1300 (Mon, 18 Oct 2010) | 2 lines

Backport performance improvements (r1186758)

------------------------------------------------------------------------
r1186763 | tokoe | 2010-10-18 02:30:57 +1300 (Mon, 18 Oct 2010) | 2 lines

Backport bugfix/performance improvments (r1186750)

------------------------------------------------------------------------
r1186766 | tokoe | 2010-10-18 02:35:52 +1300 (Mon, 18 Oct 2010) | 2 lines

Filter for categories/tags as well

------------------------------------------------------------------------
r1187237 | tokoe | 2010-10-19 07:54:02 +1300 (Tue, 19 Oct 2010) | 2 lines

Restore Qt 4.6 compatibility. Thanks to reavertm for noticing!

------------------------------------------------------------------------
r1190628 | mueller | 2010-10-29 00:43:18 +1300 (Fri, 29 Oct 2010) | 2 lines

version bump

------------------------------------------------------------------------
