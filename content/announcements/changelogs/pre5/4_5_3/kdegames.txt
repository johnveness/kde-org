------------------------------------------------------------------------
r1181771 | scripty | 2010-10-02 15:42:42 +1300 (Sat, 02 Oct 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1182107 | schwarzer | 2010-10-04 00:49:13 +1300 (Mon, 04 Oct 2010) | 5 lines

rebuild board when difficulty level has changed

backport of r1175306, r1175348, r1181471 and a part of r1181709

CCBUG: 230241
------------------------------------------------------------------------
r1182108 | schwarzer | 2010-10-04 00:49:15 +1300 (Mon, 04 Oct 2010) | 1 line

SVN_SILENT: changelog
------------------------------------------------------------------------
r1182109 | schwarzer | 2010-10-04 00:49:17 +1300 (Mon, 04 Oct 2010) | 11 lines

fix logical error

Checking this option ensures that the created games are solvable.
The option label was wrong. I could have kept the label and
adjusted the code accordingly but I decided to keep the code and
switch the meaning of the label, since it makes more sense
this way.

backport of r1181702, r1181704, r1181709, r1181811 and r1181819

CCBUG: 145953
------------------------------------------------------------------------
r1182110 | schwarzer | 2010-10-04 00:49:19 +1300 (Mon, 04 Oct 2010) | 1 line

SVN_SILENT: changelog
------------------------------------------------------------------------
r1182812 | lueck | 2010-10-06 07:20:50 +1300 (Wed, 06 Oct 2010) | 1 line

documentation backport from trunk for 4.5.3
------------------------------------------------------------------------
r1183273 | lueck | 2010-10-07 08:22:00 +1300 (Thu, 07 Oct 2010) | 1 line

backport for 4.5
------------------------------------------------------------------------
r1183601 | scripty | 2010-10-08 15:56:25 +1300 (Fri, 08 Oct 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1187941 | schwarzer | 2010-10-21 09:30:08 +1300 (Thu, 21 Oct 2010) | 2 lines

unify indentation + some whitespace fixes

------------------------------------------------------------------------
