------------------------------------------------------------------------
r945020 | huynhhuu | 2009-03-26 17:59:57 +0000 (Thu, 26 Mar 2009) | 4 lines

Backport of r933132:
part one of the "make oxygen with webkit" work

the pixel metrics aren't optional. we'll need more of those.
------------------------------------------------------------------------
r945021 | huynhhuu | 2009-03-26 18:00:10 +0000 (Thu, 26 Mar 2009) | 2 lines

Backport of r942914:
link to kio so that qt only apps will use the kde file dialog
------------------------------------------------------------------------
r945027 | ppenz | 2009-03-26 18:07:09 +0000 (Thu, 26 Mar 2009) | 4 lines

Backport of SVN commit 945024: Fixed incorrect grouping of dates. Thanks to Jacopo De Simoi for the patch!

CCBUG: 187958

------------------------------------------------------------------------
r945164 | pino | 2009-03-27 00:34:56 +0000 (Fri, 27 Mar 2009) | 3 lines

proof-read
(the string is not extracted yet, so that's why i'm changing it in a string-frozen branch)

------------------------------------------------------------------------
r945590 | darioandres | 2009-03-27 18:53:47 +0000 (Fri, 27 Mar 2009) | 4 lines

Use proper icon for filetype (fixes bug 185307 on 4.2.x branch)

CCBUG:185307

------------------------------------------------------------------------
r945879 | annma | 2009-03-28 13:58:45 +0000 (Sat, 28 Mar 2009) | 2 lines

update

------------------------------------------------------------------------
r945886 | fredrik | 2009-03-28 14:12:25 +0000 (Sat, 28 Mar 2009) | 8 lines

Fix local service menus and built-in local services not being shown
in the context menu for files in desktop:/.

This bug fix will be in KDE 4.2.3.

BUG: 182922
BUG: 184234

------------------------------------------------------------------------
r945894 | annma | 2009-03-28 14:15:56 +0000 (Sat, 28 Mar 2009) | 2 lines

reverse previous change as we are frozen for doc in 4.2 branch, I keep forgetting...Sorry

------------------------------------------------------------------------
r946318 | scripty | 2009-03-29 08:00:00 +0000 (Sun, 29 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r946619 | jacopods | 2009-03-29 19:53:41 +0000 (Sun, 29 Mar 2009) | 7 lines

Avoid the context being reset()-ed before running the item
(not yet closing #181453 for now, even if it should be fixed - awaiting feedback)

CCBUG:181453
CCBUG:186036
CCBUG:188186

------------------------------------------------------------------------
r946641 | mlaurent | 2009-03-29 21:39:19 +0000 (Sun, 29 Mar 2009) | 3 lines

Backport:
start kttsd when necessary

------------------------------------------------------------------------
r947007 | piggz | 2009-03-30 18:16:18 +0000 (Mon, 30 Mar 2009) | 2 lines

Backport from trunk to allow the kc module to open if powerdevil is registered, and the power policy is registered

------------------------------------------------------------------------
r947077 | freininghaus | 2009-03-30 21:04:24 +0000 (Mon, 30 Mar 2009) | 6 lines

Backport commit 923757 to the 4.2 branch.

Fixes the problem that multiple tabs might get opened on middle clicking a folder in Columns View.

BUG: 188497

------------------------------------------------------------------------
r947284 | mlaurent | 2009-03-31 11:18:32 +0000 (Tue, 31 Mar 2009) | 3 lines

backport:
fix execute on windows too

------------------------------------------------------------------------
r947485 | fredrik | 2009-03-31 15:39:58 +0000 (Tue, 31 Mar 2009) | 1 line

Backport r947484.
------------------------------------------------------------------------
r947578 | pino | 2009-03-31 19:58:59 +0000 (Tue, 31 Mar 2009) | 4 lines

extract .cc files, not .cpp
(gives a new .pot of 1 message)
CCMAIL: kde-i18n-doc@kde.org

------------------------------------------------------------------------
r947693 | scripty | 2009-04-01 07:39:54 +0000 (Wed, 01 Apr 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r947731 | graesslin | 2009-04-01 10:00:12 +0000 (Wed, 01 Apr 2009) | 2 lines

Backport: 947729
MagicLamp requires OpenGL.
------------------------------------------------------------------------
r947904 | fredrik | 2009-04-01 15:35:28 +0000 (Wed, 01 Apr 2009) | 1 line

Backport r947900.
------------------------------------------------------------------------
r947942 | darioandres | 2009-04-01 18:42:10 +0000 (Wed, 01 Apr 2009) | 9 lines

Backport to 4.2branch of:

SVN commit 947937 by darioandres:

Properly stop the query and close the dialog when closing the dialog's "Close"
button

CCBUG: 187309

------------------------------------------------------------------------
r947944 | darioandres | 2009-04-01 18:50:06 +0000 (Wed, 01 Apr 2009) | 3 lines

Now using "locate" returns results properly


------------------------------------------------------------------------
r948237 | lunakl | 2009-04-02 16:03:21 +0000 (Thu, 02 Apr 2009) | 4 lines

Backport r948236.
Use just kWarning() for X errors, so that users don't report various
"errors".

------------------------------------------------------------------------
r948349 | ianjo | 2009-04-02 20:14:56 +0000 (Thu, 02 Apr 2009) | 9 lines

Backport r948339.
Pass along resize events to the embedded widget, and also set the
correct size for the widget before using XReparentWindow on it.

Thanks for all the feedback and testing of the patch!

CCBUG: 66492


------------------------------------------------------------------------
r948408 | dfaure | 2009-04-02 22:42:08 +0000 (Thu, 02 Apr 2009) | 4 lines

Maksim debugged that openUrlRequest with target = _blank made drops the browserArgs, e.g. the post data.
Testcase: <form action="http://bugs.kde.org/show_bug.cgi" method="post" target="_blank"><input type="hidden" name="id" value="176916"><input type="submit"></form>
Clicking on submit should open a specific bug report page.

------------------------------------------------------------------------
r948414 | dfaure | 2009-04-02 23:18:51 +0000 (Thu, 02 Apr 2009) | 4 lines

When looking for a frame by name, look first in the calling part. Makes a difference in case of duplicates...
Fix will be in 4.2.3.
BUG: 133967

------------------------------------------------------------------------
r948561 | scripty | 2009-04-03 07:28:28 +0000 (Fri, 03 Apr 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r948755 | jacopods | 2009-04-03 17:20:42 +0000 (Fri, 03 Apr 2009) | 2 lines

backporting 948752; take care of whitespaces in history as well + fix m_queryRunning  

------------------------------------------------------------------------
r949360 | jacopods | 2009-04-05 04:31:44 +0000 (Sun, 05 Apr 2009) | 3 lines

backport 949359.
CCBUG: 183206

------------------------------------------------------------------------
r949859 | jacopods | 2009-04-06 04:15:18 +0000 (Mon, 06 Apr 2009) | 2 lines

backporting 949858 - avoid white flashes when approaching the glowbar

------------------------------------------------------------------------
r949865 | ppenz | 2009-04-06 05:58:18 +0000 (Mon, 06 Apr 2009) | 3 lines

Backport of SVN commit 949864: Fixed regression introduced with KDE 4.2.0 that the column width setting was ignored.

CCBUG: 188701
------------------------------------------------------------------------
r950712 | dafre | 2009-04-07 15:56:35 +0000 (Tue, 07 Apr 2009) | 2 lines

Ops, codestyle

------------------------------------------------------------------------
r950731 | dafre | 2009-04-07 17:02:34 +0000 (Tue, 07 Apr 2009) | 2 lines

Completing, and Fixing compilation of previous half-failed backport

------------------------------------------------------------------------
r951675 | aacid | 2009-04-09 21:16:47 +0000 (Thu, 09 Apr 2009) | 3 lines

Backport r951674
leaks -= 4

------------------------------------------------------------------------
r951691 | aacid | 2009-04-09 21:33:31 +0000 (Thu, 09 Apr 2009) | 3 lines

Backport r951689.
leaks -= 4

------------------------------------------------------------------------
r951716 | sebas | 2009-04-09 22:23:49 +0000 (Thu, 09 Apr 2009) | 4 lines

Remove the shadow

It's not in line with how other applets look, and just adds noise

------------------------------------------------------------------------
r951768 | scripty | 2009-04-10 07:26:12 +0000 (Fri, 10 Apr 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r951827 | stikonas | 2009-04-10 10:21:06 +0000 (Fri, 10 Apr 2009) | 1 line

Backport r950373 to fix bug #187699 in calendar applet.
------------------------------------------------------------------------
r951853 | dafre | 2009-04-10 12:51:27 +0000 (Fri, 10 Apr 2009) | 2 lines

Backporting ck fix

------------------------------------------------------------------------
r951897 | darioandres | 2009-04-10 14:43:28 +0000 (Fri, 10 Apr 2009) | 7 lines

Backport ot 4.2branch of:
SVN commit 951895 by darioandres:

Adjust the first column width to the maximum needed

CCBUG: 178794

------------------------------------------------------------------------
r951962 | freininghaus | 2009-04-10 16:49:11 +0000 (Fri, 10 Apr 2009) | 6 lines

Show tab names containing '&' correctly in Konqueror.

Fix will be in KDE 4.2.3.

CCBUG: 189281

------------------------------------------------------------------------
r951969 | freininghaus | 2009-04-10 16:57:05 +0000 (Fri, 10 Apr 2009) | 4 lines

Fix compile problem which was introduced with rev. 951827.

CCMAIL: stikonas@gmail.com

------------------------------------------------------------------------
r952068 | darioandres | 2009-04-10 20:15:54 +0000 (Fri, 10 Apr 2009) | 10 lines

Backport to 4.2branch of:

SVN commit 952067 by darioandres:

Use a standard QHBoxLayout instead of a QFormLayout for two groupboxes
Fixes a layouting issue
(The dialog may still need some work to be perfect anyways)

CCBUG: 178953

------------------------------------------------------------------------
r952110 | darioandres | 2009-04-10 23:05:30 +0000 (Fri, 10 Apr 2009) | 7 lines

Backport ot 4.2branch of:
SVN commit 952109 by darioandres:

Proper deletion of char[]

CCBUG: 173113

------------------------------------------------------------------------
r952114 | darioandres | 2009-04-10 23:23:08 +0000 (Fri, 10 Apr 2009) | 4 lines

Fixing my own error

CCBUG: 173113

------------------------------------------------------------------------
r952151 | scripty | 2009-04-11 07:58:19 +0000 (Sat, 11 Apr 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r952591 | scripty | 2009-04-12 07:21:34 +0000 (Sun, 12 Apr 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r952797 | ogoffart | 2009-04-12 15:11:30 +0000 (Sun, 12 Apr 2009) | 3 lines

Backport 952796 : make sure to connect dbus signals even when knotify4 is started after plasma
CCBUG: 189221

------------------------------------------------------------------------
r952926 | dafre | 2009-04-12 21:28:33 +0000 (Sun, 12 Apr 2009) | 2 lines

Backporting fix

------------------------------------------------------------------------
r952928 | dafre | 2009-04-12 21:31:55 +0000 (Sun, 12 Apr 2009) | 3 lines

Fixing small logic error in backport


------------------------------------------------------------------------
r952939 | dafre | 2009-04-12 22:10:50 +0000 (Sun, 12 Apr 2009) | 3 lines

Backporting QHash performance improvements


------------------------------------------------------------------------
r952948 | dfaure | 2009-04-12 22:56:47 +0000 (Sun, 12 Apr 2009) | 6 lines

Backport SVN commit 950518:

We don't know the mimetype here, so better not set it at all, konq will determine it.
Better than setting inode/directory for everything due to the wrong S_IFDIR.
Fixes "foo is a file but a dir was expected" error when doing MMB on a file.

------------------------------------------------------------------------
r953264 | mlaurent | 2009-04-13 14:39:38 +0000 (Mon, 13 Apr 2009) | 3 lines

Backport :
not necessary to make it twice

------------------------------------------------------------------------
r954130 | scripty | 2009-04-15 08:16:57 +0000 (Wed, 15 Apr 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r954374 | ppenz | 2009-04-15 18:04:04 +0000 (Wed, 15 Apr 2009) | 10 lines

Backport of SVN commit 954371:
Initialize the name-label widget with a maximum size. This is important,
otherwise at least one resize event would be needed to apply a maximum width.
This gets a problem if the dock is overlapped by another dock: No resize event
is emitted and the preferred size increases in an unlimited way. Result: when
hovering files with very long filenames, the overall dock width will increase
in an unexpected way.

CCBUG: 189596

------------------------------------------------------------------------
r954410 | ppenz | 2009-04-15 18:44:29 +0000 (Wed, 15 Apr 2009) | 4 lines

Backport of SVN commit 954402:
After restoring the current item when going back to a folder, the current-item URL must be cleared to prevent moving the focus when the directory content will be changed later.

CCBUG: 189522
------------------------------------------------------------------------
r954732 | scripty | 2009-04-16 07:39:40 +0000 (Thu, 16 Apr 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r955026 | aseigo | 2009-04-16 19:15:26 +0000 (Thu, 16 Apr 2009) | 2 lines

backport the withdrawn-is-ok-for-attention-seeking-windows fix

------------------------------------------------------------------------
r955815 | shantanu | 2009-04-18 15:33:35 +0000 (Sat, 18 Apr 2009) | 2 lines

backport for trunk revision 928770

------------------------------------------------------------------------
r955867 | mart | 2009-04-18 17:42:30 +0000 (Sat, 18 Apr 2009) | 2 lines

backport a rtl fix in r955865, fixes bug 165100

------------------------------------------------------------------------
r956384 | hindenburg | 2009-04-20 00:05:50 +0000 (Mon, 20 Apr 2009) | 4 lines

Allow telnet:// to work for bookmarks.

CCBUG: 173212

------------------------------------------------------------------------
r956386 | hindenburg | 2009-04-20 00:11:46 +0000 (Mon, 20 Apr 2009) | 4 lines

Do not delete whitespace if at end of wrapping line.

CCBUG: 90201

------------------------------------------------------------------------
r956397 | hindenburg | 2009-04-20 01:09:15 +0000 (Mon, 20 Apr 2009) | 1 line

update version
------------------------------------------------------------------------
r957295 | aseigo | 2009-04-21 18:44:01 +0000 (Tue, 21 Apr 2009) | 2 lines

backport mem leak plugging; deleting stuff is, you know, good.

------------------------------------------------------------------------
r957458 | scripty | 2009-04-22 07:50:16 +0000 (Wed, 22 Apr 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r957518 | mleupold | 2009-04-22 10:15:36 +0000 (Wed, 22 Apr 2009) | 10 lines

Backport of r944097.

Rename keyboard.desktop to kcm_keyboard.desktop to avoid conflict with gnome's keyboard.desktop (but they should also 
rename theirs, it's in our namespace after all, it starts with a k :-)

Left keyboard.desktop around so that `kcmshell4 keyboard` still works, for compat.
CCBUG: 169710
CCMAIL: dfaure@kde.org
CCMAIL: toma@kde.org

------------------------------------------------------------------------
r957547 | dfaure | 2009-04-22 12:40:03 +0000 (Wed, 22 Apr 2009) | 4 lines

Backport r929806 by stikonas:
Fix spinBox suffix in kcmtrash which wasn't translatable at all.
BUG: 190313

------------------------------------------------------------------------
r957706 | freininghaus | 2009-04-22 18:41:25 +0000 (Wed, 22 Apr 2009) | 8 lines

Make it impossible to hide one of the views in a split view setup
completely by dragging the QSplitterHandle to the left or right. This
fixes the problem that a hidden view disappears completely after
closing the visible view. The root cause of this issue still seems to
be unclear though.

CCBUG: 190265

------------------------------------------------------------------------
r957717 | dfaure | 2009-04-22 19:00:01 +0000 (Wed, 22 Apr 2009) | 3 lines

Backport: Fix "opening the cookie config changed the default setting to Ask", as discovered by Sho_.
The default setting in the kcookiejar code was changed to Accept, but not this one.

------------------------------------------------------------------------
r957823 | jacopods | 2009-04-22 23:06:40 +0000 (Wed, 22 Apr 2009) | 2 lines

Backport the use of isValid() in runners; saves some good number of iterations when typing quickly.

------------------------------------------------------------------------
r957838 | jacopods | 2009-04-22 23:45:11 +0000 (Wed, 22 Apr 2009) | 2 lines

Backport 957836

------------------------------------------------------------------------
r957963 | scripty | 2009-04-23 07:32:30 +0000 (Thu, 23 Apr 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r958172 | lunakl | 2009-04-23 13:42:01 +0000 (Thu, 23 Apr 2009) | 5 lines

Make ksmserver not use autogenerated interface for klauncher,
because kdecore provides an old binary-incompatible copy of it.
(http://lists.kde.org/?l=kde-core-devel&m=123896026829059&w=2)


------------------------------------------------------------------------
r958315 | dfaure | 2009-04-23 19:08:25 +0000 (Thu, 23 Apr 2009) | 3 lines

Prefer local urls if possible, to avoid problems with desktop:/ urls from other users.
CCBUG: 184403

------------------------------------------------------------------------
r958509 | scripty | 2009-04-24 07:33:38 +0000 (Fri, 24 Apr 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r958549 | lunakl | 2009-04-24 09:53:54 +0000 (Fri, 24 Apr 2009) | 3 lines

Oops, the kdecore header name is different.


------------------------------------------------------------------------
r958942 | scripty | 2009-04-25 07:53:13 +0000 (Sat, 25 Apr 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r959069 | darioandres | 2009-04-25 15:17:55 +0000 (Sat, 25 Apr 2009) | 13 lines

Backport to 4.2branch of:
SVN commit 959066 by darioandres:

Preserve global shortcut when switching menu-style Kickoff<->SimpleMenu
This is just a workaround (because all the settings are lost on the switch)

The real fix would be not destroying the current applet and creating a new one
on switch but changing its style internally

Part 1

CCBUG: 178926

------------------------------------------------------------------------
r959074 | darioandres | 2009-04-25 15:28:33 +0000 (Sat, 25 Apr 2009) | 14 lines

Backport to 4.2branch of:
SVN commit 959066 by darioandres:

Preserve global shortcut when switching menu-style Kickoff<->SimpleMenu
This is just a workaround (because all the settings are lost on the switch)

The real fix would be not destroying the current applet and creating a new one
on switch but changing its style internally

Part 2

CCBUG: 178926


------------------------------------------------------------------------
r959322 | darioandres | 2009-04-26 00:52:39 +0000 (Sun, 26 Apr 2009) | 10 lines

Backport to 4.2branch of:
SVN commit 959320 by darioandres:

Validate the image the favicon engine provides to the web-browser applet.
In some case it could return an invalid image, which when being setted as the
PopupIcon
broke the layouting of the plasmoid

CCBUG: 179815

------------------------------------------------------------------------
r959378 | aseigo | 2009-04-26 08:28:52 +0000 (Sun, 26 Apr 2009) | 2 lines

backport fix for popups without a timeout

------------------------------------------------------------------------
r959740 | ilic | 2009-04-27 00:39:21 +0000 (Mon, 27 Apr 2009) | 1 line

i18n fix: translate language (mode) mode name in status bar. (bport: 959739)
------------------------------------------------------------------------
r959819 | dfaure | 2009-04-27 09:58:48 +0000 (Mon, 27 Apr 2009) | 5 lines

Repair regression "URL auto-completion doesn't query bookmarks anymore".
QComboBox sends keypress events _directly_ to the lineedit, bypassing any event filters; so our delayed-init
event filter must be installed on the combobox itself, not on the lineedit, otherwise it's not called, as Edulix found out.
BUG: 181785

------------------------------------------------------------------------
r960088 | freininghaus | 2009-04-27 22:02:17 +0000 (Mon, 27 Apr 2009) | 6 lines

Make sure that the sizes of other views are not changed when one view is split.

Fix will be in KDE 4.2.3.

CCBUG: 160407

------------------------------------------------------------------------
r960115 | aseigo | 2009-04-27 23:01:40 +0000 (Mon, 27 Apr 2009) | 2 lines

backport fix for BR#190886

------------------------------------------------------------------------
r960132 | aseigo | 2009-04-27 23:56:05 +0000 (Mon, 27 Apr 2009) | 3 lines

backport fix for bug #190786
CCBUG:190786

------------------------------------------------------------------------
r960497 | lunakl | 2009-04-28 14:03:53 +0000 (Tue, 28 Apr 2009) | 4 lines

CARD8 is not int.
CCBUG: 190811


------------------------------------------------------------------------
r960514 | graesslin | 2009-04-28 15:07:26 +0000 (Tue, 28 Apr 2009) | 4 lines

Backport revision 960039:
The assert for popRenderTarget in kwinshadereffect caused the kwin freezes as if assert has not been compiled in, the fbo will never be detached. So now we have working looking glass and sharpen effect back.
CCBUG: 183981

------------------------------------------------------------------------
r960520 | graesslin | 2009-04-28 15:10:24 +0000 (Tue, 28 Apr 2009) | 4 lines

Backport rev 960048:
Mouse polling was deactivated wrongly in all zooming effects.
CCBUG: 183983

------------------------------------------------------------------------
r960521 | graesslin | 2009-04-28 15:12:16 +0000 (Tue, 28 Apr 2009) | 4 lines

Backport revision 959982:
Trigger a full repaint when mouse moves in magnifier effect. This is a slight overhead but fast mouse movements caused artefacts as some change events were lost.
CCBUG: 187658

------------------------------------------------------------------------
r960559 | lunakl | 2009-04-28 16:28:42 +0000 (Tue, 28 Apr 2009) | 3 lines

Backport r960555, just in case.


------------------------------------------------------------------------
r960895 | scripty | 2009-04-29 08:18:03 +0000 (Wed, 29 Apr 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r961201 | freininghaus | 2009-04-29 16:51:46 +0000 (Wed, 29 Apr 2009) | 6 lines

Fix an issue which is similar to bug 160407: Splitting the first child of a KonqFrameContainer and then removing one of the grandchildren also resized the bottom child to zero size.

Fix will be in KDE 4.2.3.

CCBUG: 160407

------------------------------------------------------------------------
r961314 | jacopods | 2009-04-29 21:44:15 +0000 (Wed, 29 Apr 2009) | 2 lines

Prevent a late match result to steal the item focus in case the user is manually selecting the item

------------------------------------------------------------------------
r961318 | jacopods | 2009-04-29 21:49:40 +0000 (Wed, 29 Apr 2009) | 2 lines

Avoid interference between the bookmarks runner and the nepomuk runner by slightly raising the partial match relevance of the former

------------------------------------------------------------------------
r961518 | dimsuz | 2009-04-30 09:03:18 +0000 (Thu, 30 Apr 2009) | 3 lines

Merge a case-sensitivity bugfix from trunk (r961516)
BUG: 191057

------------------------------------------------------------------------
