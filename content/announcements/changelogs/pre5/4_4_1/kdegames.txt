------------------------------------------------------------------------
r1085787 | jobermayr | 2010-02-05 22:30:12 +0000 (Fri, 05 Feb 2010) | 6 lines

forward port commit 986188 (KDE 4.3):
--> remove unused "ksnapshots" for kdegames docs <--

These snapshots have not been used for a longer time. So I think we should remove them also from /trunk ...

CCMAIL:kde-games-devel@kde.org
------------------------------------------------------------------------
r1086309 | schwarzer | 2010-02-06 21:00:28 +0000 (Sat, 06 Feb 2010) | 1 line

changelog
------------------------------------------------------------------------
r1086312 | schwarzer | 2010-02-06 21:06:49 +0000 (Sat, 06 Feb 2010) | 1 line

typo in changelog
------------------------------------------------------------------------
r1086830 | jobermayr | 2010-02-07 23:53:56 +0000 (Sun, 07 Feb 2010) | 6 lines

Apply working icons for ksirkskineditor and palapeli until Oxygen team provides its own...

Discussed on mailinglist:
http://lists.kde.org/?l=kde-games-devel&m=126375024121184&w=2

CCMAIL: kde-games-devel@kde.org
------------------------------------------------------------------------
r1086882 | scripty | 2010-02-08 04:27:08 +0000 (Mon, 08 Feb 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1087469 | scripty | 2010-02-09 04:13:38 +0000 (Tue, 09 Feb 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1087854 | lueck | 2010-02-09 17:21:21 +0000 (Tue, 09 Feb 2010) | 1 line

doc backport from trunk
------------------------------------------------------------------------
r1088671 | dimsuz | 2010-02-11 10:58:07 +0000 (Thu, 11 Feb 2010) | 4 lines

Merge a commit 1088626 from trunk, so that the fix will appear in KDE SC 4.4.1

CCBUG: 226234

------------------------------------------------------------------------
r1089586 | kleag | 2010-02-13 13:07:03 +0000 (Sat, 13 Feb 2010) | 1 line

Correct static skin
------------------------------------------------------------------------
r1091592 | scripty | 2010-02-17 04:22:50 +0000 (Wed, 17 Feb 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1092023 | scripty | 2010-02-18 04:42:46 +0000 (Thu, 18 Feb 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1092495 | scripty | 2010-02-19 04:23:51 +0000 (Fri, 19 Feb 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1093064 | scripty | 2010-02-20 04:30:50 +0000 (Sat, 20 Feb 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1095359 | scripty | 2010-02-24 04:25:52 +0000 (Wed, 24 Feb 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1095661 | majewsky | 2010-02-24 19:44:29 +0000 (Wed, 24 Feb 2010) | 2 lines

Backport 1095660. Patch by Paul Bunbury, thx!

------------------------------------------------------------------------
