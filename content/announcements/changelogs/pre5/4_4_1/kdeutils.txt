------------------------------------------------------------------------
r1085105 | scripty | 2010-02-04 11:21:22 +0000 (Thu, 04 Feb 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1086379 | scripty | 2010-02-07 05:00:52 +0000 (Sun, 07 Feb 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1087418 | rkcosta | 2010-02-09 00:44:27 +0000 (Tue, 09 Feb 2010) | 9 lines

Backport r1087415.

Revert "Make subfolder detection work with one-file archives."

This reverts commit 1079099: the decision didn't seem to be
very popular.

CCBUG: 225426

------------------------------------------------------------------------
r1087470 | scripty | 2010-02-09 04:13:59 +0000 (Tue, 09 Feb 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1087860 | lueck | 2010-02-09 17:25:43 +0000 (Tue, 09 Feb 2010) | 1 line

doc backport from trunk
------------------------------------------------------------------------
r1088456 | kossebau | 2010-02-10 21:47:44 +0000 (Wed, 10 Feb 2010) | 2 lines

backport of 1088455: install ELF structure description

------------------------------------------------------------------------
r1088503 | rkcosta | 2010-02-11 02:27:01 +0000 (Thu, 11 Feb 2010) | 10 lines

Backport r1088502.

Make preview dialogs behave as windows, not dialogs.

Commit 1067160 made the preview dialogs non-modal, but they still
appeared on top of the main window. Now we set them as Qt::Window's
so that this does not happen.

CCBUG: 226123

------------------------------------------------------------------------
r1088513 | scripty | 2010-02-11 04:39:19 +0000 (Thu, 11 Feb 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1089788 | rkcosta | 2010-02-14 00:37:52 +0000 (Sun, 14 Feb 2010) | 6 lines

Backport r1089787.

Ignore entries called only "."

It's present in ISO files and should not represent anything.

------------------------------------------------------------------------
r1089791 | rkcosta | 2010-02-14 00:53:56 +0000 (Sun, 14 Feb 2010) | 10 lines

Backport r1089789.

Add application/x-cd-image to the mimetype list.

This brings read-write support for ISO files back via libarchive.
There has been no specific test for ISO files, but they should
work just like any other format supported by libarchive.

CCBUG: 191807

------------------------------------------------------------------------
r1090138 | kossebau | 2010-02-14 18:41:34 +0000 (Sun, 14 Feb 2010) | 2 lines

backport of 1090136: slot onModified() needs to match ByteArrayDocument::localSyncStateChanged() in parameters

------------------------------------------------------------------------
r1090340 | scripty | 2010-02-15 04:25:24 +0000 (Mon, 15 Feb 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1091537 | rkcosta | 2010-02-16 23:47:52 +0000 (Tue, 16 Feb 2010) | 7 lines

Backport r1091102.

Do not delete m_iface in the destructor.

We already set ArchiveBase as its parent in the constructor,
so there's no need to delete it explicitly.

------------------------------------------------------------------------
r1091544 | rkcosta | 2010-02-17 00:24:02 +0000 (Wed, 17 Feb 2010) | 4 lines

Backport r1091538.

If qRegisterMetaType is needed, call it before connect().

------------------------------------------------------------------------
r1091597 | rkcosta | 2010-02-17 05:27:47 +0000 (Wed, 17 Feb 2010) | 4 lines

Backport r1091596.

Use K_PLUGIN_EXPORT to get rid of the annoying warning on startup.

------------------------------------------------------------------------
r1092024 | scripty | 2010-02-18 04:43:09 +0000 (Thu, 18 Feb 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1092496 | scripty | 2010-02-19 04:24:12 +0000 (Fri, 19 Feb 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1094090 | dakon | 2010-02-22 07:11:55 +0000 (Mon, 22 Feb 2010) | 1 line

fix broken connect() when trying to decrypt files
------------------------------------------------------------------------
r1094449 | dakon | 2010-02-22 19:40:57 +0000 (Mon, 22 Feb 2010) | 1 line

fix some stale references when deleting keys
------------------------------------------------------------------------
r1095885 | scripty | 2010-02-25 04:17:15 +0000 (Thu, 25 Feb 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
