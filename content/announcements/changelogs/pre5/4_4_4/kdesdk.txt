------------------------------------------------------------------------
r1121852 | shaforo | 2010-05-03 01:49:13 +1200 (Mon, 03 May 2010) | 7 lines

BUG: 181989
"i use an english locale while translating kde pot files from english to hebrew"

all the research and attention is done by Laszlo Papp.
CCMAIL:djszapi@archlinux.us


------------------------------------------------------------------------
r1121861 | shaforo | 2010-05-03 02:47:34 +1200 (Mon, 03 May 2010) | 2 lines

eliminate possible 0-pointer crash

------------------------------------------------------------------------
r1121874 | shaforo | 2010-05-03 03:49:05 +1200 (Mon, 03 May 2010) | 3 lines

BUG: 232417
fix UpdateJob, SelectJob scheduling order. Thanks everybody for the report.

------------------------------------------------------------------------
r1121878 | shaforo | 2010-05-03 03:56:21 +1200 (Mon, 03 May 2010) | 6 lines

BUG:218564
List of translation units does not show the currently active unit

please reopen the bug if the fix doesn't work for you


------------------------------------------------------------------------
r1121931 | lpapp | 2010-05-03 06:23:59 +1200 (Mon, 03 May 2010) | 1 line

Remove trailing whitespaces in xlifftextedit.cpp
------------------------------------------------------------------------
r1121932 | lpapp | 2010-05-03 06:25:16 +1200 (Mon, 03 May 2010) | 4 lines

Add more BIDI langues to Right to left layout direction
BUG: 181989


------------------------------------------------------------------------
r1122454 | shaforo | 2010-05-04 10:31:08 +1200 (Tue, 04 May 2010) | 8 lines

CCBUG: 229159

defer projectLoaded() call until event loop is running to eliminate QWidgetPrivate::showChildren(bool) startup crash.

the fix will be available in KDE 4.4.4
dear bug reporters: please tell me if your crashes disappear in 4.4.4. 


------------------------------------------------------------------------
r1124937 | dhaumann | 2010-05-10 22:49:14 +1200 (Mon, 10 May 2010) | 5 lines

backport SVN commit 1124516 by dhaumann:
    don't focus filesystembrowser on open-file if autosync enabled
    
CCBUG: 228319

------------------------------------------------------------------------
r1124938 | dhaumann | 2010-05-10 22:51:40 +1200 (Mon, 10 May 2010) | 8 lines

backport SVN commit 1124518 by dhaumann:
    attempt to fix crash in file system browser
    
    from the backtrace it's not entirely clear what happens. Maybe the
    m_fileBrowser pointer is invalid... Check this now.
    
CCBUG: 231261

------------------------------------------------------------------------
r1124939 | dhaumann | 2010-05-10 22:53:12 +1200 (Mon, 10 May 2010) | 6 lines

backport SVN commit 1124522 by cullmann:

Miquel Sabaté <mikisabate@gmail.com>
fix crash on view closing
CCBUG: 232140

------------------------------------------------------------------------
r1127586 | scripty | 2010-05-17 14:17:16 +1200 (Mon, 17 May 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1130965 | dhaumann | 2010-05-27 09:52:53 +1200 (Thu, 27 May 2010) | 5 lines

fix crash in sort method of document sort list proxy model for KDE >= 4.4

BUG: 220308
BUG: 203774

------------------------------------------------------------------------
r1131023 | scripty | 2010-05-27 14:05:52 +1200 (Thu, 27 May 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
