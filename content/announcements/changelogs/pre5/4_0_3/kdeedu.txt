2008-03-01 10:17 +0000 [r780835-780834]  gladhorn

	* branches/KDE/4.0/kdeedu/libkdeedu/keduvocdocument/keduvocexpression.cpp,
	  branches/KDE/4.0/kdeedu/libkdeedu/keduvocdocument/keduvoctranslation.h:
	  When changing the text of a translation, only change the text,
	  not reset all it's other properties like conjugations. BUG:
	  158459

	* branches/KDE/4.0/kdeedu/libkdeedu/keduvocdocument/kvtml2.dtd:
	  update dtd a little

2008-03-01 10:44 +0000 [r780839]  gladhorn

	* branches/KDE/4.0/kdeedu/parley/src/docprop-dialogs/TypeOptPage.cpp:
	  Fix crash when adding a subtype. Thanks for reporting! BUG:
	  158460

2008-03-01 17:52 +0000 [r780948]  lueck

	* branches/KDE/4.0/kdeedu/doc/kalzium/index.docbook,
	  branches/KDE/4.0/kdeedu/doc/klettres/index.docbook,
	  branches/KDE/4.0/kdeedu/doc/kmplot/reference.docbook,
	  branches/KDE/4.0/kdeedu/doc/kgeography/index.docbook,
	  branches/KDE/4.0/kdeedu/doc/kmplot/commands.docbook,
	  branches/KDE/4.0/kdeedu/doc/marble/index.docbook,
	  branches/KDE/4.0/kdeedu/doc/khangman/index.docbook,
	  branches/KDE/4.0/kdeedu/doc/kmplot/dcop.docbook,
	  branches/KDE/4.0/kdeedu/doc/kmplot/using.docbook,
	  branches/KDE/4.0/kdeedu/doc/kmplot/firststeps.docbook,
	  branches/KDE/4.0/kdeedu/doc/kmplot/install.docbook,
	  branches/KDE/4.0/kdeedu/doc/kmplot/configuration.docbook,
	  branches/KDE/4.0/kdeedu/doc/kmplot/index.docbook,
	  branches/KDE/4.0/kdeedu/doc/kalgebra/index.docbook: backport from
	  trunk

2008-03-05 17:23 +0000 [r782665]  lueck

	* branches/KDE/4.0/kdeedu/doc/kmplot/reference.docbook,
	  branches/KDE/4.0/kdeedu/doc/kmplot/configuration.docbook:
	  backport from trunk

2008-03-07 17:23 +0000 [r783293]  kkofler

	* branches/KDE/4.0/kdeedu/kalzium/src/CMakeLists.txt: Fix Kalzium
	  build with OpenBabel and without Eigen. (backport rev 783288 from
	  trunk)

2008-03-10 23:28 +0000 [r784343]  lunakl

	* branches/KDE/4.0/kdeedu/kwordquiz/src/kwordquiz.cpp,
	  branches/KDE/4.0/kdeedu/kstars/kstars/kstarsactions.cpp,
	  branches/KDE/4.0/kdeedu/kig/kig/kig_view.cpp,
	  branches/KDE/4.0/kdesdk/kate/app/katemainwindow.cpp,
	  branches/KDE/4.0/kdeedu/marble/src/QtMainWindow.cpp,
	  branches/KDE/4.0/kdegames/kgoldrunner/src/kgoldrunner.cpp,
	  branches/KDE/4.0/kdenetwork/kopete/kopete/chatwindow/chatview.cpp,
	  branches/KDE/4.0/kdegraphics/kolourpaint/mainWindow/kpMainWindow_Settings.cpp,
	  branches/KDE/4.0/kdegraphics/okular/shell/shell.cpp,
	  branches/KDE/4.0/kdegames/ktuberling/toplevel.cpp,
	  branches/KDE/4.0/kdeedu/marble/src/marble_part.cpp,
	  branches/KDE/4.0/kdeedu/kmplot/kmplot/kmplot.cpp: Backport
	  r784333 (remove usage of QWidget::showFullScreen() etc.), use new
	  KToggleFullScreenAction::setFullScreen() helper.

2008-03-11 16:05 +0000 [r784531]  mueller

	* branches/KDE/4.0/kdeedu/kmplot/kmplot/kmplot.cpp: the usual daily
	  unbreak compilation

2008-03-15 23:21 +0000 [r786065]  apol

	* branches/KDE/4.0/kdeedu/kalgebra/src/analitza/analitza.cpp:
	  Backport of the fix. CCBUG: 159376

2008-03-17 14:03 +0000 [r786628-786627]  cniehaus

	* branches/KDE/4.0/kdeedu/kalzium/data/iconsets/school/15.svg,
	  branches/KDE/4.0/kdeedu/kalzium/data/iconsets/school/25.svg
	  (added),
	  branches/KDE/4.0/kdeedu/kalzium/data/iconsets/school/57.svg: Sync
	  with trunk

	* branches/KDE/4.0/kdeedu/libkdeedu/libscience/data/isotopes.xml:
	  Sync with trunk

2008-03-24 13:23 +0000 [r789504]  gladhorn

	* branches/KDE/4.0/kdeedu/libkdeedu/keduvocdocument/keduvocdocument.cpp:
	  Preserve the csv delimiter when opening a document. Only
	  documents with TAB were read properly. BUG: 159053

2008-03-26 08:45 +0000 [r790226]  annma

	* branches/KDE/4.0/kdeedu/klettres/src/klettres.cpp: enable toolbar
	  chars to be pasted in input line, thanks to bdgraue

2008-03-26 17:46 +0000 [r790372]  annma

	* branches/KDE/4.0/kdeedu/klettres/src/klettresview.cpp: backport
	  of r790364 which fixed 2 more bugs

2008-03-27 06:54 +0000 [r790655-790654]  annma

	* branches/KDE/4.0/kdeedu/klettres/icons/hi128-action-klettres_kids.png,
	  branches/KDE/4.0/kdeedu/klettres/icons/hi22-action-klettres_grownup.png,
	  branches/KDE/4.0/kdeedu/klettres/icons/hisc-action-klettres_kids.svgz,
	  branches/KDE/4.0/kdeedu/klettres/icons/hi32-action-klettres_grownup.png,
	  branches/KDE/4.0/kdeedu/klettres/icons/hi48-action-klettres_kids.png,
	  branches/KDE/4.0/kdeedu/klettres/icons/hi16-action-klettres_grownup.png,
	  branches/KDE/4.0/kdeedu/klettres/icons/hi64-action-klettres_grownup.png,
	  branches/KDE/4.0/kdeedu/klettres/icons/hi128-action-klettres_grownup.png,
	  branches/KDE/4.0/kdeedu/klettres/icons/hisc-action-klettres_grownup.svgz,
	  branches/KDE/4.0/kdeedu/klettres/icons/hi48-action-klettres_grownup.png,
	  branches/KDE/4.0/kdeedu/klettres/icons/hi22-action-klettres_kids.png,
	  branches/KDE/4.0/kdeedu/klettres/icons/hi32-action-klettres_kids.png,
	  branches/KDE/4.0/kdeedu/klettres/icons/hi16-action-klettres_kids.png,
	  branches/KDE/4.0/kdeedu/klettres/icons/hi64-action-klettres_kids.png:
	  new "kids" and "grown-up" oxygen icons, thanks to Marc Cheng!
	  Awesome! CCMAIL=bdgraue@web.de

	* branches/KDE/4.0/kdeedu/klettres/icons/hisc-action-klettres_kids.svg
	  (added),
	  branches/KDE/4.0/kdeedu/klettres/icons/hisc-action-klettres_grownup.svg
	  (added): add those as well!

