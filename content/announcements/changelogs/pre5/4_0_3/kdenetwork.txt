2008-03-01 16:36 +0000 [r780917]  segato

	* branches/KDE/4.0/kdenetwork/kopete/kopete/config/behavior/behaviorconfig_general.h,
	  branches/KDE/4.0/kdenetwork/kopete/protocols/msn/ui/msneditaccountwidget.cpp,
	  branches/KDE/4.0/kdenetwork/kopete/libkopete/private/kopeteviewmanager.cpp,
	  branches/KDE/4.0/kdenetwork/kopete/kopete/config/behavior/behaviorconfig_general.cpp,
	  branches/KDE/4.0/kdenetwork/kopete/kopete/config/behavior/behaviorconfig.cpp:
	  Fix message handling and the override default server information
	  in msn config dialog

2008-03-04 19:23 +0000 [r782286-782283]  uwolfer

	* branches/KDE/4.0/kdenetwork/krdc/mainwindow.cpp: Backport:
	  (without i18n changes) SVN commit 774275 by uwolfer: tell the
	  truth SVN commit 776798 by uwolfer: Fix problem that KRDC could
	  not be exited anymore because user has clicked 'No' and
	  'Remember'. Just provide a 'Quit' and 'Cancel' button. #158011

	* branches/KDE/4.0/kdenetwork/krdc/vnc/vncview.cpp,
	  branches/KDE/4.0/kdenetwork/krdc/vnc/vncview.h: Backport: SVN
	  commit 779316 by uwolfer: Avoid infinite recursion with recent Qt
	  4.4 snapshot.

2008-03-07 19:33 +0000 [r783309]  rjarosz

	* branches/KDE/4.0/kdenetwork/kopete/libkopete/kopetemessage.cpp:
	  Backport commit 783308. Remove Public Text Identifier from
	  messages. Please test it. CCBUG: 158919

2008-03-07 21:49 +0000 [r783341]  rjarosz

	* branches/KDE/4.0/kdenetwork/kopete/kopete/chatwindow/kopetechatwindow.cpp:
	  Backport commit 783340. Remove keepScrolledDown hack because its
	  not needed anymore. Commit 783256 has fixed the real problem.

2008-03-08 14:30 +0000 [r783533]  rjarosz

	* branches/KDE/4.0/kdenetwork/kopete/protocols/jabber/jabbercontact.cpp,
	  branches/KDE/4.0/kdenetwork/kopete/protocols/jabber/jabberprotocol.cpp:
	  Backport commit 783532. Fix bug 155181: Crash when Jabber
	  protocol does not provide identity for transport account. Fix
	  crash when transport configuration dialog is opened for offline
	  jabber account. CCBUG: 155181

2008-03-09 14:12 +0000 [r783750]  uwolfer

	* branches/KDE/4.0/kdenetwork/krfb/CMakeLists.txt: Backport: SVN
	  commit 783749 by uwolfer: Fix typo. Patch by crazy. #155173

2008-03-09 20:55 +0000 [r783871]  rjarosz

	* branches/KDE/4.0/kdenetwork/kopete/libkopete/kopetechatsession.cpp,
	  branches/KDE/4.0/kdenetwork/kopete/libkopete/kopetechatsession.h:
	  Backport fix for bug 144527: Crash on shutdown from systray
	  context menu Jabber was using already deleted objects. CCBUG:
	  144527

2008-03-10 23:28 +0000 [r784343]  lunakl

	* branches/KDE/4.0/kdeedu/kwordquiz/src/kwordquiz.cpp,
	  branches/KDE/4.0/kdeedu/kstars/kstars/kstarsactions.cpp,
	  branches/KDE/4.0/kdeedu/kig/kig/kig_view.cpp,
	  branches/KDE/4.0/kdesdk/kate/app/katemainwindow.cpp,
	  branches/KDE/4.0/kdeedu/marble/src/QtMainWindow.cpp,
	  branches/KDE/4.0/kdegames/kgoldrunner/src/kgoldrunner.cpp,
	  branches/KDE/4.0/kdenetwork/kopete/kopete/chatwindow/chatview.cpp,
	  branches/KDE/4.0/kdegraphics/kolourpaint/mainWindow/kpMainWindow_Settings.cpp,
	  branches/KDE/4.0/kdegraphics/okular/shell/shell.cpp,
	  branches/KDE/4.0/kdegames/ktuberling/toplevel.cpp,
	  branches/KDE/4.0/kdeedu/marble/src/marble_part.cpp,
	  branches/KDE/4.0/kdeedu/kmplot/kmplot/kmplot.cpp: Backport
	  r784333 (remove usage of QWidget::showFullScreen() etc.), use new
	  KToggleFullScreenAction::setFullScreen() helper.

2008-03-12 19:03 +0000 [r784898]  rjarosz

	* branches/KDE/4.0/kdenetwork/kopete/libkopete/chatsessionmemberslistmodel.cpp,
	  branches/KDE/4.0/kdenetwork/kopete/libkopete/chatsessionmemberslistmodel.h:
	  Backport fix for bug 157102: crash when deleting contact while
	  chat window is open Disconnect old ChatSession connections when
	  ChatSession was changed. CCBUG: 157102

2008-03-12 20:54 +0000 [r784948]  segato

	* branches/KDE/4.0/kdenetwork/kopete/libkopete/kopeteavatarmanager.cpp:
	  Backport fix avatar scaling CCBUG:158957

2008-03-13 22:13 +0000 [r785387]  rjarosz

	* branches/KDE/4.0/kdenetwork/kopete/kopete/identity/identitystatuswidget.cpp:
	  Backport commit 785386. Don't show the identity status widget
	  till the height is set. This will make the animation of identity
	  status widget fluent.

2008-03-20 20:53 +0000 [r788144]  uwolfer

	* branches/KDE/4.0/kdenetwork/krdc/specialkeysdialog.cpp,
	  branches/KDE/4.0/kdenetwork/krdc/specialkeysdialog.h: Backport:
	  SVN commit 788141 by uwolfer: Fix sending special keys to to
	  remote desktop (e.g. Ctrl+Alt+Del). #153782

2008-03-23 20:41 +0000 [r789289]  rjarosz

	* branches/KDE/4.0/kdenetwork/kopete/protocols/oscar/liboscar/oscarmessage.cpp,
	  branches/KDE/4.0/kdenetwork/kopete/protocols/oscar/aimcontactbase.cpp,
	  branches/KDE/4.0/kdenetwork/kopete/protocols/oscar/liboscar/oscarmessage.h,
	  branches/KDE/4.0/kdenetwork/kopete/protocols/oscar/liboscar/tasks/messagereceivertask.cpp,
	  branches/KDE/4.0/kdenetwork/kopete/protocols/oscar/icqcontactbase.cpp,
	  branches/KDE/4.0/kdenetwork/kopete/protocols/oscar/liboscar/tasks/sendmessagetask.cpp,
	  branches/KDE/4.0/kdenetwork/kopete/protocols/oscar/liboscar/tasks/profiletask.cpp:
	  Backport commit 789284. Make text encoding more robust by using
	  optimal encoding. Allow offline messages and all AIM messages
	  (AIM doesn't sets the CAP_UTF8 anymore) to be UNICODE. Fix max
	  size of a message. CCBUG: 159652 CCBUG: 154983

2008-03-25 20:55 +0000 [r790088]  rjarosz

	* branches/KDE/4.0/kdenetwork/kopete/protocols/oscar/liboscar/tasks/ssimodifytask.cpp:
	  Backport fix for bug 159841: groups with russian names are
	  dislplays like "????" CCBUG: 159841

2008-03-26 18:40 +0000 [r790401]  uwolfer

	* branches/KDE/4.0/kdenetwork/kget/main.cpp: bump version

