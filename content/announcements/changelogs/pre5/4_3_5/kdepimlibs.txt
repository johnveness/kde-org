------------------------------------------------------------------------
r1056877 | mmrozowski | 2009-12-01 01:57:10 +0000 (Tue, 01 Dec 2009) | 1 line

[akonadi/CMakeLists.txt] Backport of #1056870, CMake related QA, just for the record
------------------------------------------------------------------------
r1060402 | winterz | 2009-12-08 20:02:15 +0000 (Tue, 08 Dec 2009) | 4 lines

Backport r1060313 by nlecureuil from trunk to the 4.3 branch:

Fix a crash

------------------------------------------------------------------------
r1062059 | winterz | 2009-12-13 17:12:43 +0000 (Sun, 13 Dec 2009) | 10 lines

Backport r1062016 by winterz from trunk to the 4.3 branch:

Fix "LDAP directory contents cannot be modified"
Thanks to the patch from rdratlos@yahoo.co.uk

CCBUG: 218353
MERGE: 4.3,e4



------------------------------------------------------------------------
r1064071 | scripty | 2009-12-20 04:12:22 +0000 (Sun, 20 Dec 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1064859 | winterz | 2009-12-22 00:04:21 +0000 (Tue, 22 Dec 2009) | 4 lines

Backport r1063984 by otrichet from trunk to the 4.3 branch:

Fix parsing of header "Mail-Copies-To: nobody"

------------------------------------------------------------------------
r1065903 | otrichet | 2009-12-24 18:16:16 +0000 (Thu, 24 Dec 2009) | 6 lines

Backport r1062490 by otrichet from trunk to the 4.3 branch:
kio_nntp could send a broken XOVER request to the server and hang on an error answer.

Backport r1065263 by otrichet from trunk to the 4.3 branch:
Don't use QDir::separator() when we meant '/'. This does not work on Windows...

------------------------------------------------------------------------
r1065914 | winterz | 2009-12-24 19:35:33 +0000 (Thu, 24 Dec 2009) | 7 lines

Backport r1065913 by winterz from trunk to the 4.3 branch:

updated, from Juliano
CCBUG: 219758
MERGE: 4.3


------------------------------------------------------------------------
r1066384 | tmcguire | 2009-12-26 23:05:36 +0000 (Sat, 26 Dec 2009) | 3 lines

Backport the bidi spoofing from trunk to the KDE 4.3 branch:
r1066360, r1066375 and r1066378

------------------------------------------------------------------------
r1066859 | scripty | 2009-12-28 04:22:34 +0000 (Mon, 28 Dec 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1069390 | osterfeld | 2010-01-03 11:30:30 +0000 (Sun, 03 Jan 2010) | 2 lines

Match the API docs: return 0, not -1 if there is no length given.

------------------------------------------------------------------------
r1072383 | scripty | 2010-01-10 04:11:25 +0000 (Sun, 10 Jan 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1074179 | winterz | 2010-01-13 15:59:17 +0000 (Wed, 13 Jan 2010) | 6 lines

backport SVN commit r1074158 by ttrnka:

Properly check errors during authentication to avoid falling into an infinite loop.

CCBUG:203715

------------------------------------------------------------------------
r1076756 | mueller | 2010-01-18 20:40:10 +0000 (Mon, 18 Jan 2010) | 2 lines

bump version

------------------------------------------------------------------------
