------------------------------------------------------------------------
r837261 | mlaurent | 2008-07-24 13:28:51 +0200 (Thu, 24 Jul 2008) | 3 lines

Backport:
Fix mem leak

------------------------------------------------------------------------
r840466 | mueller | 2008-08-01 10:42:32 +0200 (Fri, 01 Aug 2008) | 1 line

link fixes
------------------------------------------------------------------------
r841785 | scripty | 2008-08-04 06:56:53 +0200 (Mon, 04 Aug 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r842308 | johach | 2008-08-05 10:16:15 +0200 (Tue, 05 Aug 2008) | 4 lines

Backport boost detection to 4.1 branch.
See http://lists.kde.org/?l=kde-devel&m=121788299924181&w=2
CCMAIL:plasma-devel@kde.org

------------------------------------------------------------------------
r842655 | burmeister | 2008-08-05 20:06:27 +0200 (Tue, 05 Aug 2008) | 2 lines

Set minimum-size, since the panel does not give us the needed space otherwise.
BUG: 163870
------------------------------------------------------------------------
r844662 | scripty | 2008-08-10 07:04:41 +0200 (Sun, 10 Aug 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r846142 | scripty | 2008-08-13 07:33:33 +0200 (Wed, 13 Aug 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r846210 | mlaurent | 2008-08-13 11:25:57 +0200 (Wed, 13 Aug 2008) | 3 lines

Backport:
Fix layout

------------------------------------------------------------------------
r846212 | mlaurent | 2008-08-13 11:28:45 +0200 (Wed, 13 Aug 2008) | 3 lines

Backport:
Fix index

------------------------------------------------------------------------
r847715 | scripty | 2008-08-16 08:08:00 +0200 (Sat, 16 Aug 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r847791 | bram | 2008-08-16 12:44:33 +0200 (Sat, 16 Aug 2008) | 1 line

Decrease minimum size for Konqueror and Konsole profile applets.
------------------------------------------------------------------------
r847792 | bram | 2008-08-16 12:44:35 +0200 (Sat, 16 Aug 2008) | 6 lines

Fix Konsole and Konqueror profile applets when residing on the desktop:

o Hide the icon. Maybe someone can do something creative with it, but clicking on
  it crashed Plasma if the applet was on the desktop. Hence the extra safeguard
  in slotOpenDialog().
o Set minimum size to 100x100 when the applet is on the desktop.
------------------------------------------------------------------------
r847794 | bram | 2008-08-16 12:46:04 +0200 (Sat, 16 Aug 2008) | 1 line

Oops, this line shouldn't have been committed.
------------------------------------------------------------------------
r847946 | mlaurent | 2008-08-16 15:24:58 +0200 (Sat, 16 Aug 2008) | 2 lines

Add missing icons

------------------------------------------------------------------------
r852006 | scripty | 2008-08-25 07:27:08 +0200 (Mon, 25 Aug 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r853067 | scripty | 2008-08-27 08:06:10 +0200 (Wed, 27 Aug 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r853376 | bram | 2008-08-27 18:28:28 +0200 (Wed, 27 Aug 2008) | 1 line

Backport 853371: sort profile lists.
------------------------------------------------------------------------
r853711 | scripty | 2008-08-28 08:07:55 +0200 (Thu, 28 Aug 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
