2008-02-01 02:21 +0000 [r769302]  mattr

	* branches/KDE/4.0/kdenetwork/kopete/kopete/chatwindow/chatview.cpp:
	  Make sure we set the foreground color for the chatwindow
	  correctly. Fixes bug 156804 with the patch provided by Carlo
	  Segato. Thanks for the patch! This will be in KDE 4.0.2 CCBUG:
	  156804

2008-02-02 13:11 +0000 [r769942]  rjarosz

	* branches/KDE/4.0/kdenetwork/kopete/protocols/jabber/jabberchatsession.cpp,
	  branches/KDE/4.0/kdenetwork/kopete/protocols/msn/msnchatsession.cpp,
	  branches/KDE/4.0/kdenetwork/kopete/kopete/chatwindow/chatmessagepart.cpp:
	  Backport fix for bug 157102: crash when deleting contact while
	  chat window is open. We can't call the first() function in QList
	  when a list is empty. CCBUG: 157102

2008-02-07 03:55 +0000 [r771866]  mattr

	* branches/KDE/4.0/kdenetwork/kopete/protocols/msn/msnchatsession.cpp,
	  branches/KDE/4.0/kdenetwork/kopete/protocols/msn/msnprotocol.h,
	  branches/KDE/4.0/kdenetwork/kopete/protocols/msn/msnchatsession.h:
	  Backport patch for 156935. Should be fixed for KDE 4.0.2 CCBUG:
	  156935

2008-02-07 17:39 +0000 [r772063]  rjarosz

	* branches/KDE/4.0/kdenetwork/kopete/protocols/oscar/liboscar/tasks/buddyicontask.cpp:
	  Backport commit 772062. Delete finshed task.

2008-02-07 18:09 +0000 [r772073]  rjarosz

	* branches/KDE/4.0/kdenetwork/kopete/libkopete/kopetepropertycontainer.cpp:
	  Backport commit 772065. Emit propertyChanged after removal only
	  when property did exist.

2008-02-09 20:28 +0000 [r772956]  rjarosz

	* branches/KDE/4.0/kdenetwork/kopete/libkopete/private/kopeteemoticons.cpp:
	  Backport commit 772955. Don't add empty list.

2008-02-10 15:50 +0000 [r773225-773224]  rjarosz

	* branches/KDE/4.0/kdenetwork/kopete/kopete/contactlist/kopetecontactlistview.cpp,
	  branches/KDE/4.0/kdenetwork/kopete/kopete/contactlist/kopetecontactlistview.h:
	  Backport commit 773223. Update context menu actions when selected
	  MetaContact has changed its status and not just when user selects
	  different item.

	* branches/KDE/4.0/kdenetwork/kopete/libkopete/kopeteappearancesettings.kcfg:
	  Backport commit 773181. Show offline contacts by default.

2008-02-10 21:03 +0000 [r773353]  rjarosz

	* branches/KDE/4.0/kdenetwork/kopete/libkopete/kopetemessage.cpp:
	  Backport commit 773352. Convert plain text to html with
	  Qt::convertFromPlainText otherwise we will lose new lines.

2008-02-10 21:33 +0000 [r773367]  rjarosz

	* branches/KDE/4.0/kdenetwork/kopete/plugins/history/historylogger.cpp:
	  Backport commit 773366. The messages in history are saved as
	  plain text so load them as plain text too. Fixes new lines in
	  history.

2008-02-11 20:58 +0000 [r773817]  rjarosz

	* branches/KDE/4.0/kdenetwork/kopete/kopete/systemtray.cpp:
	  Backport fix for bug 157663: click on systray with message
	  notification also minimizes contact list. CCBUG: 157663

2008-02-11 21:23 +0000 [r773836]  rjarosz

	* branches/KDE/4.0/kdenetwork/kopete/libkopete/ui/avatarselectorwidget.cpp:
	  Backport fix for bug 157683: Adding avatar crashes kopete We have
	  to check if avatar isn't null. CCBUG: 157683

2008-02-13 22:48 +0000 [r774768]  mueller

	* branches/KDE/4.0/kdenetwork/kopete/libkopete/avdevice/videodevice.cpp:
	  compile fix for older linux kernels

2008-02-13 22:59 +0000 [r774773]  mueller

	* branches/KDE/4.0/kdenetwork/kopete/libkopete/avdevice/videodevice.cpp:
	  merge 774770

2008-02-14 20:06 +0000 [r775055]  rjarosz

	* branches/KDE/4.0/kdenetwork/kopete/protocols/msn/msnp2pdisplatcher.cpp,
	  branches/KDE/4.0/kdenetwork/kopete/protocols/msn/dispatcher.cpp,
	  branches/KDE/4.0/kdenetwork/kopete/protocols/msn/msnsocket.cpp,
	  branches/KDE/4.0/kdenetwork/kopete/protocols/msn/messageformatter.cpp,
	  branches/KDE/4.0/kdenetwork/kopete/protocols/msn/msncontact.cpp,
	  branches/KDE/4.0/kdenetwork/kopete/protocols/msn/msnchallengehandler.cpp,
	  branches/KDE/4.0/kdenetwork/kopete/protocols/msn/webcam.cpp,
	  branches/KDE/4.0/kdenetwork/kopete/protocols/msn/msnswitchboardsocket.cpp:
	  Backport fix for bug 154972: MSN Contact pictures and custom
	  emoticons are not displayed This will probably fix other problems
	  too because for QDataStreams we were using Qt 3.1 version which
	  didn't write int64 correctly. CCBUG: 154972

2008-02-14 20:38 +0000 [r775075-775073]  rjarosz

	* branches/KDE/4.0/kdenetwork/kopete/kopete/chatwindow/krichtexteditpart.cpp:
	  Backport SVN commit 773899 by cconnell: Set correct location of
	  UI .rc file

	* branches/KDE/4.0/kdenetwork/kopete/kopete/chatwindow/chattexteditpart.cpp:
	  Backport ChatTextEditPart setContents fix from commit 773908 by
	  cconnell

2008-02-15 12:06 +0000 [r775294]  rjarosz

	* branches/KDE/4.0/kdenetwork/kopete/protocols/msn/msnchatsession.cpp,
	  branches/KDE/4.0/kdenetwork/kopete/protocols/msn/ui/msneditaccountwidget.cpp,
	  branches/KDE/4.0/kdenetwork/kopete/protocols/msn/ui/msneditaccountui.ui:
	  Backport commit 775292. Save settings from account preferences
	  tab. CCBUG: 154972

2008-02-15 21:22 +0000 [r775454]  rjarosz

	* branches/KDE/4.0/kdenetwork/kopete/protocols/msn/p2p.cpp:
	  Backport commit 775453. Add missing null character, this fixes
	  photo downloading from offcial MSN client. CCBUG: 154972

2008-02-16 18:15 +0000 [r775820]  segato

	* branches/KDE/4.0/kdenetwork/kopete/protocols/msn/p2p.cpp,
	  branches/KDE/4.0/kdenetwork/kopete/protocols/msn/outgoingtransfer.cpp:
	  Fix sending of msn contact picture

2008-02-17 11:24 +0000 [r776072]  rjarosz

	* branches/KDE/4.0/kdenetwork/kopete/protocols/msn/dispatcher.cpp,
	  branches/KDE/4.0/kdenetwork/kopete/protocols/msn/incomingtransfer.cpp:
	  Backport commit 776071. Remove porting bugs. There are two more
	  reserve bugs that will be fixed by Carlo Segato when he commits
	  fix for photo upload.

2008-02-17 14:24 +0000 [r776151]  woebbe

	* branches/KDE/4.0/kdenetwork/kget/plasma/applet/plasma-kget.cpp:
	  compile

2008-02-17 21:28 +0000 [r776344]  raabe

	* branches/KDE/4.0/kdenetwork/knewsticker/feedsettingswidget.cpp,
	  branches/KDE/4.0/kdenetwork/knewsticker/feedsettingswidget.h:
	  automatically merged revision 776340: - This configuration widget
	  exists from the first show() until the end of the applet.
	  Consequently, it got notified about all feeds loaded and kept
	  adding them to the list widget in the configuration dialog. This
	  bug was reported in BR157948. The primary fix is to not stay
	  connected to the NewsFeedManager all the time but only while
	  we're adding a feed. We do an aditional check for the URL which
	  was loaded, just in case a feed update of some other URL finishes
	  while we're retrieving the to-be-added feed.

2008-02-17 22:14 +0000 [r776364]  rjarosz

	* branches/KDE/4.0/kdenetwork/kopete/libkopete/kopetemessage.cpp:
	  Backport commit 776363. Fix bug 157739: Text auto-wrapping
	  doesn't work in chat and history window CCBUG: 157739

2008-02-19 01:28 +0000 [r776912]  segato

	* branches/KDE/4.0/kdenetwork/kopete/protocols/msn/msnchatsession.cpp,
	  branches/KDE/4.0/kdenetwork/kopete/protocols/msn/msncontact.cpp,
	  branches/KDE/4.0/kdenetwork/kopete/protocols/msn/msnswitchboardsocket.cpp:
	  Fix to make msn reads the real account config

2008-02-19 18:48 +0000 [r777097]  rjarosz

	* branches/KDE/4.0/kdenetwork/kopete/protocols/oscar/liboscar/xtrazxservice.cpp,
	  branches/KDE/4.0/kdenetwork/kopete/protocols/oscar/liboscar/xtrazxawayservice.cpp:
	  Backport commit 777096. Fix miscompilation of Q_ASSERT with Qt
	  4.4

2008-02-20 22:56 +0000 [r777586]  segato

	* branches/KDE/4.0/kdenetwork/kopete/protocols/msn/dispatcher.cpp:
	  Fix outgoing file transfer with official msn client

2008-02-24 18:07 +0000 [r778828]  rjarosz

	* branches/KDE/4.0/kdenetwork/kopete/libkopete/kopeteonlinestatusmanager.cpp:
	  Backport commit 778826. Fix overlay icons in icons that don't
	  have 32px size.

2008-02-24 18:37 +0000 [r778838]  rjarosz

	* branches/KDE/4.0/kdenetwork/kopete/plugins/history/historydialog.cpp:
	  Backport commit 778837. Remove margin.

2008-02-24 20:10 +0000 [r778881]  rjarosz

	* branches/KDE/4.0/kdenetwork/kopete/libkopete/kopetecontact.cpp:
	  Backport commit 778880. Fix icon.

2008-02-25 00:04 +0000 [r778945]  segato

	* branches/KDE/4.0/kdenetwork/kopete/protocols/msn/msnsocket.cpp:
	  Fix crash when connecting to msn with HTTP method

2008-02-27 19:08 +0000 [r779996]  uwolfer

	* branches/KDE/4.0/kdenetwork/kget/ui/transfersviewdelegate.cpp:
	  Backport: SVN commit 773257 by dario: * Another optimization.
	  Store the gradient background pixmap in a static variable so that
	  we don't have to recalculate the gradient every paint event. This
	  makes repaint events weight more than 1/10 less than before *
	  Make sure the pixmap gets updated when the palette change.

2008-02-27 19:18 +0000 [r780000]  uwolfer

	* branches/KDE/4.0/kdenetwork/kget/main.cpp: bump version

