------------------------------------------------------------------------
r1169467 | gladhorn | 2010-08-29 22:56:29 +1200 (Sun, 29 Aug 2010) | 5 lines

when a synonym is entered, it would make the word count as wrong
instead just ask to try again

backport r1169466

------------------------------------------------------------------------
r1169753 | scripty | 2010-08-30 14:23:43 +1200 (Mon, 30 Aug 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1170123 | jmhoffmann | 2010-08-31 08:08:30 +1200 (Tue, 31 Aug 2010) | 6 lines

Backport of commit 1168541.

StackedTileLoader::updateTextureLayers: Clear d->m_sceneLayers and
d->m_textureLayers to be able to really update them. It might very well be
that map themes or layers are being removed.

------------------------------------------------------------------------
r1170370 | nienhueser | 2010-09-01 06:05:46 +1200 (Wed, 01 Sep 2010) | 4 lines

Fix setCoordinates call on gpx waypoints. Backport of commit 1170366. Will be in KDE 4.5.2 / Marble 0.10.2.
CCBUG: 249632


------------------------------------------------------------------------
r1170426 | gladhorn | 2010-09-01 08:31:03 +1200 (Wed, 01 Sep 2010) | 6 lines

Only add wrong answers once.

Wrong answers would get added to the list of wrong answers twice when
giving up by pressing enter twice for example.
backport r1170424

------------------------------------------------------------------------
r1170617 | nienhueser | 2010-09-02 04:57:33 +1200 (Thu, 02 Sep 2010) | 4 lines

Make download region dialog usable on Maemo. Backport of commit 1169429, will be in Marble 0.10.2.
CCMAIL: kspt.tor@gmail.com


------------------------------------------------------------------------
r1170657 | lueck | 2010-09-02 07:27:15 +1200 (Thu, 02 Sep 2010) | 1 line

doc backport for 4.5.2
------------------------------------------------------------------------
r1171083 | rahn | 2010-09-03 04:53:42 +1200 (Fri, 03 Sep 2010) | 5 lines

Backport of #1171082

Fix for runtimePluginPath by Konrad Enzensberger.


------------------------------------------------------------------------
r1171679 | makc | 2010-09-05 08:30:43 +1200 (Sun, 05 Sep 2010) | 2 lines

merge r1171674: 
Fix build on system without libdl
------------------------------------------------------------------------
r1171680 | makc | 2010-09-05 08:31:32 +1200 (Sun, 05 Sep 2010) | 2 lines

merge r1171676: 
Adjust in accordance with CMake Coding Style
------------------------------------------------------------------------
r1171683 | makc | 2010-09-05 08:40:21 +1200 (Sun, 05 Sep 2010) | 2 lines

merge r1146867: 
Rewrite Findlibgps.cmake without using PkgConfig.
------------------------------------------------------------------------
r1172219 | nienhueser | 2010-09-07 06:38:04 +1200 (Tue, 07 Sep 2010) | 3 lines

Hide now invalid route summary after clearing a route. Prevent am/pm from turning up in estimated travel duration.
BUG: 250338

------------------------------------------------------------------------
r1172281 | nienhueser | 2010-09-07 08:47:41 +1200 (Tue, 07 Sep 2010) | 2 lines

RIP namefinder. http://lists.openstreetmap.org/pipermail/dev/2010-August/020234.html. "Backport" of commit 1172277.

------------------------------------------------------------------------
r1172867 | scripty | 2010-09-08 15:20:18 +1200 (Wed, 08 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1173833 | rahn | 2010-09-11 02:30:24 +1200 (Sat, 11 Sep 2010) | 5 lines

Backport of r1183831

- Really, really fix the proxy issue once and for all.


------------------------------------------------------------------------
r1173908 | annma | 2010-09-11 08:02:57 +1200 (Sat, 11 Sep 2010) | 2 lines

backport of 1173906

------------------------------------------------------------------------
r1174111 | mmrozowski | 2010-09-11 21:57:30 +1200 (Sat, 11 Sep 2010) | 1 line

Backport 1137419
------------------------------------------------------------------------
r1174219 | nienhueser | 2010-09-12 02:19:37 +1200 (Sun, 12 Sep 2010) | 6 lines

- The position marker is a bit hard to spot on the N900 when there's only time for a quick glance at the screen. To improve that paint a half-transparent red circle at the current position (all versions). To give it a right to exist other than enhanced visibility, its size depends on the accuracy of the current position. The circle can be interpreted "the device is pretty sure that the real position is within the colored circle".
RB: 5168
Use a less transparent color and a minimum circle size for small screen devices to enhance the visibility of the position marker there. Backport of commit 1170361.
- Non-inline ctor. Backport of commit 1174217.
- Hopefully fix the build of the GpsdPositionProviderPlugin (builds at least on SUSE 11.2 right now). Backport of commit 1173799.

------------------------------------------------------------------------
r1175001 | pino | 2010-09-14 08:50:36 +1200 (Tue, 14 Sep 2010) | 11 lines

install the action icons in the data dir of step, and as hicolor

this way:
- we avoid polluting the oxygen icon theme with step-specific icons
  (and the conflict of the 'pointer' icon with oxygen, #201953)
- the icons are used even if the current icon theme is not oxygen
- the icons can still be themed by other themes

BUG: 201953
FIXED-IN: 4.5.2

------------------------------------------------------------------------
r1175092 | conet | 2010-09-14 18:11:22 +1200 (Tue, 14 Sep 2010) | 5 lines

BUG: 240979
Fix for KWorldClock that displays the UTC time instead of the local time after
loading the session until the mouse hovers over it. Backport to the 4.5 branch.


------------------------------------------------------------------------
r1177302 | scripty | 2010-09-20 14:47:44 +1200 (Mon, 20 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1179795 | nienhueser | 2010-09-26 21:52:49 +1300 (Sun, 26 Sep 2010) | 3 lines

Intercept the OpenRouteService "No streets within 300 m" error message and use our own, less confusing messages: A suggestion to move the point closer to a street at the first time ORS reports an error, a suggestion to check openrouteservice.org whether the country is supported at all the second time the error comes up. This is not perfect (ideally OpenRouteService could be queried for the areas it has map data for), but at least gives an indication now that OpenRouteService map data does not cover the whole world.
CCMAIL: lindsay.mathieson@gmail.com

------------------------------------------------------------------------
r1179820 | nienhueser | 2010-09-26 22:01:30 +1300 (Sun, 26 Sep 2010) | 4 lines

Write xml that validates against http://www.topografix.com/GPX/1/1/gpx.xsd
BUG: 251676
Backport of commit 1179736.

------------------------------------------------------------------------
r1179853 | nienhueser | 2010-09-26 22:41:50 +1300 (Sun, 26 Sep 2010) | 3 lines

Add tr() calls to the invalid via point error messages.
CCMAIL: kde-i18n-doc@kde.org

------------------------------------------------------------------------
