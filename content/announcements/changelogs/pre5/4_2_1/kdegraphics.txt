------------------------------------------------------------------------
r915013 | gateau | 2009-01-22 11:10:51 +0000 (Thu, 22 Jan 2009) | 4 lines

Do not emit dataChanged() for removed urls.

BUG:180498

------------------------------------------------------------------------
r915014 | gateau | 2009-01-22 11:10:56 +0000 (Thu, 22 Jan 2009) | 4 lines

Ensure mUrlToSelect is visible.

BUG:180580

------------------------------------------------------------------------
r915229 | lueck | 2009-01-22 15:48:10 +0000 (Thu, 22 Jan 2009) | 1 line

add lang="&language;" to article to make the markup translatable
------------------------------------------------------------------------
r915639 | scripty | 2009-01-23 13:14:29 +0000 (Fri, 23 Jan 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r916204 | cgilles | 2009-01-24 20:36:36 +0000 (Sat, 24 Jan 2009) | 2 lines

update internal LibRaw to 0.6.10. Binary compatibility preserved

------------------------------------------------------------------------
r916396 | cgilles | 2009-01-25 11:07:43 +0000 (Sun, 25 Jan 2009) | 2 lines

update interanl LibRaw to 0.6.11. binary compatibility preserved

------------------------------------------------------------------------
r916858 | gateau | 2009-01-26 09:32:15 +0000 (Mon, 26 Jan 2009) | 2 lines

Do not handle one-frame gif as animations. BUG:181177

------------------------------------------------------------------------
r917110 | gateau | 2009-01-26 22:05:38 +0000 (Mon, 26 Jan 2009) | 3 lines

Make sure the full image is getting loaded when waitUntilLoaded() is called.


------------------------------------------------------------------------
r917111 | gateau | 2009-01-26 22:05:48 +0000 (Mon, 26 Jan 2009) | 3 lines

Make sure KFileDialog is gone while saving. BUG:171430


------------------------------------------------------------------------
r917285 | cgilles | 2009-01-27 14:53:00 +0000 (Tue, 27 Jan 2009) | 2 lines

fix qt url

------------------------------------------------------------------------
r917412 | gateau | 2009-01-27 20:21:41 +0000 (Tue, 27 Jan 2009) | 5 lines

Correctly initialize buffer and reuse existing content whenever possible.

BUG:166075
BUG:169449

------------------------------------------------------------------------
r918139 | cgilles | 2009-01-29 12:30:27 +0000 (Thu, 29 Jan 2009) | 3 lines

backport commit #918138 from trunk
CCBUG: 175746

------------------------------------------------------------------------
r918642 | cgilles | 2009-01-30 12:20:53 +0000 (Fri, 30 Jan 2009) | 2 lines

backport commit #918640 from trunk

------------------------------------------------------------------------
r920672 | pino | 2009-02-03 15:27:37 +0000 (Tue, 03 Feb 2009) | 4 lines

Backport: check for errors after document loading.
Will be in KDE 4.2.1.
CCBUG: 182971

------------------------------------------------------------------------
r920833 | pino | 2009-02-03 21:03:25 +0000 (Tue, 03 Feb 2009) | 4 lines

Backport: really exclude user input while doing the sync loading of the HTML documents.
Will be in KDE 4.2.1.
CCBUG: 178539

------------------------------------------------------------------------
r922555 | scripty | 2009-02-07 08:48:52 +0000 (Sat, 07 Feb 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r922591 | pino | 2009-02-07 11:03:39 +0000 (Sat, 07 Feb 2009) | 2 lines

backport: cancel the loading loop on errors too

------------------------------------------------------------------------
r922604 | pino | 2009-02-07 11:30:49 +0000 (Sat, 07 Feb 2009) | 2 lines

backport: handle/discard LCHM entries with no urls

------------------------------------------------------------------------
r922824 | pino | 2009-02-07 16:16:32 +0000 (Sat, 07 Feb 2009) | 4 lines

Backport: properly keep the vertical position on horizontal resize.
Will be in KDE 4.2.1
CCBUG: 183475

------------------------------------------------------------------------
r922876 | cgilles | 2009-02-07 18:05:13 +0000 (Sat, 07 Feb 2009) | 2 lines

updated internal LibRaw to 0.6.12. BC still preserved

------------------------------------------------------------------------
r922877 | cgilles | 2009-02-07 18:05:49 +0000 (Sat, 07 Feb 2009) | 2 lines

update

------------------------------------------------------------------------
r923021 | pino | 2009-02-08 01:33:25 +0000 (Sun, 08 Feb 2009) | 3 lines

backport: on document closing, clear the fifo with allocated text pages
(fixes potential crash because of references to potentially non-existing pages in the next open document)

------------------------------------------------------------------------
r923029 | sengels | 2009-02-08 02:30:11 +0000 (Sun, 08 Feb 2009) | 1 line

fix crash in okular - I am not sure why this didn't happen before (the fix comes from msdn http://msdn.microsoft.com/en-us/library/aa366589.aspx - look at the example)
------------------------------------------------------------------------
r925416 | cgilles | 2009-02-13 09:36:49 +0000 (Fri, 13 Feb 2009) | 2 lines

update internal libraw to 0.6.13. BC preserved

------------------------------------------------------------------------
r926107 | pino | 2009-02-14 17:13:16 +0000 (Sat, 14 Feb 2009) | 4 lines

Backport: Add a "Find Previous" action.
Will be in KDE 4.2.1.
CCBUG: 184230

------------------------------------------------------------------------
r928890 | pino | 2009-02-20 11:18:58 +0000 (Fri, 20 Feb 2009) | 4 lines

backport: save the real parent widget and use it later for getting the screen number of the okular application
will be in KDE 4.2.1
BUG: 185013

------------------------------------------------------------------------
r929836 | cgilles | 2009-02-22 11:05:54 +0000 (Sun, 22 Feb 2009) | 2 lines

backport commit #929807 from trunk

------------------------------------------------------------------------
r929988 | cgilles | 2009-02-22 14:13:14 +0000 (Sun, 22 Feb 2009) | 2 lines

compile with Exiv2 < 0.18

------------------------------------------------------------------------
r930843 | wkai | 2009-02-24 13:01:11 +0000 (Tue, 24 Feb 2009) | 1 line

backport r930839
------------------------------------------------------------------------
r931168 | pino | 2009-02-24 21:47:15 +0000 (Tue, 24 Feb 2009) | 2 lines

bump to 0.8.1 for kde 4.2.1

------------------------------------------------------------------------
r932356 | cgilles | 2009-02-26 12:53:10 +0000 (Thu, 26 Feb 2009) | 2 lines

fix memeory leak

------------------------------------------------------------------------
r932366 | cgilles | 2009-02-26 13:28:24 +0000 (Thu, 26 Feb 2009) | 2 lines

fix lmemory leak

------------------------------------------------------------------------
