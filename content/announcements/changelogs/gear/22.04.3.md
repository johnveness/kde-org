---
aliases:
- ../../fulllog_releases-22.04.3
title: KDE Gear 22.04.3 Full Log Page
type: fulllog
gear: true
hidden: true
---
{{< details id="akonadi" title="akonadi" link="https://commits.kde.org/akonadi" >}}
+ Don't delete mariadb log files. [Commit.](http://commits.kde.org/akonadi/aebb20a082d05b36458008fedff4397f022c3ffa) Fixes bug [#454948](https://bugs.kde.org/454948)
{{< /details >}}
{{< details id="akonadi-calendar" title="akonadi-calendar" link="https://commits.kde.org/akonadi-calendar" >}}
+ Prevent crashing from null AlarmNotification pointer. [Commit.](http://commits.kde.org/akonadi-calendar/76bcb55f38d7496e1aa91c8fd77a5296bedef516) 
+ Fix crash when clicking on dismiss, sometimes. [Commit.](http://commits.kde.org/akonadi-calendar/26694e8c82562eac2fdbdb6fa85f56b27fca82a0) Fixes bug [#455472](https://bugs.kde.org/455472)
{{< /details >}}
{{< details id="akonadi-contacts" title="akonadi-contacts" link="https://commits.kde.org/akonadi-contacts" >}}
+ Akonadi-contacts links to contact-editor, so add to find_dependency. [Commit.](http://commits.kde.org/akonadi-contacts/e74adfd30eae3f70b3b5452c52f33d285188568d) 
+ Remove duplicated install() calls. [Commit.](http://commits.kde.org/akonadi-contacts/7bc13fe24daaf2a2faa56f45b8e82f1915892523) 
{{< /details >}}
{{< details id="elisa" title="elisa" link="https://commits.kde.org/elisa" >}}
+ Fix case sensitivity sorting issue in viewsproxymodel. [Commit.](http://commits.kde.org/elisa/2f466c49d52858823003e56f4472051b6d13a707) 
+ Rescale internal Slider values to avoid integer overflow. [Commit.](http://commits.kde.org/elisa/b3d081d14a6da1e18ac220867eb7961b8a017837) See bug [#455339](https://bugs.kde.org/455339)
{{< /details >}}
{{< details id="itinerary" title="itinerary" link="https://commits.kde.org/itinerary" >}}
+ Add 22.04.3 release notes. [Commit.](http://commits.kde.org/itinerary/544b8c56fec2f3f5cfb802632b4d3af26013a3af) 
{{< /details >}}
{{< details id="kalarm" title="kalarm" link="https://commits.kde.org/kalarm" >}}
+ Append the KDE Gear version to KAlarm's version. [Commit.](http://commits.kde.org/kalarm/ecd226d55520477a95f94538ae5ca2cdb994bba8) 
{{< /details >}}
{{< details id="kdenlive" title="kdenlive" link="https://commits.kde.org/kdenlive" >}}
+ Supplement to "Disable pip on Flatpak (we bundle the deps there)". [Commit.](http://commits.kde.org/kdenlive/03170f45d2978fae72b45f6bb6efea072e10db6a) 
+ Fix syntax error. [Commit.](http://commits.kde.org/kdenlive/1d82251684c0f37d6a9d3b78cc668b685aec5d9c) 
+ [Python Interface] Disable pip on Flatpak (we bundle the deps there). [Commit.](http://commits.kde.org/kdenlive/13b46025711bbe74d47b6652e1f627905112d624) 
+ Fix type (spotted by erjiang). [Commit.](http://commits.kde.org/kdenlive/e77e578e1f8298dce2319b3c88c94ff73e4337d1) 
+ Fix effect parameter spin box incrementing twice on mouse wheel. [Commit.](http://commits.kde.org/kdenlive/59a39a977d9057078a620da44227741186369c50) 
+ Fix compilation - wrong change committed. [Commit.](http://commits.kde.org/kdenlive/5c9135c962854de074b494d264c3799c1b9e1cfd) 
+ Fix bug and warning calculating available mix duration when no frame is available. [Commit.](http://commits.kde.org/kdenlive/aefb8432540208bf9292b5819f4e596052338fac) 
+ [Scene Split] reimplement threshold. [Commit.](http://commits.kde.org/kdenlive/aa544462f2043155ee0f52fec02597d71b98d5f1) 
+ Fix keyframe view seeking with effect zones. [Commit.](http://commits.kde.org/kdenlive/2a44964b71949ce42eb6d4cd538a15ad7d5defe5) 
+ Fix timeline playing autoscroll incorrectly enabled. [Commit.](http://commits.kde.org/kdenlive/388c8188b93209d3e5e01c05b07e2239f4fa627e) See bug [#455512](https://bugs.kde.org/455512)
+ Fix timeline scrolling broken after opening a widget from timeline menu, like edit clip duration. [Commit.](http://commits.kde.org/kdenlive/52e5f1bd33a6ad0e318bb333eb2d96cfb7df0310) 
+ Fix oversized UI on Windows. [Commit.](http://commits.kde.org/kdenlive/ba1cbea6838f0b17e3711719cec71c890d01757c) 
+ Fix incorrect encoding in rendered clip name on Windows. [Commit.](http://commits.kde.org/kdenlive/ceb9ee08e1bcc28a206583d4c8874e89d57fc869) Fixes bug [#455286](https://bugs.kde.org/455286)
+ Fix incorrect ungroup when dragging selection. [Commit.](http://commits.kde.org/kdenlive/edede016f553f917740a192b06c2ed134333f19e) 
+ Fix incorrect behavior of external proxies, allow multiple patterns by profile. [Commit.](http://commits.kde.org/kdenlive/3e871332c6cc6c3c849211f837c297fd90c3c39a) See bug [#455140](https://bugs.kde.org/455140)
+ Fixes for external proxies. [Commit.](http://commits.kde.org/kdenlive/e5911772c142d0d0bc45c944ab743d93bb84f9e0) See bug [#455140](https://bugs.kde.org/455140)
+ Correctly enable current bin item proxy action after proxy is enabled/disabled in project settings. [Commit.](http://commits.kde.org/kdenlive/5d7f592fba28a70054f4829c4d4aa5ccf4338cc4) 
+ Fix timeline cursor sometimes losing sync with wuler playhead. [Commit.](http://commits.kde.org/kdenlive/5af84df5e92e583c693e96a48546d4aa8f06ded9) 
+ Fix freeze copying proxy clips. [Commit.](http://commits.kde.org/kdenlive/d05bb528afd1160f0be97f1c96ad5ccd27df0045) 
+ [Render Presets] Follow ffmpeg defaults. [Commit.](http://commits.kde.org/kdenlive/a658c8052137d2fd4b5d9c08cc2593c57e88957d) 
+ Proper rounding for persistant quality slider value. [Commit.](http://commits.kde.org/kdenlive/db6f817469e5b96f18f952e6066a060968a7a26c) 
+ [Render Widget] Backend option to set speed default index. [Commit.](http://commits.kde.org/kdenlive/c10a0984ff145f5b2d4ecc397792cb0a66ff82e7) 
+ [Render Widget] Don't enable "Custom Quality" by default, remember state. [Commit.](http://commits.kde.org/kdenlive/7a54eea5ab7d928b1af1e1e3b13cc4bf5049d370) 
+ Fix compilation with Qt < 5.14. [Commit.](http://commits.kde.org/kdenlive/4b087384a2718a17f7c643554cfdf5fe549f4521) 
{{< /details >}}
{{< details id="klickety" title="klickety" link="https://commits.kde.org/klickety" >}}
+ Fix score being wrongly calculated if clicking while animation is running. [Commit.](http://commits.kde.org/klickety/54cabb534358f90734578a029d4c9f609be572bd) Fixes bug [#455871](https://bugs.kde.org/455871)
{{< /details >}}
{{< details id="konsole" title="konsole" link="https://commits.kde.org/konsole" >}}
+ Url filter: do not allow parenthesis in hostname. [Commit.](http://commits.kde.org/konsole/013cf65c52106954f7958eb481b598797e362e7d) 
+ Only recognize URIs with balanced parentheses. [Commit.](http://commits.kde.org/konsole/e925acc1ca60ef1116fa84cc35328d5accad23c5) See bug [#455166](https://bugs.kde.org/455166)
+ Match urls between grave `. [Commit.](http://commits.kde.org/konsole/6e7cf530a8a9ef2676a1b4faf9a06af9f955c5be) See bug [#455166](https://bugs.kde.org/455166)
+ URI regexp: allow empty queries and fragments. [Commit.](http://commits.kde.org/konsole/6e2eb175f2051a05729b44c03eb45f4cb8340cec) 
+ URI regexp: support ? in query and fragment. [Commit.](http://commits.kde.org/konsole/f499af3ca8f186910e279c3af7b06c299bea0e15) 
+ URI regexp: allow / as path. [Commit.](http://commits.kde.org/konsole/12f920231a14623f8e7aa078471aafd85ba83234) 
+ URI regexp: allow more than one colon in userinfo. [Commit.](http://commits.kde.org/konsole/d6960e92dd4db3255a4bce86420f825c142a1371) 
+ Allow IPv6 literals in URIs. [Commit.](http://commits.kde.org/konsole/bd290db4367dd971cadf61bb17e14342a5a54501) 
+ URI: be more strict with www. URIs. [Commit.](http://commits.kde.org/konsole/a2d91de8f39188bba26ef65d0aa97599f35409cb) 
+ Make UrlFilter::CompleteUrlRegExp match case-insensitively. [Commit.](http://commits.kde.org/konsole/8e8b3978e978ed236d152e73ddd489e80f348fec) 
+ Use possessive quantifiers to optimise regex matching performance. [Commit.](http://commits.kde.org/konsole/65d5d45c18493deebb3b5a8581ac167d228b2563) 
+ UrlFilter::FullUrlRegExp matches more valid urls. [Commit.](http://commits.kde.org/konsole/c8d8abd1b2b8e93cc38b3bedb1b63f69791e5882) Fixes bug [#452978](https://bugs.kde.org/452978)
+ Fix z-modem detection by using proper string index. [Commit.](http://commits.kde.org/konsole/94b23518853affdf1ed9d74e8bb86e9e3b607a75) 
+ Include CheckFunctionExists for check_function_exists. [Commit.](http://commits.kde.org/konsole/922abd0cd68f02c57cc498a2bdfecccccf6a016d) 
+ Detect the presence of malloc_trim to fix the build with musl. [Commit.](http://commits.kde.org/konsole/b8c90a830ceaf293e61f6cd5217bb3e584f997b8) 
{{< /details >}}
{{< details id="korganizer" title="korganizer" link="https://commits.kde.org/korganizer" >}}
+ Fix special dates summary configuration. [Commit.](http://commits.kde.org/korganizer/9a73aa319ed25e75d0719667138749f93f159b55) See bug [#393054](https://bugs.kde.org/393054)
+ Fix crash when completing a to-do in the summary view. [Commit.](http://commits.kde.org/korganizer/c4e58599b3f3eeece0d94d5cee9259be904cdbe7) Fixes bug [#454536](https://bugs.kde.org/454536)
+ Make summary's todo settings consistent. [Commit.](http://commits.kde.org/korganizer/f2ceb65a847ff27c3eef8360116e94fa8f12e3e3) 
{{< /details >}}
{{< details id="kosmindoormap" title="kosmindoormap" link="https://commits.kde.org/kosmindoormap" >}}
+ Don't detect monorail systems as regular railways. [Commit.](http://commits.kde.org/kosmindoormap/1245545f5fe4e59e752f52d5f2136c0952b04e7d) 
{{< /details >}}
{{< details id="ktuberling" title="ktuberling" link="https://commits.kde.org/ktuberling" >}}
+ Use ‘bow’ sound instead of ‘tie’ sound for bow images. [Commit.](http://commits.kde.org/ktuberling/4f4c9cf5d1506ca0a9695a013f996b497f6df93f) 
{{< /details >}}
{{< details id="libkleo" title="libkleo" link="https://commits.kde.org/libkleo" >}}
+ Add the required dependency on QGpgme to the Config.cmake file. [Commit.](http://commits.kde.org/libkleo/ac2e62e11a091e43ba6c5fb561e91ecc4bea04ac) 
{{< /details >}}
{{< details id="libksieve" title="libksieve" link="https://commits.kde.org/libksieve" >}}
+ It's a boolean. [Commit.](http://commits.kde.org/libksieve/00923dec214ffe94385c7d321264d3095f503b7f) 
+ Fix menu position. [Commit.](http://commits.kde.org/libksieve/032422928542c5d1db2a7eb862e9a4a27cfaa589) 
{{< /details >}}
{{< details id="messagelib" title="messagelib" link="https://commits.kde.org/messagelib" >}}
+ When we don't found info => it's ok. [Commit.](http://commits.kde.org/messagelib/bc10f5111b40d56d7b484dc3a4f0df1c84e161ee) 
+ Remove unused debug. [Commit.](http://commits.kde.org/messagelib/700b61e64b0d8950a7b619de77b7aecbb7289f10) 
{{< /details >}}
{{< details id="okular" title="okular" link="https://commits.kde.org/okular" >}}
+ Don't leave temp files around when saving. [Commit.](http://commits.kde.org/okular/e7db8c92ae398a0c75be471aea01d44d5880e8a4) Fixes bug [#453421](https://bugs.kde.org/453421)
+ Extract i18n from gui. [Commit.](http://commits.kde.org/okular/079a13d6221ec037a12d11ba561371e7415cf4eb) 
+ Allow word-wrapping for the "Welcome to Okular" message. [Commit.](http://commits.kde.org/okular/1e6ee25f0bf1a541f9eb3fd525efc4614a9e0d13) 
{{< /details >}}
{{< details id="partitionmanager" title="partitionmanager" link="https://commits.kde.org/partitionmanager" >}}
+ Fix desktop file. [Commit.](http://commits.kde.org/partitionmanager/3dc7bcdc7ae85584748e67f93c166fcf19ed8625) Fixes bug [#455090](https://bugs.kde.org/455090)
{{< /details >}}
