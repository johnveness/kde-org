import logging
import os
import shutil
import sys
import urllib.request

from pathlib import Path


level = logging.INFO
logging.basicConfig(format='%(levelname)s: %(message)s', level=level)

urllib.request.urlretrieve('https://invent.kde.org/frameworks/kxmlgui/-/raw/master/src/kxmlgui.xsd', 'static/standards/kxmlgui/1.0/kxmlgui.xsd')
urllib.request.urlretrieve('https://invent.kde.org/frameworks/kconfig/-/raw/master/src/kconfig_compiler/kcfg.xsd', 'static/standards/kcfg/1.0/kcfg.xsd')

def is_branch_master():
    ci_branch = os.environ.get("CI_COMMIT_REF_NAME")
    if ci_branch == "master":
        return True
    elif ci_branch is not None:
        # We are here as part of a GitLab CI,
        # but the branch is not "master"
        return False

    # we are not on GitLab CI, try to detect branch
    head_dir = Path(".") / ".git" / "HEAD"
    with head_dir.open("r") as f: content = f.read().splitlines()
    logging.info(content)
    head_rev = content[0]
    master_dir = Path('.') / '.git' / 'refs' / 'remotes' / 'origin' / 'master'
    with master_dir.open("r") as f: content = f.read().splitlines()
    logging.info(content)
    master_rev = content[0]
    return master_rev == head_rev

if not is_branch_master():
    sys.exit()

os.environ["PACKAGE"] = 'websites-kde-org'
os.system('git clone https://invent.kde.org/websites/hugo-i18n && pip3 install ./hugo-i18n')
os.system('hugoi18n compile po')  # compile translations in folder "po"
os.system('hugoi18n generate')

# process frameworks announcements: special cases are 5.0.0 and 5.1.0, which have custom intros.
# any language that has both of these:
# - at least one of three messages in the intro translated
# - at least one announcement (including 5.0.0 and 5.1.0) translated
# will have all absent announcement files (excluding 5.0.0.md and 5.1.0.md) copied from English
fw_path = 'announcements/frameworks/5'
en_path = f'content/{fw_path}'
en_versions = set(os.listdir(en_path))
en_versions -= {'5.0.0.md', '5.1.0.md'}

for lang in os.listdir('content-trans'):
    i18n_path = f'i18n/{lang}.yaml'
    if not os.path.isfile(i18n_path):
        continue
    with open(i18n_path) as f_i18n:
        content = f_i18n.read()
    if not ('annc-frameworks-text' in content or 'annc-frameworks-intro' in content):
        continue
    lang_path = f'content-trans/{lang}/{fw_path}'
    if not os.path.isdir(lang_path):
        continue

    lang_versions = set(os.listdir(lang_path))
    to_copy = en_versions - lang_versions
    for v in to_copy:
        shutil.copy2(f'{en_path}/{v}', lang_path)

# We host copies of some pages on www.kde.org for historical reasons
# Using urllib.request results in 403 Forbidden error, so we use a subshell
os.system('curl "https://store.kde.org/content.rdf" -o static/kde-look-content.rdf')
os.system('curl "https://dot.kde.org/rss.xml" -o static/dot/rss.xml')
os.system('cp -f static/dot/rss.xml static/dotkdeorg.rdf')
